maintainer       "Mintlab B.V."
maintainer_email "ops@mintlab.nl"
license          "EUPL"
description      "Installs/Configures Zaaksysteem"
long_description IO.read(File.join(File.dirname(__FILE__), 'README.rdoc'))
version          "0.1"

depends          "apt"
depends          "ntp"
depends          "postgresql"
depends          "database"
depends          "openldap"

%w{ ubuntu }.each do |os|
      supports os
end
