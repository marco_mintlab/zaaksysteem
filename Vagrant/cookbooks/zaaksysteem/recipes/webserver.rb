#
# Cookbook Name:: zaaksysteem
# Recipe:: webserver
#
# Copyright 2013, Example Com
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

service "nginx" do
        supports :status => true, :restart => true, :reload => true
        action [ :enable, :start ]
end

template '/etc/nginx/sites-available/zaaksysteem.conf' do
    source 'nginx/zaaksysteem-virtualhost.conf'
    notifies  :restart, 'service[nginx]'
end

template '/etc/nginx/fastcgi_params' do
    source 'nginx/fastcgi_params'
    notifies  :restart, 'service[nginx]'
end

template '/etc/nginx/zaaksysteem.conf' do
    source 'nginx/zaaksysteem.conf'
    notifies  :restart, 'service[nginx]'
end

execute "enable_site_zaaksysteem" do
    command "/usr/sbin/nxensite zaaksysteem.conf"
    user "root"
    notifies  :reload, 'service[nginx]'
    not_if 'ls /etc/nginx/sites-enabled/zaaksysteem.conf'
end

execute "disable_site_default" do
    command "/usr/sbin/nxdissite default"
    user "root"
    notifies  :reload, 'service[nginx]'
    only_if 'ls /etc/nginx/sites-enabled/zaaksysteem.conf'
end


