#!/bin/bash

# Usage: [CATALYST_DEBUG=0] ./script/dev_fastcgi.pl [hostname [port]]

# Default parameters for hostname:port combination.
HOSTNAME=${1:-'127.0.0.1'};
PORT=${2:-'3333'};

echo "Starting on ${HOSTNAME}:${PORT}"

# grep over all processes, find fcgi + zaaksysteem process, select PID column, dispatch all PIDs to kill for termination
ps aux | grep perl-fcgi-pm | grep Zaaksysteem | awk '{ print $2; }' | xargs -r kill

# we exec the perl script, to prevent this file from lingering as a process.
# default CATALYST_DEBUG to 1, unless it already exists in the environment
CATALYST_DEBUG=${CATALYST_DEBUG:-1} exec script/zaaksysteem_fastcgi.pl -e -l $HOSTNAME:$PORT -n 2
