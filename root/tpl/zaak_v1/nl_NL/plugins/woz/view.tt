[% USE DateFormattingFilter %]
[% USE PriceFormattingFilter %]

[% woz_object_data = woz_object.object_data %]
    <div class="woz">

        <div class="woz-info">
        
            <h1 class="woz-straat">
                [% woz_object_data.Straatnaam %] 
                [% woz_object_data.Huisnummer | format('%0d')  %]
                [% woz_object_data.Huisletter %]
                [% woz_object_data.Huisnummertoevoeging %]
            </h1>

            [% woz_objectnummer = woz_object_data.item('WOZ-objectnummer') |format('%d') %]

            [% IF !c.user_exists  && show_comment_link %]
            <a href="[% c.uri_for('/zaak/create/webformulier/',
                { 
                     zaaktype_id                => woz_zaaktype_id,
                     sessreset                  => 1,
                     ztc_aanvrager_type         => 'natuurlijk_persoon',
                     authenticatie_methode      => c.session.authenticated_by,
                     prefill_woz_objectnummer   => woz_objectnummer,
                 })
                 %]"
                 class="knop bewerk-16 in-header">Opmerking doorgeven</a>
            [% END %]

            [% woz_intro_text %]        
            <div class="woz-taxatierapport">
                <div class="woz-taxatierapport-row">
                    <div class="woz-taxatierapport-label"><label>Straatnaam</label></div>
                    <div class="woz-taxatierapport-text">[% woz_object_data.Straatnaam %]</div>
                </div>
                <div class="woz-taxatierapport-row">
                    <div class="woz-taxatierapport-label"><label>Huisnummer</label></div>
                    <div class="woz-taxatierapport-text">[% woz_object_data.Huisnummer | format('%0.0f')  %] [% woz_object_data.Huisletter %] [% woz_object_data.Huisnummertoevoeging %]</div>
                </div>
                <div class="woz-taxatierapport-row">
                    <div class="woz-taxatierapport-label"><label>Postcode</label></div>
                    <div class="woz-taxatierapport-text">[% woz_object_data.Postcode %]</div>
                </div>
                <div class="woz-taxatierapport-row">
                    <div class="woz-taxatierapport-label"><label>Woonplaats</label></div>
                    <div class="woz-taxatierapport-text">[% woz_object_data.Woonplaatsnaam %]</div>
                </div>
                <div class="woz-taxatierapport-row">
                    <div class="woz-taxatierapport-label"><label>WOZ-objectnummer</label></div>
                    <div class="woz-taxatierapport-text">[% woz_objectnummer %] 
                    [% IF c.config.customer.start_config.woz_info %]
                        <div class="ezra_woz_object_picture inline" rel="postcode:[% woz_object_data.Postcode %];huisnummer:[% woz_object_data.Huisnummer |format('%d') %]" style="display:none;">
                            <a href="#" class="knop ezra_woz_object_picture_popup knop-bootstrap"><i class="icon icon-image"></i> Panoramafoto</a>
                        </div>
                    [% END %]
                    </div>
                </div>
                <div class="woz-taxatierapport-row">
                    <div class="woz-taxatierapport-label"><label>Waardepeildatum</label></div>
                    <div class="woz-taxatierapport-text-smal">[% woz_object_data.Waardepeildatum | $DateFormattingFilter %]</div>
                    <div class="woz-taxatierapport-label"><label>Toestandspeildatum</label></div>
                    <div class="woz-taxatierapport-text-smal">[% woz_object_data.onderbouwing_taxatie.Toestandspeildatum  | $DateFormattingFilter %]</div>
                </div>
                <div class="woz-taxatierapport-row">
                    <div class="woz-taxatierapport-label"><label>Vastgestelde WOZ-Waarde</label></div>
                    <div class="woz-taxatierapport-text-smal"><strong>&euro; [% woz_object_data.item('Vastgestelde waarde') | $PriceFormattingFilter %]</strong></div>
                    <div class="woz-taxatierapport-uitleg">(Waardepeildatum [% woz_object_data.Waardepeildatum | $DateFormattingFilter %])</div>
                </div>
                [% IF woz_object_data.item('Vorige waarde') %]
                    <div class="woz-taxatierapport-row">
                        <div class="woz-taxatierapport-label">Vorige vastgestelde WOZ-waarde</div>
                        <div class="woz-taxatierapport-text-smal">[% IF woz_object_data.item('Vorige waarde') %]&euro; [% woz_object_data.item('Vorige waarde').waarde | $PriceFormattingFilter %][% ELSE %]-[% END %]</div>
                        <div class="woz-taxatierapport-uitleg">
                            (Waardepeildatum 
                            [% woz_object_data.item('Vorige waarde').item('peildatum') | $DateFormattingFilter %])
                        </div>
                    </div>
                    [% nieuw = woz_object_data.item('Vastgestelde waarde')  %]
                    [% oud   = woz_object_data.item('Vorige waarde').waarde %]
                    [% IF oud %]
                        [% percentage = 100 * (nieuw-oud) / oud %]
                    [% END %]
                    <div class="woz-taxatierapport-row">
                        <div class="woz-taxatierapport-label">Verandering van de WOZ-Waarde</div>
                        <div class="woz-taxatierapport-text">[% IF oud %][% percentage | format('%0.1f') %]%[% ELSE %]-[% END %]</div>
                    </div>
                [% END %]
            </div>
      
            <a href="[% IF pip %]/pip[% ELSE %]/beheer[% END %]/woz/report/[% woz_object.id %]?object_id=[% woz_object.object_id %]&amp;owner=[% woz_object.owner %]">Bekijk volledig taxatieverslag</a> 
        <div class="woz-uitleg">
        [% woz_waardebepaling_text %]
        </div>
    </div>

        <div class="woz-photo woz-photo-unavailable">
            <img style="max-width: 300px;" src="/plugins/woz/photo/[% woz_object.id %]?owner=[% woz_object.owner %]&amp;object_id=[% woz_object.object_id %]" />
            <a class="knop knop-bootstrap ezra_woz_photo_zoom ezra_dialog"><i class="icon-zoom-in icon-font-awesome icon"></i></a>
            <span>Geen foto beschikbaar</span>
        </div>

    


[% BLOCK disabled %]
    [% UNLESS pip %]
        <a href="/plugins/woz/object/[% woz_object.id %]?owner=[% woz_object.owner %]&amp;object_id=[% woz_object.object_id %]">JSON</a>
    [% END %]
[% END %]
