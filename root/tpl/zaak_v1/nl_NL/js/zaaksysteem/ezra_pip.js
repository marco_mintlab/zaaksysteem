/** Ezra PIP functions
 *
 */

(function($) {
    var methods = {
        init: function(options) {
            var settings = $.extend(
                {},
                {
                    'modulename': 'ezra_pip_update_kenmerk'
                },
                options
            );

            return this.each(function() {
                var $this = $(this),
                    data = $this.data('ezra_pip_update_kenmerk');

                /* Already initialized? */
                if (data) {
                    $this.ezra_pip_update_kenmerk('log','ezra_pip_update_kenmerk already initialized!');
                    return true;
                }

                $this.ezra_pip_update_kenmerk('load_events');

                $(this).addClass('ezra_pip_update_kenmerk-initialized');
            });
        },
        load_events: function() {
            var $this = $(this),
                data = $this.data('ezra_pip_update_kenmerk');

            var container_elem = $(this);

            $(this).find('.ezra_pip_update_kenmerk-input').hide();
            $(this).find('.ezra_pip_update_kenmerk-opslaan_button').hide();

            $(this).find('.ezra_pip_update_kenmerk-update_button')
                .click(function() {
                    $(this).closest('.ezra_pip_update_kenmerk')
                        .find('.ezra_pip_update_kenmerk-input')
                        .show();
                    $(this).closest('.ezra_pip_update_kenmerk')
                        .find('.ezra_pip_update_kenmerk-opslaan_button')
                        .show();
                    $(this).closest('.ezra_pip_update_kenmerk')
                        .find('.ezra_pip_update_kenmerk-value_view')
                        .hide();
                    $(this).hide();

                    return false;
                });

            $(this).find('.ezra_pip_update_kenmerk-opslaan_button')
                .click(function() {
                    var localcontainer  = $(this).closest('.ezra_pip_update_kenmerk');
                    var localform       = localcontainer.find('form');
                    localform.submit();
                });
        },
        show: function() {
            var $this = $(this),
                data = $this.data('ezra_pip_update_kenmerk');
        },
        retrieve_options: function(line) {
            var rv = {};

            if (!line) { return {}; }

            /* Get sets */
            sets = line.split(/;/);

            for(i = 0; i < sets.length; i++) {
                set     = sets[i];
                keyval  = set.split(/:/);
                key     = keyval[0];
                key     = key.replace(/^\s+/g, '');
                key     = key.replace(/\s+$/g, '');
                value   = keyval[1];
                if (!value) { continue; }
                value   = value.replace(/^\s+/g, '');
                value   = value.replace(/\s+$/g, '');
                rv[key] = value;
            }

            return rv;
        },
        log: function(message) {
            if (typeof console != 'undefined' && typeof console.log == 'function') {
                console.log(message);
            }
        }
    };

    $.fn.ezra_pip_update_kenmerk = function(method) {
        if ( methods[method] ) {
            return methods[method].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof method === 'object' || ! method ) {
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Method ' +  method + ' does not exist on jQuery.ezra_pip_update_kenmerk' );
        }
    }
})(jQuery);


$(document).ready(function() {
    $('.ezra_pip_update_kenmerk').ezra_pip_update_kenmerk();
});
