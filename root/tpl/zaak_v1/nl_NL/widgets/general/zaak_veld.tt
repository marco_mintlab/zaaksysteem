[%
# Display a 'zaak veld' based on a few parameters
# This template expects output from the DB::Zaak finder
#
# input:
# - display_field.class: a string describing the type of formatting that is needed for this data item
# - display_field.fieldname: a string for fieldname
# - zaak: the complete zaak object. this template can pick and choose from this 
#
%]

[% USE Scalar %]
[% USE date %]

[% BLOCK status_display %]
    [% zaak_status_icon = 'icon-zaak';
        IF (zaakdata.status == 'open'); zaak_status_icon = 'icon-zaak-open'; END;

        IF (zaakdata.status == 'resolved' && zaakdata.te_vernietigen);
            zaak_status_icon = 'icon-zaak-todelete';
        ELSIF (zaakdata.status == 'resolved');
            zaak_status_icon = 'icon-zaak-closed';
        END;

        IF (zaakdata.status == 'stalled');
            zaak_status_icon = 'icon-zaak-stalled';
        END;

        IF (zaakdata.status == 'overdragen');
            zaak_status_icon = 'icon-zaak-next';
        END;
    %]

    <a href="[% c.uri_for(( pip ? '/pip' : '') _ '/zaak/' _ zaakdata.nr) %][% IF case_intake_automatic_open %]/open[% END %]" class="icon [% zaak_status_icon %]"></a>
[% END %]

[% BLOCK zaaknummer_display %]
    [% IF pip %]
        <div class="pip-zaaknummer"><h2>
    [% END %]

    <a href="[% c.uri_for(( pip ? '/pip' : '')_ '/zaak/' _ zaakdata.nr) %][% IF case_intake_automatic_open %]/open[% END %]">
        <span class="hide">Zaak</span> [% zaakdata.nr %]
    </a>

    [% IF pip %]
        </h2></div>
    [% END %]
[% END %]

[% BLOCK voortgang_display %]
    [%
        total_statussen = zaakdata.scalar.zaaktype_node_id.scalar.zaaktype_statussen.count;
        IF total_statussen;
            status_perc = ((zaakdata.milestone / total_statussen) * 100);
        END;

        status_perc = status_perc | format('%.0f');

        IF status_perc > 100;
            status_perc=100;
        END;
    %]

    <div class="progress">
        <div class="progress-bar holder rounded">
            <div class="progress-value value rounded">
                <div class="perc">[% status_perc | format('%.0f') %]</div>
            </div>
        </div>
    </div>
[% END %]

[% BLOCK zaaktype_display %]
    [% IF pip %]
        <div class="pip-zaaktitel">
            <h2>
    [% END %]
            
    [% IF show_zaaktype_popup %]
        <a href="[% (pip ? '/pip' : '') %]/zaak/[% zaakdata.nr %]/zaaktypeinfo" class="ezra_dialog" title="Informatie over dit zaaktype">[% zaakdata.zaaktype_node_id.titel %]</a>
    [% ELSE %]
        [% zaakdata.zaaktype_node_id.titel %]
    [% END %]

    [% IF pip %]
            </h2>
            <span>
                Registratiedatum: [% zaakdata.registratiedatum.strftime('%d-%m-%y') %] &#8729;
                Streefafhandeldatum: [% zaakdata.systeemkenmerk('streefafhandeldatum') %]
            </span>
        </div>
    [% END %]
[% END %]

[% BLOCK subject_display %]
    [% zaakdata.onderwerp | html_line_break %]
[% END %]

[% BLOCK route_ou_display %]
    [% UNLESS pip %]
        [% zaakdata.ou_object.omschrijving %]
    [% END %]
[% END %]

[% BLOCK aanvrager_display %]
    [% UNLESS pip %]
       [% zaakdata.aanvrager.naam %]
	[% END %]
[% END %]

[% BLOCK besluit_display %]
    besluit TODO
[% END %]

[% BLOCK remainingtime_display %]
    [% IF pip %]
        <div class="pip-voortgang">
            <div class="pip-voortgang-wrap[% ((!zaakdata.is_afgehandeld && zaakdata.days_perc > 100) ? ' too_late' : '') %][% IF zaakdata.status == 'stalled' %] stalled[% END %]">
                <div class="pip-voortgang-perc" style="width:[% ((zaakdata.is_afgehandeld || zaakdata.days_perc > 100) ? '100' : zaakdata.days_perc) %]%"></div>
                <div class="pip-voortgang-dagen">
            [% IF zaakdata.is_afgehandeld %]
                Afgehandeld
            [% ELSIF zaakdata.status == 'stalled' %]
                [% zaakdata.systeemkenmerk('streefafhandeldatum') %]
            [% ELSIF zaakdata.days_left < 0 %]
                [% zaakdata.days_left.replace('-','') %] dag[% (zaakdata.days_left.replace('-', '') != 1 ? 'en' : '') %] te laat
            [% ELSE %]
                Nog [% zaakdata.days_left %] dag[% (zaakdata.days_left != 1 ? 'en' : '') %]
            [% END %]
                 </div>
            </div>
        </div>
    [% ELSIF !zaakdata.is_afgehandeld %]
        [% PROCESS widgets/general/remaining_time.tt
            percentage   = zaakdata.days_perc
            days         = zaakdata.days_left
            IS_STALLED   = (zaakdata.status == 'stalled')
        %]
    [% END %]
[% END %]


[% BLOCK actie_display %]
	[% UNLESS pip %]

    <div class="dropdown-wrap">
        <div class="dropdown-init dropdown-init-block ezra_dropdown_init"><span></span></div>
        <div class="dropdown ezra_dropdown dropdown-menu ezra_actie_container">
            [% IF zaakdata.status == 'resolved' || zaakdata.status == 'overdragen' %]
                <a href="[% c.uri_for('/zaak/' _ zaakdata.nr) %]">Openen</a>
                <a href="[% c.uri_for('/zaak/duplicate/' _ zaakdata.nr) %]" class="popup">Zaak kopieren</a>
            [% ELSE %]   
                [% IF zaakdata.status == 'new' && !force_result_finish %]
                    <a href="[% c.uri_for('/zaak/' _ zaakdata.nr _ '/open') %]">In behandeling nemen</a>
                    <a href="[% c.uri_for('/zaak/' _ zaakdata.nr) %]">Zaak bekijken</a>
                    <a href="[% c.uri_for('/zaak/' _ zaakdata.nr _ '/actie/weiger') %]" title="Zaak weigeren" class="popup">Weigeren</a>
                    <!--a href="[% c.uri_for('/zaak/' _ zaakdata.nr _ '/update/behandelaar') %]" class="popup" title="Behandelaar wijzigen" rel="layout: mediumlarge">Behandelaar wijzigen</a-->
                    <a href="[% c.uri_for('/zaak/' _ zaakdata.nr _ '/update/allocation' ) %]" title="Toewijzing wijzigen" class="popup" rel="layout: mediumlarge">Toewijzing wijzigen</a>

                    <a href="[% c.uri_for('/zaak/duplicate/' _ zaakdata.nr) %]" class="popup" title="Zaak kopi&euml;ren" >Zaak kopieren</a>
                [% ELSE %]
                    <a href="[% c.uri_for('/zaak/' _ zaakdata.nr) %]">Openen</a>            
                    <a title="Volgende fase" href="[% c.uri_for('/zaak/' _ zaakdata.nr _ '/status/next') %]" rel="zaak_id: [% zaakdata.nr %]" class="ezra_validate_next_phase">Volgende fase</a>
                    <a href="[% c.uri_for('/zaak/' _ zaakdata.nr _ '/update/allocation' ) %]" title="Toewijzing wijzigen" class="popup" rel="layout: mediumlarge">Toewijzing wijzigen</a>
                    <a class="popup" title="Verlengen" href="[% c.uri_for('/zaak/' _ zaakdata.nr _ '/update/verlengen') %]">Verlengen</a>
                    [% IF zaakdata.status == 'stalled' %]
                    <a class="popup" title="Hervatten" href="[% c.uri_for('/zaak/' _ zaakdata.nr _ '/update/resume') %]">Hervatten</a>
                    [% ELSE %]
                    <a class="popup" title="Opschorten" href="[% c.uri_for('/zaak/' _ zaakdata.nr _ '/update/opschorten') %]">Opschorten</a>
                    [% END %]
                    <a href="[% c.uri_for('/zaak/duplicate/' _ zaakdata.nr) %]" class="popup" title="Zaak kopi&euml;ren">Zaak kopi&euml;ren</a>
                [% END %]
            [% END %]
        </div>
    </div>
	
	[% END %]
[% END %]

[% BLOCK attachments_display %]
   [% IF zaakdata.bericht.is_alert %]

   <!-- TODO LET OP: Dit is tijdelijk, attachment icon -->
   <a href="[% c.uri_for('/zaak/' _ zaakdata.nr _ '#zaak-elements-documents') %]">
		<img
            src="images/icon_attachment.png"
            width="16"
            height="16"
            alt="Er staan documenten in de wachtrij voor deze zaak"
            border="0"
        />
	</a>

	[% END %]
[% END %]


[% BLOCK kenmerk_display %]
    [% kenmerken    = zaakdata.scalar.zaak_kenmerken.scalar.search(bibliotheek_kenmerken_id => display_field.replace('\D', '')) %]
    [% ztkenmerk    = zaakdata.scalar.zaaktype_node_id.scalar.zaaktype_kenmerken.search(
        {
            'bibliotheek_kenmerken_id.id' => display_field.replace('\D', '')
        },
        {
            join    => 'bibliotheek_kenmerken_id',
        }
    ).first %]

    [% IF ztkenmerk.bibliotheek_kenmerken_id.value_type == 'file' %]
        [% kenmerkdocs = zaakdata.scalar.files.scalar.search_by_case_document_id(
                ztkenmerk.id
           )
        %]
        [% ### MULTIPLE DOCS READY: %]
        [% WHILE (kenmerkdoc = kenmerkdocs.next) %]
            <a href="[% c.uri_for('/zaak/' _ zaak.id _ '/document/' _ kenmerkdoc.id _ '/download') %]">
                [% kenmerkdoc.filename %]
            </a>
            <br />
        [% END %]
    [% ELSIF kenmerken.count %]
        [% values = [] %]
        [% WHILE (kenmerk = kenmerken.next) %]
            [% kenmerk_type = kenmerk.bibliotheek_kenmerken_id.value_type %]
            [% values.push(kenmerk.value) %]
        [% END %]

        [% PROCESS widgets/general/veldoptie_view.tt 
            veldoptie_type = kenmerk_type
            veldoptie_value=values.join(', ') 
        %]
    [% END %]
[% END %]




[% # Default display %]
[% BLOCK text_display %]
    [% zaakdata.systeemkenmerk(display_field) %]
[% END %]


[% BLOCK level_display %]
    [% IF !level %]
        Error: Field 'level' not found.
        [% RETURN %]
    [% END %]
    [% level %]
[% END %]

[% BLOCK info_display %]
    <a href="[% c.uri_for('/zaak/get_meta/' _ zaakdata.nr) %]" class="ezra_dialog icon icon-toelichting icon-hover right" title="Informatie over zaak"></a>
[% END %]

[% BLOCK behandelaar_display %]
    <a href="[% c.uri_for('/betrokkene/get/' _ zaakdata.behandelaar_object.id) %]" class="ezra_dialog" title="Informatie over behandelaar">[% zaakdata.behandelaar_object.naam %]</a>
[% END %]

[% BLOCK besluit_display %]
    [% IF zaakdata.besluit %]
        [% zaakdata.besluit %]
    [% ELSE %]
        Onbekend
    [% END %]
[% END %]

[% BLOCK resultaat_display %]
    [% IF zaakdata.resultaat %]
        [% zaakdata.resultaat | ucfirst %]
    [% ELSE %]
        Onbekend
    [% END %]
[% END %]

[% BLOCK relaties_display %]
    [% zaakdata.systeemkenmerk('alle_relaties').join(', ') %]
[% END %]

[% IF (display_class && display_field && zaak) %]
    [% 
    #
    # This is the code choosing a display block 
    #
    %]

    [% display_block = display_class _ '_display' %]
    [% INCLUDE $display_block %]
[% ELSE %]
    <pre>
    Missing info: 
    - display class: [% display_class %]
    - display field: [% display_field %]
    - zaak object:   [% zaak %]
    </pre>
[% END %]
