package Zaaksysteem::ZTT;

use Moose;

use Zaaksysteem::Profile;
use Zaaksysteem::Exception;
use Zaaksysteem::Constants;

use Zaaksysteem::ZTT::Element;
use Zaaksysteem::ZTT::Template;

=head1 Attributes

=head2 string_fetchers

This attribute contains all the stringfetchers the ZTT instance can use.

String fetchers are coderefs that reference functions that take as their first
argument a ZTT::Tag object, and return a ZTT::Element object containing the
value to be injected in the document.

=cut

has string_fetchers => (
    is => 'rw',
    isa => 'ArrayRef[CodeRef]',
    default => sub { [] }
);

has iterators => (
    is => 'ro',
    isa => 'HashRef[CodeRef]',
    default => sub { {} }
);

=head1 Instantiators

=head2 new_from_case

This instantiator builds a ZTT object from a Zaak component object.

=cut

define_profile new_from_case => (
    required => [qw[case]],
    typed => {
        case => 'Zaaksysteem::Model::DB::Zaak'
    }
);

sub new_from_case {
    my $class = shift;

    #my $opts = assert_profile({ case => shift })->valid;

    #my $case = $opts->{ case };
    my $case = shift;
    my $self = $class->new;

    $self->add_string_fetcher(sub {
        my $tagname = shift->name;

        my $value;

        eval {
            $value = $case->systeemkenmerk($tagname);
        };

        return unless $value;

        return Zaaksysteem::ZTT::Element->new(value => $value);
    });

    $self->add_string_fetcher(sub {
        my $tag = shift;

        my $betrokkenen = $case->zaak_betrokkenen->search_gerelateerd;

        # Usual magic strings here look like 'aanvrager_naam'
        my ($prefix, $tagname) = split m[_], $tag->name;

        # TODO remove this iterator, search the db directly for related subjects
        # that match the prefix
        for my $betrokkene ($betrokkenen->all) {
            next unless $prefix eq $betrokkene->magic_string_prefix;

            return Zaaksysteem::ZTT::Element->new(
                value => ZAAKSYSTEEM_BETROKKENE_SUB->(
                    $case,
                    $case->betrokkene_object({ magic_string_prefix => $prefix }),
                    $tagname
                )
            );
        }
    });

    $self->add_string_fetcher(sub {
        my $tag = shift;

        my $attribute = $case->zaaktype_node_id->zaaktype_kenmerken->search(
            { 'library_attribute.magic_string' => $tag->name },
            { join => 'library_attribute' }
        )->first;

        return unless $attribute;

        my $library_attribute = $attribute->bibliotheek_kenmerken_id;

        my $field_values = $case->field_values({
            bibliotheek_kenmerken_id => $library_attribute->id
        });

        my $element = Zaaksysteem::ZTT::Element->new(
            attribute => $attribute,
            value => $field_values->{ $library_attribute->id }
        );

        if ($attribute->referential && $case->pid) {

            $field_values = $case->pid->field_values({
                bibliotheek_kenmerken_id => $library_attribute->id
            });

            $element->value($field_values->{ $library_attribute->id });
        }

        return $element;
    });

    $self->add_iterator('zaak_relaties', $case->zaak_relaties);

    return $self;
}

sub fetch_string {
    my $self = shift;
    my $tag = shift;

    for my $fetcher (@{ $self->string_fetchers }) {
        my $interpolation = $fetcher->($tag);

        return $interpolation if $interpolation;
    }

    # Return an empty element by default, if it cannot be found by string fetchers
    # the tag should be removed from the document still
    return Zaaksysteem::ZTT::Element->new(value => '');
}

sub process_template {
    my $self = shift;
    my $template = shift;

    unless (eval { $template->isa('Zaaksysteem::ZTT::Template'); }) {
        $template = Zaaksysteem::ZTT::Template->new_from_thing($template);
    }

    my $recurse_limit = 10;

    while($recurse_limit--) {
        my @mods = $template->modifications;

        last unless scalar(@mods);

        for my $mod (@mods) {
            $self->apply($mod, $template);
        }
    }

    return $template;
}

sub apply {
    my ($self, $mod, $template) = @_;
    
    my %dispatch = (
        replace => sub {
            $template->replace($mod->selection, $self->fetch_string($mod->selection->tag))
        },

        iterate => sub {
            my $iterator_type = $mod->selection->iterate;

            unless (exists $self->iterators->{ $iterator_type }) {
                throw('ztt/template/iterator', 'Unable to find iterator for type ' . $iterator_type);
            }

            $template->iterate(
                $self->iterators->{ $iterator_type },
                $mod->selection
            );
        },

        remove => sub {
        }
    );

    unless (exists $dispatch{ $mod->type }) {
        throw('ztt/template/apply', "Don't know how to handle modifications of '$mod->type' type.");
    }

    $dispatch{ $mod->type }->($mod, $template);
}

=head2 add_string_fetcher

With this method you can add additional string fetchers to the ZTT object.
The string fetchers are represented as an chronologically ordered array, first
in, first hit.

=cut

define_profile add_string_fetcher => (
    required => [qw[fetcher]],
    typed => {
        fetcher => 'CodeRef'
    }
);

sub add_string_fetcher {
    my $self = shift;

    #my $opts = assert_profile({ fetcher => shift })->valid;

    push @{ $self->string_fetchers }, shift; #$opts->{ fetcher };
}

sub add_iterator {
    my $self = shift;
    my $name = shift;
    my @items = @_;

    $self->iterators->{ $name } = \@items;
}

1;
