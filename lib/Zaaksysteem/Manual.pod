=head1 NAME

Zaaksysteem::Manual - Documentation Map

=head1 DOCUMENTATION

The complexity of zaaksysteem makes it hard to understand it. To make things
less complicated, we defined several components as a starting point. Every
component has two documentation points:

=over 4

=item API Documentation

This defines the public API for our frontend developers and you. Here you will
find the various JSON calls to work with the zaaksysteem Backend.

=item BACKEND Documentation

For hardcore backend developers, we supply you with information about our
modules, and how to use the inner calls.

=back

=head1 DEVELOPMENT

Before starting with the different components, be sure to read the below
documents for proper documenting and basic knowledge of our JSON API

=over 4

=item L<Zaaksysteem::Manual::API> - A description about how to use our JSON API

=item L<Zaaksysteem::Manual::Template> - A template for creating API
documentation

=item L<Zaaksysteem::Manual::BackendTemplate> - A template for creating
backend documentation

=item L<Zaaksysteem::Manual::CodingStandard> - Coding standard for developing on/with
zaaksysteem

=item L<Zaaksysteem::ZAPI> - ZAPI Documentation

=back

=head1 COMPONENTS

=head2 ZAAKSYSTEEM

When we look at zaaksysteem as a tree, we have some methods at the root of
zaaksysteem. Examples are authentication, configuration etc.

=over 4

=item L<Zaaksysteem::Backend> - Subject Backend

=back

=head2 SUBJECT

When you use the term customer or company, we call these subjects. You can use
the subject API for generating these.

The following API is for you to use:

=over 4

=item L<Zaaksysteem::Controller::Subject> - Subject API

=item L<Zaaksysteem::Backend::Subject> - Subject Backend

=back

=head2 CASE

=over 4

=item L<Zaaksysteem::Controller::Case> - Case API TODO

=item L<Zaaksysteem::Backend::Case> - Case Backend TODO

=back

=head2 CASETYPE

=over 4

=item L<Zaaksysteem::Controller::Casetype> - Casetype API

=item L<Zaaksysteem::Backend::Casetype> - Casetype Backend

=back

=head2 SYSIN

Sysin, our System Integration module. Our goal is putting every interaction
with the outside world in this babe.

=over 4

=item L<Zaaksysteem::Controller::Sysin> - Sysin API

Start here when you would like to know how to talk to our different Zaaksysteem
API modules.

=item L<Zaaksysteem::Backend::Sysin> - Sysin Model

For development pointers 

=back

=head2 KCC

Having such a nice incoming phone call notification box, it would almost be a
shame that you cannot connect your PhoneBox XYZ to it. Well, just find your
local software farmer, and let it connect to our calls API

=over 4

=item L<Zaaksysteem::Controller::KCC> - KCC API

=item L<Zaaksysteem::Backend::KCC> - KCC Backend

=back

=head2 DOCUMENTS

You like our new spiffy document structure right? Well, besides from using the
frontend, just like the frontend you will be able to connect to it. Please use
the following documentation

=over 4

=item L<Zaaksysteem::Controller::Documents> - Documents API

=item L<Zaaksysteem::Backend::Documents> - Documents Backend

=back

=head1 EXAMPLES

See L<SYNOPSIS>

=head1 SEE ALSO

L<Zaaksysteem>

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Rudolf Leermakers

Marco Baan

Dario Gieselaar

Nick Diel

Laura van der Kaaij

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

