package Zaaksysteem::SBUS::Types::StUF::NNP;

use Data::Dumper;
use Moose;

use Zaaksysteem::SBUS::Constants;

extends 'Zaaksysteem::SBUS::Types::StUF::GenericAdapter';

use constant ADRES_MAPPING => {
    'huisnummer'            => 'vestiging_huisnummer',
    'huisnummertoevoeging'  => 'vestiging_huisnummertoevoeging',
    'postcode'              => 'vestiging_postcode',
    'straatnaam'            => 'vestiging_straatnaam',
    'woonplaats'            => 'vestiging_woonplaats',
};

sub _stuf_to_params {
    my ($self, $nnp_xml, $stuf_options) = @_;

    my $nnp_mapping     = STUF_NNP_MAPPING;

    my $create_params   = $self->_convert_stuf_to_hash(
        $nnp_xml,
        {
            %{ $nnp_mapping },
        }
    );

    return $create_params;
}

sub _stuf_relaties {
    my ($self, $nnp_xml, $stuf_options, $create_options) = @_;

    if ($nnp_xml->{extraElementen}) {
        my $extra = $self->_convert_stuf_extra_elementen_to_hash(
            $nnp_xml->{extraElementen}
        );

        if (length($extra->{handelsRegisterVolgnummer})) {
            $create_options->{subdossiernummer} =
                sprintf("%04d", $extra->{handelsRegisterVolgnummer});
        }
    }

    my $address = $self->handle_adr_relations($nnp_xml, 'NNP');

    $create_options->{ $_ } = $address->{ $_ }
        for keys %{ $address };

    my $address_map = ADRES_MAPPING;
    for my $key (keys %{ $address_map }) {
        $create_options->{
            $address_map->{ $key }
        } = $create_options->{ $key };

        delete($create_options->{$key});
    }

    return $create_options;
}


1;

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut

