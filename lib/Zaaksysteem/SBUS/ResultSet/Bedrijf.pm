package Zaaksysteem::SBUS::ResultSet::Bedrijf;

use Moose;

extends qw/DBIx::Class::ResultSet Zaaksysteem::SBUS::ResultSet::GenericImport/;

use Zaaksysteem::Constants;
use Zaaksysteem::SBUS::Constants;
use Zaaksysteem::SBUS::Logging::Object;
use Zaaksysteem::SBUS::Logging::Objecten;

use Data::Dumper;


use constant IMPORT_KERNGEGEVEN_LABEL   => 'dossiernummer';


has '_import_kerngegeven_label'  => (
    'is'        => 'ro',
    'lazy'      => 1,
    default     => sub {
        return IMPORT_KERNGEGEVEN_LABEL;
    }
);

has '_import_objecttype'  => (
    'is'        => 'ro',
    'lazy'      => 1,
    default     => sub {
        return SBUS_LOGOBJECT_PRS;
    }
);

has '_import_entry_profile' => (
    'is'        => 'ro',
    'lazy'      => 1,
    default     => sub {
        return GEGEVENSMAGAZIJN_KVK_PROFILE;
    }
);

sub _get_kern_record {
    my ($self, $params, $options) = @_;

    ### Check against database
    my $records = $self->search(
        {
            dossiernummer               => $params->{dossiernummer},
            subdossiernummer            => $params->{subdossiernummer},
            vestiging_straatnaam        => $params->{vestiging_straatnaam},
            vestiging_huisnummer        => $params->{vestiging_huisnummer},
            authenticated               => 1,
        }
    );

    die('FOUND MORE THAN 1 ENTRY, CANNOT CONTINUE') if (
        $records->count && $records->count > 1
    );

    return $records->first;
}


sub _delete_real_entry {
    my ($self, $params, $options) = @_;

    my $record = $self->_get_kern_record($params, @_);

    $record->deleted_on(DateTime->now());
    $record->update;
}

sub _import_real_entry {
    my ($self, $params, $options) = @_;
    my ($record, $adres_record);

    if (uc($options->{mutatie_type}) =~ /W|V/) {
        $record = $self->_get_kern_record($params, @_);

        return unless $record;
    } else {
        $record = $self->create(
            {
                dossiernummer   => $params->{dossiernummer},
                handelsnaam     => $params->{handelsnaam},
            }
        );
    }

    ### Detect changes
    $self->_detect_changes($params, $options, $record);

    $params->{authenticated}    = 1;
    $params->{authenticatedby}  = 'kvk';

    $record         ->update($params);
}

1;

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut

