package Zaaksysteem::Schema::GmAdres;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp");

=head1 NAME

Zaaksysteem::Schema::GmAdres

=cut

__PACKAGE__->table("gm_adres");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'gm_adres_id_seq'

=head2 straatnaam

  data_type: 'varchar'
  is_nullable: 1
  size: 80

=head2 huisnummer

  data_type: 'smallint'
  is_nullable: 1

=head2 huisletter

  data_type: 'char'
  is_nullable: 1
  size: 1

=head2 huisnummertoevoeging

  data_type: 'varchar'
  is_nullable: 1
  size: 4

=head2 nadere_aanduiding

  data_type: 'varchar'
  is_nullable: 1
  size: 35

=head2 postcode

  data_type: 'varchar'
  is_nullable: 1
  size: 6

=head2 woonplaats

  data_type: 'varchar'
  is_nullable: 1
  size: 75

=head2 gemeentedeel

  data_type: 'varchar'
  is_nullable: 1
  size: 75

=head2 functie_adres

  data_type: 'char'
  is_nullable: 1
  size: 1

=head2 datum_aanvang_bewoning

  data_type: 'date'
  is_nullable: 1

=head2 woonplaats_id

  data_type: 'varchar'
  is_nullable: 1
  size: 32

=head2 gemeente_code

  data_type: 'smallint'
  is_nullable: 1

=head2 hash

  data_type: 'varchar'
  is_nullable: 1
  size: 32

=head2 import_datum

  data_type: 'timestamp'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "gm_adres_id_seq",
  },
  "straatnaam",
  { data_type => "varchar", is_nullable => 1, size => 80 },
  "huisnummer",
  { data_type => "smallint", is_nullable => 1 },
  "huisletter",
  { data_type => "char", is_nullable => 1, size => 1 },
  "huisnummertoevoeging",
  { data_type => "varchar", is_nullable => 1, size => 4 },
  "nadere_aanduiding",
  { data_type => "varchar", is_nullable => 1, size => 35 },
  "postcode",
  { data_type => "varchar", is_nullable => 1, size => 6 },
  "woonplaats",
  { data_type => "varchar", is_nullable => 1, size => 75 },
  "gemeentedeel",
  { data_type => "varchar", is_nullable => 1, size => 75 },
  "functie_adres",
  { data_type => "char", is_nullable => 1, size => 1 },
  "datum_aanvang_bewoning",
  { data_type => "date", is_nullable => 1 },
  "woonplaats_id",
  { data_type => "varchar", is_nullable => 1, size => 32 },
  "gemeente_code",
  { data_type => "smallint", is_nullable => 1 },
  "hash",
  { data_type => "varchar", is_nullable => 1, size => 32 },
  "import_datum",
  { data_type => "timestamp", is_nullable => 1 },
);
__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 gm_natuurlijk_persoons

Type: has_many

Related object: L<Zaaksysteem::Schema::GmNatuurlijkPersoon>

=cut

__PACKAGE__->has_many(
  "gm_natuurlijk_persoons",
  "Zaaksysteem::Schema::GmNatuurlijkPersoon",
  { "foreign.adres_id" => "self.id" },
  {},
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2013-01-10 07:44:26
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:znm39b7GN3tISufNdP5J7w





# You can replace this text with custom content, and it will be preserved on regeneration
1;
