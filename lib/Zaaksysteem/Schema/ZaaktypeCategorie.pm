package Zaaksysteem::Schema::ZaaktypeCategorie;

use strict;
use warnings;

use base 'DBIx::Class';

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp", "Core");
__PACKAGE__->table("zaaktype_categorie");
__PACKAGE__->add_columns(
  "id",
  {
    data_type => "integer",
    default_value => "nextval('zaaktype_categorie_id_seq'::regclass)",
    is_nullable => 0,
    size => 4,
  },
  "categorie",
  {
    data_type => "character varying",
    default_value => undef,
    is_nullable => 1,
    size => 128,
  },
  "eigenaar",
  {
    data_type => "text",
    default_value => undef,
    is_nullable => 1,
    size => undef,
  },
  "behandelaar",
  {
    data_type => "text",
    default_value => undef,
    is_nullable => 1,
    size => undef,
  },
  "created",
  {
    data_type => "timestamp without time zone",
    default_value => undef,
    is_nullable => 1,
    size => 8,
  },
  "last_modified",
  {
    data_type => "timestamp without time zone",
    default_value => undef,
    is_nullable => 1,
    size => 8,
  },
  "deleted_on",
  {
    data_type => "timestamp without time zone",
    default_value => undef,
    is_nullable => 1,
    size => 8,
  },
  "pid",
  { data_type => "integer", default_value => undef, is_nullable => 1, size => 4 },
);
__PACKAGE__->set_primary_key("id");
__PACKAGE__->add_unique_constraint("zaaktype_categorie_pkey", ["id"]);
__PACKAGE__->has_many(
  "zaaktypes",
  "Zaaksysteem::Schema::Zaaktype",
  { "foreign.zaaktype_categorie_id" => "self.id" },
);
__PACKAGE__->belongs_to(
  "pid",
  "Zaaksysteem::Schema::ZaaktypeCategorie",
  { id => "pid" },
);
__PACKAGE__->has_many(
  "zaaktype_categories",
  "Zaaksysteem::Schema::ZaaktypeCategorie",
  { "foreign.pid" => "self.id" },
);
__PACKAGE__->has_many(
  "zaaktype_nodes",
  "Zaaksysteem::Schema::ZaaktypeNode",
  { "foreign.zaaktype_categorie_id" => "self.id" },
);


# Created by DBIx::Class::Schema::Loader v0.04006 @ 2012-05-16 12:12:12
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:WiqM8MYwQdgMzvhE9wrltw


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
