package Zaaksysteem::Schema::ChecklistStatus;

use strict;
use warnings;

use base 'DBIx::Class';

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp", "Core");
__PACKAGE__->table("checklist_status");
__PACKAGE__->add_columns(
  "id",
  {
    data_type => "integer",
    default_value => "nextval('checklist_status_id_seq'::regclass)",
    is_nullable => 0,
    size => 4,
  },
  "checklist_id",
  { data_type => "integer", default_value => undef, is_nullable => 1, size => 4 },
  "status",
  { data_type => "integer", default_value => undef, is_nullable => 1, size => 4 },
  "help",
  {
    data_type => "text",
    default_value => undef,
    is_nullable => 1,
    size => undef,
  },
  "zaaktype_status_id",
  { data_type => "integer", default_value => undef, is_nullable => 1, size => 4 },
  "zaaktype_checklist_id",
  { data_type => "integer", default_value => undef, is_nullable => 1, size => 4 },
);
__PACKAGE__->set_primary_key("id");
__PACKAGE__->add_unique_constraint("checklist_status_pkey", ["id"]);
__PACKAGE__->belongs_to(
  "checklist_id",
  "Zaaksysteem::Schema::ChecklistZaak",
  { id => "checklist_id" },
);
__PACKAGE__->has_many(
  "checklist_vraags",
  "Zaaksysteem::Schema::ChecklistVraag",
  { "foreign.status_id" => "self.id" },
);


# Created by DBIx::Class::Schema::Loader v0.04006 @ 2012-05-16 12:12:12
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:Q/eRXgJjcT5qtis0Uvcdig


# You can replace this text with custom content, and it will be preserved on regeneration
1;
