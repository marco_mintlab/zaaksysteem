package Zaaksysteem::Schema::Medewerker;

use strict;
use warnings;

use base 'DBIx::Class';

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp", "Core");
__PACKAGE__->table("medewerker");
__PACKAGE__->add_columns(
  "id",
  {
    data_type => "integer",
    default_value => "nextval('medewerker_id_seq'::regclass)",
    is_nullable => 0,
    size => 4,
  },
  "gm_natuurlijk_persoon_id",
  { data_type => "integer", default_value => undef, is_nullable => 1, size => 4 },
  "voorletters",
  {
    data_type => "character varying",
    default_value => undef,
    is_nullable => 1,
    size => 10,
  },
  "voornamen",
  {
    data_type => "character varying",
    default_value => undef,
    is_nullable => 1,
    size => 200,
  },
  "geslachtsnaam",
  {
    data_type => "character varying",
    default_value => undef,
    is_nullable => 1,
    size => 200,
  },
  "voorvoegsel",
  {
    data_type => "character varying",
    default_value => undef,
    is_nullable => 1,
    size => 50,
  },
  "geslachtsaanduiding",
  {
    data_type => "character varying",
    default_value => undef,
    is_nullable => 1,
    size => 3,
  },
  "org_eenheid_id",
  { data_type => "integer", default_value => undef, is_nullable => 1, size => 4 },
);
__PACKAGE__->set_primary_key("id");
__PACKAGE__->add_unique_constraint("medewerker_pkey", ["id"]);


# Created by DBIx::Class::Schema::Loader v0.04006 @ 2012-05-16 12:12:12
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:OxlXYHA9Yk3CJEmDTVS9jQ


# You can replace this text with custom content, and it will be preserved on regeneration
1;
