package Zaaksysteem::Schema::LegacyDocumentsMail;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp");

=head1 NAME

Zaaksysteem::Schema::LegacyDocumentsMail

=cut

__PACKAGE__->table("legacy_documents_mail");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'legacy_documents_mail_id_seq'

=head2 document_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 rcpt

  data_type: 'text'
  is_nullable: 1

=head2 message

  data_type: 'text'
  is_nullable: 1

=head2 subject

  data_type: 'text'
  is_nullable: 1

=head2 created

  data_type: 'timestamp'
  is_nullable: 1

=head2 last_modified

  data_type: 'timestamp'
  is_nullable: 1

=head2 option_order

  data_type: 'integer'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "legacy_documents_mail_id_seq",
  },
  "document_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "rcpt",
  { data_type => "text", is_nullable => 1 },
  "message",
  { data_type => "text", is_nullable => 1 },
  "subject",
  { data_type => "text", is_nullable => 1 },
  "created",
  { data_type => "timestamp", is_nullable => 1 },
  "last_modified",
  { data_type => "timestamp", is_nullable => 1 },
  "option_order",
  { data_type => "integer", is_nullable => 1 },
);
__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 document_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::LegacyDocuments>

=cut

__PACKAGE__->belongs_to(
  "document_id",
  "Zaaksysteem::Schema::LegacyDocuments",
  { id => "document_id" },
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2013-05-24 14:24:04
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:QH/94pcztCpvLc2x9Q/LXg


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
