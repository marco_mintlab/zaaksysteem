package Zaaksysteem::Schema::Logging;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp");

=head1 NAME

Zaaksysteem::Schema::Logging

=cut

__PACKAGE__->table("logging");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'logging_id_seq'

=head2 loglevel

  data_type: 'varchar'
  is_nullable: 1
  size: 32

=head2 zaak_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 betrokkene_id

  data_type: 'varchar'
  is_nullable: 1
  size: 128

=head2 aanvrager_id

  data_type: 'varchar'
  is_nullable: 1
  size: 128

=head2 is_bericht

  data_type: 'integer'
  is_nullable: 1

=head2 component

  data_type: 'varchar'
  is_nullable: 1
  size: 64

=head2 component_id

  data_type: 'integer'
  is_nullable: 1

=head2 seen

  data_type: 'integer'
  is_nullable: 1

=head2 onderwerp

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 bericht

  data_type: 'text'
  is_nullable: 1

=head2 created

  data_type: 'timestamp'
  is_nullable: 1

=head2 last_modified

  data_type: 'timestamp'
  is_nullable: 1

=head2 deleted_on

  data_type: 'timestamp'
  is_nullable: 1

=head2 event_type

  data_type: 'varchar'
  is_nullable: 1
  size: 32

=head2 event_data

  data_type: 'text'
  is_nullable: 1

=head2 created_by

  data_type: 'varchar'
  is_nullable: 1
  size: 64

=head2 modified_by

  data_type: 'varchar'
  is_nullable: 1
  size: 64

=head2 deleted_by

  data_type: 'varchar'
  is_nullable: 1
  size: 64

=head2 created_for

  data_type: 'varchar'
  is_nullable: 1
  size: 64

=head2 created_by_name_cache

  data_type: 'text'
  is_nullable: 1
  original: {data_type => "varchar"}

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "logging_id_seq",
  },
  "loglevel",
  { data_type => "varchar", is_nullable => 1, size => 32 },
  "zaak_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "betrokkene_id",
  { data_type => "varchar", is_nullable => 1, size => 128 },
  "aanvrager_id",
  { data_type => "varchar", is_nullable => 1, size => 128 },
  "is_bericht",
  { data_type => "integer", is_nullable => 1 },
  "component",
  { data_type => "varchar", is_nullable => 1, size => 64 },
  "component_id",
  { data_type => "integer", is_nullable => 1 },
  "seen",
  { data_type => "integer", is_nullable => 1 },
  "onderwerp",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "bericht",
  { data_type => "text", is_nullable => 1 },
  "created",
  { data_type => "timestamp", is_nullable => 1 },
  "last_modified",
  { data_type => "timestamp", is_nullable => 1 },
  "deleted_on",
  { data_type => "timestamp", is_nullable => 1 },
  "event_type",
  { data_type => "varchar", is_nullable => 1, size => 32 },
  "event_data",
  { data_type => "text", is_nullable => 1 },
  "created_by",
  { data_type => "varchar", is_nullable => 1, size => 64 },
  "modified_by",
  { data_type => "varchar", is_nullable => 1, size => 64 },
  "deleted_by",
  { data_type => "varchar", is_nullable => 1, size => 64 },
  "created_for",
  { data_type => "varchar", is_nullable => 1, size => 64 },
  "created_by_name_cache",
  {
    data_type   => "text",
    is_nullable => 1,
    original    => { data_type => "varchar" },
  },
);
__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 zaak_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::Zaak>

=cut

__PACKAGE__->belongs_to("zaak_id", "Zaaksysteem::Schema::Zaak", { id => "zaak_id" });


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2013-09-06 10:34:52
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:zbMHJWSlMYaZL56tkAQJKQ

__PACKAGE__->load_components(
    "+Zaaksysteem::DB::Component::Logging",
    "+DBIx::Class::Helper::Row::ToJSON",
    __PACKAGE__->load_components()
);

__PACKAGE__->belongs_to('maybezaak' => 'Zaaksysteem::Schema::Zaak', { id => 'zaak_id' }, { join_type => 'left' });

__PACKAGE__->add_columns('last_modified',
    { %{ __PACKAGE__->column_info('last_modified') },
    set_on_update => 1,
    set_on_create => 1,
});

__PACKAGE__->add_columns('created',
    { %{ __PACKAGE__->column_info('created') },
    set_on_create => 1,
});

__PACKAGE__->resultset_class('Zaaksysteem::DB::ResultSet::Logging');

__PACKAGE__->add_columns(
  "id",
  {
    data_type => "INTEGER",
    default_value => undef,
    is_nullable => 1,
    size => undef,
    is_auto_increment => 1,
  }
);




# You can replace this text with custom content, and it will be preserved on regeneration
1;
