package Zaaksysteem::Schema::BagWoonplaats;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp");

=head1 NAME

Zaaksysteem::Schema::BagWoonplaats

=head1 DESCRIPTION

11.7 : een woonplaats is een door de gemeenteraad als zodanig aangewezen gedeelte van het gemeentelijk grondgebied.

=cut

__PACKAGE__->table("bag_woonplaats");

=head1 ACCESSORS

=head2 identificatie

  data_type: 'varchar'
  is_nullable: 0
  size: 16

11.03 : de landelijk unieke aanduiding van een woonplaats, zoals vastgesteld door de beheerder van de landelijke tabel voor woonplaatsen.

=head2 begindatum

  data_type: 'varchar'
  is_nullable: 0
  size: 14

11.73 : de begindatum van een periode waarin een of meer gegevens die worden bijgehouden over een woonplaats een wijziging hebben ondergaan.

=head2 einddatum

  data_type: 'varchar'
  is_nullable: 1
  size: 14

11.74 : de einddatum van een periode waarin er geen wijzigingen hebben plaatsgevonden in de gegevens die worden bijgehouden over een woonplaats.

=head2 officieel

  data_type: 'varchar'
  is_nullable: 1
  size: 1

11.72 : een aanduiding waarmee kan worden aangegeven dat een object in de registratie is opgenomen als gevolg van een feitelijke constatering, zonder dat er op het moment van opname sprake is van een formele grondslag voor deze opname.

=head2 naam

  data_type: 'varchar'
  is_nullable: 0
  size: 80

11.70 : de benaming van een door het gemeentebestuur aangewezen woonplaats.

=head2 status

  data_type: 'varchar'
  is_nullable: 0
  size: 80

11.79 : de fase van de levenscyclus van een woonplaats, waarin de betreffende woonplaats zich bevindt.

=head2 inonderzoek

  data_type: 'varchar'
  is_nullable: 0
  size: 1

11.75 : een aanduiding waarmee wordt aangegeven dat een onderzoek wordt uitgevoerd naar de juistheid van een of meerdere gegevens van het betreffende object.

=head2 documentdatum

  data_type: 'varchar'
  is_nullable: 1
  size: 14

11.77 : de datum waarop het brondocument is vastgesteld, op basis waarvan een opname, mutatie of een verwijdering van gegevens ten aanzien van een woonplaats heeft plaatsgevonden.

=head2 documentnummer

  data_type: 'varchar'
  is_nullable: 1
  size: 20

11.78 : de unieke aanduiding van het brondocument op basis waarvan een opname, mutatie of een verwijdering van gegevens ten aanzien van een woonplaats heeft plaatsgevonden, binnen een gemeente.

=head2 correctie

  data_type: 'varchar'
  is_nullable: 1
  size: 1

het gegeven is gecorrigeerd.

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'bag_woonplaats_id_seq'

=cut

__PACKAGE__->add_columns(
  "identificatie",
  { data_type => "varchar", is_nullable => 0, size => 16 },
  "begindatum",
  { data_type => "varchar", is_nullable => 0, size => 14 },
  "einddatum",
  { data_type => "varchar", is_nullable => 1, size => 14 },
  "officieel",
  { data_type => "varchar", is_nullable => 1, size => 1 },
  "naam",
  { data_type => "varchar", is_nullable => 0, size => 80 },
  "status",
  { data_type => "varchar", is_nullable => 0, size => 80 },
  "inonderzoek",
  { data_type => "varchar", is_nullable => 0, size => 1 },
  "documentdatum",
  { data_type => "varchar", is_nullable => 1, size => 14 },
  "documentnummer",
  { data_type => "varchar", is_nullable => 1, size => 20 },
  "correctie",
  { data_type => "varchar", is_nullable => 1, size => 1 },
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "bag_woonplaats_id_seq",
  },
);
__PACKAGE__->set_primary_key("id");


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2013-08-15 10:46:15
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:oC+B4V0g5z/qASJHZpa1Ug

__PACKAGE__->resultset_class('Zaaksysteem::Backend::BagWoonplaats::ResultSet');

__PACKAGE__->load_components(
    '+Zaaksysteem::DB::Component::BagWoonplaats',
    __PACKAGE__->load_components()
);

__PACKAGE__->has_many(
  "openbareruimten",
  "Zaaksysteem::Schema::BagOpenbareruimte",
  { "foreign.woonplaats" => "self.identificatie" },
);


# You can replace this text with custom content, and it will be preserved on regeneration
1;
