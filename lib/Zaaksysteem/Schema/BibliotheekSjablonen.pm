package Zaaksysteem::Schema::BibliotheekSjablonen;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp");

=head1 NAME

Zaaksysteem::Schema::BibliotheekSjablonen

=cut

__PACKAGE__->table("bibliotheek_sjablonen");

=head1 ACCESSORS

=head2 search_index

  data_type: 'tsvector'
  is_nullable: 1

=head2 search_term

  data_type: 'text'
  is_nullable: 1

=head2 object_type

  data_type: 'varchar'
  default_value: 'bibliotheek_sjablonen'
  is_nullable: 1
  size: 100

=head2 searchable_id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'searchable_searchable_id_seq'

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'bibliotheek_sjablonen_id_seq'

=head2 bibliotheek_categorie_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 naam

  data_type: 'varchar'
  is_nullable: 1
  size: 256

=head2 label

  data_type: 'text'
  is_nullable: 1

=head2 description

  data_type: 'text'
  is_nullable: 1

=head2 help

  data_type: 'text'
  is_nullable: 1

=head2 created

  data_type: 'timestamp'
  is_nullable: 1

=head2 last_modified

  data_type: 'timestamp'
  is_nullable: 1

=head2 filestore_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 deleted

  data_type: 'timestamp'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "search_index",
  { data_type => "tsvector", is_nullable => 1 },
  "search_term",
  { data_type => "text", is_nullable => 1 },
  "object_type",
  {
    data_type => "varchar",
    default_value => "bibliotheek_sjablonen",
    is_nullable => 1,
    size => 100,
  },
  "searchable_id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "searchable_searchable_id_seq",
  },
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "bibliotheek_sjablonen_id_seq",
  },
  "bibliotheek_categorie_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "naam",
  { data_type => "varchar", is_nullable => 1, size => 256 },
  "label",
  { data_type => "text", is_nullable => 1 },
  "description",
  { data_type => "text", is_nullable => 1 },
  "help",
  { data_type => "text", is_nullable => 1 },
  "created",
  { data_type => "timestamp", is_nullable => 1 },
  "last_modified",
  { data_type => "timestamp", is_nullable => 1 },
  "filestore_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "deleted",
  { data_type => "timestamp", is_nullable => 1 },
);
__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 filestore_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::Filestore>

=cut

__PACKAGE__->belongs_to(
  "filestore_id",
  "Zaaksysteem::Schema::Filestore",
  { id => "filestore_id" },
);

=head2 bibliotheek_categorie_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::BibliotheekCategorie>

=cut

__PACKAGE__->belongs_to(
  "bibliotheek_categorie_id",
  "Zaaksysteem::Schema::BibliotheekCategorie",
  { id => "bibliotheek_categorie_id" },
);

=head2 bibliotheek_sjablonen_magic_strings

Type: has_many

Related object: L<Zaaksysteem::Schema::BibliotheekSjablonenMagicString>

=cut

__PACKAGE__->has_many(
  "bibliotheek_sjablonen_magic_strings",
  "Zaaksysteem::Schema::BibliotheekSjablonenMagicString",
  { "foreign.bibliotheek_sjablonen_id" => "self.id" },
  {},
);

=head2 zaaktype_sjablonens

Type: has_many

Related object: L<Zaaksysteem::Schema::ZaaktypeSjablonen>

=cut

__PACKAGE__->has_many(
  "zaaktype_sjablonens",
  "Zaaksysteem::Schema::ZaaktypeSjablonen",
  { "foreign.bibliotheek_sjablonen_id" => "self.id" },
  {},
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2013-01-28 07:34:21
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:ohVeK6YtgU3e2r/lMl/3iQ

__PACKAGE__->resultset_class('Zaaksysteem::Backend::BibliotheekSjablonen::ResultSet');

__PACKAGE__->load_components(qw/
    +Zaaksysteem::DB::Component::BibliotheekSjablonen
    +Zaaksysteem::Backend::BibliotheekSjablonen::Component
    +DBIx::Class::Helper::Row::ToJSON
/);

__PACKAGE__->belongs_to(
    "filestore",
    "Zaaksysteem::Schema::Filestore",
    { id => "filestore_id" },
);


# You can replace this text with custom content, and it will be preserved on regeneration
1;
