package Zaaksysteem::Schema::Straatnaam;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp");

=head1 NAME

Zaaksysteem::Schema::Straatnaam

=cut

__PACKAGE__->table("straatnaam");

=head1 ACCESSORS

=head2 search_index

  data_type: 'tsvector'
  is_nullable: 1

=head2 search_term

  data_type: 'text'
  is_nullable: 1

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'straatnaam_id_seq'

=head2 naam

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 status

  data_type: 'varchar'
  is_nullable: 1
  size: 100

=head2 object_type

  data_type: 'varchar'
  is_nullable: 1
  size: 100

=head2 searchable_id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'searchable_searchable_id_seq'

=cut

__PACKAGE__->add_columns(
  "search_index",
  { data_type => "tsvector", is_nullable => 1 },
  "search_term",
  { data_type => "text", is_nullable => 1 },
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "straatnaam_id_seq",
  },
  "naam",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "status",
  { data_type => "varchar", is_nullable => 1, size => 100 },
  "object_type",
  { data_type => "varchar", is_nullable => 1, size => 100 },
  "searchable_id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "searchable_searchable_id_seq",
  },
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2012-05-02 14:11:07
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:2aKzht4+PgGuJjNP/HWdcQ


# You can replace this text with custom content, and it will be preserved on regeneration
1;
