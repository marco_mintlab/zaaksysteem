package Zaaksysteem::Schema::TransactionRecordToObject;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp");

=head1 NAME

Zaaksysteem::Schema::TransactionRecordToObject

=cut

__PACKAGE__->table("transaction_record_to_object");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'transaction_record_to_object_id_seq'

=head2 transaction_record_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 local_table

  data_type: 'varchar'
  is_nullable: 1
  size: 100

=head2 local_id

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 mutations

  data_type: 'text'
  is_nullable: 1

=head2 date_deleted

  data_type: 'timestamp'
  is_nullable: 1

=head2 mutation_type

  data_type: 'varchar'
  is_nullable: 1
  size: 100

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "transaction_record_to_object_id_seq",
  },
  "transaction_record_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "local_table",
  { data_type => "varchar", is_nullable => 1, size => 100 },
  "local_id",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "mutations",
  { data_type => "text", is_nullable => 1 },
  "date_deleted",
  { data_type => "timestamp", is_nullable => 1 },
  "mutation_type",
  { data_type => "varchar", is_nullable => 1, size => 100 },
);
__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 transaction_record_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::TransactionRecord>

=cut

__PACKAGE__->belongs_to(
  "transaction_record_id",
  "Zaaksysteem::Schema::TransactionRecord",
  { id => "transaction_record_id" },
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2013-08-13 08:52:11
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:NLLFjSw+/USVQex4EzDqAw

__PACKAGE__->resultset_class('Zaaksysteem::Backend::Sysin::TransactionRecordToObject::ResultSet');

__PACKAGE__->load_components(qw/
    +Zaaksysteem::Backend::Sysin::TransactionRecordToObject::Component
    +DBIx::Class::Helper::Row::ToJSON
/);

# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
