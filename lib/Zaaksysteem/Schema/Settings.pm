package Zaaksysteem::Schema::Settings;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp");

=head1 NAME

Zaaksysteem::Schema::Settings

=cut

__PACKAGE__->table("settings");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'settings_id_seq'

=head2 key

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 value

  data_type: 'text'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "settings_id_seq",
  },
  "key",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "value",
  { data_type => "text", is_nullable => 1 },
);
__PACKAGE__->set_primary_key("id");


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2013-01-10 07:44:26
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:WNOyRP9UJopbyMIDgXaRrw

__PACKAGE__->resultset_class('Zaaksysteem::DB::ResultSet::Settings');

# You can replace this text with custom content, and it will be preserved on regeneration
1;
