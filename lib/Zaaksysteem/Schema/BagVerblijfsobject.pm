package Zaaksysteem::Schema::BagVerblijfsobject;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp");

=head1 NAME

Zaaksysteem::Schema::BagVerblijfsobject

=head1 DESCRIPTION

56 : een verblijfsobject is de kleinste binnen een of meerdere panden gelegen en voor woon -, bedrijfsmatige - of recreatieve doeleinden geschikte eenheid van gebruik, die ontsloten wordt via een eigen toegang vanaf de openbare weg, een erf of een gedeelde verkeersruimte en die onderwerp kan zijn van rechtshandelingen.

=cut

__PACKAGE__->table("bag_verblijfsobject");

=head1 ACCESSORS

=head2 identificatie

  data_type: 'varchar'
  is_nullable: 0
  size: 16

56.01 : de unieke aanduiding van een verblijfsobject

=head2 begindatum

  data_type: 'varchar'
  is_nullable: 0
  size: 14

56.91 : de begindatum van een periode waarin een of meer gegevens die worden bijgehouden over een verblijfsobject een wijziging hebben ondergaan.

=head2 einddatum

  data_type: 'varchar'
  is_nullable: 1
  size: 14

56.92 : de einddatum van een periode waarin er geen wijzigingen hebben plaatsgevonden in de gegevens die worden bijgehouden over een verblijfsobject.

=head2 officieel

  data_type: 'varchar'
  is_nullable: 1
  size: 1

56.02 : een aanduiding waarmee kan worden aangegeven dat een object in de registratie is opgenomen als gevolg van een feitelijke constatering, zonder dat er op het moment van opname sprake is van een formele grondslag voor deze opname

=head2 hoofdadres

  data_type: 'varchar'
  is_nullable: 0
  size: 16

56:10 : de identificatiecode nummeraanduiding waaronder het hoofdadres van een verblijfsobject, dat in het kader van de basis gebouwen registratie als zodanig is aangemerkt, is opgenomen in de basis registratie adressen.

=head2 oppervlakte

  data_type: 'integer'
  is_nullable: 1

56.31 : de gebruiksoppervlakte van een verblijfsobject in gehele vierkante meters.

=head2 status

  data_type: 'varchar'
  is_nullable: 0
  size: 80

56.32 : de fase van de levenscyclus van een verblijfsobject, waarin het betreffende verblijfsobject zich bevindt.

=head2 inonderzoek

  data_type: 'varchar'
  is_nullable: 0
  size: 1

56.93 : een aanduiding waarmee wordt aangegeven dat een onderzoek wordt uitgevoerd naar de juistheid van een of meerdere gegevens van het betreffende object.

=head2 documentdatum

  data_type: 'varchar'
  is_nullable: 1
  size: 14

56.97 : de datum waarop het brondocument is vastgesteld, op basis waarvan een opname, mutatie of een verwijdering van gegevens ten aanzien van een verblijfsobject heeft plaatsgevonden.

=head2 documentnummer

  data_type: 'varchar'
  is_nullable: 1
  size: 20

56.98 : de unieke aanduiding van het brondocument op basis waarvan een opname, mutatie of een verwijdering van gegevens ten aanzien van een verblijfsobject heeft plaatsgevonden, binnen een gemeente.

=head2 correctie

  data_type: 'varchar'
  is_nullable: 1
  size: 1

het gegeven is gecorrigeerd.

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'bag_verblijfsobject_id_seq'

=cut

__PACKAGE__->add_columns(
  "identificatie",
  { data_type => "varchar", is_nullable => 0, size => 16 },
  "begindatum",
  { data_type => "varchar", is_nullable => 0, size => 14 },
  "einddatum",
  { data_type => "varchar", is_nullable => 1, size => 14 },
  "officieel",
  { data_type => "varchar", is_nullable => 1, size => 1 },
  "hoofdadres",
  { data_type => "varchar", is_nullable => 0, size => 16 },
  "oppervlakte",
  { data_type => "integer", is_nullable => 1 },
  "status",
  { data_type => "varchar", is_nullable => 0, size => 80 },
  "inonderzoek",
  { data_type => "varchar", is_nullable => 0, size => 1 },
  "documentdatum",
  { data_type => "varchar", is_nullable => 1, size => 14 },
  "documentnummer",
  { data_type => "varchar", is_nullable => 1, size => 20 },
  "correctie",
  { data_type => "varchar", is_nullable => 1, size => 1 },
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "bag_verblijfsobject_id_seq",
  },
);
__PACKAGE__->set_primary_key("id");


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2013-08-15 10:46:15
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:yhKx8nbqgU/CMe6wmDHzIg

__PACKAGE__->belongs_to(
  "hoofdadres",
  "Zaaksysteem::Schema::BagNummeraanduiding",
  { "identificatie" => "hoofdadres" },
);

__PACKAGE__->might_have(
  "gebruiksdoel",
  "Zaaksysteem::Schema::BagVerblijfsobjectGebruiksdoel",
  { "foreign.identificatie" => "self.identificatie" },
);

__PACKAGE__->has_many(
  "panden",
  "Zaaksysteem::Schema::BagVerblijfsobjectPand",
  { "foreign.identificatie" => "self.identificatie" },
);

__PACKAGE__->has_many(
  "natuurlijk_personen",
  "Zaaksysteem::Schema::NatuurlijkPersoon",
  { "foreign.verblijfsobject_id" => "self.identificatie" },
);

__PACKAGE__->has_many(
  "bedrijven",
  "Zaaksysteem::Schema::Bedrijf",
  { "foreign.verblijfsobject_id" => "self.identificatie" },
);

__PACKAGE__->belongs_to(
  "identificatie",
  "Zaaksysteem::Schema::BagVerblijfsobjectGebruiksdoel",
  { identificatie => "identificatie" },
);

__PACKAGE__->load_components(
    "+DBIx::Class::Helper::Row::ToJSON",
    "+Zaaksysteem::Backend::Component",
    __PACKAGE__->load_components()
);



# You can replace this text with custom content, and it will be preserved on regeneration
1;
