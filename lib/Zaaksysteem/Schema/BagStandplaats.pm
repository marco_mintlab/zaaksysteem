package Zaaksysteem::Schema::BagStandplaats;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp");

=head1 NAME

Zaaksysteem::Schema::BagStandplaats

=head1 DESCRIPTION

57 : een standplaats is een formeel door de gemeenteraad als zodanig aangewezen terrein of een gedeelte daarvan, dat bestemd is voor het permanent plaatsen van een niet direct en duurzaam met de aarde verbonden en voor woon -, bedrijfsmatige - of recreatieve doeleinden geschikte ruimte.

=cut

__PACKAGE__->table("bag_standplaats");

=head1 ACCESSORS

=head2 identificatie

  data_type: 'varchar'
  is_nullable: 0
  size: 16

57.01 : de unieke aanduiding van een standplaats.

=head2 begindatum

  data_type: 'varchar'
  is_nullable: 0
  size: 14

57.91 : de begindatum van een periode waarin een of meer gegevens die worden bijgehouden over een standplaats een wijziging hebben ondergaan.

=head2 einddatum

  data_type: 'varchar'
  is_nullable: 1
  size: 14

57.92 : de einddatum van een periode waarin er geen wijzigingen hebben plaatsgevonden in de gegevens die worden bijgehouden over een standplaats.

=head2 officieel

  data_type: 'varchar'
  is_nullable: 1
  size: 1

57.02 : een aanduiding waarmee kan worden aangegeven dat een object in de registratie is opgenomen als gevolg van een feitelijke constatering, zonder dat er op het moment van opname sprake is van een formele grondslag voor deze opname.

=head2 status

  data_type: 'varchar'
  is_nullable: 0
  size: 80

57.03 : de fase van de levenscyclus van een standplaats, waarin de betreffende standplaats zich bevindt.

=head2 hoofdadres

  data_type: 'varchar'
  is_nullable: 0
  size: 16

57:10 : de identificatiecode nummeraanduiding waaronder het hoofdadres van een standplaats, dat in het kader van de basis gebouwen registratie als zodanig is aangemerkt, is opgenomen in de basis registratie adressen.

=head2 inonderzoek

  data_type: 'varchar'
  is_nullable: 0
  size: 1

57.93 : een aanduiding waarmee wordt aangegeven dat een onderzoek wordt uitgevoerd naar de juistheid van een of meerdere gegevens van het betreffende object.

=head2 documentdatum

  data_type: 'varchar'
  is_nullable: 1
  size: 14

57.97 : de datum waarop het brondocument is vastgesteld, op basis waarvan een opname, mutatie of een in de historie plaatsen van gegevens ten aanzien van een standplaats heeft plaatsgevonden.

=head2 documentnummer

  data_type: 'varchar'
  is_nullable: 1
  size: 20

57.98 : de unieke aanduiding van het brondocument op basis waarvan een opname, mutatie of een verwijdering van gegevens ten aanzien van een standplaats heeft plaatsgevonden, binnen een gemeente.

=head2 correctie

  data_type: 'varchar'
  is_nullable: 1
  size: 1

het gegeven is gecorrigeerd.

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'bag_standplaats_id_seq'

=cut

__PACKAGE__->add_columns(
  "identificatie",
  { data_type => "varchar", is_nullable => 0, size => 16 },
  "begindatum",
  { data_type => "varchar", is_nullable => 0, size => 14 },
  "einddatum",
  { data_type => "varchar", is_nullable => 1, size => 14 },
  "officieel",
  { data_type => "varchar", is_nullable => 1, size => 1 },
  "status",
  { data_type => "varchar", is_nullable => 0, size => 80 },
  "hoofdadres",
  { data_type => "varchar", is_nullable => 0, size => 16 },
  "inonderzoek",
  { data_type => "varchar", is_nullable => 0, size => 1 },
  "documentdatum",
  { data_type => "varchar", is_nullable => 1, size => 14 },
  "documentnummer",
  { data_type => "varchar", is_nullable => 1, size => 20 },
  "correctie",
  { data_type => "varchar", is_nullable => 1, size => 1 },
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "bag_standplaats_id_seq",
  },
);
__PACKAGE__->set_primary_key("id");


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2013-08-15 10:46:15
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:Owq4qGB5/6GHcyOZFAj7Kw

__PACKAGE__->resultset_class('Zaaksysteem::DB::ResultSet::BagGeneral');

__PACKAGE__->load_components(qw/
    +DBIx::Class::Helper::Row::ToJSON
/);

__PACKAGE__->belongs_to(
  "hoofdadres",
  "Zaaksysteem::Schema::BagNummeraanduiding",
  { "identificatie" => "hoofdadres" },
);




# You can replace this text with custom content, and it will be preserved on regeneration
1;
