package Zaaksysteem::Schema::ContactmomentNote;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp");

=head1 NAME

Zaaksysteem::Schema::ContactmomentNote

=cut

__PACKAGE__->table("contactmoment_note");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'contactmoment_note_id_seq'

=head2 message

  data_type: 'text'
  is_nullable: 1
  original: {data_type => "varchar"}

=head2 contactmoment_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "contactmoment_note_id_seq",
  },
  "message",
  {
    data_type   => "text",
    is_nullable => 1,
    original    => { data_type => "varchar" },
  },
  "contactmoment_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
);
__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 contactmoment_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::Contactmoment>

=cut

__PACKAGE__->belongs_to(
  "contactmoment_id",
  "Zaaksysteem::Schema::Contactmoment",
  { id => "contactmoment_id" },
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2013-02-25 12:42:09
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:p2QvAmQDmtdJV/6Et4gi/Q

__PACKAGE__->add_columns('+message', {is_serializable => 1});

# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
