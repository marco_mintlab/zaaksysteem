package Zaaksysteem::Schema::ZaaktypeChecklistStatus;

use strict;
use warnings;

use base 'DBIx::Class';

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp", "Core");
__PACKAGE__->table("zaaktype_checklist_status");
__PACKAGE__->add_columns(
  "id",
  {
    data_type => "integer",
    default_value => "nextval('zaaktype_checklist_status_id_seq'::regclass)",
    is_nullable => 0,
    size => 4,
  },
  "zaaktype_checklist_id",
  { data_type => "integer", default_value => undef, is_nullable => 1, size => 4 },
  "status",
  { data_type => "integer", default_value => undef, is_nullable => 1, size => 4 },
  "help",
  {
    data_type => "text",
    default_value => undef,
    is_nullable => 1,
    size => undef,
  },
);
__PACKAGE__->set_primary_key("id");
__PACKAGE__->add_unique_constraint("zaaktype_checklist_status_pkey", ["id"]);
__PACKAGE__->belongs_to(
  "zaaktype_checklist_id",
  "Zaaksysteem::Schema::ZaaktypeChecklist",
  { id => "zaaktype_checklist_id" },
);
__PACKAGE__->has_many(
  "zaaktype_checklist_vraags",
  "Zaaksysteem::Schema::ZaaktypeChecklistVraag",
  { "foreign.zaaktype_checklist_status_id" => "self.id" },
);


# Created by DBIx::Class::Schema::Loader v0.04006 @ 2012-05-16 12:12:12
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:l6agO7EufmVHX4LCixVmIw


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
