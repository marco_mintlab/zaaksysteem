package Zaaksysteem::Schema::ChecklistZaak;

use strict;
use warnings;

use base 'DBIx::Class';

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp", "Core");
__PACKAGE__->table("checklist_zaak");
__PACKAGE__->add_columns(
  "id",
  {
    data_type => "integer",
    default_value => "nextval('checklist_zaak_id_seq'::regclass)",
    is_nullable => 0,
    size => 4,
  },
  "zaaktype_id",
  { data_type => "integer", default_value => undef, is_nullable => 1, size => 4 },
  "description",
  {
    data_type => "text",
    default_value => undef,
    is_nullable => 1,
    size => undef,
  },
);
__PACKAGE__->set_primary_key("id");
__PACKAGE__->add_unique_constraint("checklist_zaak_pkey", ["id"]);
__PACKAGE__->has_many(
  "checklist_statuses",
  "Zaaksysteem::Schema::ChecklistStatus",
  { "foreign.checklist_id" => "self.id" },
);


# Created by DBIx::Class::Schema::Loader v0.04006 @ 2012-05-16 12:12:12
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:UEPaYQAS5GTFtI62ZvoOQA


# You can replace this text with custom content, and it will be preserved on regeneration
1;
