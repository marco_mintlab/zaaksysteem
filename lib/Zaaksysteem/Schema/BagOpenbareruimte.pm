package Zaaksysteem::Schema::BagOpenbareruimte;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp");

=head1 NAME

Zaaksysteem::Schema::BagOpenbareruimte

=head1 DESCRIPTION

11.1 : een openbare ruimte is een door de gemeenteraad als zodanig aangewezen benaming van een binnen een woonplaats gelegen buitenruimte.

=cut

__PACKAGE__->table("bag_openbareruimte");

=head1 ACCESSORS

=head2 identificatie

  data_type: 'varchar'
  is_nullable: 0
  size: 16

11.01 : de unieke aanduiding van een openbare ruimte.

=head2 begindatum

  data_type: 'varchar'
  is_nullable: 0
  size: 14

11.12 : de begindatum van een periode waarin een of meer gegevens die worden bijgehouden over een openbare ruimte een wijziging hebben ondergaan.

=head2 einddatum

  data_type: 'varchar'
  is_nullable: 1
  size: 14

11.13 : de einddatum van een periode waarin er geen wijzigingen hebben plaatsgevonden in de gegevens die worden bijgehouden over een openbare ruimte.

=head2 naam

  data_type: 'varchar'
  is_nullable: 0
  size: 80

11.10 : een naam die aan een openbare ruimte is toegekend in een daartoe strekkend formeel gemeentelijk besluit.

=head2 officieel

  data_type: 'varchar'
  is_nullable: 1
  size: 1

11.11 : een aanduiding waarmee kan worden aangegeven dat een object in de registratie is opgenomen als gevolg van een feitelijke constatering, zonder dat er op het moment van opname sprake is van een formele grondslag voor deze opname.

=head2 woonplaats

  data_type: 'varchar'
  is_nullable: 1
  size: 4

11.15 : unieke aanduiding van de woonplaats waarbinnen een openbare ruimte is gelegen.

=head2 type

  data_type: 'varchar'
  is_nullable: 0
  size: 40

11.16 : de aard van de als zodanig benoemde openbare ruimte.

=head2 inonderzoek

  data_type: 'varchar'
  is_nullable: 0
  size: 1

11.14 : een aanduiding waarmee wordt aangegeven dat een onderzoek wordt uitgevoerd naar de juistheid van een of meerdere gegevens van het betreffende object.

=head2 documentdatum

  data_type: 'varchar'
  is_nullable: 1
  size: 14

11.17 : de datum waarop het brondocument is vastgesteld, op basis waarvan een opname, mutatie of een verwijdering van gegevens ten aanzien van een openbare ruimte heeft plaatsgevonden.

=head2 documentnummer

  data_type: 'varchar'
  is_nullable: 1
  size: 20

11.18 : de unieke aanduiding van het brondocument op basis waarvan een opname, mutatie of een verwijdering van gegevens ten aanzien van een openbare ruimte heeft plaatsgevonden, binnen een gemeente.

=head2 status

  data_type: 'varchar'
  is_nullable: 0
  size: 80

11.19 : de fase van de levenscyclus van een openbare ruimte, waarin de betreffende openbare ruimte zich bevindt.

=head2 correctie

  data_type: 'varchar'
  is_nullable: 1
  size: 1

het gegeven is gecorrigeerd.

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'bag_openbareruimte_id_seq'

=cut

__PACKAGE__->add_columns(
  "identificatie",
  { data_type => "varchar", is_nullable => 0, size => 16 },
  "begindatum",
  { data_type => "varchar", is_nullable => 0, size => 14 },
  "einddatum",
  { data_type => "varchar", is_nullable => 1, size => 14 },
  "naam",
  { data_type => "varchar", is_nullable => 0, size => 80 },
  "officieel",
  { data_type => "varchar", is_nullable => 1, size => 1 },
  "woonplaats",
  { data_type => "varchar", is_nullable => 1, size => 4 },
  "type",
  { data_type => "varchar", is_nullable => 0, size => 40 },
  "inonderzoek",
  { data_type => "varchar", is_nullable => 0, size => 1 },
  "documentdatum",
  { data_type => "varchar", is_nullable => 1, size => 14 },
  "documentnummer",
  { data_type => "varchar", is_nullable => 1, size => 20 },
  "status",
  { data_type => "varchar", is_nullable => 0, size => 80 },
  "correctie",
  { data_type => "varchar", is_nullable => 1, size => 1 },
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "bag_openbareruimte_id_seq",
  },
);
__PACKAGE__->set_primary_key("id");


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2013-08-15 10:46:15
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:SJLy8A2Xq95kM3oTKZUW7w

__PACKAGE__->resultset_class('Zaaksysteem::SBUS::ResultSet::BAG');

__PACKAGE__->load_components(
    "+Zaaksysteem::DB::Component::BagOpenbareruimte",
    __PACKAGE__->load_components()
);

__PACKAGE__->belongs_to(
  "woonplaats",
  "Zaaksysteem::Schema::BagWoonplaats",
  { "identificatie" => "woonplaats" },
);

__PACKAGE__->has_many(
  "hoofdadressen",
  "Zaaksysteem::Schema::BagNummeraanduiding",
  { "foreign.openbareruimte" => "self.identificatie" },
);





# You can replace this text with custom content, and it will be preserved on regeneration
1;
