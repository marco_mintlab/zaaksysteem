package Zaaksysteem::Schema::FileCaseDocument;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp");

=head1 NAME

Zaaksysteem::Schema::FileCaseDocument

=cut

__PACKAGE__->table("file_case_document");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'file_case_document_id_seq'

=head2 file_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 case_document_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "file_case_document_id_seq",
  },
  "file_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "case_document_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
);
__PACKAGE__->set_primary_key("id");
__PACKAGE__->add_unique_constraint(
  "file_case_document_file_id_case_document_id_key",
  ["file_id", "case_document_id"],
);

=head1 RELATIONS

=head2 file_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::File>

=cut

__PACKAGE__->belongs_to("file_id", "Zaaksysteem::Schema::File", { id => "file_id" });

=head2 case_document_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::ZaaktypeKenmerken>

=cut

__PACKAGE__->belongs_to(
  "case_document_id",
  "Zaaksysteem::Schema::ZaaktypeKenmerken",
  { id => "case_document_id" },
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2013-10-04 08:10:18
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:GELatHE8Wm6yterZdDOHIw

__PACKAGE__->belongs_to(
  "case_document",
  "Zaaksysteem::Schema::ZaaktypeKenmerken",
  { id => "case_document_id" },
);

# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
