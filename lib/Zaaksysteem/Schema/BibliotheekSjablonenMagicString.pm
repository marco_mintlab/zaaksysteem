package Zaaksysteem::Schema::BibliotheekSjablonenMagicString;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp");

=head1 NAME

Zaaksysteem::Schema::BibliotheekSjablonenMagicString

=cut

__PACKAGE__->table("bibliotheek_sjablonen_magic_string");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'bibliotheek_sjablonen_magic_string_id_seq'

=head2 bibliotheek_sjablonen_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 value

  data_type: 'text'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "bibliotheek_sjablonen_magic_string_id_seq",
  },
  "bibliotheek_sjablonen_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "value",
  { data_type => "text", is_nullable => 1 },
);
__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 bibliotheek_sjablonen_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::BibliotheekSjablonen>

=cut

__PACKAGE__->belongs_to(
  "bibliotheek_sjablonen_id",
  "Zaaksysteem::Schema::BibliotheekSjablonen",
  { id => "bibliotheek_sjablonen_id" },
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2013-01-10 07:42:00
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:uqIZKqZntRwFL5A9sC5HDw





# You can replace this text with custom content, and it will be preserved on regeneration
1;
