package Zaaksysteem::Schema::BibliotheekNotificatieKenmerk;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp");

=head1 NAME

Zaaksysteem::Schema::BibliotheekNotificatieKenmerk

=cut

__PACKAGE__->table("bibliotheek_notificatie_kenmerk");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'bibliotheek_notificatie_kenmerk_id_seq'

=head2 bibliotheek_notificatie_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 bibliotheek_kenmerken_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "bibliotheek_notificatie_kenmerk_id_seq",
  },
  "bibliotheek_notificatie_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "bibliotheek_kenmerken_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
);
__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 bibliotheek_notificatie_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::BibliotheekNotificaties>

=cut

__PACKAGE__->belongs_to(
  "bibliotheek_notificatie_id",
  "Zaaksysteem::Schema::BibliotheekNotificaties",
  { id => "bibliotheek_notificatie_id" },
);

=head2 bibliotheek_kenmerken_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::BibliotheekKenmerken>

=cut

__PACKAGE__->belongs_to(
  "bibliotheek_kenmerken_id",
  "Zaaksysteem::Schema::BibliotheekKenmerken",
  { id => "bibliotheek_kenmerken_id" },
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2013-04-16 14:54:31
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:6cVdk/pJ4+7zMIlw3Cw46g

__PACKAGE__->load_components(
    "+DBIx::Class::Helper::Row::ToJSON",
    __PACKAGE__->load_components()
);

# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
