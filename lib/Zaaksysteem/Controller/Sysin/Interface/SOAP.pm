package Zaaksysteem::Controller::Sysin::Interface::SOAP;

use strict;
use warnings;

use Data::Dumper;
use Zaaksysteem::Backend::Sysin::Modules;

use Scalar::Util qw/blessed/;

use XML::LibXML;

use Moose;
use utf8;

BEGIN {extends 'Zaaksysteem::General::SOAPController'; }

sub enter :Chained('/sysin/interface/base'): PathPart('soap'): Args(0): SOAP('DocumentLiteral')  {
    my ( $self, $c) = @_;

    my $interface   = $c->stash->{entry};

    unless ($interface->active) {
        $c->stash->{soap}->fault(
           {
               code     => '404',
               reason   => 'Not found',
               detail   => 'Given interface not active'
           }
        );

        return;
    }

    my $xml             = $c->stash->{soap}->envelope();

    my $transaction     = $interface->process({
        input_data      => $xml,
        transaction_id  => 'unknown'
    });

    unless ($transaction->transaction_records->count) {
        $c->stash->{soap}->fault(
           {
               code     => '500',
               reason   => 'No response from Sysin',
               detail   => 'Error from Sysin, see logfiles for transaction id'
                            . $transaction->id
           }
        );

        return;
    }

    my $record = $transaction->transaction_records->first;

    if ($record->is_error || !$record->output) {
        $c->stash->{soap}->fault(
           {
               code     => '500',
               reason   => 'Failed processing record, see logfile.',
               detail   => 'Transaction id: ' . $transaction->id
           }
        );
    }

    my $responsexml             = XML::LibXML->load_xml(
        string => $record->output
    )->documentElement();

    $c->stash->{soap}->literal_return($responsexml);
}

1;

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut

