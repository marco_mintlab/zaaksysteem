package Zaaksysteem::Controller::Betrokkene;

use Moose;

BEGIN { extends 'Zaaksysteem::Controller'; }

use JSON;
use Data::Dumper;

use Zaaksysteem::Constants qw/
    ZAAKSYSTEEM_GM_AUTHENTICATEDBY_BEHANDELAAR
    VALIDATION_CONTACT_DATA
    ZAAKSYSTEEM_CONSTANTS
    LOGGING_COMPONENT_BETROKKENE
    DOCUMENTS_STORE_TYPE_NOTITIE
/;

sub index :Path :Args(0) {
    my ( $self, $c ) = @_;

    $c->response->body('Matched Zaaksysteem::Controller::Betrokkene in Betrokkene.');
}

sub base : Chained('/') : PathPart('betrokkene'): CaptureArgs(0) {
    my ($self, $c) = @_;

    ## Zaakid?
    if ($c->req->params->{'zaak'}) {
        $c->stash->{zaak} = $c->model('DB::Zaak')->find($c->req->params->{'zaak'});
    }
}

{
    Zaaksysteem->register_profile(
        method  => 'create',
        profile => {
            required => [ qw/
                betrokkene_type
                np-geslachtsnaam
                np-huisnummer
                np-postcode
                np-straatnaam
                np-voornamen
                np-woonplaats
                np-geslachtsaanduiding
            /],
            optional => [ qw/
                create
                np-burgerservicenummer
                np-huisnummertoevoeging
                np-voorvoegsel
                np-geboortedatum
                npc-telefoonnummer
                npc-email
                npc-mobiel
            /],
            constraint_methods  => {
                'np-burgerservicenummer'    => qr/^\d{1,9}$/,
                'np-geboortedatum'          => qr/[\d-]+/,
                'np-geslachtsnaam'          => qr/.+/,
                'np-huisnummer'             => qr/[\d]+/,
                'np-postcode'               => qr/^\d{4}\w{2}$/,
                'np-straatnaam'             => qr/.+/,
                'np-voorletters'            => qr/[\w.]+/,
                'np-voornamen'              => qr/.+/,
                'np-woonplaats'             => qr/.+/,
                'npc-email'                 => qr/^.+?\@.+\.[a-z0-9]{2,}$/,
                'npc-telefoonnummer'        => qr/^[\d\+]{6,15}$/,
                'npc-mobiel'                => qr/^[\d\+]{6,15}$/,
            },
            msgs                => {
                'format'    => '%s',
                'missing'   => 'Veld is verplicht.',
                'invalid'   => 'Veld is niet correct ingevuld.',
                'constraints' => {
                    '(?-xism:^\d{4}\w{2}$)' => 'Postcode zonder spatie (1000AA)',
                    '(?-xism:^[\d\+]{6,15}$)' => 'Nummer zonder spatie (e.g: +312012345678)',
                }
            },
        }
    );

    sub create : Chained('/') : PathPart('betrokkene/create'): Args(0) {
        my ($self, $c) = @_;

        if ($c->req->is_xhr) {
            $c->zvalidate;
            $c->detach;
        }

        ### Default: view
        $c->stash->{template}   = 'betrokkene/create.tt';

        if ($c->req->method eq 'POST') {
            # Validate information
            my $params = $c->req->params;
            return unless $c->zvalidate && $params->{create};


            ### Create person

            # Convert postcode
            $params->{'np-postcode'} = uc($params->{'np-postcode'});

            my $id = $c->model('Betrokkene')->create(
                'natuurlijk_persoon',
                {
                    %$params,
                    authenticatedby =>
                        ZAAKSYSTEEM_GM_AUTHENTICATEDBY_BEHANDELAAR,
                }
            );

            $c->model('DB::Logging')->trigger('subject/create', {
                component => LOGGING_COMPONENT_BETROKKENE,
                component_id => $id,
                created_for => $id,
                data => {
                    subject_id => $id
                }
            });
            
            if ($id) {
                $c->push_flash_message('Natuurlijk persoon aangemaakt');
                $c->res->redirect(
                    $c->uri_for(
                        '/betrokkene/' . $id,
                        { gm => 1, type => 'natuurlijk_persoon' }
                    )
                );
            }
        }

    }
}


sub view_base : Chained('base'): PathPart('') : CaptureArgs(1) {
    my ($self, $c, $id) = @_;

    die "view_base: id not set, nothing to do here" unless $id =~ /^\d+$/;

    my $betrokkene_type = $c->req->params->{type};

    $c->stash->{requested_bid} = $id;

    if ($c->req->params->{gm}) {
        $c->stash->{betrokkene} = $c->model('Betrokkene')->get(
            {
                type    => $betrokkene_type,
                intern  => 0,
            },
            $id
        );

        $c->stash->{'betrokkene_edit'} = 1 unless
            !$c->req->params->{edit} ||
            $c->stash->{'betrokkene'}->authenticated
    } else {
        $c->stash->{betrokkene} = $c->model('Betrokkene')->get(
            {},
            $id
        );

    }

    if ($c->stash->{betrokkene} && 0) {
        $c->stash->{notities} = $c->model('DB::Documenten')->search(
            {
                betrokkene_dsn  =>
                    $c->stash->{betrokkene}->betrokkene_identifier,
                store_type      => DOCUMENTS_STORE_TYPE_NOTITIE
            },
            {
                order_by => { '-desc'   => 'ontvangstdatum' },
                join     => 'notitie_id',
            }
        );
    }

    $c->forward('include_woz_tab');

    $c->detach unless $c->stash->{betrokkene};


    $c->forward('handle_betrokkene_session');
}


sub include_woz_tab : Private {
    my ($self, $c) = @_;

    if ($c->check_any_user_permission(qw/woz_objects/) &&
        $c->stash->{ betrokkene }->can('burgerservicenummer') &&
        $c->stash->{betrokkene}->burgerservicenummer
    ) {
        my $owner = 
            $c->stash->{betrokkene}->btype . '-' .
            $c->stash->{betrokkene}->burgerservicenummer;

        my $arguments = [$owner];
        if (my $woz_id = $c->req->params->{woz_id}) {
            push @$arguments, $woz_id;
        }

        $c->forward('/beheer/woz/view', $arguments);
    }
}

sub woz_object : Chained('view_base'): PathPart('woz_object') {
    my ($self, $c) = @_;

    warn "woz_object: ". Dumper $c->req->params;
}


sub handle_betrokkene_session : Private {
    my ($self, $c) = @_;

    if ($c->req->params->{enable_betrokkene_session}) {
        $c->betrokkene_session_enable($c->stash->{betrokkene});

        if ($c->req->is_xhr) {
            $c->stash->{json} = {
                succes  => 1,
                naam    => $c->betrokkene_session->naam,
                url     => $c->uri_for(
                    '/betrokkene/' . $c->betrokkene_session->ex_id,
                    {
                        gm  => 1,
                        type => $c->betrokkene_session->btype

                    }
                )->as_string
            };

            $c->detach('Zaaksysteem::View::JSONlegacy');
        }
    }
}

=head1 Disable subject session

JSON callable for removing / disabling the current active subject session, if any.

=head2 URL Construction

B</betrokkene/disable_session>

=cut

sub disable_betrokkene_session : JSON : Chained('/') : PathPart('betrokkene/disable_session') : Args(0) {
    my ($self, $c) = @_;

    unless ($c->betrokkene_session_disable) {
        $c->stash->{ json } = { success => 0, error => 'Unable to disable current subject session' };
        $c->detach('Zaaksysteem::View::JSONlegacy');
    }

    $c->stash->{json} = { succes  => 1 };
    $c->detach('Zaaksysteem::View::JSON');
}

=head1 Enable a subject session

JSON callable for enabling the subject session for a specific subject.

=head2 URL Construction

B</betrokkene/enable_session/betrokkene-[type]-[gmid]>

=head2 Response

In case of success, responds with HTTP 200.

In case of failure, responds with HTTP 404 in case the supplied subject identifier did not resolve to an actual subject instance.

=cut

sub enable_betrokkene_session : Chained('/') : JSON : PathPart('betrokkene/enable_session') : Args(1) {
    my ($self, $c, $id) = @_;

    my $betrokkene = $c->model('Betrokkene')->get({}, $id);

    unless($betrokkene) {
        $c->res->status(404);

        $c->stash->{ json } = { success => 0, error => 'No such subject' };
        return $c->detach('Zaaksysteem::View::JSON');
    }

    $c->betrokkene_session_enable($betrokkene);

    $c->detach('session_betrokkene');
}

=head1 Get the current subject session

JSON callable for retrieving the currently active subject session details, if any are set.

=head2 URL construction

B</betrokkene/get_session>

=head2 Response

Should not return anything but HTTP 200, unless something broke further down the calling stack in Zaaksysteem.

Response body shall be a JSON string, in the following format

    {
        "rows_total": <integer>,                    # Total number of rows in this resultset, 0 or 1
        "rows": <integer>,                          # Total number of rows on this page, 0 or 1
        "page": 1,
        "pages_total": 1,
        "next": null,
        "prev": null,
        "result": [
            "id": <string>,                         # Subject identifier in the common string format
            "name": <string>,                       # Display name of the subject
            "street": <string>,                     # Full street address
            "city": <string>
            "postal_code": <string>,
            "telephone_numbers": [ <string> ],      # Array of defined phonenumbers, can either be mobile or landline
            "email_addresses": [ <string> ],        # Array of e-mail addresses know for this subject
        ]
    }

=cut

sub session_betrokkene : Chained('/') : JSON : PathPart('betrokkene/get_session') : Args(0) {
    my ($self, $c) = @_;

    my $subject = $c->betrokkene_session;
    my @result;

    if($subject) {
        if($subject->isa('Zaaksysteem::Betrokkene::Object::NatuurlijkPersoon')) {
           push(@result, { subject => {
                id => $subject->betrokkene_identifier,
                name => $subject->display_name,
                street => join(' ', grep { $_ } ( $subject->straatnaam, $subject->huisnummer, $subject->huisnummertoevoeging )),
                city => $subject->woonplaats,
                postal_code => $subject->postcode,
                telephone_numbers => [ grep { $_ } ($subject->telefoonnummer, $subject->mobiel) ],
                email_addresses => [ grep { $_ } ($subject->email) ],
                type => 'natuurlijk_persoon',
                gmid => $subject->gmid,
            }});
        }

        if($subject->isa('Zaaksysteem::Betrokkene::Object::Bedrijf')) {
            push(@result, { subject => {
                id => $subject->betrokkene_identifier,
                name => $subject->display_name,
                street => join(' ', grep { $_ } ( $subject->vestiging_straatnaam, $subject->vestiging_huisnummer, $subject->vestiging_huisnummertoevoeging )),
                city => $subject->vestiging_woonplaats,
                postal_code => $subject->vestiging_postcode,
                telephone_numbers => [ grep { $_ } ($subject->telefoonnummer, $subject->mobiel) ],
                email_addresses => [ grep { $_ } ($subject->email) ],
                type => 'bedrijf',
                gmid => $subject->gmid
            }});
        }
    }

    $c->res->content_type('application/json');
    $c->res->body(to_json({
        rows_total => scalar(@result),
        page => 1,
        pages_total => 1,
        rows => scalar(@result),
        result => \@result,
        next => undef,
        prev => undef
    }));
}

sub _betrokkene_zaken : Private {
    my ($self, $c, $opts)   = @_;

    $c->stash->{results_per_page} = (
        $c->req->params->{results_per_page} ||
        $opts->{rows} ||
        10
    );

    my $resultset = $c->model('Zaken')->zaken_pip(
        {
            page                    => ($opts->{page} || $c->req->params->{'page'} || 1),
            rows                    => ($c->req->params->{results_per_page} || $opts->{rows} || 10),
            betrokkene_type         => $opts->{betrokkene}->btype,
            gegevens_magazijn_id    => $opts->{betrokkene}->ex_id,
            type_zaken              => $opts->{type_zaken},
            'sort_direction'        => $c->req->params->{sort_direction},
            'sort_field'            => $c->req->params->{sort_field},
        }
    );

    return $c->model('Zaken')->filter({
        resultset      => $resultset,
        textfilter     => $opts->{textfilter},
        dropdown       => (
            $opts->{statusfilter} ||
            $c->req->params->{'statusfilter'},
        )
    });
}

sub view : Chained('view_base'): PathPart('') : Args() {
    my ($self, $c) = @_;

    $c->stash->{template}   = 'betrokkene/view.tt';

    if ($c->check_any_user_permission(qw/contact_nieuw contact_search/)) {
        $c->stash->{can_betrokkene_edit} = 1;
    }


    if ($c->user_exists && $c->stash->{'betrokkene'}) {
        $c->stash->{'betrokkene'}->log_view(
            'betrokkene-medewerker-' . $c->user->uidnumber
        );
    }

    $c->stash->{force_result_finish} = 1;

    $c->stash->{zaken}  = $c->forward('_betrokkene_zaken', [
        {
            rows                    => 10,
            betrokkene              => $c->stash->{betrokkene},
            type_zaken              => ['resolved', 'overdragen', 'new', 'open'],
        }
    ]);
    $c->stash->{'zaken_display_fields'} = $c->model('SearchQuery')->get_display_fields();

    $c->stash->{open_zaken}  = $c->forward('_betrokkene_zaken', [
        {
            rows                    => 10,
            betrokkene              => $c->stash->{betrokkene},
            type_zaken              => ['new', 'open', 'stalled'],
            textfilter              => $c->req->params->{open_textfilter}
        }
    ]);

    # status
    {
        my $open_display_fields = {
            map { $_->{fieldname} => $_ }
            @{ $c->model('SearchQuery')->get_display_fields() }
        };

        $open_display_fields->{'days_left'}->{label} = 'Dagen';

        $c->stash->{'open_display_fields'} = [];
        push(
            @{ $c->stash->{'open_display_fields'} },
            $open_display_fields->{ $_ }
        ) for qw/status me.id zaaktype_node_id.titel
        me.onderwerp days_left/;
    }



    if ($c->stash->{betrokkene}->verblijfsobject) {
        $c->stash->{adres_zaken}        = $c->model('Zaken')->adres_zaken(
            {
                page                    => ($c->req->params->{'page'} || 1), 
                rows                    => 10,
                nummeraanduiding        => $c->stash->{betrokkene}
                                                ->verblijfsobject
                                                ->hoofdadres,
                'sort_direction'        => $c->req->params->{sort_direction},
                'sort_field'            => $c->req->params->{sort_field},
            }
        );

        $c->stash->{'adres_display_fields'} = $c->model('SearchQuery')->get_display_fields();
    }
    
    my $betrokkene_obj = $c->stash->{betrokkene};
    if (
        $betrokkene_obj &&
        $betrokkene_obj->can('messages') &&
        $betrokkene_obj->messages &&
        scalar(keys %{ $betrokkene_obj->messages })
    ) {
        $c->push_message(
            'Let op: '
                . join(', ',
                    map(
                        { ucfirst($_) }
                        values %{ $betrokkene_obj->messages }
                    )
                ),
        );
    }

    ### Meldingen
    $c->stash->{ $_ } = $c->req->params->{ $_ } for qw/sort_direction sort_field/;
}

sub search : Chained('base'): PathPart('search') {
    my ($self, $c) = @_;

    $c->stash->{ $_ } = $c->req->params->{ $_ } for (
        keys (%{ $c->req->params })
    );

    my $stufconfig = $c->model('DB::Interface')
                ->find_by_module_name('stufconfig');

    if ($stufconfig && $stufconfig->active) {
        my $stuf_params = $stufconfig->get_interface_config;

        if ($stuf_params && $stuf_params->{bidirectional} && $stuf_params->{makelaar_search}) {
            $c->stash->{search_stuf} = 1;
        }
    }

    if ($c->req->is_xhr) {
        $c->stash->{nowrapper} = 1;

        $c->stash->{betrokkene_type} = $c->req->params->{betrokkene_type} ||
            $c->req->params->{jstype};

        if (exists($c->req->params->{search})) {
            my %sparams = ();

            for my $key (keys %{ $c->req->params }) {
                if ($c->req->params->{$key}) {
                    my $rawkey = $key;
                    $key =~ s/np-//g;
                    $sparams{$key} = $c->req->params->{$rawkey};
                }
            }

            # Geboortedatum...
            if ($sparams{'geboortedatum-dag'}) {
                $sparams{'geboortedatum'} =
                    sprintf('%02d', $sparams{'geboortedatum-jaar'}) . '-'
                    . sprintf('%02d', $sparams{'geboortedatum-maand'}) . '-'
                    .$sparams{'geboortedatum-dag'};
            } elsif ($sparams{'geboortedatum'}) {
                $sparams{'geboortedatum'} =~ s/^(\d{2})-(\d{2})-(\d{4})$/$3-$2-$1/;
            }


            my $rows_per_page = $c->req->param('rows_per_page') || 40;
            delete($sparams{$_}) for qw/import_datum url method jscontext jsversion jsfill submit search jstype rows_per_page/;

            my $type    = $c->req->params->{jstype};
            if ($c->req->params->{jsversion} == 3) {
                $c->log->debug('Betrokkene server VERSION 3');
                delete($sparams{$_}) for grep { /^ezra_client_info/ } keys %{
                    $c->req->params
                };
                $type   = $c->req->params->{betrokkene_type};
            }

            $c->stash->{betrokkene_type} = $type;

            delete($sparams{betrokkene_type});


            $c->stash->{template} = 'betrokkene/popup/search_resultrows.tt';

            if (
                $c->stash->{search_stuf} &&
                $c->req->params->{external_search}
            ) {
                my $stufprs     = $c->model('DB::Interface')
                                ->find_by_module_name('stufprs');

                $sparams{geboortedatum} =~ s/-//g;

                $c->stash->{results} = $stufprs->process_trigger(
                    'search',
                    \%sparams
                );

                $c->log->debug('makelaar subjects' . Dumper(
                        $c->stash->{results}
                ));
            } else {
                my $betrokkenen = $c->model('Betrokkene')->search(
                    {
                        type    => $type,
                        intern  => 0,
                        rows_per_page => $rows_per_page,
                    },
                    \%sparams
                );

                $c->stash->{results} = [];

                if ($betrokkenen) {
                    while (my $bet = $betrokkenen->next) {
                        push (@{ $c->stash->{results} }, $bet);
                    }
                }

            }

            $c->detach;
        }

        $c->stash->{template} = 'betrokkene/popup/search.tt';
    } else {
        $c->stash->{template} = 'betrokkene/search.tt';

        ## Paging
        $c->stash->{ $_ } = $c->req->params->{ $_ }
            for grep {
                $c->req->params->{ $_ } &&
                $c->req->params->{ $_ } =~ /^\d+/
            } qw/paging_page paging_rows/;

        my %sparams = ();
        my ($startsearch, $betrokkene_type);

        if (exists($c->req->params->{search})) {
            for my $key (keys %{ $c->req->params }) {
                if ($c->req->params->{betrokkene_type} eq 'natuurlijk_persoon') {
                    if ($c->req->params->{$key} && $key =~ /^np-/) {
                        my $rawkey = $key;
                        $key =~ s/np-//g;
                        $sparams{$key} = $c->req->params->{$rawkey};
                    }
                } elsif ($c->req->params->{betrokkene_type} eq 'bedrijf') {
                    my $rawkey = $key;
                    next if (
                        lc($rawkey) eq 'search' ||
                        lc($rawkey) eq 'betrokkene_type'
                    );
                    $sparams{$key} = $c->req->params->{$rawkey};
                } elsif ($c->req->params->{betrokkene_type} eq 'medewerker') {
                    my $rawkey = $key;
                    next if (
                        lc($rawkey) eq 'search' ||
                        lc($rawkey) eq 'betrokkene_type'
                    );
                    $sparams{$key} = $c->req->params->{$rawkey};
                }

            }
            $betrokkene_type = $c->req->params->{'betrokkene_type'};

            $startsearch++;
        } elsif (
            (
                $c->stash->{paging_page} ||
                $c->req->params->{order}
            ) && $c->session->{betrokkene_search_data}
        ) {
            %sparams            = %{ $c->session->{betrokkene_search_data} };
            $betrokkene_type    = $c->session->{betrokkene_type};
            $startsearch++;
        } else {
            delete($c->session->{betrokkene_search_data});
        }

        if ($startsearch) {
            $c->session->{betrokkene_search_data} = \%sparams;
            $c->session->{betrokkene_type} = $betrokkene_type;

            $c->stash->{template} = 'betrokkene/search_results.tt';

            $c->log->debug('Search for betrokkene with params' .
                Dumper(\%sparams));


            # Geboortedatum...
            if ($sparams{'geboortedatum-dag'}) {
                $sparams{'geboortedatum'} =
                    sprintf('%02d', $sparams{'geboortedatum-jaar'}) . '-'
                    . sprintf('%02d', $sparams{'geboortedatum-maand'}) . '-'
                    .$sparams{'geboortedatum-dag'};
            } elsif ($sparams{'geboortedatum'}) {
                $sparams{'geboortedatum'} =~ s/^(\d{2})-(\d{2})-(\d{4})$/$3-$2-$1/;
            }

$c->log->debug("contact search params: " . Dumper \%sparams);
            $c->stash->{betrokkenen} = $c->model('Betrokkene')->search(
                {
                    type    => $betrokkene_type,
                    intern  => 0,
                },
                \%sparams
            );

            $c->stash->{betrokkene_type} = $betrokkene_type;
        }

    }
}

sub external_import : Chained('/'): PathPart('betrokkene/external_import'): Args(0) {
    my ($self, $c)  = @_;

    if ($c->req->params->{system_of_record_id}) {
        my $stufprs     = $c->model('DB::Interface')
                        ->find_by_module_name('stufprs');

        my $transaction;
        eval {
            $transaction = $stufprs->process_trigger(
                'import',
                {
                    sleutelGegevensbeheer   => $c->req->params->{system_of_record_id},
                }
            );
        };

        if ($@ || !$transaction || !$transaction->success_count) {
            $c->stash->{json} = $c->format_error($@);
        } else {
            my $record      = $transaction->records->first;

            my $npm         = $record->transaction_record_to_objects->search(
                {
                    'local_table'           => 'NatuurlijkPersoon',
                    'transaction_record_id' => $record->id,
                }
            )->first;

            $c->stash->{json} = $c->view('JSON')->prepare_json_row(
                {
                    subject_source_identifier   => 'betrokkene-natuurlijk_persoon-' . $npm->local_id
                }
            );
        }
    }

    $c->forward('View::JSON');
}



sub snapshot : Chained('base'): PathPart('snapshot'): Args(2) {
    my ($self, $c, $betrokkene_type, $id) = @_;


    $c->stash->{'betrokkene'} = $c->model('Betrokkene')->get(
        {
            intern  => 1,
            type    => $betrokkene_type,
        },
        $id
    );

    $c->stash->{nowrapper} = 1;
    $c->stash->{template} = 'betrokkene/popup/get.tt';
}


sub get : Chained('base'): PathPart('get'): Args(1) {
    my ($self, $c, $id) = @_;
    
    die "betrokkene_id not set" unless($id);

    my $params = $c->req->params;

    if ($params->{betrokkene_type}) {
        $c->stash->{'betrokkene'} = $c->model('Betrokkene')->get(
            {
                intern  => 0,
                type    => $params->{betrokkene_type},
            },
            $id
        ) or return;
    } else {
        $c->stash->{'betrokkene'} = $c->model('Betrokkene')->get({}, $id)
            or return;
    }

    if ($c->user_exists && $c->stash->{'betrokkene'}) {
        $c->stash->{'betrokkene'}->log_view(
            'betrokkene-medewerker-' . $c->user->uidnumber
        );
    }

    if ($params->{actueel} && $params->{actueel} =~ /^\d+$/) {
        if ($c->stash->{'betrokkene'}->gm_extern_np) {
            my $gegevens_magazijn_id =
                $c->stash->{'betrokkene'}->gm_extern_np->id;

            $c->stash->{betrokkene} = $c->model('Betrokkene')->get(
                {
                    intern  => 0,
                    type    => $c->stash->{betrokkene}->btype,
                },
                $gegevens_magazijn_id
            );

            $c->log->debug('Externe betrokkene vraag');
        }
    }

    if ($c->req->is_xhr) {
        $c->stash->{nowrapper} = 1;
        $c->stash->{template} = 'betrokkene/popup/get.tt';
    }
}



{
    sub _load_update_profile {
        my ($self, $c) = @_;

        if ($c->req->params->{betrokkene_type} eq 'bedrijf') {
            ### Get profile from Model
            my $profile = $c->get_profile(
                'method'=> 'create',
                'caller' => 'Zaaksysteem::Betrokkene::Object::Bedrijf'
            ) or return;

            my @required_fields = grep {
                $_ ne 'vestiging_postcodewoonplaats' ||
                $_ ne 'vestiging_adres'
            } @{ $profile->{required} };

            push(@required_fields, 'rechtsvorm');

            $profile->{required} = \@required_fields;

            $c->register_profile(
                method => 'update',
                profile => $profile,
            );
        } else {
            $c->register_profile(
                method => 'update',
                profile => 'Zaaksysteem::Controller::Betrokkene::create'
            );
        }
    }

    my $BETROKKENE_MAP = {
        bedrijf => 2,
        natuurlijk_persoon => 1,
    };

    Zaaksysteem->register_profile(
        method => 'update',
        profile => VALIDATION_CONTACT_DATA
    );

    sub update : Chained('base'): PathPart('info/update'): Args(1) {
        my ($self, $c, $gmid) = @_;

        return unless $c->check_any_user_permission(qw/contact_nieuw contact_search/);

        my $params = $c->req->params();

        ### Betrokkene edit only 
        if ($params->{betrokkene_edit}) {
            $self->_load_update_profile($c)
                if $params->{betrokkene_edit};
        } else {
            $c->register_profile(
                'method'    => 'update',
                profile     => VALIDATION_CONTACT_DATA,
            );
        }

        if ($c->req->is_xhr) {
            $c->zvalidate;
            $c->detach;
        }

        ### END Betrokkene edit only

        my $contact_data = $c->model('DB::ContactData')->search({
            gegevens_magazijn_id  => $gmid,
            betrokkene_type         => $BETROKKENE_MAP->{
                $params->{betrokkene_type}
            },
        });

        my $betrokkene_ident = sprintf(
            'betrokkene-%s-%d',
            $params->{ betrokkene_type },
            $gmid
        );

        if ($contact_data->count) {
            $contact_data = $contact_data->first;
        } else {
            $contact_data = $c->model('DB::ContactData')->create({
                    gegevens_magazijn_id    => $gmid,
                    betrokkene_type         => $BETROKKENE_MAP->{
                        $params->{betrokkene_type}
                    },
            });
        }

        # Update niet authentieke gegevens
        if ($params->{betrokkene_edit} && (my $dv = $c->zvalidate)) {
            my $gmbetrokkene = $c->model('Betrokkene')->get(
                {
                    type    => $params->{betrokkene_type},
                    intern  => 0,
                },
                $gmid
            );

            unless ($gmbetrokkene->authenticated) {
                if ($params->{betrokkene_type} eq 'bedrijf') {
                    my $params = $dv->valid;
                    for my $dbkey (keys %{ $params }) {

                        $gmbetrokkene->$dbkey($params->{$dbkey})
                            if $gmbetrokkene->can($dbkey);
                    }
                } else {
                    for my $pkey (grep(/^np-/, keys %{ $params })) {
                        my $dbkey = $pkey;
                        $dbkey =~ s/^np-//g;

                        warn('UPDATING:' . $dbkey);

                        $gmbetrokkene->$dbkey($params->{$pkey})
                            if $gmbetrokkene->can($dbkey);
                    }
                }
            }
        }

        # Update contactgegevens
        if ($c->zvalidate) {
            $contact_data->mobiel($params->{'npc-mobiel'});
            $contact_data->telefoonnummer($params->{'npc-telefoonnummer'});
            $contact_data->email($params->{'npc-email'});
            $contact_data->update;

            $c->model('DB::Logging')->trigger('subject/update', {
                component => LOGGING_COMPONENT_BETROKKENE,
                component_id => $gmid,
                created_for => $betrokkene_ident,
                data => {
                    subject_id => $betrokkene_ident,
                    parameters => $params
                }
            });
        }

        if ($c->stash->{zaak}) {
            $c->res->redirect($c->uri_for('/zaak/' . $c->stash->{zaak}->nr));
        } else {
            # Remove edit on post
            my $referer = $c->req->referer;
            $referer =~ s/[&\?]?edit=1//;

            $c->res->redirect($referer);
        }
    }
}

{
    sub verwijder : Chained('base'): PathPart('verwijder'): Args(2) {
        my ($self, $c, $betrokkene_type, $gmid) = @_;

        return unless $c->check_any_user_permission(qw/contact_nieuw contact_search/);

        return unless $gmid;

        my $gmbetrokkene = $c->model('Betrokkene')->get(
            {
                type    => $betrokkene_type,
                intern  => 0,
            },
            $gmid
        );
        
        my $params = $c->req->params();

        # Update niet authentieke gegevens
        if (%$params && $params->{confirmed}) {
            $c->response->redirect(
                $c->uri_for(
                    '/betrokkene/search'
                )
            );

            do {
                $c->push_flash_message('Deze betrokkene kan niet'
                    . ' worden verwijderd');
                $c->detach;
            } unless $gmbetrokkene->can_verwijderen;

            if ($gmbetrokkene->verwijder) {
                my $event = $c->model('DB::Logging')->trigger('subject/remove', {
                    component => LOGGING_COMPONENT_BETROKKENE,
                    component_id => $gmid,
                    data => {
                        subject_name => $gmbetrokkene->naam,
                        subject_id => $gmid
                    }
                });

                $c->push_flash_message($event->onderwerp);
            }
        }

        $c->stash->{confirmation}->{message}    =
            'Weet u zeker dat u betrokkene "'
            . $gmbetrokkene->naam . '" wilt verwijderen?';

        $c->stash->{confirmation}->{type}       = 'yesno';
        $c->stash->{confirmation}->{uri}        =
            $c->uri_for(
                '/betrokkene/verwijder/' . $betrokkene_type . '/' . $gmid
            );


        $c->forward('/page/confirmation');
        $c->detach;
    }
}

sub betrokkene : Chained('/') : PathPart('betrokkene'): CaptureArgs(1) {
    my ($self, $c, $betrokkene_identifier) = @_;

    if ($betrokkene_identifier) {
        my ($betrokkene_type, $betrokkene_id)
            = $betrokkene_identifier =~ /^betrokkene-(.*?)-(\d+)$/;

        unless ($betrokkene_type && $betrokkene_id) {
            $c->res->redirect($c->uri_for('/'));
            $c->detach;
        }

        $c->stash->{betrokkene} = $c->model('Betrokkene')->get(
            {
                'type'      => $betrokkene_type,
                'intern'    => 0,
            },
            $betrokkene_id
        );
    }
}

my $rechtsvormen = [];
{
    my $kvkrechtsvormen_enabled = ZAAKSYSTEEM_CONSTANTS
                                    ->{kvk_rechtsvormen_enabled};

    for my $code (@{ $kvkrechtsvormen_enabled }) {
        if (ZAAKSYSTEEM_CONSTANTS->{kvk_rechtsvormen}->{ $code }) {
            push(@{ $rechtsvormen },
                {
                    value   => $code,
                    label   => ZAAKSYSTEEM_CONSTANTS
                        ->{kvk_rechtsvormen}
                        ->{ $code }
                }
            );
        }
    }
}

my $BETROKKENE_TEMPLATE = {
    natuurlijk_persoon  => [
        {
            label   => 'BSN',
            name    => 'np-burgerservicenummer',
            classes => ['input_medium'],
        },
        {
            label   => 'Voornamen',
            name    => 'np-voornamen',
            classes => ['input_medium'],
        },
        {
            label   => 'Tussenvoegsel',
            name    => 'np-voorvoegsel',
            classes => ['input_mini'],
        },
        {
            label   => 'Achternaam',
            name    => 'np-geslachtsnaam',
            classes => ['input_medium'],
        },
        {
            label       => 'Geslacht',
            name        => 'np-geslachtsaanduiding',
            type        => 'radio',
            options     => [
                {
                    label   => 'Man',
                    name    => 'np-geslachtsaanduiding',
                    value   => 'M',
                },
                {
                    label   => 'Vrouw',
                    name    => 'np-geslachtsaanduiding',
                    value   => 'V',
                }
            ],
        },
        {
            label   => 'Straat',
            name    => 'np-straatnaam',
            classes => ['input_medium'],
        },
        {
            label   => 'Huisnummer',
            name    => 'np-huisnummer',
            classes => ['input_mini'],
        },
        {
            label   => 'Huisnummer toevoeging',
            name    => 'np-huisnummertoevoeging',
            classes => ['input_mini'],
        },
        {
            label       => 'Postcode',
            name        => 'np-postcode',
            classes     => ['input_mini'],
            post_label  => '1234AZ',
        },
        {
            label   => 'Woonplaats',
            name    => 'np-woonplaats',
            classes => ['input_medium'],
        },
    ],
    bedrijf     => [
        {
            label   => 'Rechtsvorm',
            name    => 'rechtsvorm',
            type    => 'select',
            options => $rechtsvormen,
        },
        {
            label   => 'KVK-nummer',
            name    => 'dossiernummer',
            classes => ['input_medium'],
        },
        {
            label   => 'Handelsnaam',
            name    => 'handelsnaam',
            classes => ['input_medium'],
        },
        {
            label   => 'Vestiging straat',
            name    => 'vestiging_straatnaam',
            classes => ['input_medium'],
        },
        {
            label   => 'Vestiging huisnummer',
            name    => 'vestiging_huisnummer',
            classes => ['input_mini'],
        },
        {
            label   => 'Vestiging toevoeging',
            name    => 'vestiging_huisnummertoevoeging',
            classes => ['input_mini'],
        },
        {
            label   => 'Vestiging postcode',
            name    => 'vestiging_postcode',
            classes => ['input_mini'],
        },
        {
            label   => 'Vestiging woonplaats',
            name    => 'vestiging_woonplaats',
            classes => ['input_medium'],
        },
    ],
};

sub bewerken : Chained('betrokkene') : PathPart('bewerken'): Args(0) {
    my ($self, $c) = @_;

    $c->stash->{betrokkene_type}    = (
        $c->req->params->{betrokkene_type}
        || 'natuurlijk_persoon'
    );

    $c->stash->{betrokkene_type}    = 'natuurlijk_persoon'
        unless defined($BETROKKENE_TEMPLATE->{
                $c->stash->{betrokkene_type}
        });

    $c->stash->{betrokkene_template} = $BETROKKENE_TEMPLATE->{
        $c->stash->{betrokkene_type}
    };

    my $profile;
    if ($c->stash->{betrokkene_type} eq 'bedrijf') {
        $profile                    = $c->forward(
            '/betrokkene/bedrijf/bedrijven_profile'
        );
    } else {
        $profile                    = Zaaksysteem->get_profile(
            method  => __PACKAGE__ . '::create'
        );
    }

    if (
        my $dv = $c->forward('/page/dialog', [{
            validatie           => $profile,
            user_permissions    => [qw/contact_nieuw/],
            template            => 'widgets/betrokkene/bewerken.tt',
        }])
    ) {
        my $params  = $dv->valid;

        if (
            my $id = $c->forward(
                '_create_betrokkene',
                [
                    $c->stash->{betrokkene_type},
                    $params
                ],
            )
        ) {
            my $betrokkene = $c->model('Betrokkene')->get(
                {},
                'betrokkene-' . $c->stash->{betrokkene_type} . '-' . $id
            );


            # Add logging            
            my $logging_description = join ", ", map { $_ . ': ' . $params->{$_} } sort keys %$params;

            $c->model('DB::Logging')->trigger('subject/create', {
                component => LOGGING_COMPONENT_BETROKKENE,
                component_id => $id,
                data => {
                    subject_id => $betrokkene->betrokkene_identifier,
                    parameters => $params
                }
            });

            # TODO what the actual fuck guys...
            $c->stash->{json} = {
                'succes'    => 1,
                'bericht'   => 'Betrokkene aangemaakt: '
                    .'<a href="' . $c->uri_for(
                        '/betrokkene/' . $id,
                        {
                            gm      => 1,
                            type    => $c->stash->{betrokkene_type}
                        }
                    ) . '">' .  $betrokkene->display_name . '</a>'
                    . ' (<a href="' . $c->uri_for(
                        '/zaak/create',
                        {
                            aanvraag_trigger    => 'extern',
                            betrokkene_naam     => $betrokkene->display_name,
                            betrokkene_id       =>
                                $betrokkene->betrokkene_identifier,
                            betrokkene_type     => $betrokkene->btype,
                        },
                    ) . '" class="ezra_nieuwe_zaak_tooltip-show">'
                    . 'Zaak aanmaken</a>)'
            }
        } else {
            $c->stash->{json} = {
                'succes'    => 0,
                'bericht'   => 'Fout bij aanmaken betrokkene',
            }
        }

        $c->forward('Zaaksysteem::View::JSONlegacy');
    }
}

sub _create_betrokkene : Private {
    my ($self, $c, $betrokkene_type, $opts)  = @_;

    $opts->{'np-postcode'} = uc($opts->{'np-postcode'})
        if defined($opts->{'np-postcode'});

    my $id = $c->model('Betrokkene')->create(
        $betrokkene_type,
        {
            %{ $opts },
            authenticatedby =>
                ZAAKSYSTEEM_GM_AUTHENTICATEDBY_BEHANDELAAR,
        }
    );

    return $id;
}



1;

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut

