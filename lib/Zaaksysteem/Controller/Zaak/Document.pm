package Zaaksysteem::Controller::Zaak::Document;

use Moose;

use Zaaksysteem::Exception;
use File::Temp qw[tempfile];

use Data::Dumper;

BEGIN { extends 'Zaaksysteem::Controller'; }

sub remove_file : Chained('/zaak/base') : PathPart('remove_file') : Args(2) {
    my ($self, $c, $attribute_id, $file_id) = @_;

    my $attribute = $c->model('DB::ZaaktypeKenmerken')->search({
        bibliotheek_kenmerken_id => $attribute_id,
        zaaktype_node_id => $c->stash->{ zaak }->get_column('zaaktype_node_id')
    })->first;

    unless($attribute) {
        $c->stash->{ json } = { success => 0, message => 'Could not find attribute' };
        $c->detach('Zaaksysteem::View::JSON');
    }

    my $files = $c->stash->{ zaak }->files->search(
        {'case_documents.case_document_id' => $attribute->id},
        {join => {case_documents => 'file_id'}}
    );

    if($file_id eq 'all') {
        $files->delete_all;
    } else {
        $files->search({ id => $file_id })->delete_all;
    }

    $c->stash->{ json } = {
        success => 1,
        message => 'Removed case document(s)'
    };

    $c->detach('Zaaksysteem::View::JSON');
}

sub document_base : Chained('/zaak/base') : PathPart('document') : CaptureArgs(1) {
    my ($self, $c, $file_id) = @_;

    unless($file_id =~ m[\d+]) {
        throw('request/invalid_parameter', 'file_id parameter must be numeric');
    }

    my $file = $c->stash->{ zaak }->files->find($file_id);

    unless($file) {
        throw('request/invalid_parameter', 'file_id parameter did not resolve to a file in the database');
    }

    unless($file->filestore->filestat) {
        throw('file/not_available', 'file_id parameter resolved to a file that does not physically exist on this server');
    }

    if($file->destroyed) {
        throw('file/not_available', 'file_id parameter resolved to a file that has been purged');
    }

    $c->stash->{ file } = $file;
}

sub download : Chained('document_base') : PathPart('download') {
    my ($self, $c, $format) = @_;

    my $file = $c->stash->{ file };

    my ($path, $mime, $size, $name) = $file->get_download_info($format);

    $c->serve_static_file($path);
    $c->res->headers->content_length($size);
    $c->res->headers->content_type($mime);
    $c->res->header('Content-Disposition' => sprintf('attachment; filename="%s"', $name));
}

1;
