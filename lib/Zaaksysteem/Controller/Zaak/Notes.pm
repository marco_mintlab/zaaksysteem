package Zaaksysteem::Controller::Zaak::Notes;

use Moose;

use Zaaksysteem::Exception;

BEGIN { extends 'Zaaksysteem::Controller'; }

use Zaaksysteem::Constants qw/LOGGING_COMPONENT_NOTITIE/;

sub index : JSON : Chained('/zaak/base') : PathPart('notes'): Args(0) {
    my ( $self, $c ) = @_;

    ### TODO, depending on reuqest, show notes wrapper
    $c->stash->{nowrapper} = 1;
    $c->stash->{template} = 'zaak/elements/notes.tt';
}

=head1 create_note

Create a note on a case object.

=head2 URL construction

C</zaak/B<[case_id]>/notes/create>

=head2 Parameters

=over 4

=item content (string, I<required>)

The text content of the note to be created

=back

=head2 Responses

=over 4

=item HTTP 201

Note was succesfully created. JSON body may be ignored.

=item HTTP 405

Incorrect request method, this URL requires HTTP POST.

=item HTTP 400

No content specified

=back

=head2 Events

=over 4

=item C<case/note/create>

=back

=cut

sub create_note : Chained('/zaak/base') : PathPart('notes/create') : Args(0) {
    my ($self, $c) = @_;

    unless($c->req->method eq 'POST') {
        throw('request/method', "Invalid request method, should be POST.");
    }

    unless($c->req->param('content')) {
        throw('request/parameter/missing', "Missing the `content` request parameter.");
    }

    $c->stash->{ json } = $c->stash->{ zaak }->logging->trigger('case/note/create', {
        component => 'zaak',
        data => {
            case_id => $c->stash->{ zaak }->id,
            content => $c->req->param('content')
        }
    });

    $c->detach('Zaaksysteem::View::JSON');
}

1;

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut

