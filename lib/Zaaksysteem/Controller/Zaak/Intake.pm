package Zaaksysteem::Controller::Zaak::Intake;

use strict;
use warnings;

use Exception::Class (
    'Zaaksysteem::Controller::Zaak::Intake::Exception' => {fields => 'code'},
    'Zaaksysteem::Controller::Zaak::Intake::Exception::General' => {
        isa         => 'Zaaksysteem::Controller::File::Exception',
        description => 'Zaak intake controller exception',
        alias       => 'throw_controller_exception',
    },
);
use File::Temp qw(tempfile);
use Moose;

BEGIN { extends 'Zaaksysteem::Controller'; }

sub download : Regex('^zaak/intake/(\d+)/download\/?(.*)?') {
    my ($self, $c) = @_;

    my ($file_id, $format) = @{$c->req->captures};
    my ($file) = $c->model('DB::File')->search({ id => $file_id });

    if (!$file) {
        throw_controller_exception(
            code  => '/zaak/intake/download/file_not_found',
            error => "File with ID $file_id not found",
        );        
    }

    if ($file->case) {
        throw_controller_exception(
            code  => '/zaak/intake/file_has_case_defined',
            error => sprintf(
                "File with ID %d is already bound to a case and cannot be downloaded through the document intake.",
                $file->id
            ),
        );
    }

    my ($path, $mime, $size, $name) = $file->get_download_info($format);

    $c->serve_static_file($path);
    $c->res->headers->content_length($size);
    $c->res->headers->content_type($mime);
    $c->res->header('Content-Disposition' => sprintf('attachment; filename="%s"', $name));
}


1;

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut

