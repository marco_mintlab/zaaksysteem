package Zaaksysteem::Controller::Filestore;

use Moose;

BEGIN { extends 'Zaaksysteem::General::ZAPIController'; }

=head1 NAME

Zaaksysteem::Controller::Filestore - Zaaksysteem Filestore API

=head1 SYNOPSIS

 # /filestore/upload

=head1 DESCRIPTION

Interacts with our filestore

=head1 API

=head2 /filestore/upload [POST]

    {
       "next" : null,
       "status_code" : "200",
       "prev" : null,
       "num_rows" : 1,
       "rows" : 1,
       "comment" : null,
       "at" : null,
       "result" : [
          {
             "date_created" : null,
             "original_name" : "useravatar.png",
             "uuid" : "327a0482-aea6-4f58-b7b2-91427535c0fa",
             "size" : 2996,
             "id" : 5,
             "thumbnail_uuid" : null,
             "mimetype" : "image/png",
             "md5" : "d43338446c75425cbcb9fc2ef2bd763c"
          }
       ]
    }

Uploads a file in our filestore, and returns the json data for the result.

Make sure the file gets uploaded in a C<POST> request, and the input name for
this file is embedded in the C<POST> variable C<file>

=cut 

sub upload
    : Chained('/')
    : PathPart('filestore/upload')
    : Args(0)
    : ZAPI
{
    my ($self, $c)  = @_;

    throw('httpmethod/post', 'Invalid HTTP Method, no POST', []) unless
        lc($c->req->method) eq 'post';

    my $upload          = $c->req->upload('file');

    $c->stash->{zapi}   = $c->model('DB')
                        ->resultset('Filestore')
                        ->filestore_create(
                            {
                                original_name       => $upload->filename,
                                file_path           => $upload->tempname,
                            }
                        );
}

# sub uploadtest : Local {
#     my ($self, $c) = @_;

#     $c->stash->{current_view} = 'TT';
#     $c->stash->{template} = 'uploadtest.tt';
# }
#
# Insert this html in uploadtest.tt
#
# <form method='POST' enctype='multipart/form-data' action="/filestore/upload">
# File to upload: <input type=file name=file><br>
# Notes about the file: <input type=text name=note><br>
# <br>
# <input type=submit value=Press> to upload the file!
# </form>

=head1 EXAMPLES

See L<SYNOPSIS>

=head1 SEE ALSO

L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::API>

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Rudolf Leermakers

Marco Baan

Dario Gieselaar

Nick Diel

Laura van der Kaaij

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut

1;