package Zaaksysteem::Controller::Auth::SAML;

use Moose;

use Zaaksysteem::Exception;
use Zaaksysteem::Profile;

use Zaaksysteem::SAML2;
use Zaaksysteem::SAML2::SP;

use Try::Tiny;

BEGIN { extends 'Zaaksysteem::Controller'; }

sub base : Chained('/') : PathPart('auth/saml') : CaptureArgs(0) {
    my ($self, $c) = @_;

    my $koppeling = 'retrieve_koppeling'; # TODO

#    $c->stash->{ saml_sp } = Net::SAML2::SP->new(
#        id => $koppeling->sp_identity,
#        url => $koppeling->sp_url,
#        signing_certificate => $koppeling->signing_cert_file->path,
#        ca_certificate => $koppeling->ca_certificate->path
#    );

    $c->stash->{ saml } = Zaaksysteem::SAML2->new_from_relaystate(
        {
            uri                 => $c->uri_for('/'),
            RelayState          => $c->req->params->{RelayState},
        },
        $c->model('DB')
    );
}

=head2 metadata

This controller action returns an XML document representing the configuration
of this Zaaksysteem instance as SP in the SAML authentication process.

=cut

sub metadata : Chained('/') : PathPart('auth/saml/metadata') : Args(0) {
    my ($self, $c) = @_;

    # Here starts a kludge to make metadata work without an IdP definition.

    my $spi = $c->model('DB::Interface')->search({ module => 'samlsp' })->first;

    unless($spi) {
        throw('saml2/metadata/no_sp_defined', 'Unable to find a SP definition, cannot build SP metadata');
    }

    my $cert = $c->model('DB::Filestore')->find($spi->get_interface_config->{ sp_cert }->[0]->{ id });

    my $sp = Zaaksysteem::SAML2::SP->new(
        id => $spi->get_interface_config->{ sp_webservice },
        url => $c->uri_for('/') . 'auth',
        cert => $cert->get_path,
        cacert => '',
        org_contact => $spi->get_interface_config->{ sp_contact_email },
        org_display_name => $spi->get_interface_config->{ sp_contact_name },
        org_name => $spi->get_interface_config->{ sp_application_name }
    );

    $c->response->content_type('application/xml');
    $c->response->body($sp->metadata);
}

=head2 single_logout (base)

This action is here merely as a placeholder for a generic 'single logout'
action. The actual implementation depends on the selected L<Net::SAML2::Binding>

=cut

sub single_logout : Chained('base') : PathPart('') : CaptureArgs(0) {
    my ($self, $c) = @_;

}

sub single_logout_soap : Chained('single_logout') : PathPart('slo-soap') : Args(0) {
    my ($self, $c) = @_;

}

sub single_logout_redirect : Chained('single_logout') : PathPart('sls-redirect-response') : Args(0) {
    my ($self, $c) = @_;

}

=head2 consumer (base)

This action is here merely as a placeholder for a generic 'consumer' action. The
actualy implementation depends on the selected L<Net::SAML2::Binding>.

=cut

sub consumer : Chained('base') : PathPart('') : CaptureArgs(0) {
    my ($self, $c) = @_;

}

=head2 consumer (post)

This controller action catches the UA coming back from the IdP with a
SAMLResponse encoded as HTTP Post data. We start the process of validating
the response, and if it does, the user is essentially authenticated.

=cut

define_profile consumer_post => (
    required => [], # Dude... rancid
    require_some => {
        saml_reply => [qw[SAMLResponse SAMLart]]
    }
);

sub consumer_post : Chained('consumer') : PathPart('consumer-post') : Args(0) {
    my ($self, $c) = @_;

    my $params          = assert_profile($c->req->params)->valid;

    my $saml_response   = $params->{ SAMLResponse };

    if($params->{ SAMLart }) {
        $saml_response  = $c->stash->{ saml }->resolve_artifact($params->{ SAMLart });
    }

    my $assertion;

    try {
        $assertion       = $c->stash->{ saml }->handle_response($saml_response);
    } catch {
        $c->session->{ _saml_error } = $_->object->{ status };
        $c->res->redirect($c->stash->{ saml }->state->{ fail });
        $c->detach;
    };

    my %namespec = split m[:], $assertion->nameid; 
    
    $c->session->{ _saml } = {
        success => 1,
        used_profile => 'digid',
        uid => $namespec{ s00000000 } # Sectorcode for BSN
    };

    $c->session_expire_key('_saml' => 900);

    $c->res->redirect($c->stash->{ saml }->state->{ redirect });
    $c->detach;

    # This flow step models the initial SAML2 plugin in that $c->authenticate
    # gets dispatched to C::Auth::Cred::SAML, which extracts the useful info
    # from the request and does the actual validation. Perhaps move some of that
    # stuff here.
    $c->log->debug('Try to authenticate user, method: saml');
    unless (
        $c->authenticate(
            {
                'credential_module'                 => 'Saml',
                'authentication_source_identifier'  => $assertion,
            }
        )
    ) {
        $c->log->debug('Failed to authenticate user, method: saml');
        $c->stash->{message}    = $c->loc('Failed to login.');
        $c->stash->{template}   = 'user/login.tt';
        $c->detach;
    }

    $c->log->debug('Authenticated user, method: saml');

    ### Logged in, redirect
    $c->res->redirect($c->req->params->{RelayState});
    $c->detach;
}

=head2 consumer (artifact)

This controller action catches the UA coming back from the IdP with a reference
to a SAML artifact. This basically means we need to poke the configured IdP
with an L<Net::SAML2::Protocol::ArtifactResolve> request to the the data we
actually want (the SAMLResponse).

=cut

define_profile consumer_artifact => (
    required => [qw[SAMLart RelayState]]
);

sub consumer_artifact : Chained('consumer') : PathPart('consumer-artifact') : Args(0) {
    my ($self, $c) = @_;

}


1;
