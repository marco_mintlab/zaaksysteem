package Zaaksysteem::Controller::Plugins::Maps;

use strict;
use warnings;
use parent 'Catalyst::Controller';

use Geo::Coder::Google;
#use Geo::Proj4;
use Data::Dumper;
use JSON;

use constant PROXY_MAP  => {
    geodata => 'http://geodata.nationaalgeoregister.nl',
};

sub coordinates : Local {
    my ($self, $c) = @_;
}

=head2 proxy(type)

Poor mans proxy, use the following url to use it with nationaalregister.nl
geodata.

  http://catalyst.url/plugins/maps/proxy/geodata/tms/1.0.0/brtachtergrondkaart/3/3/3.png8

B<options>

=over 4

=item type

Which site type to use, see constant PROXY_MAP

=back

=cut

sub proxy : Local {
    my ($self, $c, $type)   = @_;

    $c->detach('/forbidden') unless PROXY_MAP->{$type};

    my $currentaction       = $c->uri_for($c->action) . '/' . $type;

    my $path                = $c->req->uri;
    $path                   =~ s/^$currentaction//g;

    $c->res->redirect(
        PROXY_MAP->{$type} . $path
    );
}

sub read : Chained('/'): PathPart('plugins/maps'): Args(0) {
    my ($self, $c, $term) = @_;
    my $local;

    if ($term) {
        $local++;
    }

    $term               ||= $c->req->params->{term};

    $c->detach('View::JSONlegacy') unless ($term);

    my $json_data       = { success => 0, addresses => [] };

    my $geocoder        = $c->model('Geo');

    eval {
        $geocoder->query($term);
        $geocoder->geocode;

        if ($geocoder->success) {
            $json_data->{addresses} = $geocoder->TO_JSON;
        }
    };

    if (scalar(@{ $json_data->{addresses} })) {
        $json_data->{success} = 1;
    }

    unless ($local) {
        $c->stash->{json}   = $json_data;
        $c->detach('View::JSONlegacy');
    }

    return $json_data;
}

sub _get_locations_from_case_query : Private {
    my ($self, $c, $search_query) = @_;

    my $results = $search_query->results(
        {
            c           => $c,
            get_total   => 'GET_TOTAL'
        }
    );

    $results    = $results->search(
        {
            'locatie_zaak'                          => { '!='   => undef },
            '-or'                                   => [
                'locatie_zaak.bag_nummeraanduiding_id'  => { '!='   => undef },
                'locatie_zaak.bag_openbareruimte_id'    => { '!='   => undef },
            ],
        },
        {
            join    => 'locatie_zaak'
        }
    );

    my @entries;
    while (my $case = $results->next) {
        my $bag = $case->locatie_zaak;

        unless ($bag->bag_coordinates_wsg) {
            $c->forward('_set_coordinates_on_bag', [$bag]);
        }

        next unless $bag->bag_coordinates_wsg;

        my $case_info       = {
            'id'            => $case->id,
            'description'   => $case->onderwerp,
            'requestor'     => (
                $case->aanvrager_object
                    ? $case->aanvrager_object->naam
                    : ''
            ),
            'manager'       => (
                $case->behandelaar_object
                    ? $case->behandelaar_object->naam
                    : ''
            ),
            'zaaktype'       => (
                $case->zaaktype_node_id->titel
            ),
            address         => $bag->maps_adres,
            url             => $c->uri_for('/zaak/' . $case->id)->as_string
        };

        $case_info->{id}    = $case->id;

        my $coords          = $bag->bag_coordinates_wsg;
        $coords             =~ s/[\(\)]//g;
        my @raw_coords      = split(',', $coords);
        $case_info->{coordinates}   = {
            lat => $raw_coords[0],
            lng => $raw_coords[1]
        };

        push(@entries, to_json($case_info));
    }

    return \@entries;
}

sub _set_coordinates_on_bag : Private {
    my ($self, $c, $bag) = @_;

    my $address = $bag->maps_adres;

    return unless $address;

    my $json_addresses = $c->forward('read', [ $address ]);

    return unless ($json_addresses && $json_addresses->{success});

    my $json_address = shift(@{ $json_addresses->{addresses} });

    my $coords       = $json_address->{coordinates};

    $bag->bag_coordinates_wsg($coords->{lat} . ',' . $coords->{lng});
    $bag->update;
}


1;

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut

