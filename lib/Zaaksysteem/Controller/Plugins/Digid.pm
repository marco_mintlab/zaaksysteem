package Zaaksysteem::Controller::Plugins::Digid;

use strict;
use warnings;
use Data::Dumper;

use Moose;
use Moose::Util qw/apply_all_roles does_role/;
use JSON;
use MIME::Base64;

BEGIN { extends 'Catalyst::Controller'; }

use Zaaksysteem::Constants qw/
    ZAAKSYSTEEM_GM_AUTHENTICATEDBY_DIGID
    ZAAKSYSTEEM_GM_AUTHENTICATEDBY_BEDRIJFID

    VALIDATION_CONTACT_DATA
/;

#use Moose::Util qw/apply_all_roles/;

#around 'register_actions'  => sub {
#    my $orig    = shift;
#    my $class   = shift;
#
#    apply_all_roles( Zaaksysteem->controller('Zaak'), 'Zaaksysteem::Auth::Digid::WebformAuth');
#
#    warn('Applied ROLES: ' . does_role(Zaaksysteem->controller('Zaak'),
#            'Zaaksysteem::Auth::Digid::WebformAuth'));
#
#    my $rv      = $class->$orig(@_);
#};

#sub register_actions {
#    my $self    = shift;
#
#    $self->next::method(@_);
#
#    die(Dumper(shift->controller('Zaak')));
#    return $self;
#
#
#};

#apply_all_roles(Zaaksysteem->controller('Zaak'), 'Zaaksysteem::Auth::Digid::WebformAuth');



sub login : Chained('/') : PathPart('auth/digid'): Args() {
    my ($self, $c, $do_auth) = @_;

    if ($c->user_exists) {
        $c->delete_session;
    }

    ### In case of an XHR, and we get here...define we are not logged in
    if ($c->req->is_xhr) {
        $c->res->status('401');
    }

    $c->stash->{ verified_url } = $c->req->param('verified_url');

    $c->stash->{ idps } = [ $c->model('DB::Interface')->search({ module => 'samlidp' }) ];

    if($c->session->{ _saml_error }) {
        my %dispatch = (
            'urn:oasis:names:tc:SAML:2.0:status:AuthnFailed' => 'cancelled',
            'urn:oasis:names:tc:SAML:2.0:status:NoAuthnContext' => 'context_insufficient',
            'urn:oasis:names:tc:SAML:2.0:status:PartialLogout' => 'partial_logout',
            'urn:oasis:names:tc:SAML:2.0:status:RequestDenied' => 'denied'
        );

        $c->stash->{ saml_error } = $dispatch{ $c->session->{ _saml_error } } || 'unknown';

        warn $c->stash->{ saml_error };
    }

    $c->stash->{template} = 'plugins/digid/login.tt';
}

sub saml : Chained('/') : PathPart('auth/saml') : Args(1) {
    my ($self, $c, $idp_interface_id) = @_;

    my $saml = Zaaksysteem::SAML2->new_from_interfaces({
        uri => $c->uri_for('/'),
        idp => $c->model('DB::Interface')->find($idp_interface_id)
    });

    my $redirect = $saml->authentication_redirect({
        state => encode_base64(encode_json({
            redirect => $c->req->param('verified_url'),
            fail => $c->req->referer,
            idp => $idp_interface_id
        }))
    });

    $c->res->redirect($redirect);
    $c->detach;
}


sub logout : Chained('/') : PathPart('auth/digid/logout'): Args() {
    my ($self, $c) = @_;

    delete $c->session->{ _saml };
    $c->stash->{template}   = 'plugins/digid/login.tt';
    $c->stash->{logged_out} = 1;
    $c->detach;
}


sub _zaak_create_secure_digid : Private {
    my ($self, $c) = @_;

    my $saml_state = $c->session->{ _saml } || {};

    if (
        $c->req->params->{authenticatie_methode} eq 'digid' ||
        $c->session->{_zaak_create}->{extern}->{verified} eq 'digid'
    ) {
        if($saml_state->{ success }) {
            $c->session->{_zaak_create}->{extern} = {};

            ### Check if we are allowed to crate this zaaktype
            $c->session->{_zaak_create}->{extern}->{aanvrager_type}
                = 'natuurlijk_persoon';
            $c->session->{_zaak_create}->{extern}->{verified}
                = 'digid';
            $c->session->{_zaak_create}->{extern}->{id}
                = $saml_state->{ uid };

            $c->stash->{aanvrager_type} = 'natuurlijk_persoon'
        } else {
            my $arguments = {};
            $arguments->{'authenticatie_methode'} = $c->req->params->{authenticatie_methode} if ($c->req->params->{authenticatie_methode});
            $arguments->{'ztc_aanvrager_type'} = $c->req->params->{ztc_aanvrager_type} if ($c->req->params->{ztc_aanvrager_type});
            $arguments->{'sessreset'} = $c->req->params->{sessreset} if ($c->req->params->{sessreset});
            $arguments->{'zaaktype_id'} = $c->req->params->{zaaktype_id} if ($c->req->params->{zaaktype_id});

            $c->res->redirect(
                $c->uri_for(
                    '/auth/digid',
                    {
                        verified_url    => $c->uri_for(
                            '/zaak/create/webformulier/',
                            $arguments,
                        )
                    }
                )
            );

            ### Wipe out externe authenticatie
            if (
                $c->session->{_zaak_create}->{extern} &&
                $c->session->{_zaak_create}->{verified} eq 'digid'
            ) {
                delete($c->session->{_zaak_create}->{extern});
            }

            $c->detach;
        }
    } else {

        ### Geen digiid, stop here
        return;
    }

    ### Save aanvrager data
    $c->forward('_zaak_create_aanvrager');
}

sub _zaak_create_aanvrager : Private {
    my ($self, $c) = @_;

    return unless (
        $c->req->params->{aanvrager_update}
    );

    $c->log->debug('_zaak_create_aanvrager: Aanvrager update');

    my $callerclass     = 'Zaaksysteem::Betrokkene::Object::NatuurlijkPersoon';


    ### Only validate contact, which are all optional
    my $profile;
    if ($c->req->params->{contact_edit}) {
        $profile = VALIDATION_CONTACT_DATA;
    } else {
        ### Get profile from Model
        $profile         = $c->get_profile(
            'method'    => 'create',
            'caller'    => 'Zaaksysteem::Controller::Betrokkene'
        ) or die('Terrible die here');

        ### MERGE
        my $contact_profile = VALIDATION_CONTACT_DATA;
        while (my ($key, $data) = each %{ $contact_profile }) {
            unless ($profile->{$key}) {
                $profile->{$key} = $data;
                next;
            }

            if (UNIVERSAL::isa($data, 'ARRAY')) {
                push(@{ $profile->{$key} }, @{ $data });
                next;
            }

            if (UNIVERSAL::isa($data, 'HASH')) {
                while (my ($datakey, $dataval) = each %{ $data }) {
                    $profile->{$key}->{$datakey} = $dataval;
                }
                next;
            }
        }
    }

    Zaaksysteem->register_profile(
        method => '_zaak_create_aanvrager',
        profile => $profile,
    );

    if ($c->req->is_xhr) {
        $c->zvalidate;
        $c->detach;
    }

    my $dv      = $c->zvalidate;
    return unless ref($dv);

    return unless $dv->success;

    ### Post
    $c->log->debug('_zaak_create_aanvrager: Updated aanvrager');
    if ($c->req->params->{aanvrager_edit}) {
        $c->session->{_zaak_create}->{aanvrager_update} = $dv->valid;
    } elsif ($c->req->params->{contact_edit}) {
        for (qw/npc-email npc-telefoonnummer npc-mobiel/) {
            if (defined($c->req->params->{ $_ })) {
                $c->session->{_zaak_create}->{ $_ } =
                    $c->req->params->{ $_ };
            }
        }

    }
}



sub _zaak_create_load_externe_data : Private {
    my ($self, $c) = @_;

    return unless $c->session->{_zaak_create}->{extern}->{verified} eq 'digid' &&
        $c->session->{_zaak_create}->{aanvrager_update};

    if($c->req->params->{aanvrager_update}) {

        my $id = $c->model('Betrokkene')->create(
            'natuurlijk_persoon',
            {
                %{ $c->session->{_zaak_create}->{aanvrager_update} },
                'np-authenticated'   => 0,
                'np-authenticatedby' => ZAAKSYSTEEM_GM_AUTHENTICATEDBY_DIGID,
            }
        );
    
        $c->session->{_zaak_create}->{ztc_aanvrager_id}
            = 'betrokkene-natuurlijk_persoon-' .  $id;
    }
}


1;

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut

