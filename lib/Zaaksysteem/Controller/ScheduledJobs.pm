package Zaaksysteem::Controller::ScheduledJobs;

use Moose;

use strict;
use warnings;
use Data::Dumper;
use Zaaksysteem::Exception;

use Zaaksysteem::Backend::Email;

BEGIN {extends 'Catalyst::Controller'; }


=head1

This Controller is the glue between cron trigger and application. Cron
trigger calls check_schedule(), giving it $c context.

=cut

# sub check_schedule : Chained('/') : PathPart('check_schedule') {
#     my ($self, $c) = @_;

#     $c->forward("run_scheduled_jobs");

#     my $scheduled_jobs = $c->model('DB::ScheduledJobs');
#     $c->stash->{pending} = $scheduled_jobs->pending;
#     $c->stash->{template} = 'scheduledjobs.tt';
# }



# sub run_scheduled_jobs : Private {
#     my ($self, $c) = @_;

#     # assert that the request is coming from the local server -- the purpose is cron 
#     my $server_ips = { 
#         map { m|inet addr:([0-9\.]+)| => 1 } 
#         split /\n/, `/sbin/ifconfig | grep inet | grep -v inet6 | grep -v 127.0.0.1` 
#     };

#     return unless 
#         $c->check_any_user_permission('admin') || 
#         exists $server_ips->{$c->req->address};


#     my $scheduled_jobs = $c->model('DB::ScheduledJobs');
#     my $ready = $scheduled_jobs->ready;

#     my $jobs_done = '';
#     while(my $scheduled_job = $ready->next()) {

#         my $task        = $scheduled_job->task();
#         my $parameters  = $scheduled_job->parameters();

#         $c->log->debug("handling scheduled job: " . $task . ", parameters: " . Dumper $parameters);

#         my $dispatch_table = {
#             'case/mail' => sub {
#                 $jobs_done .= "<pre>Sent mail: ". Dumper ($parameters) ."</pre>";
#                 $c->forward("send_case_mail", [$task, $parameters]);
#             },
#         };

#         unless(exists $dispatch_table->{$task}) {
#             die "unhandled task: $task encountered, aborting";
#         }

#         $dispatch_table->{$task}->();

#         $scheduled_job->deleted(DateTime->now());
#         $scheduled_job->update();
#     }

#     $c->stash->{jobs_done} = $jobs_done;
# }


# sub send_case_mail : Private {
#     my ($self, $c, $task, $parameters) = @_;

#     my $schema          = $c->model('DB');
#     my $default_from    = $c->config->{gemeente}->{zaak_email};
#     my $betrokkene_id   = (
#         $c->user_exists
#             ? 'betrokkene-medewerker-' . $c->user->uidnumber
#             : $c->stash->{zaak}->aanvrager_object->rt_setup_identifier
#     );

#     my $bibliotheek_notificaties_id = $parameters->{bibliotheek_notificaties_id}
#         or die "need bibliotheek_notificatie_id";

#     my $notificatie = $schema->resultset('BibliotheekNotificaties')->find(
#         $bibliotheek_notificaties_id
#     ) or die "notificatie $bibliotheek_notificaties_id not present in database";

#     my $zaak_id = $parameters->{zaak_id} 
#         or die "need zaak_id"; 

#     my $case = $schema->resultset('Zaak')->find($zaak_id)
#         or die "zaak $zaak_id not present in database";

#     my $prepared_notification = $case->prepare_notification({
#         recipient_type  => $parameters->{recipient_type},
#         behandelaar     => $parameters->{behandelaar}, 
#         email           => $parameters->{email},
#         body            => $notificatie->message,
#         subject         => $notificatie->subject,
#     });

#     my $mailer  = Zaaksysteem::Backend::Email->new(
#         default_from_email      => $default_from,
#         schema                  => $schema,
#     );

#     $mailer->send_from_case(
#         {
#             case                => $case,
#             notification        => $prepared_notification,
#             betrokkene_id       => $betrokkene_id,
#         }
#     );
# }

1;

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut

