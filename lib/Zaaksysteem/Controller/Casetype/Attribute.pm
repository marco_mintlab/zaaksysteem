package Zaaksysteem::Controller::Casetype::Attribute;

use Moose;
use Zaaksysteem::Exception;
use namespace::autoclean;

use Data::Dumper;

BEGIN {extends 'Zaaksysteem::General::ZAPIController'; }

=head1 NAME

Zaaksysteem::Controller::Casetype::Attribute - ZAPI Controller

=head1 SYNOPSIS

 # /casetype/attribute

=head1 DESCRIPTION

Zaaksysteem API Controller for attributes related to Casetypes.

=head1 METHODS

=head2 /casetype/ID/attribute [GET READ]

Returns a resultset of attributes related to a casetype

B<Query Parameters>

=over 4

=item query

Type: STRING

 # /casetype/1/attribute?query=test
 
Filters the results with the given query string

=back

=cut

sub index
    : Chained('/casetype/base')
    : PathPart('attribute')
    : Args(0)
    : ZAPI
{
    my ($self, $c) = @_;

    throw('httpmethod/post', 'Invalid HTTP Method, no GET', []) unless
        lc($c->req->method) eq 'get';

    my $rs              = $c->model('DB::BibliotheekKenmerken')
                        ->search_freeform(
                            $c->req->params->{query},
                        );

    ### Scope to zaaktype_id
    $rs                 = $rs->search(
        {
            'zaaktype_kenmerkens.zaaktype_node_id' => $c->stash
                                                        ->{zaaktype}
                                                        ->zaaktype_node_id
                                                        ->id,
        },
        {
            'join'  => 'zaaktype_kenmerkens',
        }
    );

    $c->stash->{zapi} = $rs;
}



=head1 EXAMPLES

See L<SYNOPSIS>

=head1 SEE ALSO

L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::API>

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Rudolf Leermakers

Marco Baan

Dario Gieselaar

Nick Diel

Laura van der Kaaij

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut

1;