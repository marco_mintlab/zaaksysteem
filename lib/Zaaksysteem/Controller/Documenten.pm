package Zaaksysteem::Controller::Documenten;

use strict;
use warnings;
use parent 'Catalyst::Controller';

=head1 NAME

Zaaksysteem::Controller::Documenten::Notitie - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut

sub base : Chained('/') : PathPart('documenten') : CaptureArgs(1) {
    my ($self, $c, $id) = @_;

    die unless ($id =~ /^\d+$/);

    $c->stash->{documenten_id} = $id;

    if ($id > 0) {
        $c->stash->{document}  = $c->model('DB::Documenten')->find(
            $c->stash->{documenten_id},
            {
                prefetch    => [qw/notitie_id filestore_id/]
            }
        );
    }

    ### PIP SECURITY
    $c->forward('_pip_security');
}

sub _pip_security : Private {
    my ($self, $c) = @_;

    return 1 if $c->user_exists;
}

sub get : Chained('base') : PathPart('get') : Args(0) {
    my $self    = shift;
    my $c       = shift;

    $c->forward('dispatch_action', [ 'get', @_ ]);
}

sub accepteer : Chained('base') : PathPart('accepteer') : Args(0) {
    my $self    = shift;
    my $c       = shift;

    $c->forward('dispatch_action', [ 'accepteer', @_ ]);
}

sub weiger : Chained('base') : PathPart('weiger') : Args(0) {
    my $self    = shift;
    my $c       = shift;

    $c->forward('dispatch_action', [ 'weiger', @_ ]);
}

sub dispatch_action : Private {
    my ($self, $c, $action) = @_;

    unless (
        $c->stash->{document} &&
        $c->controller(
            'Documenten::' . ucfirst($c->stash->{document}->store_type)
        ) &&
        $c->controller(
            'Documenten::' . ucfirst($c->stash->{document}->store_type)
        )->action_for($action)
    ) {
        $c->detach('/forbidden');
    }

    return $c->forward(
        '/documenten/' .
        lc($c->stash->{document}->store_type) .
        '/' . $action
    );
}

sub verwijderen : Chained('base'): PathPart('verwijderen'): Args(0) {
    my ($self, $c, $id) = @_;

    ### Post
    if (
        lc($c->req->method) eq 'post' &&
        %{ $c->req->params } &&
        $c->req->params->{confirmed}
    ) {
        $c->res->redirect(
            $c->req->params->{referer}
        );

        $c->stash->{document}->delete_document;

        ### Msg
        $c->push_flash_message('Document succesvol verwijderd.');
    }


    $c->stash->{confirmation}->{message}    =
        'Weet u zeker dat u het document met naam: "'
        . $c->stash->{document}->naam
        . '" wilt verwijderen?';

    $c->stash->{confirmation}->{type}       = 'yesno';
    $c->stash->{confirmation}->{uri}        = 
        $c->uri_for(
            '/documenten/'
            . $c->stash->{document}->id
            .'/verwijderen'
        );

    $c->stash->{confirmation}->{params}     = {
        'referer' => $c->req->referer
    };


    $c->forward('/page/confirmation');
    $c->detach;
}

1;
