package Zaaksysteem::Controller::Directory;

use Moose;

BEGIN { extends 'Zaaksysteem::Controller'; }

=head1 NAME

Zaaksysteem::Controller::Directory - Frontend library for the Zaaksysteem
file structure.

=cut

=head2 create

Create a new directory.

=head3 Arguments

=over

=item name [required]

=item case_id [required]

=back

=head3 Returns

A JSON structure containing file properties.

=cut

sub create : JSON : Local {
    my ($self, $c) = @_;

    my $result =$c->model('DB::Directory')->directory_create({
        %{$c->req->params}
    });
    $c->{stash}->{json} = $result;
    $c->forward('Zaaksysteem::View::JSON');
}

=head2 update

Create a new directory.

=head3 Arguments

=over

=item name [required]

=item directory_id [required]

Set to 0 for no directory.

=back

=head3 Returns

A JSON structure containing the directory properties.

=cut

sub update : JSON : Local {
    my ($self, $c) = @_;

    my %params = %{$c->req->params()};

    my $dir_id = $params{directory_id};
    my ($directory) = $c->model('DB::Directory')->search({
        id => $dir_id,
    });
    if (!$directory) {
        die "Directory with ID $dir_id not found";
    }
    my $result = $directory->update_properties({
        %params,
    });

    $c->{stash}->{json} = $result;
    $c->forward('Zaaksysteem::View::JSON');
}

=head2 search

Retrieve a directory and its properties. Either a directory_id or case_id needs
to be passed. Can yield multiple results.

=head3 Arguments

=over

=item document_id [optional]

Get the directory matching the given id.

=item case_id [optional]

Get all directories related to the given case id.

=back

=head3 Returns

A JSON structure containing one or more results detailing the directory
properties.
 
=cut

sub search : JSON : Regex('^directory/search/(directory_id|case_id)/(\d+)') {
    my ($self, $c) = @_;

    my ($key, $value) = @{$c->req->captures};

    # Convert to the DB-key for the search
    if ($key eq 'directory_id') {
        $key = 'id';
    }
    
    my @result = $c->model('DB::Directory')->search({$key => $value});

    $c->{stash}->{json} = \@result;
    $c->forward('Zaaksysteem::View::JSON');
}

=head2 delete

Delete a directory.

=head3 Arguments

=over

=item directory_id [required]

=back

=head3 Returns

True upon succes.

=cut

sub delete : JSON : Local {
    my ($self, $c) = @_;

    my $result = $c->model('DB::Directory')->find($c->req->param('directory_id'));
    $c->{stash}->{json} = $result->delete();
    $c->forward('Zaaksysteem::View::JSON');
}

=head2 help

Returns a HTMLified version of the POD in this library.

=cut

sub help : Local {
    my ($self, $c) = @_;

    require Pod::Simple::HTML;
    my $p = Pod::Simple::HTML->new;
    $p->output_string(\$c->stash->{pod_output});
    $p->parse_file('lib/Zaaksysteem/Controller/Directory.pm');
    $c->stash->{template} = 'pod.tt';
}

1;
