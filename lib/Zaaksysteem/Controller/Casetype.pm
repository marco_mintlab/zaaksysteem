package Zaaksysteem::Controller::Casetype;

use Moose;
use Zaaksysteem::Exception;
use namespace::autoclean;

use Data::Dumper;

BEGIN {extends 'Zaaksysteem::General::ZAPIController'; }

=head1 NAME

Zaaksysteem::Controller::Casetype - ZAPI Controller

=head1 SYNOPSIS

 # /casetype

=head1 DESCRIPTION

Zaaksysteem API Controller for Casetypes

=head1 METHODS

=head2 /casetype [GET READ]

Returns a resultset of zaaktypen, possibly filtered by a query filter

B<Query Parameters>

=over 4

=item query

Type: STRING

 # /casetype?query=test
 
Filters the results with the given query string

=back

=cut

sub index
    : Chained('/')
    : PathPart('casetype')
    : Args(0)
    : ZAPI
{
    my ($self, $c) = @_;

    throw('httpmethod/post', 'Invalid HTTP Method, no GET', []) unless
        lc($c->req->method) eq 'get';

    $c->stash->{zapi}   = $c->model('DB::Zaaktype')->search_freeform(
        $c->req->params->{query}
    );
}

sub base
    : Chained('/')
    : PathPart('casetype')
    : CaptureArgs(1)
{
    my ($self, $c, $id)     = @_;

    $c->stash->{zaaktype}   = $c->model('DB::Zaaktype')->find($id);

    $c->error('Invalid zaaktype id given') unless $c->stash->{zaaktype};
}

=head1 EXAMPLES

See L<SYNOPSIS>

=head1 SEE ALSO

L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::API>

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Rudolf Leermakers

Marco Baan

Dario Gieselaar

Nick Diel

Laura van der Kaaij

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut

1;