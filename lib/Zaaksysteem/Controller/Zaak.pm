package Zaaksysteem::Controller::Zaak;

use strict;
use warnings;



use Clone qw(clone);
use Data::Dumper;
use File::Basename;
use File::stat;
use Moose;
use OpenOffice::OODoc;
use HTML::TagFilter;
use MIME::Lite;
use Time::localtime;
use Try::Tiny;
use Zaaksysteem::ZTT;
use Zaaksysteem::Constants;


BEGIN { extends 'Zaaksysteem::Controller'; }

use Zaaksysteem::Constants qw/
    ZAAKSYSTEEM_CONSTANTS
    ZAAKSYSTEEM_GM_AUTHENTICATEDBY_BEHANDELAAR
    LDAP_DIV_MEDEWERKER
    ZAAKSYSTEEM_AUTHORIZATION_ROLES
    ZAAKSYSTEEM_STANDAARD_KENMERKEN
    ZAAKSYSTEEM_CONTACTKANAAL_WEBFORM
    ZAAKSYSTEEM_CONTACTKANAAL_BALIE

    ZAAK_CREATE_PROFILE
    MIMETYPES_ALLOWED

    CUSTOM_WEBFORMS
/;

# an attempt at English code
use constant SAVED_CASE_PROPERTIES => '_SAVED_CASE_PROPERTIES';

sub index :Path :Args(0) {
    my ( $self, $c ) = @_;

    $c->response->body('Matched Zaaksysteem::Controller::Zaak in Zaak.');
}

sub base : Chained('/') : PathPart('zaak'): CaptureArgs(1) {
    my ($self, $c, $id) = @_;

    #$c->assert_permission('zaak_view');
    return unless $id =~ /^\d+$/;

    $c->stash->{need_openlayers} = '1';
    $c->stash->{'template'}     = 'zaak/view.tt';

    ### Retrieve zaak
    $c->stash->{'zaak'}         = $c->model('DB::Zaak')->find($id);

    if (!$c->stash->{zaak} || $c->stash->{ zaak }->status eq 'deleted') {
        $c->push_flash_message('Geen zaak gevonden met dit nummer');
        $c->response->redirect($c->uri_for('/'));
        $c->detach;
    }

    if ($c->user_exists) {
        $c->assert_any_zaak_permission('zaak_read','zaak_beheer','zaak_edit');
    }


    my $params = $c->req->params;

    ### Find fase
    my $fase;
    if (($fase = $params->{fase}) && $fase =~ /^\d+$/) {
        my $fases = $c->stash->{zaak}
            ->zaaktype_node_id
            ->zaaktype_statussen
            ->search({ status  => $fase });

        $c->stash->{requested_fase} = $fases->first if $fases->count;
    } else {
        $c->stash->{requested_fase} = (
            $c->stash->{zaak}->volgende_fase ||
            $c->stash->{zaak}->huidige_fase
        );
    }
}


sub case_information : Chained('base') : PathPart('info') :Args() {
    my ($self, $c, $item) = @_;
    
    $c->stash->{nowrapper} = 1;

    if($item && $item eq 'geolocatie') {
        $c->stash->{template} = 'zaak/elements/view_maps.tt';
    } else {
        $c->stash->{template} = 'zaak/elements/case.tt';
    }
}


sub test_magic_strings : Chained('base') : PathPart('test_magic_strings') {
    my ($self, $c) = @_;

    $c->assert_any_user_permission('admin');

    my $systeemkenmerken = ZAAKSYSTEEM_STANDAARD_KENMERKEN;

    my $content = '<table>';
    foreach my $systeemkenmerk (sort keys %$systeemkenmerken) {
        $content .= "<tr><td>$systeemkenmerk:</td><td><strong>[[$systeemkenmerk]]</strong></td></tr>\n";
    }
    $content .= "</table>";

    my $ztt = Zaaksysteem::ZTT->new_from_case($c->stash->{ zaak });

    my $output = $ztt->process_template($content)->string;

    $c->stash->{output}   = $output;

    $c->stash->{template} = 'zaak/test_magic_strings.tt';
}


my $SPIFFY_SPINNER_DEFINITION = {
    'mode'      => 'timer',
    'title'     => 'Een moment geduld a.u.b. ...',
    'checks'    => [
        {
            'naam'  => 'kenmerk',
            'label' => 'Verzenden van formulier',
            'timer' => 2000,
        },
        {
            'naam'  => 'sjabloon',
            'label' => 'Aanmaken van documenten',
            'timer' => 2000,
        },
        {
            'naam'  => 'notificatie',
            'label' => 'Verzenden van notificatie',
            'timer' => 2000,
        },
        {
            'naam'  => 'vervolgzaak',
            'label' => 'Aanmaken van zaak',
            'timer' => 2000,
        },
    ],
};

sub create_redirect : Chained('/') : PathPart('zaak/create'): Args(0) {
    my ($self, $c, $wizard_stap) = @_;

    my $url = '/zaak/create/balie';
    if ($c->is_externe_aanvraag) {
        $url = '/zaak/create/webformulier';
    } else {
        $c->session->{_zaak_create} = {};
    }

    $c->res->redirect($c->uri_for($url, $c->req->params));
}


{

    Zaaksysteem->register_profile(
        'method'    => 'create',
        'profile'   => ZAAK_CREATE_PROFILE
    );



    sub create_base : Chained('/') : PathPart('zaak/create'): CaptureArgs(1) {
        my ($self, $c, $aangevraagd_via) = @_;

        my $params = $c->req->params();

        $c->forward('_spiffy_spinner', [ $SPIFFY_SPINNER_DEFINITION ]);

        if ($c->user_exists) {
            if ($params->{tooltip}) {

                my $json = new JSON;
                #$json = $json->allow_blessed([1]); # Nodig om een HASH in JSON om te zetten :-S

                if($c->session->{remember_zaaktype}) {
                    my $remember_zaaktype = clone $c->session->{remember_zaaktype};
                    $c->stash->{remember_zaaktype_json} = $json->encode($remember_zaaktype);
                }                
                $c->stash->{'template'} = 'widgets/zaak/create.tt';
                $c->stash->{'nowrapper'} = 1;
            } else {
                $c->stash->{'template'} = 'zaak/annuleer.tt';
            }
        } else {
            $c->stash->{'template'} = 'forbidden.tt';
        }

        if ($params->{sessreset}) {
            $c->log->debug('Wiping zaak create session, session reset asked');
            $c->session->{_zaak_create} = {};
        }

        ### Start van aanvraag, delete create session
        if ($c->user_exists && !scalar keys %$params) {
            $c->forward('_zaak_create_aanmaak_meldingen');

            $c->log->debug('Wiping zaak create session, no params');
            $c->session->{_zaak_create}                 = {};
            $c->detach;
        }

        ### POST
        $c->session->{_zaak_create}->{aangevraagd_via}      = $aangevraagd_via;
        $c->session->{_zaak_create}{ ztc_aanvrager_type }   ||= $c->req->params->{ztc_aanvrager_type};

        ### Remember zaaktype
        
        
        if (defined $params->{remembered_zaaktype}) { # hacky - to see if the form was actually submitted
            if($params->{remember_zaaktype}) {
                $c->session->{remember_zaaktype}->{
                   $params->{ztc_trigger}
                } = {
                    zaaktype_id     => $c->req->params->{zaaktype_id},
                    zaaktype_titel  => $c->req->params->{zaaktype_name}, # discrepancy in naming, TODO
                };
            } else {       
                $c->session->{remember_zaaktype} = {};
            }
        }

        if (
            defined($c->req->params->{prefill_zaaktype_id}) &&
            defined($c->req->params->{prefill_zaaktype_name}) &&
            $c->req->params->{prefill_zaaktype_id} &&
            $c->req->params->{prefill_zaaktype_name}
        ) {
            my $json = new JSON;

            $c->stash->{prefill_zaaktype} = $json->encode({
                extern  => {
                    zaaktype_id     => $c->req->params->{prefill_zaaktype_id},
                    zaaktype_titel  => $c->req->params->{prefill_zaaktype_name}, # discrepancy in naming, TODO
                }
            });
        }

        $c->forward('_create_verify_security');

        $c->forward('_create_verify_externe_data');

        $c->forward('_create_validation');

        $c->forward('_set_double_click_security');

        $c->forward('_create_load_stash');

        $c->forward('_create_zaaktype_validation');

        $c->forward('_create_load_preset_client');

        $c->forward('_create_load_externe_data');

        $c->forward('_create_load_saved_case_properties');

        $c->forward('_create_load_prefill_case_properties');


        # Kijken of er een zaak moet worden afgerond
        if ($c->req->params->{afronden}) {
            $c->session->{afronden} = 1;
            $c->session->{afronden_gezet} = 0;
        }


		if ($c->req->is_xhr) {
	        $c->stash->{nowrapper} = 1;
        }
    }

    sub _set_double_click_security : Private {
        my ($self, $c) = @_;

        if (
            defined($c->session->{_zaak_create}) &&
            !defined($c->session->{_zaak_create}->{dbl_click_token})
        ) {
            $c->session->{_zaak_create}->{dbl_click_token} =
                $c->model('DB::Zaak')->_generate_uuid;
        }
    }

    sub _check_double_click_security : Private {
        my ($self, $c) = @_;

        if (
            defined($c->session->{_zaak_create}) &&
            defined($c->session->{_zaak_create}->{dbl_click_token}) &&
            !$c->session->{_zaak_create}->{dbl_click_token}
        ) {
            if ($c->user_exists) {
                $c->res->redirect($c->uri_for('/'));
            } else {
                $c->res->redirect($c->uri_for('/pip'));
            }

            $c->detach;
        }
    }

    sub _clear_double_click_security : Private {
        my ($self, $c) = @_;

        if (
            defined($c->session->{_zaak_create}) &&
            defined($c->session->{_zaak_create}->{dbl_click_token})
        ) {
            $c->session->{_zaak_create}->{dbl_click_token} = 0;

            ### Early Session SAVE
            $c->_save_session;

        }
    }


    sub _create_load_preset_client : Private {
        my ($self, $c) = @_;
        
        if(
            !$c->user_exists &&
            #!$c->session->{_zaak_create}->{aanvrager} &&
            $c->stash->{zaaktype_node}->zaaktype_definitie_id->preset_client &&
            !$c->session->{ _saml }{ uid } &&
            !$c->model('Plugins::Bedrijfid')->login
        ) {
            my $preset_client = $c->session->{_zaak_create}->{aanvrager} =
                $c->stash->{zaaktype_node}->zaaktype_definitie_id->preset_client;
        
            my ($aanvrager_type, $id) = $preset_client =~ m|^betrokkene-(\w*)-(\d*)$|is;
            $c->session->{_zaak_create}->{extern}->{aanvrager_type} = $aanvrager_type;
            $c->session->{_zaak_create}->{extern}->{verified} = 'preset_client';
            $c->session->{_zaak_create}->{extern}->{id} = $id;

            $c->stash->{logged_in_by} =
                $c->session->{_zaak_create}->{extern}->{verified};
            $c->stash->{aanvrager_type} = 'preset_client';
        }
    }

    sub _create_zaaktype_validation : Private {
        my ($self, $c) = @_;

        my $registratie_fase    = $c->stash->{zaak_status};

        unless ($registratie_fase) {
            $c->stash->{template}   = 'form/aanvraag_error.tt';
            $c->stash->{error}      = {
                titel   => 'Helaas, deze zaak kan niet worden aangevraagd',
                bericht => 'Wij kunnen uw aanvraag helaas niet uitvoeren, '
                    .'omdat dit zaaktype niet gevonden kan worden'
                    . '. [invalid]'
            };
            $c->detach;
        }

        if ($c->stash->{zaaktype}->deleted) {
            $c->stash->{template}   = 'form/aanvraag_error.tt';
            $c->stash->{error}      = {
                titel   => 'Helaas, deze zaak kan niet worden aangevraagd',
                bericht => 'Wij kunnen uw aanvraag helaas niet uitvoeren, '
                    .'omdat dit zaaktype niet gevonden kan worden'
                    . '. [deleted]'
            };
            $c->detach;
        }
    }

    sub _zaak_create_aanmaak_meldingen : Private {
        my ($self, $c) = @_;

        if ($c->req->params->{actie} && $c->req->params->{actie} eq 'doc_intake') {
            $c->stash->{flash} = 'Document toevoegen aan zaak';
        }

    }

    sub _create_load_externe_data : Private {
        my ($self, $c) = @_;

        ### Only for externe aanvragen
        return 1 if $c->user_exists;

        return 1 if
            (
                $c->stash
                ->{zaaktype_node}
                ->zaaktype_definitie_id
                ->preset_client
                &&
                !$c->session->{ _saml }{ uid } &&
                !$c->model('Plugins::Bedrijfid')->login
            );


        unless (
            $c->session->{_zaak_create}->{extern} &&
            $c->session->{_zaak_create}->{extern}->{aanvrager_type}
        ) {
            $c->detach('/form/aanvrager_type');
        }

        $c->forward('_create_secure_aanvrager_bekend');

        ### Fallback, NOT AUTHORIZED
        unless (
            $c->session->{_zaak_create}->{extern} &&
            $c->session->{_zaak_create}->{extern}->{verified}
        ) {
            $c->detach('/form/aanvrager_type');
        }

        $c->session->{_zaak_create}->{aanvraag_trigger} = 'extern';

        ### Verify bussumid or digid
        $c->forward('/plugins/digid/_zaak_create_load_externe_data');
        $c->forward('/plugins/bedrijfid/_zaak_create_load_externe_data');
    }


    sub _create_load_prefill_case_properties : Private {
        my ($self, $c) = @_;
    
        my $params = $c->req->params();

        my $PREFIX = 'prefill_';

        # extract prefilled magic_string parameters
        my $magic_strings = [map /^$PREFIX(.*)/, grep /^$PREFIX/, keys %$params];

        #$c->log->debug("magic strings: " . Dumper $magic_strings);
        
        # avoid querying if there's no prefill anyway.
        return unless scalar @$magic_strings;

        # find the bibliotheek_kenmerken_id for the prefilled kenmerken by magic string
        my $fields_rs = $c->stash->{zaaktype_node}->zaaktype_kenmerken->search({
            'bibliotheek_kenmerken_id.magic_string' => { 
                '-in' => $magic_strings 
            }
        }, {
            join    => [qw/bibliotheek_kenmerken_id/],
        });

        my $case_properties = $c->session->{_zaak_create}->{form}->{kenmerken} ||= {};

        # assign the prefilled values using the found bibliotheek_kenmerken_id's.
        map { 
            $case_properties->{
                $_->bibliotheek_kenmerken_id->id
            } = $params->{
                $PREFIX . $_->magic_string
            };
        } $fields_rs->all;
    }



    # when using Regels (Rules) to redirect to another zaaktype, the entered data can be 
    # retained. it is restored here.
    #
    sub _create_load_saved_case_properties : Private {
        my ($self, $c) = @_;

        my $saved_case_properties = $c->session->{SAVED_CASE_PROPERTIES};
        my $case_properties = $c->session->{_zaak_create}->{form}->{kenmerken} ||= {};

        foreach my $property_id (keys %$saved_case_properties) {
            $case_properties->{$property_id} = $saved_case_properties->{$property_id};
        }

        delete $c->session->{SAVED_CASE_PROPERTIES};
    }


    sub _create_secure_aanvrager_bekend : Private {
        my ($self, $c) = @_;

        ### Not logged in, in any way
        unless ($c->session->{_zaak_create}->{extern}->{verified}) {
            $c->detach;
        }

    }

    sub _create_verify_externe_data : Private {
        my ($self, $c) = @_;

        ### Only for externe aanvragen
        return 1 if $c->user_exists;

        ### Verify bussumid or digid
        my $params          = $c->req->params();

        my $aanvrager_type  = $c->session->{ _zaak_create }{ ztc_aanvrager_type };
        $c->log->debug('Aanvrager type: ' . $c->session->{_zaak_create}->{ztc_aanvrager_type});
        unless($aanvrager_type && $aanvrager_type eq 'unknown') {
            $c->forward('/plugins/digid/_zaak_create_secure_digid');
            $c->forward('/plugins/bedrijfid/_zaak_create_security');
        }

    }
 
    sub create : Chained('create_base') : PathPart(''): Args() {
        my ($self, $c, $wizard_stap) = @_;

        ### Dispatch to form
        if ($c->user_exists) {
            $wizard_stap ||= 'zaakcontrole';
        } else {
            $wizard_stap ||= 'aanvrager';
        }

        $c->forward('/form/' . $wizard_stap);

        $c->detach unless $c->stash->{publish_zaak};

        $c->forward('_check_double_click_security');


        ### Publish zaak
        $c->forward('_create_zaak', [ $c->session->{_zaak_create} ]);
    }



    # 1) if a rule determines the price, use that value, return that.
    # 2) otherwise use zaaktype_definitie->pdc_tarief, unless:
    # 3) if a specific contactchannel has been configured, use that.
    sub _create_zaak_set_offline_payment_info : Private {
        my ($self, $c) = @_;

        my $rules_amount    = $c->stash->{rules_payment_amount};
        my $contactkanaal   = $c->session->{_zaak_create}->{contactkanaal};
        my $node            = $c->stash->{zaak}->zaaktype_node_id;
        my $casetype_amount = $node->zaaktype_definitie_id->pdc_tarief;


        if ($contactkanaal ne ZAAKSYSTEEM_CONTACTKANAAL_WEBFORM) {
            $casetype_amount = $node->properties->{'pdc_tarief_' . $contactkanaal};
        }

        my $payment_amount = $rules_amount || $casetype_amount || 0;

        if ($payment_amount) {
            $c->stash->{zaak}->payment_amount($payment_amount);
            $c->stash->{zaak}->update();
        }
    }

    sub _create_handle_payment : Private {
        my ($self, $c)  = @_;

        if (
            $c->session->{_zaak_create} &&
            $c->stash->{zaak}->zaaktype_node_id->online_betaling &&
            (
                !$c->user_exists ||
                exists($c->session->{behandelaar_form})
            )
        ) {
            $c->detach('/plugins/ogone/betaling');
        } else {
            $c->forward('_create_zaak_set_offline_payment_info');
            $c->forward('_create_zaak_fire_phase_actions', [ 'email' ]);
        }
    }

    sub _create_handle_finish : Private {
        my ($self, $c)  = @_;


        if ($c->session->{_zaak_create}) {
            ### Logged in
            if ($c->user_exists) {
                if (!$c->stash->{zaak}) {
                    my $errmsg  = 'Er is iets misgegaan bij het aanmaken '
                        . 'van de zaak.';
                    $c->log->error($errmsg);
                    $c->push_flash_message($errmsg);

                    $c->res->redirect(
                        $c->uri_for('/')
                    );
                    $c->detach;
                }
            }

            # Check of de klant een onafgeronde zaak heeft staan, en check dit alleen als we beide id's hebben,
            # Anders gaat find op z'n plaat omdat undef geen primary key kan zijn
            if($c->session->{ _zaak_create }{ zaaktype_id } && $c->session->{ _zaak_create }{ ztc_aanvrager_id }) {
                my $onafgeronde_zaak = $c->model('DB::ZaakOnafgerond')->find(
                    $c->session->{_zaak_create}->{zaaktype_id}, 
                    $c->session->{_zaak_create}->{ztc_aanvrager_id}
                );

                if ($onafgeronde_zaak) {
                    $onafgeronde_zaak->delete; 
                    $c->log->debug('ONAFGERONDE ZAAK UIT TABEL GEHAALD!');
                }
            }

            my $redirect = '/';
            if ($c->user_exists && !exists($c->session->{behandelaar_form})) {
                if ($c->req->params->{actie_automatisch_behandelen}) {
                    $redirect = '/zaak/' . $c->stash->{zaak}->nr;
                } else {
                    $c->push_flash_message('Uw zaak is geregistreerd onder <a href="/zaak/'.$c->stash->{zaak}->nr.'">zaaknummer '.$c->stash->{zaak}->nr.'</a>');
                    $redirect = '/';
                }

                $c->response->redirect($redirect);
                $c->detach;
            } elsif (!$c->user_exists) {
                ### External user
                $c->stash->{preset_client} = $c->stash->{zaaktype}->zaaktype_definitie_id->preset_client;
                $c->stash->{template} = 'form/finish.tt';
            } else {
                return 1;
            }

            delete($c->session->{_zaak_create});
        }
    }

    sub _create_zaak : Private {
        my ($self, $c, $params)  = @_;

        ### Fix kenmerken
        {
            $params->{kenmerken} = [];

            unless($params->{raw_kenmerken} && %{ $params->{raw_kenmerken} }) {
                $params->{raw_kenmerken} = $params->{form}->{kenmerken};
            }

            for my $kenmerk (keys %{ $params->{raw_kenmerken} }) {
                ### Normal kenmerk
                push( @{ $params->{kenmerken} },
                    { $kenmerk    => $params->{raw_kenmerken}->{$kenmerk} }
                )
            }
        }

        ### Fix betrokkene
        if ($params->{ztc_aanvrager_id} && $params->{ztc_aanvrager_id} =~ /betrokkene-.*?-.*/) {
            $params->{aanvragers} = [{
                'betrokkene'        => $params->{ztc_aanvrager_id},
                'verificatie'       => (
                    (
                        $c->session->{_zaak_create}->{aangevraagd_via} eq
                            ZAAKSYSTEEM_CONTACTKANAAL_WEBFORM
                    )
                        ? $c->session->{_zaak_create}->{extern}->{verified}
                        : 'medewerker'
                )
            }];
        }

        ### REMOVE DUMMY
        if ($params->{aanvragers}) {
            for (my $i=0; $i < scalar(@{ $params->{aanvragers} }); $i++) {
                if ($params->{aanvragers}->[$i]->{betrokkene} =~ /dummy/) {
                    delete($params->{aanvragers}->[$i]);
                }
            }
        }


        if ($c->session->{_zaak_create}->{dbl_click_token}) {
            $params->{uuid} = $c->session->{_zaak_create}->{dbl_click_token};
            $c->log->debug('Creating zaak with UUID: ' . $params->{uuid});
        }

        $c->log->debug('Create zaak with: ' . Dumper($params));


        my $zaak            = $c
                            ->model('DB::Zaak')
                            ->create_zaak($params);

        unless ($zaak) {
            $c->log->debug('ERROR: detaching to foutmelding');
            $c->stash->{foutmelding}    = 'Zaak kon niet worden aangemaakt';
            $c->stash->{template}       = 'foutmelding.tt';
            $c->detach;
        }

        $c->stash->{zaak}   = $zaak;

        $c->forward('/zaak/handle_fase_acties', [ $params ]);

        return $zaak;
    }

    sub handle_fase_acties : Private {
        my ($self, $c, $params) = @_;

        $c->forward('_create_zaak_handle_uploads', [ $params ]);
        $c->forward('_create_zaak_handle_contact', [ $params ]);
        $c->forward('_create_zaak_handle_acties', [ $params ]);

        $c->forward('_create_zaak_fire_phase_actions', [ 'allocation' ]);
        $c->forward('_create_zaak_fire_phase_actions', [ 'template' ]);
        $c->forward('_create_zaak_fire_phase_actions', [ 'case' ]);

        $c->forward('_create_handle_payment');
        $c->forward('_create_handle_finish');
    }

    sub _create_zaak_fire_phase_actions : Private {
        my ($self, $c, $type) = @_;

        return unless $type;

        my $actions = $c->stash->{ zaak }->registratie_fase->case_actions->search({
            case_id => $c->stash->{ zaak }->id,
            type => $type
        })->sorted;

        $actions->apply_rules({
            case => $c->stash->{ zaak },
            milestone => 1
        });

        for my $action ($actions->all) {
            next unless $action->automatic;

            $c->log->debug('Running action: ' . $action->label);

            # Chanle only route fields when case is automatically being assigned. This
            # will leave behandelaar and status alone.
            my %opts;
            if ($c->req->params->{actie_automatisch_behandelen}) {
                $opts{change_only_route_fields} = 1;
            }

            $c->stash->{ zaak }->fire_action({
                context => $c,
                action => $action,
                %opts,
            });
        }
    }

    sub _create_zaak_handle_acties : Private {
        my ($self, $c, $params)  = @_;

        ### This is here, because we can call this method from status/next,
        ### and then we do not want to run this routine.
        return unless ($c->session->{_zaak_create});

        if ($c->session->{_zaak_create}->{acties}->{doc_intake}) {
            my $doc_intake  = $c->session->{_zaak_create}
                                ->{acties}
                                ->{doc_intake};

            my $file = $c->model('DB::File')->find($doc_intake->{component_id});
            
            my $subject = $c->req->params->{ztc_aanvrager_id};

            if (!$subject && $c->session->{_zaak_create}->{aanvragers}) {
                $subject  = $c->session->{_zaak_create}->{aanvragers}->[0]->{betrokkene};
                $subject  = undef if $subject =~ /dummy/;
            } elsif (!$subject) {
                $subject  = $c->session->{_zaak_create}->{ztc_aanvrager_id};
            }

            my %optional;
            if ($c->req->params->{'intake_document_catalogus'}) {
                $optional{case_document_ids} = [$c->req->params->{'intake_document_catalogus'}];
            }



            $file->update_properties({
                case_id  => $c->stash->{zaak}->id, 
                accepted => 1, 
                subject  => $subject,
                metadata => {
                    description => $c->req->params->{'intake_document_help'},
                },
                %optional,
            });
        }

        if ($c->req->params->{actie_automatisch_behandelen}) {
            $c->stash->{zaak}->open_zaak;

            $c->push_flash_message('Zaak is door u in behandeling genomen');
        }

        # manual settings override the rule behaviour
        my $actie_ou_id   = $c->req->params->{actie_ou_id}   || $c->session->{create_case_allocation_rule}->{ou_id};
        my $actie_role_id = $c->req->params->{actie_role_id} || $c->session->{create_case_allocation_rule}->{role_id};

        if ($actie_ou_id || $actie_role_id) {
            my $action = $c->stash->{ zaak }->case_actions->search({
                type => 'allocation',
                casetype_status_id => $c->stash->{ zaak }->registratie_fase->id
            })->first;

            if($action) {
                my $data = $action->data;

                unless($data->{ ou_id } eq $actie_ou_id && $data->{ role_id } eq $actie_role_id) {
                    $c->log->debug(sprintf(
                        'Updating action %d with new allocation information from intake form (%d, %d)',
                        $action->id,
                        $actie_ou_id,
                        $actie_role_id
                    ));

                    $data->{ ou_id } = $actie_ou_id;
                    $data->{ role_id } = $actie_role_id;
                    $data->{ change_only_route_fields } = 1;

                    $action->label("$actie_ou_id, $actie_role_id");
                    $action->data($data);
                    $action->data_tainted(1);
                }

                # in casetype management screens this isn't set by default - which it should
                # the UI shows a disabled checkbox which is checked. this behaviour is implemented
                # here.
                # the cleanest solution would mean to implement this in case type admin (beheer/zaaktypen/milestone_definitie/ajax_table.tt)
                # and then run a migration script on ALL databases -- this is the part i'm concerned with.
                # maybe combine this with database migration solution -- so that every instance of Zaaksysteem that runs
                # new software version gets the updates once.
                $action->automatic(1);
                $action->state_tainted(1);
                $action->update;
            }

            delete $c->session->{create_case_allocation_rule};
        }
    }


    sub _create_zaak_handle_contact : Private {
        my ($self, $c, $params)  = @_;

        ### This is here, because we can call this method from status/next,
        ### and then we do not want to run this routine.
        return unless ($c->session->{_zaak_create});

        for (qw/npc-email npc-telefoonnummer npc-mobiel/) {
            my $value;
            if (defined($c->req->params->{ $_ })) {
                $value = $c->req->params->{ $_ };
            } elsif (defined($params->{ $_ })) {
                $value = $params->{$_};
            } else {
                next;
            }

            my $key     = $_;
            $key =~ s/^npc-//g;

            $c->log->debug('Add aanvrager: ' . $key . ':' . $value);
            $c->stash->{zaak}->aanvrager_object->$key($value)
        }

    }

    sub _create_zaak_handle_uploads : Private {
        my ($self, $c, $params)  = @_;

        return 1 unless ($params && $params->{uploads});

        while (my ($case_type_property_id,$upload_info) = each %{ $params->{uploads} }) {

            for my $file (@{$upload_info}) {
                next unless($file->{upload});
                my $upload = $file->{upload};

                my $case_type_properties = $c->stash->{zaak}
                    ->zaaktype_node_id
                    ->zaaktype_kenmerken
                    ->search(
                        {
                            'bibliotheek_kenmerken_id.id'           => $case_type_property_id,
                            'bibliotheek_kenmerken_id.value_type'   => 'file',
                        },
                        {
                            join    => 'bibliotheek_kenmerken_id'
                        }
                    );

                next unless $case_type_properties->count;

                my $case_type_property = $case_type_properties->first;
                if ($c->clamscan('kenmerk_id_' . $case_type_property->id)) {
                    next;
                }

                my $optionals;

                # Internal files are accepted right away, external files always go into a queue.
                if (!$c->is_externe_aanvraag) {
                    $optionals->{accepted} = 1;
                }

                # Check if this is being created from an existing filestore entry
                if (defined $file->{filestore_id}) {
                    $optionals->{filestore_id} = $file->{filestore_id};
                }

                # Find subject
                my $subject;
                if ($c->user_exists) {
                    $subject = 'betrokkene-medewerker-'.$c->user->uidnumber;
                }
                elsif ($c->req->params->{'ztc_aanvrager_id'}) {
                    $subject = $c->req->params->{'ztc_aanvrager_id'};
                }
                else {
                    $subject = $c->stash->{zaak}->aanvrager_object->rt_setup_identifier;
                }

                # Find document publish type
                my $publish_pip = $case_type_property->pip ? 1 : 0;

                # Create the file
                try {
                    my $file = $c->model('DB::File')->file_create({
                        db_params => {
                            created_by              => $subject,
                            case_id                 => $c->stash->{zaak}->id,
                            %$optionals,
                        },
                        case_document_ids => [$case_type_property->id],
                        name              => $upload->filename,
                        file_path         => $upload->tempname,
                        publish_type_name => $publish_pip,
                    });
                } catch {
                    $params->{upload_error} = $_;
                }
            }
        }
        ### Delete uploads from session
        delete($c->session->{_zaak_create}->{uploads});
    }

    sub _create_load_stash : Private {
        my ($self, $c)  = @_;

        $c->forward('_create_load_zaaktype');

        my $first_status       = $c->stash->{zaaktype}
            ->zaaktype_statussen
            ->search(
                {
                    status  => 1,
                }
            )->first;

        $c->stash->{zaak_status}    = $first_status;

        $c->stash->{fields}         = $c->stash->{zaaktype}
            ->zaaktype_kenmerken
            ->search(
                {
                    zaak_status_id  => $first_status->id,
                },
                {
                    prefetch    => ['bibliotheek_kenmerken_id', 'zaak_status_id'],
                    order_by    => 'me.id'
                }
            );

        ### Load aanvrager gegevens
        my $betrokkene_id = $c->req->params->{ztc_aanvrager_id};

        if (!$betrokkene_id && $c->session->{_zaak_create}->{aanvragers}) {
            $betrokkene_id  =
                $c->session->{_zaak_create}->{aanvragers}->[0]->{betrokkene};

            $betrokkene_id  = undef if $betrokkene_id =~ /dummy/;
        } elsif (!$betrokkene_id) {
            $betrokkene_id  = $c->session->{_zaak_create}->{ztc_aanvrager_id};
        }

        if ($betrokkene_id) {
            $c->stash->{aanvrager} = $c->model('Betrokkene')->get(
                {},
                $betrokkene_id
            );
        }

        if ($c->stash->{aanvrager}) {
            $c->stash->{aanvrager_naam} = $c->stash->{aanvrager}->naam;
        } elsif (
            $c->session->{_zaak_create}->{aanvrager_update} &&
            $c->session->{_zaak_create}->{aanvrager_update}->{'np-geslachtsnaam'}
        ) {
            my $aanvrager_sess =
                $c->session->{_zaak_create}->{aanvrager_update};

            $c->stash->{aanvrager_naam} = $aanvrager_sess->{'np-voornamen'}
                . ($aanvrager_sess->{'np-voorvoegsel'} ?
                    ' ' . $aanvrager_sess->{'np-voorvoegsel'} : ''
                ) . ' ' . $aanvrager_sess->{'np-geslachtsnaam'};
        }

        ### Load aangevraagd via
        $c->stash->{aangevraagd_via} =
            $c->session->{_zaak_create}->{aangevraagd_via};

        $c->stash->{zaak_acties} =
            $c->session->{_zaak_create}->{acties}
                if $c->session->{_zaak_create}->{acties};

        $c->stash->{aanvraag_trigger} =
            $c->session->{_zaak_create}->{aanvraag_trigger}
                if $c->session->{_zaak_create}->{aanvraag_trigger};

        $c->stash->{aanvrager_type} =
            $c->session->{_zaak_create}->{extern}->{aanvrager_type};

        $c->stash->{logged_in_by} =
            $c->session->{_zaak_create}->{extern}->{verified};

        if (
            $c->session->{_zaak_create}->{acties}->{doc_intake}
        ) {
            $c->stash->{doc_intake} = $c->session
                ->{_zaak_create}
                ->{acties}
                ->{doc_intake};
        }

    }

    sub _create_load_zaaktype : Private {
        my ($self, $c)  = @_;

        ### Geen zaaktype? detach to list
        if (
            !$c->user_exists &&
            !$c->session->{_zaak_create}->{zaaktype_id}
        ) {
            $c->detach('/form/list');
        }

        my $zaaktype_node;

        if($c->session->{_zaak_create}->{zaaktype_id}) {
            $zaaktype_node = $c->model('DB::Zaaktype')->find(
                $c->session->{_zaak_create}->{zaaktype_id},
                {
                    prefetch    => [
                    'zaaktype_node_id', { zaaktype_node_id =>
                        'zaaktype_definitie_id' }
                    ],
                }
            )->zaaktype_node_id;
        }

        unless (
            $zaaktype_node
        ) {
            $c->log->debug('Z::C::Zaak->_create_load_zaaktype: '
                . ' zaaktype not found by id: '
                . ($c->session->{_zaak_create}->{zaaktype_id}||'')
            );

            delete($c->session->{_zaak_create}->{zaaktype_id});
            $c->detach;
        }

        $c->stash->{zaaktype_node}      = $c->stash->{zaaktype}
                                        = $c->stash->{definitie}
                                        = $zaaktype_node;

        $c->stash->{zaaktype_node_id}   = $c->stash->{zaaktype_node}->id;

        my $aanvragers                  = $c->stash->{zaaktype_node}
                                            ->zaaktype_betrokkenen
                                            ->search;

        $c->stash->{type_aanvragers}    = [];
        while (my $aanvrager = $aanvragers->next) {
            push(
                @{ $c->stash->{type_aanvragers} },
                $aanvrager->betrokkene_type
            );
        }
    }

    sub _create_validation_aanvragers : Private {
        my ($self, $c, $params) = @_;

        ### FIX: Make sure aanvrager validatie werkt, alleen voor interne
        ### aanvraag

        if ($c->req->params->{ztc_aanvrager_id}) {
            $params->{aanvragers} = [{
                'betrokkene'        => $params->{aanvrager_id},
                'verificatie'       => (
                    (
                        $c->session->{_zaak_create}->{aangevraagd_via} eq
                            ZAAKSYSTEEM_CONTACTKANAAL_WEBFORM
                    )
                        ? $c->session->{_zaak_create}->{extern}->{verified}
                        : 'medewerker'
                )
            }];
        } elsif (
            $c->session->{_zaak_create}->{aanvrager_update} &&
            $c->session->{_zaak_create}->{aanvrager_update}->{create} &&
            $c->session->{_zaak_create}->{extern}->{verified}
        ) {
            $params->{aanvragers} = [{
                'create'            =>
                    $c->session->{_zaak_create}->{aanvrager_update},
                'betrokkene_type'   =>
                    $c->session->{_zaak_create}->{extern}->{aanvrager_type},
                'verificatie'       =>
                        $c->session->{_zaak_create}->{extern}->{verified},
            }];

            $c->session->{_zaak_create}->{aanvragers} =
                $params->{aanvragers};

        } elsif ($c->session->{_zaak_create}->{aanvrager_update}) {
            ### XXXX !!!!!!DUMMY!!!!!!
            $params->{aanvragers} = [{
                'betrokkene'        => 'betrokkene-dummy-99999',
                'verificatie'       => (
                    (
                        $c->session->{_zaak_create}->{aangevraagd_via} eq
                            ZAAKSYSTEEM_CONTACTKANAAL_WEBFORM
                    )
                        ? $c->session->{_zaak_create}->{extern}->{verified}
                        : 'medewerker'
                )
            }];
        }

    }

    sub _create_validation_acties : Private {
        my ($self, $c, $params) = @_;

        ### If doc_intake session, check for extra parameters
        if (
            $c->req->params->{doc_intake_update} &&
            $c->session->{_zaak_create}->{acties}->{doc_intake}
        ) {
            $c->session->{_zaak_create}->{acties}->{doc_intake}->{document_catalogus}
                    = $c->req->params->{intake_document_catalogus};

            $c->session->{_zaak_create}->{acties}->{doc_intake}->{document_help}
                    = $c->req->params->{intake_document_help};
        }


        ### Fill from params
        if ($c->req->params->{actie}) {
            $c->session->{_zaak_create}->{acties} = {};

            $c->session->{_zaak_create}->{acties}->{
                $c->req->params->{actie}
            } = {
                    component       => $c->req->params->{actie},
                    onderwerp       => $c->req->params->{actie_description},
                    component_id    => $c->req->params->{actie_value},
            };
        }
    }

    my $_create_validation_deprecation_map = {
        ztc_trigger         => 'aanvraag_trigger',
        betrokkene_type     => 'betrokkene_type',
        zaaktype            => 'zaaktype_node_id',
        ztc_aanvrager_id    => 'aanvrager_id',
        ztc_contactkanaal   => 'contactkanaal'
    };

    sub _create_validation : Private {
        my ($self, $c)  = @_;
        my $params      = {};

        ### Keys to validate?
        if (scalar(keys( %{ $c->req->params }))) {
            $params        = { %{ $c->req->params } };

            ### Translation
            for my $key (keys %{ $_create_validation_deprecation_map }) {
                $params->{ $_create_validation_deprecation_map->{ $key } }
                    = $params->{$key};

                delete($params->{$key});
            }
        }
 
 
        $c->forward('_create_validation_aanvragers', [ $params ]);
        $c->forward('_create_validation_acties', [ $params ]);

        ### Merge session data into params:
        $params->{ $_ } = $c->session->{_zaak_create}->{ $_ }
            for keys %{ $c->session->{_zaak_create} };

        ### Fix registratiedatum
        $params->{registratiedatum} = DateTime->now()
            unless $params->{registratiedatum};

        my $dv          = Params::Profile->check(
            params  => $params,
            method  => 'Zaaksysteem::Controller::Zaak::create'
        );

        my $validated_options   = $dv->valid;

        ### Depending on xml request (do_validation) or create, we detach
        if ($c->req->is_xhr &&
            $c->req->params->{do_validation} &&
            $c->req->params->{create_entry}
        ) {
            $c->zvalidate($dv);
            $c->detach;
        }

        ### Because we need extra variabled in session, we loop
        ### over the params keys.
        for my $key (keys %{ $validated_options }) {
            $c->session->{_zaak_create}->{ $key } =
            $validated_options->{ $key }
        }

        ###
        ### Betrokkenen
        ###
        for (qw/npc-email npc-telefoonnummer npc-mobiel/) {
            next unless $params->{$_};

            $c->session->{_zaak_create}->{$_} = $params->{$_};
        }

        ### Plugin structure
        my $reqparams  = $c->req->params;

        ### Plugins
        if ($reqparams->{plugin}) {
            my $webform         = CUSTOM_WEBFORMS->{ $reqparams->{plugin} };

            foreach my $controller ($c->controllers) {
                next unless (
                    $controller eq
                        'Plugins::' .  $webform->{plugin}
                        &&
                    $c->controller($controller)->can('prepare_zaak_create')
                );

                $c->log->debug(
                    'Z:C:Zaak->create[prepare_zaak_create]: Running plugin: '
                    .  ucfirst($webform->{plugin}) . ', Params: ' .
                    Dumper($params)
                );

                eval {
                    $c->controller($controller)->prepare_zaak_create($c, $reqparams);
                };

                if ($@) {
                    die($c->log->error(
                        'Z:C:Zaak->create[prepare_zaak_create]: Running plugin: '
                        .  ucfirst($webform->{plugin}) . ': ' . $@
                    ));
                }

            }
        }


        ### Add kenmerken
        $c->session->{_zaak_create}->{raw_kenmerken}
            = $c->session->{_zaak_create}->{form}->{kenmerken};

    }

    sub _create_verify_security : Private {
        my ($self, $c) = @_;

        ### Aangevraagd via correct
        {
            unless (
                grep (
                    { $c->session->{_zaak_create}->{aangevraagd_via} eq $_ }
                    ZAAKSYSTEEM_CONTACTKANAAL_WEBFORM,
                    ZAAKSYSTEEM_CONTACTKANAAL_BALIE
                )
            ) {
                $c->log->error('Aangevraagd_via not one of: ' .
                    join (',',
                        ZAAKSYSTEEM_CONTACTKANAAL_WEBFORM,
                        ZAAKSYSTEEM_CONTACTKANAAL_BALIE
                    )
                );
                $c->detach;
            }

            unless (
                (
                    $c->session->{_zaak_create}->{aangevraagd_via} eq
                        ZAAKSYSTEEM_CONTACTKANAAL_WEBFORM &&
                    !$c->user_exists
                ) ||
                (
                    $c->session->{_zaak_create}->{aangevraagd_via} ne
                        ZAAKSYSTEEM_CONTACTKANAAL_WEBFORM &&
                    $c->user_exists
                )
            ) {
                $c->log->error(
                    $c->session->{_zaak_create}->{aangevraagd_via}
                    . ' aanvraag niet via logged in user, impossible.'
                );
                $c->detach;
            }

            if ($c->is_externe_aanvraag) {
                $c->session->{_zaak_create}->{aangevraagd_via} =
                    $c->session->{_zaak_create}->{contactkanaal} =
                        ZAAKSYSTEEM_CONTACTKANAAL_WEBFORM;
            }
        }
    }
}

sub _spiffy_spinner : Private {
    my ($self, $c, $definition) = @_;

    return 1 unless ($c->req->is_xhr &&
        $c->req->params->{spiffy_spinner}
    );

    $c->stash->{json} = {
        'spinner'    => $definition,
    };
    $c->forward('Zaaksysteem::View::JSONlegacy');
    $c->detach;
}

sub duplicate : Local {
    my ($self, $c, $zaakid) = @_;

    unless ($c->req->param('confirmed')) {
        $c->stash->{ confirmation }{ message } = sprintf(
            'Weet u zeker dat u deze zaak (%d) wilt kopie&euml;ren?',
            $zaakid
        );

		$c->stash->{confirmation}{type} = 'yesno';
		$c->stash->{confirmation}{uri} = $c->uri_for(sprintf('/zaak/duplicate/%d', $zaakid));

		$c->detach('/page/confirmation');
	} else {
        my $zaak = $c->model('DB::Zaak')->duplicate($zaakid, { simpel  => 1 });

        if (!$zaak) {
            $c->push_flash_message('De zaak kon niet gekopie&euml;rd worden.');

            $c->response->redirect($c->req->referer);
            $c->detach;
        }

        $c->push_flash_message('Zaak %d succesvol gekopie&euml;rd', $zaakid);
        $c->response->redirect('/zaak/' . $zaak->nr . '#zaak-elements-case');
    }

	$c->detach;
}

# TODO rebase this as chained from the 'base' action in this module
sub meta_info : Chained('/'): PathPart('zaak/get_meta'): Args(1) {
    my ($self, $c, $id) = @_;

    return unless $id =~ /^\d+$/;

    ### Retrieve zaak
    $c->stash->{nowrapper} = 1;
    $c->stash->{'zaak'} = $c->model('DB::Zaak')->find($id);

    $c->stash->{template} = 'zaak/metainfo.tt';
}

sub start_nieuwe_zaak : Chained('/'): PathPart('zaak/start_nieuwe_zaak'): Args() {
    my ($self, $c, $zaaktype_id, $copy_properties) = @_;

    my $zaaktype = $c->model('DB::Zaaktype')->find($zaaktype_id);
    
    my $new_params = {
        'create'            => '1',
        'create_entry'      => '1',
        'sessreset'         => 1,
        'zaaktype_id'       => $zaaktype->id,
        'zaaktype_name'     => $zaaktype->zaaktype_node_id->titel,
        'ztc_contactkanaal' => $c->session->{_zaak_create}->{contactkanaal},
        'jstrigger'         => $c->session->{_zaak_create}->{aanvraag_trigger},
        'ztc_trigger'       => $c->session->{_zaak_create}->{aanvraag_trigger},
        'ztc_aanvrager_id'  => $c->session->{_zaak_create}->{aanvragers}->[0]->{betrokkene},
        'betrokkene_type'   => $c->session->{_zaak_create}->{aanvragers}->[0]->{verificatie},
    };

    if(
        exists $c->session->{_zaak_create}->{extern} &&
        $c->session->{_zaak_create}->{extern}->{id} &&
        $c->session->{_zaak_create}->{extern}->{aanvrager_type} &&        
        $c->session->{_zaak_create}->{extern}->{verified}       
    ) {
         $new_params->{betrokkene_type} = 
             $c->session->{_zaak_create}->{extern}->{aanvrager_type};
 
         $new_params->{ztc_aanvrager_id} = 
             $c->session->{_zaak_create}->{extern}->{id};
         
         $new_params->{verified} =
             $c->session->{_zaak_create}->{extern}->{verified};
     }


    if($copy_properties) {
        $c->log->debug("copying properties forward");
        unless($c->session->{SAVED_CASE_PROPERTIES}) {
            $c->session->{SAVED_CASE_PROPERTIES} = clone $c->session->{_zaak_create}->{form}->{kenmerken};
        }
    }

    my $aangevraagd_via = $c->session->{_zaak_create}->{aangevraagd_via};

    $c->res->redirect($c->uri_for('/zaak/create/' . $aangevraagd_via, $new_params));
    $c->detach;
}

sub _execute_regels : Private {
    my ($self, $c) = @_;

    my $params = $c->req->params;
#    $c->log->debug("execute regels: ". Dumper $params);

 #   my $status = $c->stash->{requested_fase}->status;
    my $status = $params->{status};


    my $rules_result = $c->stash->{regels_result} = $c->stash->{zaak}->execute_rules({
        status => $status,
        cache  => 0,
    });
    

    if(my $vul_waarde_in = $rules_result->{vul_waarde_in}) {
        my %new_values;
        while(my ($bibliotheek_kenmerken_id, $action) = each %$vul_waarde_in) {
            $new_values{$bibliotheek_kenmerken_id} = $action->{value};
        }

        $c->stash->{zaak}->zaak_kenmerken->update_fields({
            new_values                  => \%new_values,
            zaak_id                     => $c->stash->{zaak}->id,
        });
    }
    

    my $referential_rs = $c->stash->{zaak}
        ->zaaktype_node_id
        ->zaaktype_kenmerken
        ->search(
        {
            referential     => { '-not' => undef },
            zaak_status_id  => $c->stash->{requested_fase}->id,
        }
    );
    
    if($referential_rs->count && $c->stash->{zaak}->pid) {
        # so we have referential fields in this phase. let's see which fields are
        # visible and hidden in the parent case, so we can do the same for this case
        
        my $parent_case = $c->stash->{zaak}->pid;
        my $parent_visible_fields = $parent_case->visible_fields({
            aanvrager_type  => $parent_case->systeemkenmerk('aanvrager_type'),
            field_values    => $parent_case->field_values(),
            include_documents => 1
        });

        $c->stash->{parent_visible_fields} = $parent_visible_fields;
    }
}

sub fields : Chained('base'): PathPart('fields') {
    my ($self, $c, $options) = @_;
    my $params  = $c->req->params;

    $c->forward('_execute_regels');

    $c->forward('empty_hidden_fields') if $options->{empty_hidden_fields};

    $c->stash->{nowrapper}  = 1;
    $c->stash->{template}   = 'zaak/fields.tt';
}



#
# when a field value is changed, update it into the database
#
sub update_field : JSON : Chained('base'): PathPart('update_field'): Args(0) {
    my ($self, $c) = @_;

    my $params  = $c->req->params;
    my $field       = $params->{field};
    my $rule_field  = $params->{rule_field};

    ### Prevent unnecessary post
    die "unconfirmed post" unless lc($c->req->method) eq 'post' && $params->{confirmed_post};

    # result can be updated even if the case is closed
    if($field eq 'system_kenmerk_resultaat') {

        die "can't change case" unless $c->can_change({ ignore_afgehandeld => 1 });

        my $value = $params->{$field} or die "need result value";

        $c->stash->{zaak}->set_resultaat($value);

        $c->stash->{zaak}->update;

    } elsif($field =~ m|^kenmerk_id_\d+$|) {

        my ($bibliotheek_kenmerken_id) = $field =~ m|^kenmerk_id_(\d+)|;

        my @values = $c->req->param($field);

        $c->stash->{zaak}->zaak_kenmerken->update_field({
            bibliotheek_kenmerken_id    => $bibliotheek_kenmerken_id,
            new_values                  => \@values,
            zaak_id                     => $c->stash->{zaak}->id,
        });

    } elsif (!$field || $field eq 'undefined') {
        $c->stash->{json} = { result => 1 };
        $c->detach('Zaaksysteem::View::JSON');
    } else {
        die "unknown field type updated.";
    }

    if ($rule_field) {
        $c->detach('fields', [{
            empty_hidden_fields => 1
        }]);
    }

    $c->stash->{zaak}->touch;

    $c->stash->{json} = { result => 1 };

    $c->forward('Zaaksysteem::View::JSON');
}



sub empty_hidden_fields : Private {
    my ($self, $c) = @_;

    my $rules_result = $c->stash->{regels_result} or die "need rules executed at this point";

    if (my $hidden_fields = $rules_result->{verberg_kenmerk}) {

        $c->stash->{zaak}->zaak_kenmerken->delete_fields({
            bibliotheek_kenmerken_ids => [keys %$hidden_fields],
            zaak_id => $c->stash->{zaak}->id
        });

        $c->stash->{zaak}->touch;
    }
}



sub _update_verify_value {
    my ($self, $c, $id, $value) = @_;
    my $return_as_scalar    = 0;

    my $dbkenmerk   = $c->model('DB::BibliotheekKenmerken')->find(
        $id
    );

    unless (UNIVERSAL::isa($value, 'ARRAY')) {
        $return_as_scalar++;
        $value  = [ $value ];
    }

    for (my $i = 0; $i < scalar(@{ $value }); $i++) {
        my $valuepart   = $value->[$i];

        if (
            defined(
                ZAAKSYSTEEM_CONSTANTS->{veld_opties}->{
                    $dbkenmerk->value_type
                }->{constraint}
            )
        ) {
            if (
                $valuepart !~
                    ZAAKSYSTEEM_CONSTANTS->{veld_opties}->{
                        $dbkenmerk->value_type
                    }->{constraint}
            ) {
                $c->log->warn('Value with key: ' . $id . ' does not match'
                    . ' constraint defined in veld_opties'
                );
                return;
            }
        }

        if ($dbkenmerk->value_type eq 'valuta') {
            $valuepart =~ s/,/./g;
        }

        $value->[$i] = $valuepart;
    }

    return $value->[0] if $return_as_scalar;
    return $value;
}


sub open : Chained('base'): PathPart('open'): Args(0) {
    my ($self, $c) = @_;

    # Only open(/assign) a zaak if it isn't already closed. This fixes
    # a case being reopened by accident. (With double tabs, for example.)
    if (!$c->stash->{zaak}->is_afgehandeld) {
        $c->assert_any_zaak_permission('zaak_edit');

        my $logging_id = $c->stash->{zaak}->open_zaak;
        $c->push_flash_message(
            'Zaak is door u in behandeling genomen (<a href="/zaak/' . $c->stash->{zaak}->id .  
            '/undo/' . $logging_id . '">Ongedaan maken</a>)'
        );

    }
    
    $c->response->redirect('/zaak/' . $c->stash->{zaak}->nr);
    $c->detach;
}

sub undo_open : Chained('base'): PathPart('undo'): Args() {
    my ($self, $c, $logging_id) = @_;

    if($c->stash->{zaak}->undo({ logging_id => $logging_id })) {
        $c->push_flash_message('Zaak in behandeling nemen ongedaan gemaakt.');
    }

    $c->response->redirect('/');
    $c->detach;
}



sub unrelate : Chained('base') : PathPart('unrelate') : Args(1) {
    my ($self, $c, $related_case_id) = @_;

    $c->assert_any_zaak_permission('zaak_edit');

    $c->stash->{ zaak }->unrelate($related_case_id);

    $c->push_flash_message('Zaak relatie verwijderd');

    $c->response->redirect('/zaak/' . $c->stash->{ zaak }->nr);
    $c->detach;
}


sub view : Chained('base'): PathPart(''): Args(0) {
    my ($self, $c) = @_;

    my $mimetypes_allowed = MIMETYPES_ALLOWED;
    $c->stash->{pdfmimetypes} = {
        map {
            MIMETYPES_ALLOWED->{$_}->{mimetype} => MIMETYPES_ALLOWED->{$_}->{conversion}
        } 
        grep { 
            defined MIMETYPES_ALLOWED->{$_}->{conversion}  
        } 
         keys %$mimetypes_allowed
    };


    $c->stash->{page_title} = 'Zaak :: ' . $c->stash->{zaak}->id;

    ### Find fase
    {
        my $fase = $c->req->params->{fase} || '';
        if ($fase =~ /^\d+$/) {
            my $fases = $c->stash->{zaak}->fasen->search(
                {
                    status  => $fase
                }
            );

            $c->stash->{requested_fase} = $fases->first if $fases->count;
        }
    }
    
    if (
        $c->stash->{zaak}->aanvrager_object &&
        $c->stash->{zaak}->aanvrager_object->can('messages') &&
        $c->stash->{zaak}->aanvrager_object->messages &&
        scalar(keys %{ $c->stash->{zaak}->aanvrager_object->messages })
    ) {
        $c->push_message(
            'Let op: '
                . join(', ',
                    map(
                        { ucfirst($_) }
                        values %{ $c->stash->{zaak}->aanvrager_object->messages }
                    )
                ),
        );
    }

    if (
        $c->stash->{zaak}->payment_status &&
        $c->stash->{zaak}->payment_status ne CASE_PAYMENT_STATUS_SUCCESS
    ) {
        $c->push_flash_message(
            {
                message     => 'Let op, betaling niet succesvol',
                type        => 'error'
            }
        );
    }

    ### Is direct request, show flash messages directly
    if (!$c->req->is_xhr) {
        my @messages =( UNIVERSAL::isa($c->flash->{result}, 'ARRAY')
            ? @{ $c->flash->{result} }
            : $c->flash->{result}
        );

        $c->push_message(
            $_
        ) for grep { $_ } @messages;

        delete($c->flash->{result});
    }
}

{
    my $ELEMENT_MAP = {
        'zaak-elements-notes'       =>
            'zaak/elements/notes.tt',
        'zaak-elements-status'      =>
            'zaak/elements/status.tt',
        'load_algemene_zaakinformatie'      =>
            'zaak/elements/view_algemene_zaakinformatie.tt',
        'load_element_maps'      =>
            'zaak/elements/view_maps.tt',
    };

    sub view_element : Chained('base'): PathPart('view_element'): Args(1) {
        my ($self, $c, $element) = @_;

        unless ($ELEMENT_MAP->{$element}) {
            $c->res->redirect($c->uri_for(
                '/zaak/' . $c->stash->{zaak}->nr
            ));

            $c->detach;
        }

        $c->stash->{nowrapper}  = 1;
        $c->stash->{template}   = $ELEMENT_MAP->{$element};
    }
}

#
# home page, forwarded from Controller/Root.pm
#
sub list : Chained('/'): PathPart('zaak/list'): Args(0) {
    my ($self, $c) = @_;

	my $params = $c->req->params();
    my $view = $c->req->params->{view};

    unless ($params->{order}) {
        $c->stash->{order} = 'last_modified';
        $c->stash->{order_direction} = 'DESC';
    }


    ### Default descending (because order is by id)
    $c->stash->{order_direction} = 'DESC' unless
        ($c->stash->{order_direction} || $c->stash->{order});


    $c->stash->{show_more_search_queries} = 1;
    $c->forward('/search/dashboard');

    $c->stash->{template} = 'zaak/list.tt';

    ## paging
    my $paging_rows = 10;
    
    if($params->{results_per_page}) {
        $paging_rows = $params->{results_per_page};
    }

    $c->stash->{results_per_page} = $paging_rows;


    ## zaken openstaand
    my $zaken_openstaand_resultset = $c->model('Zaken')->openstaande_zaken({ 
    	page        => ($params->{'page'} || 1), 
    	rows        => $paging_rows,
    	uidnumber   => $c->user->uidnumber,
        'sort_direction'        => $params->{sort_direction},
        'sort_field'            => $params->{sort_field},
    })->with_progress();

	# use the central filter code to handle the dropdown and textfilter limiting filter options    
    $zaken_openstaand_resultset = $c->model('Zaken')->filter({
    	resultset 	=> $zaken_openstaand_resultset,
    	textfilter  => $params->{openstaand_textfilter},
		dropdown	=> $params->{statusfilter},
    });


    $c->stash->{'zaken_openstaand'} = $zaken_openstaand_resultset; 

    ### zaken intake
     my $zaken_intake_resultset = $c->model('Zaken')->intake_zaken({ 
     	page => ($params->{'page'} || 1), 
     	rows => $paging_rows,
    	user_roles_ids => [$c->user_roles_ids],
        user_ou_id     => $c->user_ou_id,
        user_roles     => [$c->user_roles],
    	uidnumber      => $c->user->uidnumber,
        'sort_direction'        => $params->{sort_direction},
        'sort_field'            => $params->{sort_field},
     })->with_progress();

	# use the central filter code to handle the dropdown and textfilter limiting filter options    
    $zaken_intake_resultset = $c->model('Zaken')->filter({
    	resultset 	   => $zaken_intake_resultset,
    	textfilter     => $params->{intake_textfilter},
		dropdown	   => $params->{statusfilter},
    });

    $c->stash->{'zaken_intake'} = $zaken_intake_resultset; 




	# get the default zaak search output structure from the SearchQuery object    
    my $search_query = $c->model('SearchQuery');
    $c->stash->{'display_fields'} = $search_query->get_display_fields();	

    $c->stash->{ $_ }     = $params->{ $_ }
            for qw/sort_direction sort_field/;
}


my $FILTER_MAP = {
    'new'       => ' AND Status="new"',
    'open'      => ' AND Status="open"',
    'stalled'   => ' AND Status="stalled"',
    'resolved'  => ' AND Status="resolved"',
    'urgent'    => 1,
};
sub own : Chained('/'): PathPart('zaak/list/own'): Args(0) {
    my ($self, $c) = @_;

	my $params = $c->req->params();
	
    my $view    = $c->req->params->{view} || '';

#    my $sql = {
#        'own'   => $self->_get_query_for_zaken($c)
#    };

	my $where = {'me.deleted' => undef};

	my $betrokkenen = $c->model('DB::ZaakBetrokkenen')->search({
		'gegevens_magazijn_id' => $c->user->uidnumber,		
	});

	$where->{'behandelaar'} = {-in => $betrokkenen->get_column('id')->as_query};

    my $sort_field = $params->{'sort_field'} || 'me.id';
    $c->stash->{'sort_field'} = $sort_field;
    
    my $sort_direction = $params->{'sort_direction'} || 'DESC';
    $c->stash->{'sort_direction'} = $sort_direction;
    my $order_by = { '-' . $sort_direction => $sort_field};

    $where->{'me.status'} = $c->req->params->{statusfilter} if
        $c->req->params->{statusfilter};
 
    ### Default descending (because order is by id)
    $c->stash->{order_direction} = 'DESC' unless
        ($c->stash->{order_direction} || $c->stash->{order});


    my $paging_rows = 10;
    
    if($params->{results_per_page}) {
        $paging_rows = $params->{results_per_page};
    }

    $c->stash->{results_per_page} = $paging_rows;

	my $page = $params->{'page'} || 1;

    $where->{'me.deleted'} = undef;

    
    my $resultset = $c->model('DB::Zaak')->search_extended($where, {
    	page     => $page,
    	rows	 => $paging_rows,
    	order_by => $order_by,
    })->with_progress();

	# use the central filter code to handle the dropdown and textfilter limiting filter options    
    $resultset = $c->model('Zaken')->filter({
    	resultset 	=> $resultset,
    	dropdown    => $params->{'filter'}, 
    	textfilter  => $params->{'textfilter'},
    });

    $c->stash->{'results'} = $resultset;   

	# get the default zaak search output structure from the SearchQuery object    
    my $search_query = $c->model('SearchQuery');
    $c->stash->{'display_fields'} = $search_query->get_display_fields();	
    $c->stash->{'template'} = 'zaak/own.tt';
}


sub eenheid : Chained('/'): PathPart('zaak/list/eenheid'): Args() {
    my ($self, $c, $filter) = @_;

	my $params = $c->req->params();
    my $view    = $c->req->params->{view};


    my $sort_field = $params->{'sort_field'} || 'me.id';
    $c->stash->{'sort_field'} = $sort_field;
    
    my $sort_direction = $params->{'sort_direction'} || 'DESC';
    $c->stash->{'sort_direction'} = $sort_direction;
    my $order_by = { '-' . $sort_direction => $sort_field};


	my $where = {
		'route_ou' => $c->user_ou_id,
	};
    $where->{'me.status'} = $c->req->params->{statusfilter} if
        $c->req->params->{statusfilter};

	my $page = $params->{'page'} || 1;

    my $paging_rows = 10;
    
    if($params->{results_per_page}) {
        $paging_rows = $params->{results_per_page};
    }

    $c->stash->{results_per_page} = $paging_rows;
    $where->{'me.deleted'} = undef;

    my $resultset = $c->model('DB::Zaak')->search_extended($where, {
    	page     => $page,
    	rows	 => $paging_rows,
    	order_by => $order_by,
    })->with_progress();

	# use the central filter code to handle the dropdown and textfilter limiting filter options    
    $resultset = $c->model('Zaken')->filter({
    	resultset 	=> $resultset,
    	dropdown    => $params->{'filter'}, 
    	textfilter  => $params->{'textfilter'},
    });

    $c->stash->{'results'} = $resultset;   

	# get the default zaak search output structure from the SearchQuery object    
    my $search_query = $c->model('SearchQuery');
    $c->stash->{'display_fields'} = $search_query->get_display_fields();
    $c->stash->{'template'} = 'zaak/eenheid.tt';
}

sub intake : Chained('/'): PathPart('zaak/intake'): Args(0) {
    my ($self, $c) = @_;

    my $view    = $c->req->params->{view};

    $c->stash->{'template'} = 'zaak/intake.tt';

    my $bid     = $c->user->uidnumber;
    my $ou_id   = $c->user_ou_id;

     $c->stash->{'dropped_documents'} = $c->model('DB::File')->search({
        accepted => 0,
        case_id => undef,
        subject_id => undef
    }, {
        order_by => { '-desc' => 'id'}
    });
}

sub zaaktypeinfo : Chained('base'): PathPart('zaaktypeinfo'): Args(0) {
    my ($self, $c) = @_;

    $c->stash->{nowrapper} = 1;
    $c->stash->{template} = 'zaak/zaaktypeinfo.tt'
}

sub fresh : Chained('/') : PathPart('fresh') : Args() {
    my ($self, $c, $nowrapper) = @_;

    $c->stash->{template} = 'zaak/fresh.tt';
    $c->stash->{nowrapper} = $nowrapper;
}

sub case_documents : JSON : Chained('base') : PathPart('case_documents') { 
    my ($self, $c) = @_;

    my $case      = $c->stash->{ zaak };

    my $case_type_properties = $case->zaaktype_node_id->zaaktype_kenmerken;

    my $case_documents  = $case_type_properties->search(
        {
            'bibliotheek_kenmerken_id.value_type' => 'file',    
        },
        {
            join => [
                'bibliotheek_kenmerken_id',
            ],
            prefetch => [
                'zaaktype_node_id',
                'zaak_status_id',
                { zaaktype_node_id => 'zaaktype_definitie_id' },
                { zaaktype_node_id => 'zaaktype_id' },
                { bibliotheek_kenmerken_id => 'bibliotheek_categorie_id' }

            ]
        }
    );

    $c->{stash}->{json} = $case_documents;
    $c->forward('Zaaksysteem::View::JSON');
}

sub get_sjablonen : JSON : Regex('^zaak/(\d+)/get_sjablonen') {
    my ($self, $c, $id) = @_;
    my ($case_id)       = @{$c->req->captures};
    my ($sjablonen);

    my $case            = $c->model('DB::Zaak')->find($case_id);

    if (
        $c->req->params->{type} &&
        $c->req->params->{type} eq 'notifications'
    ) {
        my $case_type_sjablonen = $case
                                ->zaaktype_node_id
                                ->zaaktype_notificaties;
        $sjablonen = $case_type_sjablonen->search(
            {},{join => 'bibliotheek_notificaties_id'}
        );
    } else {
        my $case_type_sjablonen = $case->zaaktype_node_id->zaaktype_sjablonen;
        $sjablonen = $case_type_sjablonen->search(
            {},{join => 'bibliotheek_sjablonen_id'}
        );
    }

    $c->{stash}->{json} = $sjablonen;
    $c->forward('Zaaksysteem::View::JSON');
}

=head2 send_mail

Send an email.

=head3 Arguments

=over

=item recipient [required]

Who the mail gets delivered to.

=item subject [required]

Subject of the mail.

=item from [optional]

Who the mail is sent as. Defaults as the value set in the config. And if that is
missing as well it will use the system default.

=item body [required]

Content of the mail.

=item case_id [required]

=item case_document_ids [optional]

A list of the case document (zaaktype_kenmerken.id) IDs that should get
attached.

=item file_attachments [optional]

A list of the file IDs you wish to send with this message.

=back

=head3 Location

POST: /zaak/send_mail

=head3 Returns

True upon message send success.

=cut

sub send_mail : JSON : Chained('/'): PathPart('zaak/send_mail') {
    my ($self, $c) = @_;

    try {
        my %params  = %{ $c->req->params };

        my $case = $c->model('DB::Zaak')->find($params{case_id});

        if (!$case) {
            die sprintf "send_mail: case with id '%d' not found", $params{case_id};
        }

        $params{recipient} = _resolve_recipient($c, $params{recipient}, $params{recipient_type}, $case);

        my $ztt = Zaaksysteem::ZTT->new_from_case($case);

        $params{ body } = $ztt->process_template($params{ body })->string;
        $params{ subject } = $ztt->process_template($params{ subject })->string;

        my $from = $params{from} ? $c->customer_instance->{start_config}->{customer_info}->{zaak_email} : undef;

        # Prepare the actual mail
        my $msg  = MIME::Lite->new(
            From    => $from,
            To      => $params{recipient},
            Subject => $params{subject},
            Type    => 'multipart/mixed'
        );

        # Set the main body in the first part
        $msg->attach(
            Type => 'TEXT',
            Data => $params{body},
        );

        my @contactmoment_attachments;
        if ($params{file_attachments}) {
            my $files = $c->model('DB::File')->search({id => $params{file_attachments}});
            while (my $f = $files->next) {
                $msg->attach(
                    Type        => $f->filestore->mimetype,
                    FH          => $f->filestore->ustore->get($f->filestore->uuid),
                    Filename    => $f->filename,
                    Disposition => 'attachment'
                );
                push @contactmoment_attachments, {
                    filename => $f->filename,
                    file_id  => $f->id,
                    email_filename => $f->filename,
                };
            }
        }

        if ($params{case_document_ids} && $params{case_id}) {
            my $files = $c->model('DB::File')->search(
                {
                    'case_documents.case_document_id' => [$params{case_document_ids}],
                    date_deleted => undef,
                    accepted     => 1,

                },
                {
                    join => {case_documents =>'file_id'},
                }
            );
            while (my $f = $files->next) {
                # If a document has already been attached as a file, there's no need to do it again.
                my $exists_as_attachment = grep {$_->{file_id} eq $f->id} @contactmoment_attachments;
                next if $exists_as_attachment;

                $msg->attach(
                    Type        => $f->filestore->mimetype,
                    FH          => $f->filestore->ustore->get($f->filestore->uuid),
                    Filename    => $f->filename,
                    Disposition => 'attachment',
                );
                push @contactmoment_attachments, {
                    filename => $f->filename,
                    file_id  => $f->id,
                    email_filename => $f->filename,
                };
            }
        }
        $params{attachments} = \@contactmoment_attachments;

        # Add contactmoment
        my $cm = $c->model('DB::Contactmoment')->contactmoment_create(
            {
                type       => 'email',
                created_by => _get_subject($c)->betrokkene_identifier,
                medium     => 'behandelaar',
                case_id    => $params{case_id},
                email      => \%params,
            }
        );

        $c->{stash}->{json} = $msg->send;
        $c->forward('Zaaksysteem::View::JSON');
    }
    catch {
        $c->{stash}->{json} = $c->format_error($_);
        $c->forward('Zaaksysteem::View::JSON');   
    }    
}

sub _get_subject {
    my ($c) = @_;
    return $c->model('Betrokkene')->get(
        {
            intern  => 0,
            type    => 'medewerker',
        },
        $c->user->uidnumber,
    );
}

sub _resolve_recipient {
    my ($c, $recipient, $recipient_type, $case) = @_;

    # 'Resolve' the recipient to a usable address
    if ($recipient_type eq 'aanvrager') {
        $recipient = $case->aanvrager_object->email;
    }

    if ($recipient_type eq 'coordinator') {
        $recipient = $case->coordinator_object->email;
    }

    if ($recipient_type eq 'medewerker_uuid') {
        $recipient = $c->model('Betrokkene')->get(
            {
                intern  => 0,
                type    => 'medewerker',
            },
            $recipient,
        )->email;
    }

    if ($recipient_type eq 'custom_address') {
        my $ztt = Zaaksysteem::ZTT->new_from_case($case);

        $recipient = $ztt->process_template($recipient)->string;
    }

    return $recipient;
}

1;

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE
The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut
