package Zaaksysteem::DB::ResultSet::ZaaktypeStatusChecklistItem;

use Moose;

use Data::Dumper;

BEGIN { extends 'DBIx::Class::ResultSet', 'Zaaksysteem::Zaaktypen::BaseResultSet'; }

around '_commit_session' => sub {
    my ($orig, $self, $node, $data, $options) = @_;

    $options //= { status_id_column_name => 'casetype_status_id' };

    return $self->$orig($node, $data, $options);
};

around 'create' => sub {
    my ($orig, $self, $data) = @_;

    delete($data->{ zaaktype_node_id });

    $self->$orig($data);
};

1;
