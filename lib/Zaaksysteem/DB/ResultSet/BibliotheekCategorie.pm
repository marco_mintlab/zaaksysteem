package Zaaksysteem::DB::ResultSet::BibliotheekCategorie;

use strict;
use warnings;

use Moose;


extends 'DBIx::Class::ResultSet';

__PACKAGE__->load_components(qw/Helper::ResultSet::SetOperations/);


1;
