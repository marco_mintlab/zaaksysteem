package Zaaksysteem::DB::Component::Logging::User::Role::Add;

use Moose::Role;

sub onderwerp {
    my $self = shift;

    sprintf(
        'Rol %s succesvol toegevoegd aan medewerker: %s',
        $self->data->{ role_dn },
        $self->data->{ entry_dn }
    );
}

1;
