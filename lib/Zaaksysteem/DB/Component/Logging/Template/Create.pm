package Zaaksysteem::DB::Component::Logging::Template::Create;

use Moose::Role;

sub onderwerp {
    my $self = shift;

    sprintf(
        'Sjabloon "%s" toegevoegd: %s',
        $self->template->naam,
        $self->data->{ reason }
    );
}

sub _add_magic_attributes {
    shift->meta->add_attribute('template' => ( is => 'ro', lazy => 1, default => sub {
        my $self = shift;

        $self->result_source->schema->resultset('BibliotheekSjablonen')->find($self->data->{ template_id });
    }));
}

1;
