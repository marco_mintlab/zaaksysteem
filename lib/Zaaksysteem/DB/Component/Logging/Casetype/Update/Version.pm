package Zaaksysteem::DB::Component::Logging::Casetype::Update::Version;

use Moose::Role;

has casetype => ( is => 'ro', lazy => 1, default => sub {
    my $self = shift;

    $self->rs('Zaaktype')->find($self->data->{ casetype_id });
});

sub onderwerp {
    my $self = shift;

    sprintf(
        'Versie van zaaktype "%s" opgehoogd naar %s: %s',
        $self->casetype->title // '[ONGELDIG ZAAKTYPE ID]',
        $self->data->{ version },
        $self->data->{ reason }
    );
}

1;
