package Zaaksysteem::DB::Component::Logging::Casetype::Unpublish;

use Moose::Role;

has casetype => ( is => 'ro', lazy => 1, default => sub {
    my $self = shift;

    $self->result_source->schema->resultset('Zaaktype')->find($self->data->{ casetype_id });
});

sub onderwerp {
    my $self = shift;

    sprintf(
        'Zaaktype "%s" gedeactiveerd: %s',
        $self->casetype->title,
        $self->data->{ reason }
    );
}

1;
