package Zaaksysteem::DB::Component::Logging::Case::Suspend;

use Moose::Role;

sub onderwerp {
    my $self = shift;

    sprintf(
        'Zaak %s opgeschort: %s',
        $self->data->{ case_id },
        $self->data->{ reason }
    );
}

sub event_category { 'case-mutation'; }

1;
