package Zaaksysteem::DB::Component::Logging::Case::Subcase;

use Moose::Role;

has subcase => ( is => 'ro', lazy => 1, default => sub {
    my $self = shift;

    $self->rs('Zaak')->find($self->data->{ subcase_id });
});

sub onderwerp {
    my $self = shift;

    my %mapping = (
        deelzaak => 'Deelzaak',
        vervolgzaak => 'Vervolgzaak',
        vervolgzaak_datum => 'Vervolgzaak',
        gerelateerd => 'Gerelateerde zaak'
    );

    my $type = $mapping{ $self->data->{ type }} // 'Zaak';

    return sprintf(
        "%s (%s) aangemaakt",
        $type,
        $self->subcase->zaaktype_node_id->titel
    );
}

1;
