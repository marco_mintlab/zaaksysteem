package Zaaksysteem::DB::Component::Logging::Case::Document::Publish;

use Moose::Role;

sub onderwerp {
    my $self = shift;

    my %mapping = (
        'pip' => 'PIP',
        'website' => 'website'
    );

    return sprintf(
        'Document "%s" gepubliceerd op de %s',
        $self->file->name,
        $mapping{ $self->data->{ publish_to } } // $self->data->{ publish_to }
    );
}

1;
