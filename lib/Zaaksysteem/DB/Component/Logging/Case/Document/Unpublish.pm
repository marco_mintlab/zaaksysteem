package Zaaksysteem::DB::Component::Logging::Case::Document::Unpublish;

use Moose::Role;

sub onderwerp {
    my $self = shift;

    my %mapping = (
        'pip' => 'PIP',
        'website' => 'website'
    );

    return sprintf(
        'Publicatie van Document "%s" op de %s teruggetrokken',
        $self->file->name,
        $mapping{ $self->data->{ publish_to } } // $self->data->{ publish_to }
    );
}

1;
