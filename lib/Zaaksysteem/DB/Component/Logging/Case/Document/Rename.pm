package Zaaksysteem::DB::Component::Logging::Case::Document::Rename;

use Moose::Role;

sub onderwerp {
    my $self = shift;

    return sprintf(
        'Document hernoemd naar "%s"',
        $self->data->{ renamed_to }
    );
}

1;
