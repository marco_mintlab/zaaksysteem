package Zaaksysteem::DB::Component::Logging::Case::Update::Result;

use Moose::Role;

sub onderwerp {
    my $self = shift;

    if($self->data->{ old_result }) {
        return sprintf(
            'Zaakresultaat gewijzigd van %s naar %s',
            $self->data->{ old_result },
            $self->data->{ result }
        );
    } else {
        return sprintf(
            'Zaakresultaat ingesteld op %s',
            $self->data->{ result }
        );
    }
}

sub event_category { 'case-mutation'; }

1;
