package Zaaksysteem::DB::Component::Logging::Case::Update::Status;

use Moose::Role;

sub onderwerp {
    sprintf('Status gewijzigd naar: %s', shift->data->{ status } // '<onbekend>');
}

sub event_category { 'case-mutation'; }

1;
