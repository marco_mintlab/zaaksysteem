package Zaaksysteem::DB::Component::Logging::Case::Update::Milestone;

use Moose::Role;

has phase => ( is => 'ro', lazy => 1, default => sub {
    my $self = shift;
    
    $self->result_source->schema->resultset('ZaaktypeStatus')->find($self->data->{ phase_id });
});

sub onderwerp {
    my $self = shift;

    if($self->data->{ admin }) {
        return sprintf('Fase aangepast naar "%s"', $self->phase->fase);
    } else {
        return sprintf('%s afgerond', $self->phase->fase);
    }
}

sub event_category { 'case-mutation'; }

1;
