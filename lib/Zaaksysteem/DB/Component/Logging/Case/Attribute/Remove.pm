package Zaaksysteem::DB::Component::Logging::Case::Attribute::Remove;

use Moose::Role;

has attribute => ( is => 'ro', lazy => 1, default => sub {
    my $self = shift;

    $self->result_source->schema->resultset('BibliotheekKenmerken')->find($self->data->{ attribute_id });
});

sub onderwerp {
    my $self = shift;

    sprintf(
        'Kenmerk "%s" verwijderd: %s.',
        $self->attribute->naam,
        $self->data->{ reason }
    );
}

sub event_category { 'case-mutation'; }

1;
