package Zaaksysteem::DB::Component::Logging::Case::Payment::Status;

use Moose::Role;

sub onderwerp {
    my $self = shift;

    sprintf(
        'Betalings status gewijzigd %d : %s',
        $self->data->{ case_id },
        $self->data->{ reason }
    );
}

sub event_category { 'case-mutation'; }

1;
