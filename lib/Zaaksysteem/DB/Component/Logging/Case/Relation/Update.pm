package Zaaksysteem::DB::Component::Logging::Case::Relation::Update;

use Moose::Role;

sub onderwerp {
    my $self = shift;

    return sprintf(
        '%s ingesteld op "%s"',
        $self->data->{ subject_relation },
        $self->data->{ subject_name }
    );
}

sub event_category { 'case-mutation'; }

1;
