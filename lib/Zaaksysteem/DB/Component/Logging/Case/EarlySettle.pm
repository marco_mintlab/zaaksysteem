package Zaaksysteem::DB::Component::Logging::Case::EarlySettle;

use Moose::Role;

sub onderwerp {
    my $self = shift;

    sprintf(
        'Zaak %s is vroegtijdig afgehandeld: %s',
        $self->data->{ case_id },
        $self->data->{ reason }
    );
}

sub event_category { 'case-mutation'; }

1;
