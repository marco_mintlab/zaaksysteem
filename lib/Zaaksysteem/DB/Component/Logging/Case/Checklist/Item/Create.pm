package Zaaksysteem::DB::Component::Logging::Case::Checklist::Item::Create;

use Moose::Role;

sub onderwerp {
    my $self = shift;

    sprintf(
        'Checklist item "%s" toegevoegd',
        $self->data->{ label }
    );
}

sub event_category { 'case-mutation'; }

1;
