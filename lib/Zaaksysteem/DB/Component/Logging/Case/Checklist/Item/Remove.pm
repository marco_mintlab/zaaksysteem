package Zaaksysteem::DB::Component::Logging::Case::Checklist::Item::Remove;

use Moose::Role;

sub onderwerp {
    my $self = shift;

    sprintf(
        'Checklist item "%s" verwijderd',
        $self->data->{ label }
    );
}

sub event_category { 'case-mutation'; }

1;
