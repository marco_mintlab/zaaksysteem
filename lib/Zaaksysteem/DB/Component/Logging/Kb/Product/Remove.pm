package Zaaksysteem::DB::Component::Logging::Kb::Product::Remove;

use Moose::Role;

sub onderwerp {
    my $self = shift;

    sprintf(
        'Kennisbank product "%s" verwijderd: %s',
        $self->product->naam,
        $self->data->{ reason }
    );
}

sub _add_magic_attributes {
    shift->meta->add_attribute('product' => (is => 'ro', lazy => 1, default => sub {
        my $self = shift;

        $self->result_source->schema->resultset('KennisbankProducten')->find($self->data->{ product_id });
    }));
}

sub event_category { 'product' }

1;
