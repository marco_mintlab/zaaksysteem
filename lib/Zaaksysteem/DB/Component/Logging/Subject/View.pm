package Zaaksysteem::DB::Component::Logging::Subject::View;

use Moose::Role;

sub onderwerp {
    my $self    = shift;

    sprintf(
        'GBA Betrokkene "%s", BSN: "%d" opgevraagd.',
        $self->data->{name},
        $self->data->{identifier}
    );
}

1;
