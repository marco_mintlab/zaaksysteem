package Zaaksysteem::DB::Component::Logging::Subject::Contactmoment::Create;

use Moose::Role;

sub onderwerp {
    sprintf('Contactmoment toegevoegd');
}

sub event_category { 'contactmoment' };

1;
