package Zaaksysteem::DB::Component::Logging::Woz::Import::Upload;

use Moose::Role;

sub onderwerp {
    my $self = shift;

    sprintf(
        'Nieuw WOZ importbestand geupload (%s)',
        $self->data->{ file_id }
    );
}

1;
