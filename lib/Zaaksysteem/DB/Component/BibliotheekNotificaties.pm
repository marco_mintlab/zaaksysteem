package Zaaksysteem::DB::Component::BibliotheekNotificaties;

use strict;
use warnings;


use base qw/DBIx::Class/;


sub insert {
    my ($self) = @_;

    # we need the database to generate the new searchable_id param. 
    # if we don't supply it, it will take its chance. so get rid
    # of whatever the app thinks it should pass.
    # is there a way to put this in the Schema? that's be less hacky.
    delete $self->{_column_data}->{searchable_id};

    $self->_set_search_term();
    return $self->next::method(@_);}

sub update {
    my ($self) = shift;

    $self->_set_search_term();
    return $self->next::method(@_);
}

sub TO_JSON {
    my $self = shift;


    my @attachments = $self->bibliotheek_notificatie_kenmerks->all;

    my @files = map {
        my $result = {
            naam  => $_->bibliotheek_kenmerken_id->naam,
            bibliotheek_kenmerk_id => $_->bibliotheek_kenmerken_id->id,
        };
        $result;
    } @attachments;

    return {
        attachments => \@files,
        label       => $self->label,
        message     => $self->message,
        subject     => $self->subject,
        %{ $self->next::method() },
    }  
}

sub _set_search_term {
    my ($self) = @_;
    my $search_term = '';
    if($self->label) {
        $search_term .= $self->label;
    }
    if($self->subject) {
        $search_term .= $self->subject;
    }
    if($self->message) {
        $search_term .= $self->message;
    }
    $self->search_term($search_term);
}


1;
