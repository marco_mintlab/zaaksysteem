package Zaaksysteem::DB::Component::Checklist;

use Moose;

use Data::Dumper;

BEGIN { extends 'DBIx::Class'; }

sub TO_JSON {
    my $self = shift;

    return {
        id => $self->id,
        milestone => $self->case_milestone,
        items => [ $self->checklist_items ]
    };
}

1;
