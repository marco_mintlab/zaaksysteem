package Zaaksysteem::DB::Component::BagNummeraanduiding;

use strict;
use warnings;

use base qw/Zaaksysteem::DB::Component::BagGeneral Zaaksysteem::Geo::BAG/;

sub nummeraanduiding {
    my $self    = shift;

    return $self->huisnummer . ($self->huisletter || '').
        ($self->huisnummertoevoeging  ? '-' . $self->huisnummertoevoeging : '');
}

=head2 geocode_term

Return value: $geocode_address

Generate a term for geocoding this location with a googlemaps or other
geocoder.

=cut

sub geocode_term {
    my $self    = shift;

    my @terms   = ('Nederland');

    push(@terms, $self->openbareruimte->woonplaats->naam);

    my $address = $self->openbareruimte->naam;
    $address    .= ' ' . $self->huisnummer;
    $address    .= $self->huisletter if $self->huisletter;
    $address    .= ' - ' . $self->huisnummertoevoeging
        if $self->huisnummertoevoeging;

    push(
        @terms,
        $address
    );

    return join(',', @terms);
}

sub to_string {
    my $self = shift;
    
    return sprintf(
        '%s %s, %s %s',
        $self->openbareruimte->naam,
        $self->nummeraanduiding,
        $self->postcode,
        $self->openbareruimte->woonplaats->naam
    );
}

sub TO_JSON {
    my $self = shift;

    return {
        $self->get_columns,
        id => 'nummeraanduiding-' . $self->identificatie,
        type => 'address',
        number => $self->nummeraanduiding,
        street => $self->openbareruimte->naam,
        city => $self->openbareruimte->woonplaats->naam,
        postal_code => $self->postcode
    };
}

1;
