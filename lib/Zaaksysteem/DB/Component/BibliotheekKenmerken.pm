package Zaaksysteem::DB::Component::BibliotheekKenmerken;

use Moose;

use strict;
use warnings;

use Zaaksysteem::Constants qw/ZAAKSYSTEEM_CONSTANTS/;

extends 'Zaaksysteem::Backend::Component';

sub options {
    my ($self) = @_;
    my (@kenmerk_options);

    if (
        exists(ZAAKSYSTEEM_CONSTANTS->{veld_opties}->{
            $self->value_type
        }->{multiple}) &&
        ZAAKSYSTEEM_CONSTANTS->{veld_opties}->{
            $self->value_type
        }->{multiple}
    ) {
        my $values = $self->bibliotheek_kenmerken_values->search(
            {},
            {
                order_by    => { -asc   => 'id' }
            }
        );
        while (my $value = $values->next) {
            push(@kenmerk_options, $value->value);
        }
    }

    return \@kenmerk_options;
}

sub insert {
    my ($self) = @_;

    # we need the database to generate the new searchable_id param. 
    # if we don't supply it, it will take its chance. so get rid
    # of whatever the app thinks it should pass.
    # is there a way to put this in the Schema? that's be less hacky.
    delete $self->{_column_data}->{searchable_id};

    $self->_set_search_term();
    return $self->next::method(@_);}

sub update {
    my ($self) = shift;

    $self->_set_search_term();
    return $self->next::method(@_);
}

sub _set_search_term {
    my ($self) = @_;
    my $search_term = '';
    if($self->naam) {
        $search_term .= $self->naam;
    }
    $self->search_term($search_term);
}

=head2 Attributes for L<Zaaksysteem::Backend::Component::Searchable>

=cut

has '_searchable_object_id' => (
    is      => 'ro',
    lazy    => 1,
    default => sub { return shift->id; }
);


has '_searchable_object_label' => (
    is      => 'ro',
    lazy    => 1,
    default => sub { return shift->naam; }
);


has '_searchable_object_description' => (
    is      => 'ro',
    lazy    => 1,
    default => sub { return shift->naam; }
);

=head2 JSON Extension for L<Zaaksysteem::Backend::Component>

=cut


before 'TO_JSON' => sub {
    my $self                    = shift;

    $self->_json_data(
        {
            %{ $self->_json_data },
            $self->get_columns,
        }
    );
};



1;
