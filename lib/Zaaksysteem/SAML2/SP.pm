package Zaaksysteem::SAML2::SP;

use Moose;

BEGIN { extends 'Net::SAML2::SP'; }

use Zaaksysteem::Profile;
use Zaaksysteem::Exception;

=head1 SAML2::SP

This class wraps L<Net::SAML2::SP> for a bit nicer integration with
L<Zaaksysteem>.

=head1 Example usage

    my $sp = Zaaksysteem::SAML2::SP->new_from_interface(
        $my_interface_object
    );

    my $metadata = $sp->metadata;

=head1 Methods

=head2 new_from_interface

This methods wraps the logic of building a SP object from an interface
definition. Works like the normal C<new> function, except that it requires
an interface object be supplied as the first argument. Remaining parameters
are interpreted as a hash passed directly to C<new>.

=head3 interface

An L<Zaaksysteem::Backend::Sysin::Interface::Component> instance.

=cut

define_profile new_from_interface => (
    required => [qw[interface]],

    # Allow passthru of all original attributes
    optional => [qw[id url cert cacert org_contact org_display_name org_name]],

    constraint_methods => {
        # Certificates need to be existing, readable files.
        cert => sub { -e pop },
        cacert => sub { -e pop },
    },

    typed => {
        interface => 'Zaaksysteem::Backend::Sysin::Interface::Component'
    },

    # Provide basic input sanitation
    field_filters => {
        org_contact => sub { "$_[0]" },
        org_display_name => sub { "$_[0]" },
        org_name => sub { "$_[0]" }
    }
);

sub new_from_interface {
    my $self = shift;
    my %params = @_;

    my $opts = assert_profile(\%params)->valid;

    my $interface = delete $opts->{ interface };

    return __PACKAGE__->new(
        id => $interface->id,
        url => $interface->base_url,
        cert => $interface->signing_certificate,
        cacert => $interface->ca_certificate,
        org_contact => $interface->contact_email,
        org_display_name => $interface->contact_display_name,
        org_name => $interface->organisation_name,
        %{ $opts }
    );
}

define_profile just_new => (
    required => [qw[a]],
    optional => [qw[b]],
    typed => {
        a => 'Int',
        b => 'Str'
    }
);

sub just_new {
    my $self = shift;
    my %params = @_;

    my $opts = assert_profile(\%params)->valid;
}

1;
