package Zaaksysteem::SAML2::IdP;

use Moose;

BEGIN { extends 'Net::SAML2::IdP'; }

use Zaaksysteem::Exception;
use Zaaksysteem::Profile;

use LWP::UserAgent;

use XML::XPath;
use XML::XPath::Node::Element;
use XML::XPath::Node::Text;

has 'interface'         => (
    is  => 'rw',
    isa => 'Zaaksysteem::Model::DB::Interface',
);

=head1 SAML2::IdP

This class wraps L<Net::SAML2::IdP> for a bit nicer integration with
L<Zaaksysteem>. In here you can do some better/fancier validation of the
object's initialization, and extend the default constructors with subs
that hook into Zaaksysteem's behavior.

=head1 Example usage

In the context of Zaaksysteem, we are the SP, not the IdP in the SAML process.
This means that we merely use this IdP package to model the authenticating side
(So DigID, E-Herkenning, Google, whoever). This class retrieves the metadata
and allows you to retrieve a base URL for doing useful things with the IdP.

The following snippet gets the base URL for single-sign-on.

    my $idp = Zaaksysteem::SAML2::IdP->new_from_url(
        url => 'https://url.to/idp/metadata',
        cacert => '/path/to/ssl/certificate/that/signs/idp/messages'
    );

    my $sso_base_url = $idp->sso_url('urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect');

You can then use this base URL to generate a full HTTP Redirect url that can
be used to redirect the user to the IdP servers for authentication.

=head2 Common pitfalls

This object requires you supply a PEM certificate(-chain) that is being used to
sign the messages coming from the IdP. B<That doesn't need to be the same
CA-certificate your SP certificate and key were signed under!>

# =head1 Methods

# =head2 new_from_interface

# This convenience constructor makes it easy to generate an IdP interface object
# from a L<Zaaksysteem::Backend::Sysin::Interface::Component> object (or deriving
# modules).

# =head3 Parameters

# =over 4

# =item interface

# Required argument, assumed and validated to be an instance of
# L<Zaaksysteem::Backend::Sysin::Interface::Component>

# =back

# =cut

# define_profile new_from_interface => (
#     required => [qw[interface]],
#     constraint_methods => {
#         interface => sub { eval { $_->isa('Zaaksysteem::Backend::Sysin::Interface::Component') } }
#     }
# );

# sub new_from_interface {
#     my ($class, %args) = @_;

#     my $opts = assert_profile(\%args)->valid;

#     return $class->new_from_url(
#         url         => $opts->{ interface }->url,
#         cacert      => $opts->{ interface }->cacert,
#     );
# }

=head2 new_from_url

Overriding constructor that does the same as C<new_from_url> from
L<Net::SAML2::IdP> but doesn't die, and throws proper exceptions on failure.

This method also verifies the existence of a C<NameIDFormat> element in the
metadata XML being retrieved from the IdP, and injects a default value with
L</patch_missing_nameid_format>.

=head3 Parameters

=over 4

=item url

String value of the URL where the IdP's metadata can be found.

=item cacert

A file containing the certificate that was used to sign the metadata returned
by the IdP. This can be a different CA certificate than the one used to sign
the SP's certificates.

=back

=cut

define_profile new_from_url => (
    required => [qw[url cacert interface]],
    constraint_methods => {
        cacert => sub { -e pop }
    }
);

sub new_from_url {
    my ($class, %args) = @_;

    my $opts = assert_profile(\%args)->valid;

    my $ua = LWP::UserAgent->new;
    my $response = $ua->get($opts->{ url });

    unless($response->is_success) {
        throw('saml2/idp/metadata', sprintf(
            'Could not retrieve IdP metadata from URL "%s" (status: %s)',
            $opts->{ url },
            $response->code
        ));
    }

    my $self = $class->new_from_xml(
        xml         => $response->content,
        cacert      => $opts->{ cacert },
    );

    $self->interface($opts->{ interface });

    return $self;
}

# Hotfix for metadatas without any nameid formats specified
around BUILDARGS => sub {
    my $orig = shift;
    my $self = shift;

    my %args = @_;

    unless(exists $args{ defualt_format }) {
        $args{ default_format } = 'entity';
        $args{ formats } = {
            entity => 'urn:oasis:names:tc:SAML:2.0:nameid-format:entity'
        };
    }

    return $self->$orig(%args);
};

1;
