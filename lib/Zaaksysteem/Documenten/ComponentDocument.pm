package Zaaksysteem::Documenten::ComponentDocument;

use strict;
use warnings;

use Moose;

use Data::Dumper;

extends 'DBIx::Class';

use constant RELATIONSHIPS  => [
    'notitie_id',
    'filestore_id'
];

sub delete_document {
    my ($self)          = @_;

    my $relationships   = RELATIONSHIPS;

    $self->result_source->schema->txn_do(sub {
        for my $relation (@{ $relationships }) {
            if (
                defined($self->$relation) &&
                $self->$relation->can('deleted')
            ) {
                $self->$relation->deleted(DateTime->now());
                $self->$relation->update;
            }
        }

        $self->deleted(DateTime->now());
        $self->update;
    });
}

1; #__PACKAGE__->meta->make_immutable;

