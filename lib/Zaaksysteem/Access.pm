package Zaaksysteem::Access;

use Moose::Exporter;

use Data::Dumper;

Moose::Exporter->setup_import_methods(
    with_meta => ['access_profile', 'default_profile'],
    class_metaroles => {
        class => ['Zaaksysteem::Metarole::AccessProfileTrait']
    }
);

sub default_profile {
    shift->_default_security_profile(@_);
}

sub access_profile {
    my ($meta, %profiles) = @_;

    my %defined_profiles = %{ $meta->_security_profiles || {} };

    for my $key (keys %profiles) {
        $defined_profiles{ $key } = $profiles{ $key };
    }

    $meta->_security_profiles(\%defined_profiles);

    $meta->_init_class_modifiers;
}

1;
