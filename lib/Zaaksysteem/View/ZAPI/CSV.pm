package Zaaksysteem::View::ZAPI::CSV;

use Moose;

use Data::Dumper;

use Text::CSV;

BEGIN { extends 'Catalyst::View'; }

__PACKAGE__->config(
    'stash_key'             => 'csv',
    'quote_char'            => '"',
    'escape_char'           => '"',
    'sep_char'              => ',',
    'eol'                   => "\n",
    'binary'                => 1,
    'allow_loose_quotes'    => 1,
    'allow_loose_escapes'   => 1,
    'allow_whitespace'      => 1,
    'always_quote'          => 1
);

sub process {
    my $self                = shift;
    my ($c, $zapi_object)   = @_;


    my $template        = $c->stash->{template};

    $c->stash->{csv}    = $self->_process_zapi_result(
        $zapi_object,
        $c->stash->{zapi}->{result}
    );

    my $content = $self->render( $c, $template, $zapi_object, $c->stash );

    $c->res->headers->header( "Content-Type" => "text/csv" )
      unless ( $c->res->headers->header("Content-Type") );

    $c->res->body($content);
}

sub _process_zapi_result {
    my $self            = shift;
    my $zapi_object     = shift;

    ### Do something with blessed references
    my $result          = shift;

    throw(
        'zaaksysteem/view/zapi/csv',
        'No zapi attribute found, cannot generate csv'
    ) unless $result;


    # examine the first row to find header fields
    my ($first_row) = @$result;

    if (UNIVERSAL::isa($first_row, 'DBIx::Class')) {
        $first_row  = { $first_row->get_columns };
    }

    return [] unless $first_row;

    my $column_order = [keys %$first_row];

    my $rows = [$column_order];
    # allow override from settings
    if(my $options = $zapi_object->options) {
        if(my $custom_order = $options->{csv}->{column_order}) {
            $column_order = $custom_order;
        }
         # discard the extracted heading row
        if($options->{csv}->{no_header}) {
            $rows = [];
        }
    }

    # loop through results, use the column order
    foreach my $entry (@$result) {
        my $row;
        if (UNIVERSAL::isa($entry, 'DBIx::Class')) {
            my $rowdata    = { $entry->get_columns };
            $row    = [
                map { $rowdata->{ $_ } } @$column_order
            ];
        } else {
            $row    = [
                map { $entry->{$_} } @$column_order
            ];
        }
        push @$rows, $row;
    }

    return $rows;
}


sub csv_options {
    my ($self, $arguments) = @_;

    my $options     = $arguments->{options}    or die "need options";

    my $config = $self->config;

    if(my $csv_options = $options->{csv}) {
        foreach my $key (keys %$config) {
            if(my $override = $csv_options->{$key}) {
                $config->{$key} = $override;
            }
        }
    }
    return $config;
}


sub render {
    my $self = shift;
    my ( $c, $template, $zapi_object, $args ) = @_;

    my $config = $self->config;

    if(my $options = $zapi_object->options) {
        $config = $self->csv_options({
            options => $options,
        });
    }

    my $stash_key = $self->config->{'stash_key'};
    return '' unless $args->{$stash_key} && ref($args->{$stash_key}) =~ /ARRAY/;

    $config = { map { $_ => $config->{$_} } qw/
        quote_char
        escape_char
        sep_char
        eol
        binary 
        allow_loose_quotes
        allow_loose_escapes
        allow_whitespace
        always_quote/ 
    };

    my $csv = Text::CSV->new($config);

    my $content;
    foreach my $row ( @{ $args->{$stash_key} } ) {
        ### Blessed objects need to be down translated
        my $status = $csv->combine( @{ $row } );
        Catalyst::Exception->throw(
            "Text::CSV->combine Error: " . $csv->error_diag() )
          if ( !$status );
        $content .= $csv->string();
    }

    return $content;
}

# sub process {
#     my $self    = shift;
#     my ($c)     = @_;

#     #$c->stash->{csv}   = $c->stash->{zapi}->{result};

#     $c->stash->{csv}    = [
#         ['one','two','three']
#     ];

#     $c->log->debug('process csv');

#     my $res = $self->next::method(@_);

#     $c->log->debug(Dumper($c->res->body));

#     return $res;
# };

1;

