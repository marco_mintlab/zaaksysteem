package Zaaksysteem::View::ZAPI::JSON;

use Moose;

use Data::Dumper;
use Zaaksysteem::ZAPI::Response;
use Zaaksysteem::ZAPI::Error;

BEGIN { extends 'Catalyst::View::JSON'; }

__PACKAGE__->config(
    expose_stash    => 'zapi',
);

around 'process' => sub {
    my $method  = shift;
    my $orig    = shift;
    my ($c)     = @_;

    if ($c->stash->{ json_content_type }) { 
        $c->res->headers->header(
            'Content-Type' => $c->stash->{ json_content_type }
        );
    }

    $c->log->debug('process json');

    $orig->$method(@_);
};


sub encode_json {
    my($self, $c, $data) = @_;

    my $encoder = JSON::XS->new->utf8->pretty->allow_nonref->allow_blessed->convert_blessed;
    $encoder->encode($data);
}

1;
