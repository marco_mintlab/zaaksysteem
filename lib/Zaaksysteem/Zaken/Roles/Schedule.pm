package Zaaksysteem::Zaken::Roles::Schedule;

use Moose::Role;
use Data::Dumper;
use Zaaksysteem::Zaken::AdvanceResult;

with 'Zaaksysteem::Zaken::Roles::ZaakSetup';


sub pending_notifications {
    my $self = shift;

    ### Get mailer interfae
    my $mail_object     = $self
                        ->result_source
                        ->schema
                        ->resultset('Interface')
                        ->search_active(
                            {
                                module => 'email',
                            }
                        )->first;

    my $transactions    = $mail_object->process_trigger(
        'get_pending',
        {
            case_id                     => $self->id,
        }
    );

    my $pending_notifications = [];

    while(my $transaction = $transactions->next) {
        push @$pending_notifications, $transaction;
    }

    return $pending_notifications;
}

=head1

Execute rules, then reschedule mails based on the outcome.

=cut
sub reschedule_phase_notifications {
    my ($self, $options) = @_;

    my $notifications   = $options->{notifications} or die "need notifications resultset";
    my $status          = $options->{status}        or die "need status";

    # Since zaak_id is stored as a json parameter, there is no database relationship
    my $scheduled_jobs = $self->result_source->schema->resultset('ScheduledJobs');
    my $rules_result = $self->execute_rules({status => $status});

    return {} unless exists $rules_result->{schedule_mail};

    return $scheduled_jobs->reschedule_pending({
        zaak_id         => $self->id,
        notifications   => $notifications,
        scheduled_mails => $rules_result->{schedule_mail},
    });
}


sub reschedule_notifications {
    my ($self, $options) = @_;

    my $statuses = $self->zaaktype_node_id->zaaktype_statussen->search({
        status => {'<=' => $self->milestone }
    }, {
        order_by => { '-asc' => 'status'}
    });

    my $scheduled_jobs = $self->result_source->schema->resultset('ScheduledJobs');

    eval {
        $self->result_source->schema->txn_do(sub {

            $scheduled_jobs->cancel_pending({
                zaak_id => $self->id
            });

            while(my $status = $statuses->next()) {
                my $notifications = $status->notifications();

                $self->reschedule_phase_notifications({
                    notifications => $notifications,
                    status        => $status->status,
                });
            }
        });
    };
    if ($@) {
        die "Error rescheduling notifications: $@";
    }
}


1;



=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut

