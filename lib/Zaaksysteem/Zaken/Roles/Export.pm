package Zaaksysteem::Zaken::Roles::Export;

use Moose::Role;
use Hash::Merge::Simple qw( clone_merge );
use Data::Dumper;
$Data::Dumper::Sortkeys = 1;

use XML::Simple;
use Archive::Zip;
use File::stat;
use XML::Dumper;
use Encode;
use Params::Profile;

use Zaaksysteem::Constants;


=head1 METHODS

=head2 export

Return value: file reference

    my $file_reference = $zaak->export();

=cut

{
    Params::Profile->register_profile(
        'method'    => 'export',
        'profile'   => {
            required    => [ qw/filepath/],
            msgs        => PARAMS_PROFILE_DEFAULT_MSGS,
        }
    );


    sub export {
        my $self        = shift;
        my $opts        = shift;

        my $dv          = Params::Profile->check(params => $opts);

        die('Parameters incorrect:' . Dumper($opts)) unless $dv->success;
        my $args = $dv->valid();

        my $joins = [qw/
            zaak_betrokkenen 
            zaak_kenmerken 
            zaak_parents 
            zaaktype_node_id 
            relates_to 
            zaak_relates_toes 
            zaaktype_id
            zaaktype_node_id
            coordinator
            behandelaar
            aanvrager
            locatie_zaak
            locatie_correspondentie
            zaak_vervolgers
            zaak_children
            zaak_meta
        /];
    
    
        my $zaak_rs = $self->result_source->schema->resultset('Zaak')->search({
            'me.id' => $self->id
        }, {
            join => $joins,
            prefetch=> $joins,
        });
        $zaak_rs->result_class('DBIx::Class::ResultClass::HashRefInflator');
    
        my $zaak = $zaak_rs->first();
        
        my $xml_output = XML::Dumper::pl2xml($zaak);    

        $xml_output = encode("utf-8", $xml_output);
               
        my $zip = Archive::Zip->new();
        
        # main xml body
        my $string_member = $zip->addString($xml_output, 'zaak.xml');
    
        # attachments
         my $attached_files = $self->_attached_files($args);
         foreach my $attached_file (keys %$attached_files) {
             my $filepath = $attached_files->{$attached_file};
     
             if($filepath && -e $filepath) {
                 warn("adding $attached_file");
                 my $file_member = $zip->addFile( $filepath, $attached_file );
             }
         }
    
        my $filename = 'zaak-'.$zaak->{id}.'.zip';
        
        my $safe_filename_characters = "a-zA-Z0-9_.-";
        $filename =~ tr/ /_/; 
        $filename =~ s/[^$safe_filename_characters]/_/g;

        my $path = $self->result_source->schema->resultset('Config')->get_value('tmp_location');        
        my $zipfilename = $path . $filename;

        unless(-w $path) {
            die "{$path} is not writable";
        }

        unless ( $zip->writeToFileNamed($zipfilename) == Archive::Zip::AZ_OK ) {
            die 'write error' . $!;
        }
    
        return {filename => $filename, path => $path};
    }
}



sub _attached_files {
    my ($self, $args) = @_;
    
    my $files = $self->files;
    
    my $attached_files = {};
    while(my $file = $files->next()) {
        my $filename = $file->name;
        my $filepath = $file->filestore->ustore->getPath($file->filestore->uuid);
        $attached_files->{$filename} = $filepath;
    }
    
    return $attached_files;
}



1;
