package Zaaksysteem::Zaken::ResultSetZaak;

use strict;
use warnings;


use Hash::Merge::Simple qw( clone_merge );
use Data::Dumper;
$Data::Dumper::Sortkeys = 1;

use XML::Simple;
use Archive::Zip qw( :ERROR_CODES :CONSTANTS );
use File::stat;
use File::Path qw/remove_tree/;
use XML::Dumper;
use Encode;
use Params::Profile;
use Net::SCP::Expect;
use Text::CSV;
use Moose;
use Data::Dumper;

use Zaaksysteem::Constants qw/
    ZAAKSYSTEEM_ZAAK_URGENTIE_PERC_HIGH
    ZAAKSYSTEEM_ZAAK_URGENTIE_PERC_MEDIUM
    ZAAKSYSTEEM_ZAAK_URGENTIE_PERC_LATE
/;

extends 'DBIx::Class::ResultSet';

with
    'Zaaksysteem::Zaken::Roles::ZaakSetup';


my $SPECIAL_WHERE_CLAUSES   = {
    'urgentie'  => '
        abs(ROUND(100 *(
            date_part(\'epoch\', COALESCE(me.afhandeldatum, NOW()) - me.registratiedatum )
            /
            GREATEST(date_part(\'epoch\', me.streefafhandeldatum - me.registratiedatum), 1)
         ) ))
    '
};

### Prevent division by zero (date_part(\'epoch\', me.streefafhandeldatum - me.registratiedatum) + 1)
my $SPECIAL_SELECTS         = {
    'days_left'     => 'date_part(\'days\', me.streefafhandeldatum - COALESCE(me.afhandeldatum, NOW()))',
    'days_perc'     => 'ROUND( 100 *(
            date_part(\'epoch\', COALESCE(me.afhandeldatum, NOW()) - me.registratiedatum )
            /
            GREATEST(date_part(\'epoch\', me.streefafhandeldatum - me.registratiedatum), 1)
         ) )
    ',
    'days_running'  => 'date_part(\'days\', COALESCE(me.afhandeldatum, NOW()) - me.registratiedatum )',
};

sub search {
    my $self    = shift;

    unless ([ caller(1) ]->[3] =~ /_prepare_search/) {
        return $self->_prepare_search(@_);
    }

    if (
        $_[1] &&
        UNIVERSAL::isa($_[1], 'HASH') &&
        (my $order_by = $_[1]->{order_by})
    ) {
        if (UNIVERSAL::isa($order_by, 'HASH')) {
            while (my ($key, $order_by) = each %{ $order_by }) {
                if ($SPECIAL_SELECTS->{$order_by}) {
                    $_[1]->{order_by}->{$key} = $SPECIAL_SELECTS->{$order_by};
                }
            }
        } else {
            if ($SPECIAL_SELECTS->{$order_by}) {
                $_[1]->{order_by} = $SPECIAL_SELECTS->{$order_by};
            }
        }
    }

    $self->next::method(@_);
}

sub _prepare_search {
    my $self                = shift;
    my $where               = shift;
    my $additional_options  = {};

    ## Additional options
    unless ($self->{attrs}->{ran}) {
        $additional_options->{'join'}   = [
            #'zaak_betrokkenen',
            {
                zaaktype_node_id    => 'zaaktype_definitie_id',
#               zaak_kenmerken      => 'zaak_kenmerken_values',
            }
        ];

        $additional_options->{'prefetch'}   = [
            'aanvrager',
            'behandelaar',
            {
                zaaktype_node_id    => 'zaaktype_definitie_id',
                #zaak_kenmerken      => 'zaak_kenmerken_values',
                #zaak_kenmerken      => 'bibliotheek_kenmerken_id',
            }
        ];

        $additional_options->{'+select'}    = [];
        for my $key (sort keys %{ $SPECIAL_SELECTS }) {
            my $value = $SPECIAL_SELECTS->{$key};
            push(
                @{ $additional_options->{'+select'} },
                \$value
            );
        }

#        $additional_options->{'+select'}    = [
#            [ \'date_part(\'days\', NOW() - me.registratiedatum )' ],
#            [ \'date_part(\'days\', me.streefafhandeldatum - NOW())'],
#            [ \'ROUND( 100 *(
#                    date_part(\'epoch\', NOW() - me.registratiedatum )
#                    /
#                    date_part(\'epoch\', me.streefafhandeldatum - me.registratiedatum)
#                 ) )
#            '],
#        ];

        # ROUND(100 * (EXTRACT( EPOCH FROM( AGE( NOW(), me.registratiedatum ) ) )
        # ) / EXTRACT( EPOCH FROM( AGE( me.streefafhandeldatum,
        # me.registratiedatum ) ) )) as percentage_complete

        $additional_options->{'+as'}        = [
            sort keys %{ $SPECIAL_SELECTS }
        ];
    }

    $self->{attrs}->{ran} = 1;

    my $rs = $self->search({}, $additional_options);


    ### SPECIAL CONSTRUCT FOR URGENT!
    while (my ($column, $definition)    = each %{ $SPECIAL_WHERE_CLAUSES }) {
        next unless UNIVERSAL::isa($where, 'HASH') && $where->{$column};

        my @where_clauses;
        if (UNIVERSAL::isa($where->{$column}, 'ARRAY')) {
            push(@where_clauses, @{ $where->{$column} });
        } else {
            push(@where_clauses, $where->{$column});
        }

        my @sql;
        for my $where_clause (@where_clauses) {
            if ($where_clause && UNIVERSAL::isa($where_clause, 'HASH')) {
                push(@sql, $definition .
                    [ keys(%{ $where_clause }) ]->[0] .
                    [ values(%{ $where_clause }) ]->[0]
                );
            } elsif (
                grep { $_ eq lc($where_clause) } qw/normal medium high late/
            ) {
                if (lc($where_clause) eq 'normal') {
                    push(@sql, $definition . '<' .
                        (100 - (ZAAKSYSTEEM_ZAAK_URGENTIE_PERC_MEDIUM * 100))
                    );
                } elsif ( lc($where_clause) eq 'medium') {
                    push(@sql, $definition . ' < ' .
                        (100 - (ZAAKSYSTEEM_ZAAK_URGENTIE_PERC_HIGH * 100)) .
                        ' AND ' .
                        $definition . ' >= ' .
                        (100 - (ZAAKSYSTEEM_ZAAK_URGENTIE_PERC_MEDIUM * 100))
                    );
                } elsif ( lc($where_clause) eq 'high') {
                    push(@sql, $definition . ' >= ' .
                        (100 - (ZAAKSYSTEEM_ZAAK_URGENTIE_PERC_HIGH * 100))
                        . ' AND ' .
                        $definition . ' < ' .
                        (ZAAKSYSTEEM_ZAAK_URGENTIE_PERC_LATE * 100)
                    );
                } elsif ( lc($where_clause) eq 'late') {
                    push(@sql, $definition . ' >= ' .
                        (ZAAKSYSTEEM_ZAAK_URGENTIE_PERC_LATE * 100)
                    );
                }
            } else {
                die(
                    'ERROR: Special Z::Zaken::ZaakResultSet where '
                    . 'column "' . $column . '" needs HASH/ARRAY as parameter.'
                );
            }
        }
        #my $sql = ' ( me.status != \'resolved\' AND me.status != \'stalled\' AND
        #me.status != \'deleted\' AND ( ' . join(' ) OR ( ', @sql) . ') )';
        my $sql = ' ( ( ' . join(' ) OR ( ', @sql) . ') )';
        $rs = $rs->search(\[ $sql ]);

        delete($where->{$column});
    }

    #$rs->{attrs}->{ran} = 1;

    return $rs->search($where, @_);
}

sub search_extended {
    my ($self, $where, $display, $opts) = @_;

    my $rs                      = $self->search($where, $display);

    my $zaaksysteembeheerder;
    my $administrator;

    if (
        $self->{attrs}->{current_user} &&
        $self->{attrs}->{current_user}->uidnumber
    ) {
        my @user_roles              = $self->{attrs}->{current_user}->get_roles;
        my $user_id                 = $self->{attrs}->{current_user}->uidnumber;
        if (
            grep (
                { $_->get_value('cn') eq 'Administrator' }
                @user_roles
            )
        ) {
            $administrator = 1;
        }

        if (
            grep (
                { $_->get_value('cn') eq 'Zaaksysteembeheerder' }
                @user_roles
            )
        ) {
            $zaaksysteembeheerder = 1;
        }

        unless ($administrator) {
            my $zaaktype_id_list        = $self->result_source->schema
                ->source('Zaaktype')->resultset->search(
                {
                    'me.deleted'                                => undef,
                },
                {
                    join        => 'zaaktype_node_id',
                }
            );

            my $ou_id       = $self->{attrs}->{current_user}->user_ou->get_value('l');

            my @role_ids    = map { $_->get_value('gidNumber') }
                @user_roles;

            my $auth_list               = $self->result_source->schema
                ->source('ZaaktypeAuthorisation')->resultset->search(
                    {
                        zaaktype_id     => {
                            -in     => $zaaktype_id_list->get_column('me.id')->as_query,
                        },
                        role_id             => {
                            -in     => [ @role_ids ],
                        },
                        ou_id               => $ou_id,
                    }
                );

            ### Make sure vertrouwelijke zaken komen niet naar voren
            $rs  = $rs->search({
                '-or'   => [
                    { 'me.zaaktype_id'   => {
                            -in => $auth_list->get_column('zaaktype_id')->as_query
                        },
                    },
                    { 'me.aanvrager_gm_id'    => $user_id },
                    { 'me.behandelaar_gm_id'  => $user_id },
                    { 'me.coordinator_gm_id'  => $user_id },
                ]
            });
        }
    }

    unless(
        $opts->{show_future_cases} &&
        ($administrator || $zaaksysteembeheerder)
    ) {
        $rs         = $rs->search({
            'me.registratiedatum'   => { '<'    => DateTime->now() }
        });
    }
    return $rs;
}

sub with_progress {
    my $self                            = shift;

    return $self->search(@_);
}

sub search_grouped {
    my $self    = shift;
    my $group   = pop;

    #$group = 'zaak_betrokkenen.gegevens_magazijn_id';

#    while (my ($key, $value) = each %{ $self->{attrs} }) {
#        next if $key eq 'betrokkene_model';
#        next if $key eq 'c';
#        warn($key . ' => ' . Dumper($value));
#    }
    my $search = $self->search(@_);
#    while (my ($key, $value) = each %{ $search->{attrs} }) {
#        next if $key eq 'betrokkene_model';
#        next if $key eq 'c';
#        warn($key . ' => ' . Dumper($value));
#    }

    ### Ok, het ziet er uit als een hack, en ja, dat is het ook. Punt is dat
    ### de prefetch functie ons problemen geeft bij het vinden van de me
    ### (zaak) tabel.
    ###
    ### De attrs +select en +as zijn voor het weergeven van extra kolommen
    ### welke uiteraard uit moeten staan: we willen immers alleen een count

    delete($search->{attrs}->{'+select'});
    delete($search->{attrs}->{'+as'});
    delete($search->{attrs}->{'prefetch'});

#    while (my ($key, $value) = each %{ $search->{attrs} }) {
#        next if $key eq 'betrokkene_model';
#        warn($key . ' => ' . Dumper($value));
#    }

    my $search_opts = {};

    my $attrs       = {
        as          => [ 'group_naam', 'group_count' ],
        group_by    => [ $group ],
    };

    if ($group eq 'behandelaar') {
        my $behandelaar_clause = '= zaak_betrokkenen.id';
        $search_opts->{'me.behandelaar'} = \$behandelaar_clause;
        $attrs->{group_by} = [ 'zaak_betrokkenen.gegevens_magazijn_id' ];
        $attrs->{select} = [ 'zaak_betrokkenen.gegevens_magazijn_id', { count => { distinct => 'me.id'} } ],
    } else {
        $attrs->{select} = [ $group, { count => { distinct => 'me.id'} } ],
    }

    return $search->search($search_opts,$attrs);
}

sub overlapt {
    my $self        = shift;
    my $startdt     = shift;
    my $stopdt      = shift;

    $startdt    = $startdt->datetime;
    $stopdt     = $stopdt->datetime;

    $startdt    =~ s/T/ /;
    $stopdt    =~ s/T/ /;

    return $self->search(\[
            "(DATE('" . $startdt . "'), DATE('" . $stopdt . "'))"
                . " OVERLAPS " .
            "(me.registratiedatum, me.afhandeldatum)"
    ]);

    return $self;
}

sub export {
    my ($self, $args) = @_;
    
    die "need filepath" unless($args->{filepath}); # TODO fix poor man's params::profile 
    my $filepath = $args->{filepath};

    my @results = ();
    
    my $zip = Archive::Zip->new();

    my $resultset = $self->search();
    my @cleanup = ();
    while(my $zaak = $resultset->next()) {
        my $file = $zaak->export($args);
        
        warn "zaak id: " . $zaak->id;
        $zip->addFile($file->{path}.$file->{filename}, $file->{filename});
        push @cleanup, $file->{path}.$file->{filename};
    }
    
    my $filename = 'zaken-export.zip';
        
    my $path = $self->result_source->schema->resultset('Config')->get_value('tmp_location');
    my $zipfilename = $path . $filename;

    unless ( $zip->writeToFileNamed($zipfilename) == AZ_OK ) {
        die 'write error' . $!;
    }
    foreach my $cleanup_filename (@cleanup) {
        system("rm $cleanup_filename");
    }

    return {filename => $filename, path => $path};
}

sub _group_geregistreerd_data {
    my $self                        = shift;
    my ($startdatum, $einddatum)    = @_;
    my $interval = '1 day';
    my $interval_label = 'day';

    if (!$startdatum) {
        #delete($self->{attrs}->{'group_by'});
        my $zaken = $self->search(
            {},
            {
                order_by => { '-asc' => 'registratiedatum' },
                group_by => undef,
                select   => ['me.registratiedatum'],
                'as'     => ['registratiedatum'],
            }
        );

        my $startzaak = $zaken->first;

        return (undef,undef) unless $startzaak;

        $startdatum = $startzaak->registratiedatum;
    }


    if (!$einddatum) {
        my $zaken = $self->search(
            {},
            {
                order_by => { '-desc' => 'registratiedatum' },
                group_by => undef,
                select   => ['registratiedatum'],
                'as'     => ['registratiedatum'],
            }
        );
        delete($zaken->{attrs}->{'group_by'});

        my $eindzaak = $zaken->first;

        return (undef,undef) unless $eindzaak;

        $einddatum = $eindzaak->registratiedatum;
    }

    ### Do some checks
    if (($einddatum->epoch - $startdatum->epoch) > ((31*86400) * 3)) {
        $interval = '1 month';
        $interval_label = 'month';
    } elsif (($einddatum->epoch - $startdatum->epoch) > (31 * 86400) ) {
        ### groter dan 3 maanden, interval 1 week
        $interval = '1 week';
        $interval_label = 'week';
#    } elsif (($einddatum->epoch - $startdatum->epoch) < 3600) {
#        $interval = '1 minute';
#        $interval_label = 'minute';
    } elsif (($einddatum->epoch - $startdatum->epoch) < 86400) {
        $interval       = '1 hour';
        $interval_label = 'hour';
    }

    return (
        DateTime->new(
            day => $startdatum->day,
            month => $startdatum->month,
            year => $startdatum->year,
        ),
        DateTime->new(
            day     => $einddatum->day,
            month   => $einddatum->month,
            year    => $einddatum->year,
            hour    => 23,
            minute  => 59,
            second  => 59
        ),
        $interval,
        $interval_label
    );
}

sub _group_geregistreerd_resultset {
    my $self        = shift;
    my $replace     = shift;
    my $interval    = shift;

    my ($query, @bindargs);

    my $queryobject   = $self->search({}, {
        select      => { 'distinct' => 'me.id' },
        group_by    => undef,
        order_by    => undef,
    })->as_query;

    ($query, @bindargs)  = @{ ${ $queryobject } };

    ### Overwrite definition
    my $source                 =
        $self->result_source->schema->source('ZaakGrafiek' . ucfirst($replace));

    my $new_definition      = $source->view_definition_template;
    $new_definition         =~ s/INNERQUERY/$query/;
    $new_definition         =~ s/INTERVAL/\'$interval\'/g;

    #$new_definition         =~ s/DATAREPL/\'$interval\'/g;

    $source->view_definition(
        $new_definition
    );

    return ($source, @bindargs);
}

sub group_geregistreerd {
    my $self        = shift;

# select period.date as period, count(zaak.id) as zaken from (select
# generate_series('2011-07-01'::timestamp, '2011-07-30'::timestamp, '1 week')
# as date) as period left outer join zaak on zaak.created between period.date
# AND (date(period.date) + interval '1 week') and zaak_id IN (QUERY) group by period.date order by period.date;
    delete($self->{attrs}->{'+as'});
    delete($self->{attrs}->{'+select'});
    delete($self->{attrs}->{'prefetch'});
    delete($self->{attrs}->{'order_by'});

    my ($startdatum,$einddatum,$interval, $interval_label)     = $self->_group_geregistreerd_data(@_);

    return unless ($startdatum && $einddatum);

    ### Define defintion
    my ($geregistreerd_resultset, $afgehandeld_resultset);

    my ($regsource, $afsource, @extraargs);

    ($regsource, @extraargs) = $self->_group_geregistreerd_resultset('geregistreerd', $interval);


    ### Get clean resultset
    my $resultset_registratie = $regsource->resultset->search(
        {},
        {
            bind    => [$startdatum,$einddatum, @extraargs],
            order_by => { '-asc' => 'periode' },
        }
    );

    ($afsource, @extraargs) = $self->_group_geregistreerd_resultset('afgehandeld', $interval);
    my $resultset_afhandeling = $afsource->resultset->search(
        {},
        {
            bind    => [$startdatum,$einddatum, @extraargs]
        }
    );

    my $axis    = $self->_define_axis($resultset_registratie, $interval_label);

    return ($resultset_registratie,$resultset_afhandeling, $axis);
}

sub _define_axis {
    my $self        = shift;
    my $resultset   = shift;
    my $interval    = shift;

    my $axis        = {
        x   => [],
    };

    my $counter = 0;
    while (my $row = $resultset->next) {
        $counter++;

        my $x;

        if ($interval eq 'month') {
            $row->periode->set_locale('nl_NL');
            if (
                $counter == 1 ||
                $counter == $resultset->count ||
                $row->periode->month == 1
            ) {
                $x = $row->periode->year . ': ' . $row->periode->month_name;
            } else {
                $x = $row->periode->month_name;
            }
        } elsif ($interval eq 'week') {
            if (
                $counter == 1 ||
                $counter == $resultset->count ||
                $row->periode->week == 1
            ) {
                $x = $row->periode->year . ': ' . $row->periode->week
            } else {
                $x = $row->periode->week;
            }
        } elsif ($interval eq 'minute') {
            $x = $row->periode->hour . ':' . $row->periode->minute;
        } elsif ($interval eq 'hour') {
            if (
                $counter == 1 ||
                $counter == $resultset->count ||
                $row->periode->hour == 0
            ) {
                $x = $row->periode->hour . "\n(" . $row->periode->day . '-' .
                $row->periode->month . ')';
            } else {
                $x = $row->periode->hour;
            }
        } elsif ($interval eq 'day') {
            $row->periode->set_locale('nl_NL');
            $x = $row->periode->day . '-' . $row->periode->month;
        }

        push(@{ $axis->{x} }, $x);
    }

    $resultset->reset;

    return $axis;
}

sub group_binnen_buiten_termijn {
    my $self        = shift;

    delete($self->{attrs}->{'+as'});
    delete($self->{attrs}->{'+select'});
    delete($self->{attrs}->{'prefetch'});
    delete($self->{attrs}->{'order_by'});

    ## Clean up attributes
    return $self->search({}, {
        select      => [
            {
                sum => 'CASE WHEN (
                    me.afhandeldatum is not null AND
                    me.afhandeldatum < me.streefafhandeldatum
                ) THEN 1 ELSE 0 END',
                -as => 'binnen',
            },
            {
                sum => 'CASE WHEN (
                    (
                        NOW() > me.streefafhandeldatum AND me.afhandeldatum is null
                    ) OR me.afhandeldatum > me.streefafhandeldatum
                ) THEN 1 ELSE 0 END',
                '-as' => 'buiten'
            },
        ],
        'as'      => [qw/binnen buiten/],
    });

    return $self;
}

sub betrokkene_model {
    my $self    = shift;

    #warn(Dumper($self));
    return $self->{attrs}->{betrokkene_model};
}

sub gegevens_model {
    my $self    = shift;

    #warn(Dumper($self));
    return $self->{attrs}->{dbic_gegevens};
}

sub config {
    my $self    = shift;

    #warn(Dumper($self));
    return $self->{attrs}->{config};
}

sub current_user {
    my $self    = shift;

    return unless $self->{attrs}->{current_user};

    return $self->betrokkene_model->get(
        {
            extern  => 1,
            type    => 'medewerker',
        },
        $self->{attrs}->{current_user}->uidnumber
    );
}


sub publish {
    my ($self, $opts) = @_;
    
    my $files_dir       = $opts->{files_dir}        or die "need files_dir";
    my $root_dir        = $opts->{root_dir}         or die "need root_dir";
    my $publish_script  = $opts->{publish_script}   or die "need publish_script";
    my $hostname        = $opts->{hostname}         or die "need hostname";
    my $profile         = $opts->{profile}          or die "need profile";
    my $display_fields  = $opts->{display_fields}   or die "need display_fields";
    my $config          = $opts->{config}           or die "need config";
    my $published_file_ids = $opts->{published_file_ids} or die "need published_file_ids";
    my $published_related_ids = $opts->{published_related_ids} or die "need published_related_ids";


    my $logging = '';

    my $tmp_publish_dir = $files_dir . '/tmp/publish/';
    
    if(-e $tmp_publish_dir) {
        remove_tree($tmp_publish_dir) 
            or die "could not clean up previous publish dir: " . $!;

        $logging .= "Publish directory: '$tmp_publish_dir'.\n";
        $logging .= "Cleaned publish directory.\n";
    }

    $logging .= "Export profile is '$profile'.\nGenerating export files..\n";

    if($profile =~ m[vergaderingen_ede] || ($config->{profile} && $config->{profile} =~ m[vergaderingen_ede])) {
        my $publish_dir = $files_dir . '/vergaderingen_ede/';

        $logging .= $self->export_vergaderingen_ede({
            files_dir           => $files_dir,
            publish_dir         => $publish_dir,
            root_dir            => $root_dir,
            published_file_ids  => $published_file_ids,
            published_related_ids => $published_related_ids
        });

    } elsif($profile eq 'perfectview_tholen') {
    
        mkdir $tmp_publish_dir 
            or die "unable to create publish dir ($tmp_publish_dir): $!";
    
        $self->log->debug("created tmp_publish_dir: $tmp_publish_dir");

        $logging .= $self->export_perfectview_tholen({
            files_dir           => $files_dir,
            tmp_publish_dir     => $tmp_publish_dir,
            display_fields      => $display_fields,
            config              => $config,
        });
    }       

    $logging .= "Finished generation of export files for profile '$profile'.\n";

    my $result = 0;

    unless ($opts->{dry_run}) {

        $logging .= "Starting transfer to external server.\n";
        # copy files to the receiving server
        $self->log->debug("Invoking publish script: $publish_script");

        unless(-e $publish_script) {
            die "Publicatie script $publish_script is niet beschikbaar, vraag beheer om de server configuratie te corrigeren.\n";
        }

        # invoke publish script
        my $publish_script_output = `$publish_script $hostname $profile`;
        $logging .= "Output from transfer script:\n-----------BEGIN-----------\n";
        $logging .= "$publish_script_output.\n------------END----------\n";
        $logging .= "Exit code from transfer script: '$?'.\n";
        $logging .= "Finished.\n";

        $self->log->debug("Invoked publish script: ---------\n" .
            $publish_script_output . "\n-------------"
        );

        $self->log->debug("Exit code of publish script: " . $?);

        $result = $?;
    }

    # $? contains the exit code from the publish script. if it is 0, all is well,
    # else there is a problem.
    return {
        result  => $result,
        logging => $logging,
    };
}


sub export_perfectview_tholen {
    my ($self, $opts) = @_;

    my $files_dir       = $opts->{files_dir}        or die "need files_dir";
    my $tmp_publish_dir = $opts->{tmp_publish_dir}  or die "need tmp_publish_dir";
    my $display_fields  = $opts->{display_fields}   or die "need display_fields";
    my $config          = $opts->{config}           or die "need config";
    
    my $csv = Text::CSV->new({ 
        binary          => 1, 
        sep_char        => ';',
        always_quote    => 1,
        eol             => "\n",
    }) or die "Cannot use CSV: ".Text::CSV->error_diag();

    my $filename = $tmp_publish_dir . "/" . $config->{csv_filename};

    my $logging = "Writing to filename: '$filename'\n";

    # check field order
    my @fields = map { $_->{class} } @$display_fields;
    $logging .= "Checking field format (@fields).\n";
    unless(@fields > 3 && $fields[0] eq 'zaaknummer' && $fields[1] eq 'kenmerk' && $fields[2] eq 'kenmerk') {
        die "Incorrect fields (@fields), use 'Alle zoekresultaten', aborting export.\n";
    }

    my $fh;
    open $fh, ">:encoding(utf8)", $filename or die "could not open $filename: $!";
    print $fh '"RegistratieId";"SubcategorieId";"Omschrijving";' .
        '"Plaats";"Datum";"Foto";"Thumbnail";"Adres";"Postcode";"Telefoon";"Email"'."\n";

    while(my $case = $self->next()) {
        my @columns = ();

        foreach my $display_field (@$display_fields) {

            my $datafieldname   = $display_field->{systeemkenmerk};
            my $value           = $case->systeemkenmerk($datafieldname) || '';    

            # get rid of newlines, they mess up csv export
            if($value =~ m|[\r\n]|) {
                die "Lege regels (newlines) aangetroffen in zaak " . $case->id . ".\n";
            }

            push @columns, $value;
        }
        $csv->print ($fh, \@columns);
    }

    close $fh or die "could not close $filename: $!";
    return $logging;
}



sub export_vergaderingen_ede {
    my ($self, $options) = @_;

    my $logging = '';
    while(my $case = $self->next()) {
        $logging .= "Generating XML export for case ". $case->id . ".\n";
        $logging .= $case->export_vergaderingen_ede($options);
    }
    return $logging;
}

sub _generate_uuid {
    my $self    = shift;

    my $ug      = new Data::UUID;
    my $uuid    = $ug->create();
    return $ug->to_string($uuid);
}

1;

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut

