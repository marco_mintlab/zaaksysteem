package Zaaksysteem::Zaken::AdvanceResult;

use Moose;

sub can_advance {
      my $self = shift;

      return 
        $self->fields_complete      &&
        $self->checklist_complete   &&
        $self->result_complete      &&
        $self->subcases_complete    &&
        $self->owner_complete       &&
        $self->documents_complete;
}

use constant ADVANCE_CHECKS => [qw/
    fields_complete
    checklist_complete
    result_complete
    subcases_complete
    owner_complete
    documents_complete
/];


has	[ @{ ADVANCE_CHECKS() }] => (
    is => 'rw',
    isa => 'Bool',
    default => 0,
);

sub TO_JSON {
    my $self    = shift;

    my $rv      = {};

    for my $check (@{ ADVANCE_CHECKS() }) {
        $rv->{$check} = $self->$check;
    }

    return $rv;
}


1;