package Zaaksysteem::Zaken::ComponentZaak;

use strict;
use warnings;

use Moose;

use Data::Dumper;

use Date::Calendar;
use Date::Calendar::Profiles qw/$Profiles/;
use DateTime::Format::Strptime;
use DateTime;

use Zaaksysteem::Zaken::Jobs;

use Data::UUID;

use Zaaksysteem::Constants;
use Zaaksysteem::ZTT;

extends 'DBIx::Class';

with
    'Zaaksysteem::Zaken::Roles::MetaObjecten',
    'Zaaksysteem::Zaken::Roles::BetrokkenenObjecten',
    'Zaaksysteem::Zaken::Roles::FaseObjecten',
    'Zaaksysteem::Zaken::Roles::DocumentenObjecten',
    'Zaaksysteem::Zaken::Roles::DeelzaakObjecten',
    'Zaaksysteem::Zaken::Roles::KenmerkenObjecten',
    'Zaaksysteem::Zaken::Roles::RouteObjecten',
    'Zaaksysteem::Zaken::Roles::ChecklistObjecten',
    'Zaaksysteem::Zaken::Roles::Acties',
    'Zaaksysteem::Zaken::Roles::Publish',
    'Zaaksysteem::Zaken::Roles::Export',
    'Zaaksysteem::Zaken::Roles::Fields',
    'Zaaksysteem::Zaken::Roles::Schedule',
    'Zaaksysteem::Zaken::Hooks';

use constant RELATED_OBJECTEN => {
    'fasen'     => 'Fasen',
    'voortgang' => 'Voortgang',
    'sjablonen' => 'Sjablonen',
    'locaties'  => 'Locaties',
    'acties'    => 'Acties'
};

has 'jobs'  => (
    'is'        => 'rw',
    'lazy'      => 1,
    'default'   => sub {
        my $self        = shift;

        return Zaaksysteem::Zaken::Jobs->new(
            'zaak'  => $self,
        );
    }
);

has 'te_vernietigen'    => (
    'is'        => 'rw',
    'lazy'      => 1,
    'default'   => sub {
        my $self        = shift;

        return unless $self->vernietigingsdatum;

        return 1 if $self->vernietigingsdatum < DateTime->now();

        return;
    }
);

sub nr  {
    my $self    = shift;

    $self->id( @_ );
}

sub set_resultaat {
    my $self            = shift;

    my $oudresultaat    = $self->resultaat;
    my $result          = $self->resultaat(@_);

    return if $oudresultaat && $oudresultaat eq $result; 

    if (@_) {
        $self->set_vernietigingsdatum;
    }

    my $event_type = 'case/update/result';
    my $data = {
        case_id     => $self->id,
        result      => $self->resultaat,
        old_result  => $oudresultaat
    };

#    unless(my $recent_event = $self->logging->find_recent({
#        data        => $data,
#        zaak_id     => $self->id,
#        component   => 'zaak',
#        event_type  => $event_type,
#    })) {
#        warn "found no recent similar";
    $self->logging->trigger($event_type, {
        component   => 'zaak',
        zaak_id     => $self->id,
        data        => $data,
    });
#    }


    $self->update;
}


sub zaaktype_definitie {
    my $self    = shift;

    return $self->zaaktype_node_id->zaaktype_definitie_id;
}



#
# TODO this should be called 'field_value' .. it's not limited to 
# systeemkenmerken.
#
# my $field_value = $case->field_value({field_name => $field_name});
#
sub systeemkenmerk {
    my $self    = shift;
    my $label   = shift;

    die "gimme label" unless $label;

    if (my ($kenmerk_id) = $label =~ /^kenmerk\D*(\d+)$/) {
        my $kenmerken = $self->zaak_kenmerken->search(
            {
                bibliotheek_kenmerken_id    => $kenmerk_id
            }
        );

        my $count = $kenmerken->count;
        my $kenmerk = $kenmerken->first;
        return unless $kenmerk;

        my $value = $kenmerk->value;

        ### Make sure we handle arrays correctly:
        if ($count > 1) {
            while(my $multiple_kenmerk = $kenmerken->next()) {
                $value .= ", \n" . $multiple_kenmerk->value;
            }
        }

        my $replace_value = $value;

        ### Make sure valuta get's showed correctly
        if ($kenmerk->bibliotheek_kenmerken_id->value_type =~ /valuta/) {
            $replace_value =~ s/\,/\./g; # perl like dots in numbers
            $replace_value = sprintf('%01.2f', $replace_value);
            $replace_value =~ s/\./,/g;  # and we like commas. ok we should think about this
        }

        ### Make sure we show bag items the 'correct' way
        if ($kenmerk->bibliotheek_kenmerken_id->value_type =~ /^bag/) {

            $replace_value = $self->result_source->schema->resultset('BagNummeraanduiding')
                ->get_human_identifier_by_source_identifier($replace_value);
        }

        return $replace_value;
    }

    if (exists ZAAKSYSTEEM_STANDAARD_KOLOMMEN->{$label}) {
        return ZAAKSYSTEEM_STANDAARD_KOLOMMEN->{$label}->($self) || '';
    }

    unless (exists ZAAKSYSTEEM_STANDAARD_KENMERKEN->{$label}) {
        die "need zaaksysteem_standaard_kenmerken config ($label)";
        return '';
    }

    return ZAAKSYSTEEM_STANDAARD_KENMERKEN->{$label}->($self) || '';
}

# Returns true if streefafhandeldatum > now()
sub is_late {
    my $self = shift;

    my $now = DateTime->now->truncate(to => 'day');
    
    my $parser = DateTime::Format::Strptime->new(pattern => '%d-%m-%Y');

    my $expected_date = $parser->parse_datetime($self->systeemkenmerk('streefafhandeldatum'));

    # Date unparsable, could be suspended case, return not-late in that case
    unless($expected_date) {
        return 0;
    }
    
    $expected_date->truncate(to => 'day');

    return DateTime->compare($now, $expected_date) >= 0;
}


# zaak->zaaktype_node_id->properties->{$property}
sub zaaktype_property {
    my ($zaak, $property) = @_;

    die "need zaak"     unless $zaak;
    die "need property" unless $property;

    my $zaaktype_node = $zaak->zaaktype_node_id or die "need zaaktype_node_id";
    
    my $properties = $zaaktype_node->properties or return; # older zaaktypen don't have this field. the return value is undef.

    return $properties->{$property};
}


=head1

Returns the selected result object for this case. Used for systeemkenmerken.

=cut
sub zaaktype_resultaat {
    my ($zaak) = @_;

    return unless $zaak->resultaat;

    return $zaak->zaaktype_node_id->zaaktype_resultaten->search({ resultaat => $zaak->resultaat })->first;
}



sub status_perc {
    my $self            = shift;

    my $numstatussen    = $self->zaaktype_node_id->zaaktype_statussen->count;

    return 0 unless $numstatussen;

    return sprintf("%.0f", ($self->milestone / $numstatussen) * 100);
}


sub open_zaak {
    my $self    = shift;

    my $former_state = {
        status      => $self->status,
        behandelaar => $self->behandelaar ? $self->behandelaar->id : undef,
        coordinator => $self->coordinator ? $self->coordinator->id : undef,
    };

    $self->status('open');

    my $current_user = $self->result_source
        ->schema
        ->resultset('Zaak')
        ->current_user;


    unless ($self->behandelaar) {
        $self->set_behandelaar($current_user->betrokkene_identifier);
    }

    unless ($self->coordinator) {
        $self->set_coordinator($current_user->betrokkene_identifier);
    }

    $self->update;

    my $new_state = {
        status      => $self->status,
        behandelaar => $self->behandelaar->id,
        coordinator => $self->coordinator->id,
    };

    my $logging_row = $self->logging->trigger('case/accept', { component => 'zaak', data => {
        case_id         => $self->id,
        acceptee_name   => $current_user->naam,
        former_state    => $former_state,
        new_state       => $new_state,
    }});

    return $logging_row->id;
}


sub unrelate {
    my ($self, $related_id) = @_;

    if($self->relates_to && $self->relates_to->id == $related_id) {
        $self->relates_to(undef);
        $self->update;
    } else {
        my $related_case = $self->result_source->schema->resultset('Zaak')->find($related_id);

        $related_case->relates_to(undef);
        $related_case->update;
    }
}

sub set_verlenging {
    my $self    = shift;
    my $dt      = shift;

    $self->streefafhandeldatum($dt);
    $self->set_vernietigingsdatum;
    $self->update;
}


sub _bootstrap {
    my ($self, $opts)   = @_;

    $self->_bootstrap_datums($opts);
    $self->_bootstrap_route($opts);

    $self->update;
}



sub _load_zaaktype_timing {
    my ($self, $now, $addtime, $addtime_type) = @_;
    my ($newdate);

    my $dt = $now->clone;

    # TEST:
    # perl -MDate::Calendar -e 'use Date::Calendar::Profiles qw/$Profiles/; use Date::Calc::Object; $calendar = Date::Calendar->new($Profiles->{"NL"});$date  = Date::Calc->gmtime(time); my $calcdate    = $calendar->add_delta_workdays(Date::Calc->gmtime(time), 6); print $calcdate->day'
    if ($addtime_type eq ZAAKSYSTEEM_NAMING->{TERMS_TYPE_WERKDAGEN}) {
        my $calendar    = Date::Calendar->new($Profiles->{"NL"});
        my $startdate   = Date::Calc->localtime($now->epoch);
        my $calcdate    = $calendar->add_delta_workdays(
            $startdate,
            ($addtime)
        );
        $newdate        = ($calcdate->mktime+86400);	# + 1, it would calculate from
        $newdate        = DateTime->from_epoch(epoch => $newdate);
    } elsif ($addtime_type eq ZAAKSYSTEEM_NAMING->{TERMS_TYPE_KALENDERDAGEN}) {
        $dt->add(days   => $addtime);
        $newdate    = $dt;
    } elsif ($addtime_type eq ZAAKSYSTEEM_NAMING->{TERMS_TYPE_WEKEN}) {
        $dt->add(weeks   => $addtime);
        $newdate    = $dt;
    } elsif ($addtime_type eq ZAAKSYSTEEM_NAMING->{TERMS_TYPE_EINDDATUM}) {
        my ($day, $month, $year) = $addtime =~ /^(\d{2})-(\d{2})-(\d{4})$/;
        $dt = DateTime->new(
            year    => $year,
            month   => $month,
            day     => $day
        );

        $newdate    = $dt;
    }

    return $newdate;
}


sub _bootstrap_datums {
    my ($self, $opts)   = @_;

    my $now             = DateTime->now;

    ### Registratiedatum not set, default to now
    if ($opts->{registratiedatum}) {
        $self->registratiedatum(
            $opts->{registratiedatum}
        );
        $now    = $opts->{registratiedatum};
    } elsif (!$self->registratiedatum) {
        $self->registratiedatum($now);
    }

    ### Streefbare afhandeling
    if ($opts->{streefafhandeldatum}) {
        $self->streefafhandeldatum(
            $opts->{streefafhandeldatum}
        );
    } else {
        my ($norm, $type)   = (
            $self->zaaktype_node_id->zaaktype_definitie_id->servicenorm,
            $self->zaaktype_node_id->zaaktype_definitie_id->servicenorm_type
        );

        if ($opts->{streefafhandeldatum_data}) {
            $norm   = $opts->{streefafhandeldatum_data}->{termijn};
            $type   = $opts->{streefafhandeldatum_data}->{type};
        }

        $self->streefafhandeldatum(
            $self->_load_zaaktype_timing(
                $now,
                $norm,
                $type,
            )
        );
    }
}

sub insert {
    my $self    = shift;

    # we need the database to generate the new searchable_id param. 
    # if we don't supply it, it will take its chance. so get rid
    # of whatever the app thinks it should pass.
    # is there a way to put this in the Schema? that's be less hacky.
    delete $self->{_column_data}->{searchable_id};
    delete $self->{_column_data}->{object_type};

    $self->{_column_data}->{uuid} = $self->result_source->resultset->_generate_uuid
        unless (
            exists $self->{_column_data}->{uuid} &&
            $self->{_column_data}->{uuid}
        );

    $self->_handle_changes({insert => 1});
    $self->next::method(@_);
}

sub update {
    my $self    = shift;
    my $columns = shift;

    $self->set_inflated_columns($columns) if $columns;
    $self->_handle_changes;
    $self->next::method(@_);
}

sub _handle_changes {
    my $self    = shift;
    my $opt     = shift;
    my $changes = {};

    if ($opt && $opt->{insert}) {
        $changes = { $self->get_columns };
        $changes->{_is_insert} = 1;
    } else {
        $changes = { $self->get_dirty_columns };
    }

    $self->{_get_latest_changes} = $changes;

    $self->_set_search_string();

    return 1;
}

sub _set_search_string {
    my ($self) = @_;

    my $search_string = $self->zaaktype_node_id->titel || '';
    
    if($self->id) { # case may not exist yet
        $search_string .= " " . $self->id;
    }
    
    if($self->aanvrager && $self->aanvrager->naam) {
        $search_string .= " " . $self->aanvrager->naam;
    }
    
    if($self->behandelaar) {
        $search_string .= " " . $self->behandelaar->naam;
    }
    
    if($self->registratiedatum) {
        $search_string .= " " . $self->registratiedatum;
    }
    #$search_string .= " " . $self->locatie_zaak->id,
    
    if($self->onderwerp) {
        $search_string .= " " . $self->onderwerp;
    }
    
    if($self->zaaktype_node_id->zaaktype_trefwoorden) {
        $search_string .= " " . $self->zaaktype_node_id->zaaktype_trefwoorden;
    }   

    $self->search_term($search_string);
}

after 'insert'  => sub {
    my $self    = shift;

    $self->_handle_logging;
    $self->touch();
};

after 'update'  => sub {
    my $self    = shift;

    $self->_handle_logging;
    $self->touch();
};

=head2 touch(\%opts)

 $case->touch();

Touches a cases, and updates magic strings in onderwerp accordingly,
updates the search index etc.

=cut

has 'touched' => (
    is  => 'rw'
);

sub touch {
    my $self = shift;

    return if $self->touched;

    $self->touched(1);

    eval {
        $self->result_source->schema->txn_do(sub {
            $self->_refresh_onderwerp;
            $self->last_modified(DateTime->now);
            $self->update;
        });
    };

    $self->touched(0);

    if($@) {
        die("Could not touch case: $@");
        return;
    }
}

sub _refresh_onderwerp {
    my $self        = shift;

    if (
        $self->zaaktype_node_id &&
        $self->zaaktype_node_id->zaaktype_definitie_id && 
        $self->zaaktype_node_id->zaaktype_definitie_id->extra_informatie
    ) {
        my $ztt = Zaaksysteem::ZTT->new_from_case($self);
        my $extra_info = $self->zaaktype_node_id->zaaktype_definitie_id->extra_informatie;

        my $toelichting = $ztt->process_template($extra_info)->string;

        if ($toelichting) {
            $self->onderwerp(substr($toelichting,0,255));
        }
    }
}


# vernietiginsdatum set
# vernietingssatum in verleden
# idem voor deelzaken
has 'can_delete'   => (
    'is'    => 'ro',
    'lazy'  => 1,
    'default'   => sub {
        my ($self) = @_;

        warn 'CHECK ZAAK: ' . $self->id;
        return 1 if $self->is_deleted;

        $self->{recursion_protection_cases_seen} ||= {};
        return 1 if($self->{recursion_protection_cases_seen}->{$self->id});
        $self->{recursion_protection_cases_seen}->{$self->id} = 1;

        unless($self->is_afgehandeld) {
            warn "no can_delete, not is_afgehandeld"; 
            return 0;
        }

        unless($self->te_vernietigen) {
            warn "no can_delete, not te_vernietigen"; 
            return 0;
        }

        my @relations = $self->result_source->schema->resultset('CaseRelation')->get_all($self->id);

        for my $rel (@relations) {
            next unless $rel->type eq 'continuation';

            unless($rel->case->can_delete) {
                warn "Can't delete this case, continuation cases cannot be deleted, related case id: " . $rel->case->id;
                return 0;
            }
        }

        ### Open hoofdzak
        if ($self->pid) {
            my $parent = $self;

            while ($parent->pid) {
                $parent = $parent->pid;
                warn 'PARENT RETUJRN 0?';
                return 0 unless (
                    $parent->is_deleted ||
                    $parent->is_afgehandeld
                );
                warn 'PARENT RETUJRN 0!';
            }
        }

        my $zaak_children = $self->zaak_children->search();
        while(my $child = $zaak_children->next()) {
            if(!$child->can_delete) {
                warn 'CHILD RETUJRN 0';
                return 0;
            }
        }
        return 1;
    }
);



#
# options:
# - override: don't do integrity checks, just get rid of the thing
#

sub set_deleted {
    my ($self, $args) = @_;

    unless($args && $args->{override}) {
        die "can't delete this zaak" unless($self->can_delete);
    }
    # TODO children, relaties. cascade?
    
    eval {
        $self->result_source->schema->txn_do(sub {
            my $case_id = $self->id;

            my $current_user = $self->result_source
                ->schema
                ->resultset('Zaak')
                ->current_user->naam;

            $self->_delete_zaak($args);

            # When the subject of a case no longer has any active cases pointing to it,
            # any object subscriptions that exist should be closed.
            # TODO: move this to subject code after dropout merge.
            if($self->aanvrager->betrokkene_type eq 'natuurlijk_persoon') {
                my ($object_subscription) = $self->result_source->schema->resultset('ObjectSubscription')->search({
                    date_deleted => undef,
                    local_table => 'NatuurlijkPersoon',
                    local_id    => $self->aanvrager_object->gmid,
                });

                if ($object_subscription) {
                    my $has_active_case;
                    my $np_case_subject = $self->result_source->schema->resultset('ZaakBetrokkenen')->search({
                        betrokkene_type => 'natuurlijk_persoon',
                        gegevens_magazijn_id   => $self->aanvrager->gegevens_magazijn_id,
                        deleted         => undef,
                    });

                    while (my $case_subject = $np_case_subject->next()) {
                        for my $case ($case_subject->zaak_aanvragers) {
                            # Case needs to have status deleted, unless it's the one we are
                            # currently trying to delete.
                            if (!$case->is_deleted && $case->id != $self->id) {
                                $has_active_case = 1;
                                last;
                            }
                        }
                    }
                    if (!$has_active_case) {
                        $object_subscription->object_subscription_delete;
                    }
                }
            }

            $self->logging->trigger('case/delete', { component => 'zaak', data => {
                case_id         => $case_id,
                acceptee_name   => $current_user,
            }});
        });
    };

    if($@) {
        warn "couldn't deleted zaken because: " . $@;
        return;
    }

    return 1;
}

has 'is_deleted'    => (
    'is'        => 'ro',
    'lazy'      => 1,
    'default'   => sub {
        my $self    = shift;
        return 1 if ($self->deleted && $self->deleted < DateTime->now());

        return;
    }
);

sub _delete_zaak {
    my ($self, $args)   = @_;

    # TEMP OPLOSSING
    my $zaak_children = $self->zaak_children->search();
    while(my $child = $zaak_children->next()) {
        $child->set_deleted($args);
    }

    $self->logging->delete_all;

    $self->status('deleted');
    $self->deleted(DateTime->now());
    $self->update;

    return;

    $self->result_source->schema->storage->dbh_do(
        sub {
            my ($storage, $dbh, @args) = @_;
            $dbh->do("SET CONSTRAINTS ALL DEFERRED");
        },
    );

    my $strong_related_columns  = STRONG_RELATED_COLUMNS;

    for my $related_column ( @{ $strong_related_columns } ) {
        next unless $self->$related_column;

        my $related_reference;
        if (ref($self->$related_column)) {
            $related_reference = $self->$related_column;
        }

        if ($related_reference) {
            if ($related_reference->can('zaak_id')) {
                $related_reference->zaak_id(undef);
                $related_reference->update;
            }
            $related_reference->delete;
        }
    }

    ### Retrieve relaties
    my @relationships           = $self
        ->result_source
        ->relationships();

    for my $relationship (@relationships) {
        next unless ref($self->$relationship);
        my $relationship_object = $self->$relationship;

        warn 'RELATIONSHIP: ' . ref($relationship_object) . ':'
            . $relationship_object->result_source->name . ':'
            . $relationship;

        ### Ontkoppel gerelateerde zaken
        if (
            (
                $relationship eq 'zaak_vervolgers' ||
                $relationship eq 'zaak_relaties'
            ) &&
            $relationship_object->count
        ) {
            my $relations   = $relationship_object->search();

            while (my $relation = $relations->next) {
                if ($relation->vervolg_van eq $self->id) {
                    $relation->vervolg_van(undef);
                }
                if ($relation->related_to eq $self->id) {
                    $relation->related_to(undef);
                }

                $relation->update;
            }
        }

        if ($relationship eq 'pid') {
            ## Has parent, do not delete it
            next;
        }

        if (
            $relationship eq 'zaak_pids' &&
            $self->zaak_pids->count
        ) {
            warn('KILL CHILDREN');
            my $children    = $self->zaak_pids->search;

            while (my $child = $children->next) {
                $child->set_deleted($args);
            }
        }

        ### Protect other zaken
        if ($relationship_object->result_source->name eq 'zaak') {
            next;
        }

        ### Resultset
        next unless $relationship_object->can('count');

        ### Delete gerelateerde zaken
        $relationship_object->search()->delete_all;
    }

    die('ROLLBACK');
    $self->delete();
}

sub zaak_relaties {
    my $self = shift;

    return $self->result_source->schema->resultset('CaseRelation')->get_all(
        $self->get_column('id')
    );
}

sub _get_latest_changes {
    my $self    = shift;

    return $self->{_get_latest_changes};
}

sub _handle_logging {}

sub duplicate {
    my $self    = shift;
    $self->result_source->schema->resultset('Zaak')->duplicate( $self, @_ );
}

sub wijzig_zaaktype {
    my $self    = shift;
    $self->result_source->schema->resultset('Zaak')->wijzig_zaaktype($self, @_);
}

# CINE for create-if-not-exists
sub case_actions_cine {
    my $self = shift;

    # Don't apply rules to unsorted action resultsets, results go all screwy
    # as the actions may not be retrieved in order.
    my $rs = $self->case_actions(@_)->sorted;

    $rs->apply_rules({ case => $self });

    return $rs if $rs->count;

    $rs->create_from_case($self);

    return $rs->reset;
}

sub format_payment_status {
    my $self = shift;

    my $payment_status = $self->payment_status;

    if($payment_status) {
        return ZAAKSYSTEEM_CONSTANTS->{payment_statuses}->{$payment_status};
    }
}


sub TO_JSON {
    my $self = shift;

    return {
        id => $self->id,
        uuid => $self->uuid,
        pid => $self->pid,

        afhandeldatum => $self->afhandeldatum,
        contactkanaal => $self->contactkanaal,
        created => $self->created,
        deleted => $self->deleted,
        last_modified => $self->last_modified,
        milestone => $self->milestone,
        object_type => $self->object_type,
        onderwerp => $self->onderwerp,
        registratiedatum => $self->registratiedatum,
        streefafhandeldatum => $self->streefafhandeldatum,
        vernietigingsdatum => $self->vernietigingsdatum,
        zaaktype_id => $self->zaaktype_id,
        zaaktype_node_id => $self->zaaktype_node_id,
        days_perc => $self->status_perc,
        resultaat => $self->resultaat,

        route_ou => $self->route_ou,
        route_role => $self->route_role,

        locatie_correspondentie => $self->locatie_correspondentie,
        locatie_zaak => $self->locatie_zaak,

        aanvraag_trigger => $self->aanvraag_trigger,
        aanvrager => $self->aanvrager_object,
        behandelaar => $self->behandelaar_object,
        coordinator => $self->coordinator_object,
    };
}

1; #__PACKAGE__->meta->make_immutable;


=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut

