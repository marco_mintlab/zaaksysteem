package Zaaksysteem::TraitFor::Request::Params;

use Data::Dumper;
use JSON::XS;
use namespace::autoclean;
use Moose::Role;
use File::Slurp;
use Data::Visitor::Callback;

after 'parameters' => sub {
    my $self = shift;

    my ($content_type, $charset) = $self->headers->content_type;

    # Handle JSON in the body
    if ($self->_body && $content_type eq "application/json") {
        my $request_body = read_file($self->_body->body->filename);

        # Parse JSON
        my $json    = JSON::XS->new->utf8->allow_nonref->allow_blessed->convert_blessed;
        my $decoded = $json->decode($request_body);

        # Play whack-a-JSON::XS::Boolean, walk the hash recursively, beat the objects into submission
        my $visitor = Data::Visitor::Callback->new(
            'JSON::XS::Boolean' => sub { $_ = int }
        );

        $visitor->visit($decoded);

        # Add it to the parameters hash
        $self->{ parameters } = { %{ $self->{ parameters } }, %{ $decoded } };
    } 
};

1;
