package Zaaksysteem::Backend::BibliotheekSjablonen::ResultSet;

use strict;
use warnings;

use Moose;
use Params::Profile;
use Zaaksysteem::Constants;

extends 'DBIx::Class::ResultSet';

use Exception::Class (
    'Zaaksysteem::Backend::BibliotheekSjablonen::ResultSet::Exception' => {fields => 'code'},
    'Zaaksysteem::Backend::BibliotheekSjablonen::ResultSet::Exception::Parameter' => {
        isa         => 'Zaaksysteem::Backend::BibliotheekSjablonen::ResultSet::Exception',
        description => 'Parameter exception',
        alias       => 'throw_parameter_exception',
    },
);

1;
