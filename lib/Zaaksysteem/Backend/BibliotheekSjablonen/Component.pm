package Zaaksysteem::Backend::BibliotheekSjablonen::Component;

use strict;
use warnings;

use Moose;
use OpenOffice::OODoc;
use Params::Profile;
use Zaaksysteem::Constants;
use Data::Dumper;

use Zaaksysteem::ZTT;

extends 'DBIx::Class';

use Exception::Class (
    'Zaaksysteem::Backend::BibliotheekSjablonen::Component::Exception' => {fields => 'code'},
    'Zaaksysteem::Backend::BibliotheekSjablonen::Component::Exception::General' => {
        isa         => 'Zaaksysteem::Backend::BibliotheekSjablonen::Component::Exception',
        description => 'General exception',
        alias       => 'throw_general_exception',
    },
    'Zaaksysteem::Backend::BibliotheekSjablonen::Component::Exception::Parameter' => {
        isa         => 'Zaaksysteem::Backend::BibliotheekSjablonen::Component::Exception',
        description => 'Parameter exception',
        alias       => 'throw_parameter_exception',
    },
);

Params::Profile->register_profile(
    method  => 'file_create',
    profile => {
        required => [qw/
            case
            name
            subject
        /],
        optional => [qw/
            target_format
        /]
    }
);

sub file_create {
    my $self = shift;
    my $opts = $_[0];

    my $dv = Params::Profile->check(
        params  => $opts,
    );
    my $valid = $dv->valid;
    if ($dv->has_invalid) {
        my @invalid = join ',',$dv->invalid;
        throw_parameter_exception(
            code  => '/sjablonen/file_create/invalid_parameters',
            error => "Invalid options given: @invalid"
        );
    }

    my $tmp_dir = $self->_tmp_dir();

    my $target_format = 'odt';
    if (exists $valid->{target_format} && defined $valid->{target_format}) {
        $target_format = lc($valid->{target_format});
    }
    my $tmp_name = $opts->{name}.'.'.$target_format;
    my $tmp_file = "$tmp_dir/$tmp_name";

    if ($target_format !~ qr/(pdf|odt)/) {
        die "$target_format is unsupported for sjabloon conversion.";
    }
    elsif ($target_format eq 'pdf') {
        $self->filestore->convert({
            target_format => $target_format,
            target_file   => $tmp_file,
            case_id       => $opts->{case}->id,
            magic_strings_convert => 1,
        });
    }
    else {
        my $odf_document = $self->_get_odf_document_handle({tmp_dir => $tmp_dir});

        my $ztt = Zaaksysteem::ZTT->new_from_case($opts->{ case });

        my $document = $ztt->process_template($odf_document)->document;

        $document->save($tmp_file);    
    }

    my %optional;
    if ($opts->{case_document_ids}) {
        $optional{case_document_ids} = [$opts->{case_document_ids}];
    }

    my $file = $self->result_source->schema->resultset('File')->file_create({
        db_params => {
            case_id    => $opts->{case}->id,
            created_by => $opts->{subject},
        },
        file_path         => $tmp_file,
        name              => $tmp_name,
        publish_type_name => 'private',
        %optional
    }) or die "unable to create file";

    unlink($tmp_file);

    return $file;
}


sub _tmp_dir {
    my ($self) = @_;

    my $tmp_dir = $self->result_source->schema->resultset('Config')->get_value('tmp_location')
        or throw_general_exception(
            code  => '/sjablonen/file_create/general_exception',
            error => "Could not get config value for tmp_location"
        );

    # check if directory is writeable
    unless(-w $tmp_dir) {
        throw_general_exception(
            code  => '/sjablonen/file_create/general_exception',
            error => "tmp_dir $tmp_dir is not writable, check permissions."
        );
    }

    return $tmp_dir;
}


sub _get_odf_document_handle {
    my ($self, $options) = @_;

    my $tmp_dir = $options->{tmp_dir} or die "need tmp_dir";

    # Add in params checking later
    my ($filestore) = $self->filestore;

    my $encoding = $OpenOffice::OODoc::XPath::LOCAL_CHARSET;
    odfWorkingDirectory($tmp_dir);

    my $path = $filestore->ustore->getPath($filestore->uuid);

    unless($path) {
        throw_general_exception(
            code  => '/sjablonen/file_create/general_exception',
            error => "not able to retrieve path for filestore entry: ". $filestore->uuid
        );
    }

    unless(-r $path) {
        throw_general_exception(
            code  => '/sjablonen/file_create/general_exception',
            error => "could not get read from path: ". $path
        );
    }

    my $document = odfDocument(
        file            => $path,
        local_encoding  => $encoding,
    );

    # check if document is found. a likely explanation for not finding it is moving a database
    # without moving the file structure with it
    unless($document) {
        throw_general_exception(
            code  => '/sjablonen/file_create/general_exception',
            error => "could not get handle on odfDocument with ". $filestore->uuid
        );
    }

    return $document;
}

1;
