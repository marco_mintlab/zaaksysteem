=head1 NAME

Zaaksysteem::Backend::Sysin - System Integration development Docs

=head1 SYNOPSIS

    my $result = $schema->resultset('Interface')->interface_create({
        name         => 'Test Interface',   ### Module label (db field)
        module       => 'csv',              ### Module name
        interface_config => {
            how_hot      => 'should pass',  ### CSV custom field (interface_config)
        }
    });

    my $result = $result->interface_update({
        interface_config => {
            how_hot      => 'other value',  ### CSV custom field (interface_config)
        }
    });


=head1 DESCRIPTION

Our SYSIN module provides an interface between the administrator of zaaksysteem.nl
and our different services. 

=head1 COMPONENTS

The Sysin module consists of several components, which combined, form a complete
system integration system.

=head2 Modules

Using predefined modules, one can create different interfaces on Sysin. For more
information about writing a module, see L<Zaaksysteem::Backend::Sysin::Modules>

=head2 Interface

=head2 Transaction

=head1 SEE ALSO

L<Zaaksysteem::Manual> L<Zaaksysteem::Controller::Sysin> L<Zaaksysteem>

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Rudolf Leermakers

Marco Baan

Dario Gieselaar

Nick Diel

Laura van der Kaaij

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.




