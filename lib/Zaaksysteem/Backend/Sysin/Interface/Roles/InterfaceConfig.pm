package Zaaksysteem::Backend::Sysin::Interface::Roles::InterfaceConfig;

use Moose::Role;
use Zaaksysteem::Backend::Sysin::Modules;

use JSON;

around '_prepare_options' => sub {
    my $method              = shift;
    my $self                = shift;
    my ($opts)              = @_;

    $self->_load_module_defaults($opts);

    return $self->$method(@_) unless (
        UNIVERSAL::isa($opts, 'HASH') &&
        exists($opts->{interface_config}) &&
        UNIVERSAL::isa($opts->{interface_config}, 'HASH')
    );

    $opts->{interface_config} = $self->_encode_interface_config(
        $opts->{interface_config}
    );

    return $self->$method(@_);
};

sub _load_module_defaults {
    my ($self, $opts)       = @_;

    ### Get module
    my $module              = Zaaksysteem::Backend::Sysin::Modules
                            ->find_module_by_id($opts->{module});

    $opts->{max_retries}    ||= $module->max_retries;
    $opts->{multiple}       ||= $module->is_multiple;
}

sub _encode_interface_config {
    my $self        = shift;
    my $properties  = shift;

    return JSON::encode_json($properties);
}

sub _decode_interface_config {
    my $self        = shift;
    my $properties  = shift;

    return JSON::decode_json($properties);
}

1;