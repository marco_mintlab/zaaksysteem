package Zaaksysteem::Backend::Sysin::Interface::ResultSet;

use Moose;
use Zaaksysteem::Exception;

use Zaaksysteem::Backend::Sysin::Modules;

extends 'Zaaksysteem::Backend::ResultSet';

with 
    'Zaaksysteem::Backend::Sysin::Interface::Roles::InterfaceConfig';

has '_active_params' => (
    is          => 'ro',
    lazy        => 1,
    default     => sub {
        return {
            date_deleted     => undef,
        }
    }
);

=head2 interface_create

Add an interface definition to the database.

=head3 Arguments

=over

=item name [required]

A name chosen by the zaaksysteem administrator to describe this interface.

=item module [required]

The perl library that this interface is set up for. This has to be -exactly- the same.

=item interface_config [optional]

The JSON hash containing all relevant configuration details.

=item case_type_id [optional]

An optional case_type_id pointing to the zaaktype->id.

=item max_retries [optional, default 10]

Defines the number of automated retries on the transactions belonging to this
interface before the process gives up. Defaults are set in the module.

=item multiple [optional, default 0]

Defines whether or not this interface will always receive a single mutation or multiple mutations.

=item active [optional, default 0]

Defines whether or not the Interface should be considered as 'in production'.

=back

=head3 Returns

A newly created Sysin::Interface object.

=cut

Params::Profile->register_profile(
    method  => 'interface_create',
    profile => {
        missing_optional_valid  => 1,
        required => [qw/
        	name
            module
        /],
        optional => [qw/
            case_type_id
            max_retries
            multiple
            interface_config
            active
        /],
        constraint_methods => {
            name           => qr/^\w[\w\s]+$/,
            max_retries    => qr/^\d+/,
            multiple       => qr/(0|1)/,
            active         => qr/(0|1)/,
            module         => sub {
                my ($dfv, $val)     = @_;

                return 1 if (
                    grep { $_->name eq $val }
                    Zaaksysteem::Backend::Sysin::Modules->list_of_modules
                );

                return;
            },
            case_type_id    => sub {
                my ($dfv, $val)     = @_;
                my ($entry);

                return unless ref($val) || $val =~ /^\d+/;

                my $schema      = $dfv->get_input_data->{schema};

                my $zt_id       = (ref($val) ? $val->id : $val);

                return 1 if (
                    ($entry = $schema->resultset('Zaaktype')->find($zt_id)) &&
                    $entry->active
                    && !$entry->deleted
                );

                return;
            }
        },
        defaults => {
            active              => 0,
            interface_config    => '{}',
        }
    }
);

sub interface_create {
    my $self    = shift;
    my $opts    = assert_profile(
        {
            %{ shift() },
            schema  => $self->result_source->schema
        }
    )->valid;

    $self->_prepare_options($opts);

    return $self->create($opts);
}

sub find_by_module_name {
    my $self    = shift;
    my $module  = shift;

    my $ifaces  = $self->search_active(
        {
            module      => $module,
        }
    );

    throw(
        'sysin/interfaces/find_by_module_name/multiple_found',
        'Cannot get interface for module ' . $module . ': found multiple objects'
    ) unless $ifaces->count < 2;

    return $ifaces->first;
}

=head1 INTERNAL METHODS

=head2 _prepare_options

Allows Moose Roles to work on the options given to a create or update call.

=cut

sub _prepare_options { shift; return shift; }


1;