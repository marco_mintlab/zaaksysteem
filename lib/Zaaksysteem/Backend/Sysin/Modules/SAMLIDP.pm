package Zaaksysteem::Backend::Sysin::Modules::SAMLIDP;

use Moose;

use Zaaksysteem::ZAPI::Form;
use Zaaksysteem::ZAPI::Form::Field;

use Storable qw/dclone/;

extends 'Zaaksysteem::Backend::Sysin::Modules';

=head1 Interface Properties

Below a list of interface properties, see
L<Zaaksysteem::Backend::Sysin::Modules> for details.

=cut

use constant INTERFACE_ID               => 'samlidp';

use constant INTERFACE_CONFIG_FIELDS    => [
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_authentication_level',
        type        => 'select',
        label       => 'Authentication Level',
        data        => {
            options     => [
                {
                    value    => 'urn:oasis:names:tc:SAML:2.0:ac:classes:PasswordProtectedTransport',
                    label    => 'password'
                },
                {
                    value    => 'urn:oasis:names:tc:SAML:2.0:ac:classes:MobileTwoFactorContract',
                    label    => 'password+sms',
                }
            ],
        },
        required    => 1,
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_idp_metadata',
        type        => 'text',
        label       => 'SAML Metadata URL',
        required    => 1,
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_idp_ca',
        type        => 'file',
        label       => 'Certificaat CA van IDP (Digid/E-Herkenning/Etc)',
        required    => 0,
    ),
];

has 'name'          => (
    is          => 'ro',
    default     => INTERFACE_ID,
    lazy        => 1
);

has 'is_multiple'  => (
    is          => 'ro',
    default     => '1',
    lazy        => 1,
);

has 'interface_config'  => (
    is          => 'ro',
    lazy        => 1,
    default     => sub { return INTERFACE_CONFIG_FIELDS; }
);

has 'is_casetype_interface'  => (
    is          => 'ro',
    default     => '0',
    lazy        => 1,
);

has 'module_type' => (
    is          => 'ro',
    default     => sub { return ['auth']; },
    lazy        => 1,
);

has 'credential_module'     => (
    is          => 'ro',
    default     => 'saml',
    lazy        => 1,
);

sub _get_interface_config {
    my $self                = shift;
    my ($interface)         = @_;

    return $self->next::method(@_) unless $interface;

    my $iface_config        = dclone($self->interface_config);

    my $subject_model       = $interface
                            ->result_source
                            ->schema
                            ->resultset('Subject');

    my @subject_sources     = $subject_model->list_of_sources;

    my @options;
    push(
        @options,
        {
            label   => $subject_model->get_source_info($_)->{label},
            value   => $_
        }
    ) for @subject_sources;

    push (
        @{ $iface_config },
        Zaaksysteem::ZAPI::Form::Field->new(
            name        => 'interface_subject',
            type        => 'radio',
            label       => 'Activeer koppeling op subject',
            required    => 1,
            data        => {
                options     => \@options
            }
        ),
    );

    return $iface_config;
}

1;
