package Zaaksysteem::Backend::Sysin::Modules::STUFPRS;

use Moose;

use Zaaksysteem::ZAPI::Form;
use Zaaksysteem::ZAPI::Form::Field;

use JSON;

use Zaaksysteem::Exception;

extends 'Zaaksysteem::Backend::Sysin::Modules';

with qw/
    Zaaksysteem::Backend::Sysin::Modules::Roles::StUF
/;

###
### Interface Properties
###
### Below a list of interface properties, see
### L<Zaaksysteem::Backend::Sysin::Modules> for details.

use constant INTERFACE_ID               => 'stufprs';

use constant INTERFACE_CONFIG_FIELDS    => [];

use constant MODULE_SETTINGS            => {
    name                            => INTERFACE_ID,
    label                           => 'StUF Koppeling PRS',
    interface_config                => INTERFACE_CONFIG_FIELDS,
    direction                       => 'incoming',
    manual_type                     => ['text', 'file'],
    is_multiple                     => 0,
    is_manual                       => 1,
    retry_on_error                  => 1,
    allow_multiple_configurations   => 0,
    is_casetype_interface           => 0,
    trigger_definition              => {
        disable_subscription   => {
            method  => 'disable_subscription',
            #update  => 1,
        },
        search          => {
            method  => 'search_prs',
            #update  => 1,
        },
        import      => {
            method  => 'import_prs',
            #update  => 1,
        },
    },
    # has_attributes                  => 1,
    # attribute_list                  => [
    #     {
    #         external_name   => 'PRS.a-nummer',
    #         internal_name   => 'a_nummer',
    #         object          => 'natuurlijk_persoon',
    #         attribute_type  => 'defined'
    #     },
    #     {
    #         external_name   => 'PRS.bsn-nummer',
    #         internal_name   => 'burgerservicenummer',
    #         attribute_type  => 'defined'
    #     },
    #     {
    #         external_name   => 'PRS.voornamen',
    #         internal_name   => 'voornamen',
    #         object          => 'natuurlijk_persoon',
    #         attribute_type  => 'defined'
    #     },
    #     {
    #         external_name   => 'PRS.voorletters',
    #         internal_name   => 'voorletters',
    #         object          => 'natuurlijk_persoon',
    #         attribute_type  => 'defined'
    #     },
    #     {
    #         external_name   => 'PRS.voorvoegselGeslachtsnaam',
    #         internal_name   => 'voorvoegsel',
    #         object          => 'natuurlijk_persoon',
    #         attribute_type  => 'defined'
    #     },
    #     {
    #         external_name   => 'PRS.geslachtsnaam',
    #         internal_name   => 'geslachtsnaam',
    #         object          => 'natuurlijk_persoon',
    #         attribute_type  => 'defined'
    #     },
    #     {
    #         external_name   => 'PRS.geboortedatum',
    #         internal_name   => 'geboortedatum',
    #         object          => 'natuurlijk_persoon',
    #         attribute_type  => 'defined'
    #     },
    #     {
    #         external_name   => 'PRS.geslachtsaanduiding',
    #         internal_name   => 'geslachtsaanduiding',
    #         object          => 'natuurlijk_persoon',
    #         attribute_type  => 'defined'
    #     },
    #     {
    #         external_name   => 'PRS.datumOverlijden',
    #         internal_name   => 'datum_overlijden',
    #         object          => 'natuurlijk_persoon',
    #         attribute_type  => 'defined'
    #     },
    #     {
    #         external_name   => 'PRS.indicatieGeheim',
    #         internal_name   => 'indicatie_geheim',
    #         object          => 'natuurlijk_persoon',
    #         attribute_type  => 'defined'
    #     },
    #     {
    #         external_name   => 'PRS.aanduidingNaamgebruik',
    #         internal_name   => 'aanduiding_naamgebruik',
    #         object          => 'natuurlijk_persoon',
    #         attribute_type  => 'defined'
    #     },
    # ]
};

has 'stuf_object_type' => (
    'is'        => 'ro',
    'default'   => 'PRS'
);

has 'stuf_subscription_table' => (
    'is'        => 'ro',
    'default'   => 'NatuurlijkPersoon'
);

###
### BUILDARGS
###

around BUILDARGS => sub {
    my $orig  = shift;
    my $class = shift;

    return $class->$orig( %{ MODULE_SETTINGS() } );
};


=head1 NAME

Zaaksysteem::Backend::Sysin::Modules::STUFPRS - STUFPRS engine for StUF PRS related queries

=head1 SYNOPSIS

    # See testfile:
    # t/431-sysin-modules-stufprs.t

=head1 DESCRIPTION

STUFPRS engine for StUF PRS related queries

=head1 TRIGGERS

=head2 search($params)

=cut

sub search_prs {
    my $self            = shift;
    my $params          = shift || {};
    my $interface       = shift;

    throw(
        'sysin/modules/stuf' . lc($self->stuf_object_type) . '/search_prs/no_params',
        'Cannot search with empty query parameters'
    ) unless scalar keys %{ $params };

    my $module_cfg                  = $self->get_config_from_config_iface(
        $interface
    );

    my $stufmsg     = Zaaksysteem::StUF->new(
        entiteittype    => $self->stuf_object_type,
        stuurgegevens   => Zaaksysteem::StUF::Stuurgegevens->new(
            zender              => {
                applicatie          => 'ZSNL',
            },
            ontvanger           => {
                applicatie          => $module_cfg->{mk_ontvanger},,
            },
        ),
    )->search(
        {
            reference_id            => 'T' . time(),
            date                    => DateTime->now(),
            %{ $params }
        }
    );

    $stufmsg->load_from_interface($self->get_config_interface($interface));

    my $reply   = $stufmsg->dispatch;

    my @subjects;
    for (my $i = 0; $i < scalar(@{ $reply->body }); $i++) {
        my $params      = $reply->get_params_for_natuurlijk_persoon($i);
        my $adr_params  = $reply->get_params_for_natuurlijk_persoon_adres($i);

        my %params      = (%$params, %$adr_params);

        $params{sleutelGegevensbeheer} = $reply->as_params->[$i]->{sleutelGegevensbeheer};

        push(@subjects, \%params);
    }

    return \@subjects;
}

sub import_prs {
    my $self            = shift;
    my $params          = shift || {};
    my $interface       = shift;

    throw(
        'sysin/modules/stuf' . lc($self->stuf_object_type) . '/import_prs/no_params',
        'Cannot import with no param: sleutelGegevensbeheer'
    ) unless $params->{sleutelGegevensbeheer};

    my $module_cfg                  = $self->get_config_from_config_iface(
        $interface
    );

    my $stufmsg     = Zaaksysteem::StUF->new(
        entiteittype    => $self->stuf_object_type,
        stuurgegevens   => Zaaksysteem::StUF::Stuurgegevens->new(
            zender              => {
                applicatie          => 'ZSNL',
            },
            ontvanger           => {
                applicatie          => $module_cfg->{mk_ontvanger},
            },
        ),
    )->search(
        {
            reference_id            => 'T' . time(),
            date                    => DateTime->now(),
            sleutelGegevensbeheer   => $params->{sleutelGegevensbeheer},
        }
    );

    $stufmsg->load_from_interface($self->get_config_interface($interface));

    my $reply       = $stufmsg->dispatch;

    $interface->process(
        {
            external_transaction_id => 'unknown',
            input_data              => $reply->parser->xml,
            processor_params        => {
                processor               => '_process_import_entry',
                sleutelGegevensbeheer   => $params->{sleutelGegevensbeheer}
            },
            direct                  => 1,
        }
    );
}

sub _process_import_entry {
    my $self                        = shift;
    my ($record,$object)            = @_;

    my $params                      = $record
                                    ->transaction_id
                                    ->get_processor_params();

    my $module_cfg                  = $self->get_config_from_config_iface(
        $record->transaction_id->interface_id
    );

    my $stufmsg                     = Zaaksysteem::StUF->new(
        entiteittype    => $self->stuf_object_type,
        stuurgegevens   => Zaaksysteem::StUF::Stuurgegevens->new(
            zender              => {
                applicatie          => 'ZSNL',
            },
            ontvanger           => {
                applicatie          => $module_cfg->{mk_ontvanger},
            },
        ),
    )->set_afnemerindicatie(
        {
            reference_id            => $record->transaction_id->id,
            date                    => $record->transaction_id->date_created,
            sleutelGegevensbeheer   => $params->{sleutelGegevensbeheer}
        }
    );

    $stufmsg->load_from_interface(
        $self->get_config_interface(
            $record->transaction_id->interface_id
        )
    );


    eval {
        my $reply_object = $stufmsg->dispatch;
    };

    if ($@) {
        if (
            UNIVERSAL::isa($@, 'Zaaksysteem::Exception::Base') &&
            $@->type eq 'stuf/soap/no_answer'
        ) {
            throw(
                $@->type,
                "\nResponse:\n" .
                $@->object->response->content
            );
        }

        die ($@);
    }

    my $stuf_prs    = Zaaksysteem::StUF->from_xml(
        $record->transaction_id->input_data,
        {
            cache => ($record->result_source->schema->default_resultset_attributes->{'stuf_cache'} ||= {})
        }
    );

    my $entry       = $self->stuf_create_entry(
        $record, $stuf_prs
    );

    $self->_add_subscription_for_entry(
        $record, $stufmsg, $entry
    );
}



=head1 PROCESSORS

=head2 CREATE SUBJECT

=head2 $module->stuf_create_entry($transaction_record, $rowobject)

Return value: $ROW_NATUURLIJK_PERSOON

Creates a new L<Zaaksysteem::DB::Component::NatuurlijkPersoon> into our database,
and sets a subscription between our data record and theirs via C<ObjectSubscription>

B<Params>

=over 4

=item $transaction_record

The L<Zaaksysteem::Backend::Sysin::Transaction::Component> row. It will be filled
with object_preview, a preview string describing the data object changed.

=item $rowobject

The source of the data, in case of a CSV this would proba

=back

=cut

sub _check_gemeentecode {
    my $self                        = shift;
    my $record                      = shift;
    my $object                      = shift;
    my $gemeentecode                = shift;

    if (!$self->get_config_from_config_iface($record->transaction_id->interface_id)->{gemeentecode}) {
        $self->stuf_throw(
            $object,
            'sysin/modules/stufprs/stuf_create_entry/no_gemeente_code_set',
            'No gemeentecode found in configuration, please set it'
        );
    }

    if (!$gemeentecode) {
        $self->stuf_throw(
            $object,
            'sysin/modules/stufprs/stuf_create_entry/no_gemeente_code',
            'No gemeentecode found in StUF message, invalid XML'
        );
    }
}

sub stuf_create_entry {
    my $self                = shift;
    my ($record, $object)   = @_;

    my $natuurlijk_persoon  = $self->_create_subject_natuurlijk_persoon(@_);
    my $adres               = $self->_create_subject_adres(@_, $natuurlijk_persoon);

    $self->_check_gemeentecode($record, $object, $adres->gemeente_code);

    $natuurlijk_persoon->adres_id($adres->id);
    $natuurlijk_persoon->update;

    $record->preview_string(
        $natuurlijk_persoon->voornamen . ' ' . (
            $natuurlijk_persoon->voorvoegsel
                ? $natuurlijk_persoon->voorvoegsel . ' '
                : ''
        ) . $natuurlijk_persoon->geslachtsnaam
    );

    return $natuurlijk_persoon;
}

sub _create_subject_natuurlijk_persoon {
    my $self                = shift;
    my ($record, $object)   = @_;

    my $subject_params      = $object->get_params_for_natuurlijk_persoon;

    my $np_rs               = $record
                            ->result_source
                            ->schema
                            ->resultset('NatuurlijkPersoon');


    ### BACKWARDS compatability
    my ($entry);
    if (
        !(
            $entry = $self->_create_subject_natuurlijk_persoon_bwcompat(
                @_, $subject_params, $np_rs
            )
        )
    ) {
        $entry               = $np_rs->create(
            {
                %{ $subject_params },
                authenticated       => 1,
                authenticatedby     => 'gba'
            }
        );
    }

    my $mutation_record     = Zaaksysteem::Backend::Sysin::Transaction::Mutation
                            ->new(
                                table       => 'NatuurlijkPersoon',
                                table_id    => $entry->id,
                                create      => 1
                            );

    $mutation_record->from_dbix($entry);

    push(
        @{ $self->process_stash->{row}->{mutations} },
        $mutation_record
    );

    return $entry;
}

=head2 $module->_create_subject_natuurlijk_persoon_bwcompat(@params)

Makes sure we do not create duplicates for already created natuurlijk_personen. Just
make sure the "afnemerindicatie" is set and the subject gets updated.

 Params:
 transaction_record, $rowobject, $subject_params, $natuurlijk_persoon

=cut

sub _create_subject_natuurlijk_persoon_bwcompat {
    my $self                        = shift;
    my ($record, $object, $subject_params, $np_rs)   = @_;

    my $entry                       = $np_rs->search(
        {
            burgerservicenummer     => $subject_params->{burgerservicenummer},
            authenticated           => 1,
            authenticatedby        => 'gba'
        }
    )->first;

    if ($entry) {
        $entry->update($subject_params);
    }

    return $entry;
}


sub _create_subject_adres {
    my $self                = shift;
    my ($record, $object, $natuurlijk_persoon)   = @_;

    my $address_params      = $object->get_params_for_natuurlijk_persoon_adres;

    my ($entry);
    if ($natuurlijk_persoon->adres_id) {
        $entry = $natuurlijk_persoon->adres_id;
    } else {
        my $adr_rs              = $record
                                ->result_source
                                ->schema
                                ->resultset('Adres');

        $entry               = $adr_rs->create($address_params);       
    }

    my $mutation_record     = Zaaksysteem::Backend::Sysin::Transaction::Mutation
                            ->new(
                                table       => 'Adres',
                                table_id    => $entry->id,
                                create      => 1
                            );

    $mutation_record->from_dbix($entry);

    push(
        @{ $self->process_stash->{row}->{mutations} },
        $mutation_record
    );

    return $entry;
}

=head2 UPDATE SUBJECT

=head2 $module->stuf_update_entry($record, $object)

Updates a PRS entry in our database

=cut


sub stuf_update_entry {
    my $self                = shift;
    my ($record, $object)   = @_;

    my $natuurlijk_persoon;
    eval {
        $natuurlijk_persoon = $self->get_entry_from_subscription(@_, 'NatuurlijkPersoon');
    };

    if ($@) {
        ### We probably did not find a subscription for this entry, check via bwcompat
        my $subject_params  = $object->get_params_for_natuurlijk_persoon;

        my $entries         = $record
                            ->result_source
                            ->schema
                            ->resultset('NatuurlijkPersoon')
                            ->search(
                                {
                                    burgerservicenummer         => $subject_params->{burgerservicenummer},
                                    authenticated               => 1,
                                    authenticatedby             => 'gba'
                                }
                            );

        if ($entries->count) {
            $natuurlijk_persoon         = $entries->first;

            ### Make sure this won't happen again
            $self->_add_subscription_for_entry($record, $object, $natuurlijk_persoon);
        } else {
            die($@);
        }
    }

    $self->_update_subject_natuurlijk_persoon(
        @_, $natuurlijk_persoon
    );


    my $adres               = $self->_update_subject_adres(@_, $natuurlijk_persoon);

    $self->_check_gemeentecode($record, $object, $adres->gemeente_code);

    $record->preview_string(
        $natuurlijk_persoon->voornamen . ' ' . (
            $natuurlijk_persoon->voorvoegsel
                ? $natuurlijk_persoon->voorvoegsel . ' '
                : ''
        ) . $natuurlijk_persoon->geslachtsnaam
    );

    my $subject_params      = $object->get_params_for_natuurlijk_persoon;

    my $module_cfg                  = $self->get_config_from_config_iface(
        $record->transaction_id->interface_id
    );



    ### Special CASE: Overlijden
    if ($subject_params->{datum_overlijden}) {
        ### Person died, let's disconnect afnemerindicatie, and make sure
        ### we immediatly delete this person from our database
        my $object_params       = $object->as_params;

        my $object_subscription = $record
                                ->result_source
                                ->schema
                                ->resultset('ObjectSubscription')
                                ->search(
                                    {
                                        interface_id    => $record
                                                        ->transaction_id
                                                        ->interface_id
                                                        ->id,
                                        local_table     => $self->stuf_subscription_table,
                                        external_id     => $object_params->{sleutelGegevensbeheer}, 
                                    }
                                )->first;

        if (
            $object_subscription &&
            $module_cfg->{bidirectional}
        ) {
            $self
                ->process_stash
                ->{transaction}
                ->interface_id
                ->process_trigger(
                    'disable_subscription',
                    {
                        subscription_id => $object_subscription,
                    }
                );
        }


        $self->stuf_delete_entry($record, $object);
    }

    ### Special CASE: Verhuizen
    if (
        $object->is_verhuisd &&
        $module_cfg->{automatic_desubscription_moved_prs}
    ) {
        my $object_params       = $object->as_params;

        my $object_subscription = $record
                                ->result_source
                                ->schema
                                ->resultset('ObjectSubscription')
                                ->search(
                                    {
                                        interface_id    => $record
                                                        ->transaction_id
                                                        ->interface_id
                                                        ->id,
                                        local_table     => $self->stuf_subscription_table,
                                        external_id     => $object_params->{sleutelGegevensbeheer}, 
                                    }
                                )->first;

        if (
            $object_subscription &&
            $module_cfg->{bidirectional}
        ) {
            $self
                ->process_stash
                ->{transaction}
                ->interface_id
                ->process_trigger(
                    'disable_subscription',
                    {
                        subscription_id => $object_subscription,
                    }
                );
        }


        $self->stuf_delete_entry($record, $object);
    }

    return $natuurlijk_persoon;    
}

sub _update_subject_natuurlijk_persoon {
    my $self                                = shift;
    my ($record, $object, $entry)           = @_;

    my $subject_params      = $object->get_params_for_natuurlijk_persoon;
    my %old_values          = $entry->get_columns;

    unless ($entry->update($subject_params)->discard_changes) {
        $self->stuf_throw(
            $object,
            'sysin/modules/stufprs/process/np_update_error',
            'Impossible to update NatuurlijkPersoon in our database, unknown error'
        );
    }

    my $mutation_record     = Zaaksysteem::Backend::Sysin::Transaction::Mutation
                            ->new(
                                table       => 'NatuurlijkPersoon',
                                table_id    => $entry->id,
                                update      => 1
                            );

    ### Log mutation
    $mutation_record->from_dbix($entry, \%old_values);

    push(
        @{ $self->process_stash->{row}->{mutations} },
        $mutation_record
    );

    return $entry;
}

sub _update_subject_adres {
    my $self                                = shift;
    my ($record, $object, $natuurlijk_persoon)    = @_;

    my $entry               = $natuurlijk_persoon->adres_id;

    $self->stuf_throw(
        $object,
        'sysin/modules/stufprs/process/no_adres_entry_found',
        'No existing NatuurlijkPersoon found for this subject (PRS) in our database'
    ) unless $entry;

    my $subject_params      = $object->get_params_for_natuurlijk_persoon_adres;
    my %old_values          = $entry->get_columns;

    unless ($entry->update($subject_params)->discard_changes) {
        $self->stuf_throw(
            $object,
            'sysin/modules/stufprs/process/adres_update_error',
            'Impossible to update Adres in our database, unknown error'
        );
    }

    my $mutation_record     = Zaaksysteem::Backend::Sysin::Transaction::Mutation
                            ->new(
                                table       => 'Adres',
                                table_id    => $entry->id,
                                update      => 1
                            );

    ### Log mutation
    $mutation_record->from_dbix($entry, \%old_values);

    push(
        @{ $self->process_stash->{row}->{mutations} },
        $mutation_record
    );

    return $entry;
}

=head2 DELETE SUBJECT

=head2 stuf_delete_entry

=cut

sub stuf_delete_entry {
    my $self                                = shift;
    my ($record, $object, $subscription)    = @_;


    my $natuurlijk_persoon                  = $self->get_entry_from_subscription(
                                                $record,
                                                $object,
                                                'NatuurlijkPersoon',
                                                $subscription
                                            );

    $record->preview_string(
        $natuurlijk_persoon->voornamen . ' ' . (
            $natuurlijk_persoon->voorvoegsel
                ? $natuurlijk_persoon->voorvoegsel . ' '
                : ''
        ) . $natuurlijk_persoon->geslachtsnaam
    );

    ### Delete subject from database
    my $address             = $natuurlijk_persoon->adres_id;
    $address->deleted_on(DateTime->now());
    $address->update;

    $natuurlijk_persoon->deleted_on(DateTime->now());
    $natuurlijk_persoon->update;

    $self->_remove_subscription_from_entry($record, $object, $natuurlijk_persoon);
}



# sub enable_subscription {
#     my $self                = shift;
#     my ($subscription, $natuurlijk_persoon)   = @_;

#     my $stuf                = Zaaksysteem::StUF->new(
#         entiteittype    => 'PRS',
#         stuurgegevens   => Zaaksysteem::StUF::Stuurgegevens->new(

#         ),
#         namespace       =>
#         element         => ''
#     );

#     $stuf->request_afnemer  = 

#     my $record              = $self->get_single_transaction_record(
#         {
#             interface       => $interface,
#             direction       => 'outgoing',
#             input_data      =>
#         }
#     )

# }




=head1 INTERNAL METHODDS



=head1 EXAMPLES

See L<SYNOPSIS>

=head1 SEE ALSO

L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::Template>

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Rudolf Leermakers

Marco Baan

Dario Gieselaar

Nick Diel

Laura van der Kaaij

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut

1;
