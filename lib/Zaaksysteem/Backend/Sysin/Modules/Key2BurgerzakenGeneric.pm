package Zaaksysteem::Backend::Sysin::Modules::Key2BurgerzakenGeneric;

use Moose::Role;


sub _process_get_next_row {
    my $self        = shift;

    return if ($self->process_stash->{once});

    return unless (
        $self->process_stash->{transaction}->get_processor_params ||
        $self->process_stash->{transaction}->input_data
    );

    $self->process_stash->{once} = 1;

    if (my $params = $self->process_stash->{transaction}->get_processor_params) {
        return ($params, Data::Dumper::Dumper($params));
    } else {
        return (
            $self->process_stash->{transaction}->input_data,
            $self->process_stash->{transaction}->input_data
        );
    }
}


sub _process_selector {
    my $self            = shift;
    my $object          = shift;

    my $transaction     = $self->process_stash->{transaction};

    if (my $processor_params = $transaction->get_processor_params()) {
        return $self->can($processor_params->{processor})
    }

    return $self->can('_process_row');
}


1;
