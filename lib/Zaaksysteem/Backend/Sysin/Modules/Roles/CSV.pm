package Zaaksysteem::Backend::Sysin::Modules::Roles::CSV;

use Moose::Role;
use Zaaksysteem::Exception;

use Text::CSV;

=head2 _csv_validate_header

Return value: $BOOL_SUCCESS

    $self->_csv_validate_header($filestore_object);

Checks for a valid CSV file and validates the header against the C<attribute_list>
attribute.

A valid CSV has a header containing all the C<external_name> attributes as given
in the attribute_list.

B<Errors>

Throws a L<Zaaksysteem::Exception::Base> error, when an error occurs.

=over 4

=item modules/csv/no_attributes_list

No C<attribute_list> is set, validating a header is useless

=item modules/csv/file_error

Cannot read the given filestore->get_path path.

=item modules/csv/no_rows

Cannot find any rows in this CSV

=item modules/csv/invalid_header

Invalid header, it does not contain all the defined fields in C<attribute_list>.

The returning object is in the form:

 {
    name    => 'header_column1',
    missing => 1,
 },
 {
    name    => 'header_column2',
    invalid => 1,
 }

=back

=cut

sub _csv_validate_header {
    my $self        = shift;
    my $filestore   = shift;


    throw(
        'modules/csv/no_attributes_list',
        'Cannot find CSV header'
    ) unless $self->attribute_list && scalar (@{ $self->attribute_list });

    my ($csv, $fh)  = $self->_get_csv_and_fh($filestore);
    my $header      = $csv->getline($fh);

    $csv->eof or $csv->error_diag();
    close $fh;

    throw(
        'modules/csv/no_rows',
        'Invalid CSV'
    ) unless $header;

    my @invalid_cols;
    for (my $i = 0; $i < scalar(@{ $self->attribute_list }); $i++) {
        my $csv_column = $header->[ $i ];
        my $def_column = $self->attribute_list->[$i]->{external_name};

        if (!$csv_column) {
            push(@invalid_cols,
                {
                    name    => $def_column,
                    missing => 1,
                }
            );
            next;
        }

        if ($csv_column ne $def_column) {
            push(@invalid_cols,
                {
                    name    => $def_column,
                    invlid  => 1,
                }
            );
            next;
        }
    }

    throw(
        'modules/csv/invalid_header',
        'Columns from csv do not match attribute definition: '
        . join(',', map { $_->{name} } @invalid_cols),
        \@invalid_cols
    ) if @invalid_cols;
}

sub _get_csv_and_fh {
    my $self        = shift;
    my $filestore   = shift;

    my $options = { binary => 1 };

    if(exists $self->parser_options->{sep_char}) {
        $options->{sep_char} = $self->parser_options->{sep_char};
    }

    my $csv = Text::CSV->new($options);

    open my $fh, "<:encoding(utf8)", $filestore->get_path or
        throw('modules/csv/file_error', 'Cannot open file: ' . $!);

    return ($csv, $fh);
}

sub _process_validate_input {
    my $self            = shift;
    my $transaction     = shift;

    ### We do not allow string CSV
    throw(
        'sysin/bagcsv/invalid_input_format',
        'Invalid input format, we only allow filestore references'
    ) if (
        $self->parser_options->{input_file_only} &&
        !$transaction->input_file
    );

    ### Check for valid CSV, checks header etc
    $self->_csv_validate_header($transaction->input_file)
        if $self->parser_options->{validate_header}
}

sub _process_get_next_row {
    my $self        = shift;

    $self->_csv_load_stash();

    my $csv         = $self->process_stash->{csv}->{parser};
    my $fh          = $self->process_stash->{csv}->{fh};

    return if $csv->eof();

    my $row         = $csv->getline_hr($fh);

    return unless $row;
    
    my @string      = @_;
    for my $key ($csv->column_names) {
        push(@string, $row->{ $key });
    }
    
    return ($row, '"' . join('","', @string) . '"');
}

sub _csv_load_stash {
    my $self        = shift;

    return if $self->process_stash->{csv}->{fh};

    my $filestore   = $self
                    ->process_stash
                    ->{transaction}
                    ->input_file;

    my ($csv, $fh)  = $self->_get_csv_and_fh(
        $filestore
    );

    $self->_csv_load_header($csv, $fh);

    $self->process_stash->{csv} = {
        parser      => $csv,
        fh          => $fh
    };
}

sub _csv_load_header {
    my $self        = shift;
    my $csv         = shift;
    my $fh          = shift;

    return unless $self->attribute_list && scalar (@{ $self->attribute_list });

    $csv->column_names($csv->getline($fh));
}

1;
