package Zaaksysteem::Backend::Sysin::Modules;

use Moose;
use JSON;

use Data::Dumper;

use Zaaksysteem::Exception;

use Zaaksysteem::ZAPI::Form;
use Zaaksysteem::ZAPI::Form::Field;
use Zaaksysteem::ZAPI::Form::FieldSet;

use Storable qw(dclone);

use constant    MODULELIST  => [qw/
    +SAMLIDP
    +SAMLSP
    +BAGCSV
    +Key2Finance
    +STUFCONFIG
    +STUFPRS
    +STUFNNP
    +STUFADR
    +STUFVBO
    +Email
    +Key2BurgerzakenVerhuizing
/];

use constant    MODULE_ATTRS    => [qw/
    name
    direction
    is_multiple
    allow_multiple_configurations
    is_casetype_interface
    has_attributes
    attribute_type
    manual_type
    max_retries
    retry_interval
    label
    test_interface
    additional_description
/];

=head1 NAME

Zaaksysteem::Backend::Sysin::Modules - A base class for Sysin Interface Modules.

=head1 SYNOPSIS

    my $module = Zaaksysteem::Backend::Sysin::Modules::CSV->new();

    $module->process();

=head1 DESCRIPTION

Modules are interfaces with the outside world in catalyst. These interfaces can
be configured in our sysin/interface component.

=head1 ATTRIBUTES

=head2 name

Type: String

Identifier of this module, use module name in lower case,
e.g. 'gba_via_stuf'. Please do not use whitespace for the identifier and make
sure it is lowercase.

=cut

has 'name'        => (
    is          => 'ro',
    required    => 1,
    isa         => 'Str',
);

=head2 label

Type: String

Friendly string describing this integration module.

=cut

has 'label'        => (
    is          => 'ro',
    isa         => 'Str',
    lazy        => 1,
    default     => sub {
        return ucfirst(shift->name);
    }
);

=head2 direction

Type: String [One of "outgoing", "incoming"]

Identifies whether input data is generated for an outgoing or incoming request.

e.g:
incoming    - A StUF GBA kennisgeving
outgoing    - A generated CSV for download by external parties.

=cut

has 'direction'     => (
    is          => 'rw',
    default     => 'incoming',
    lazy        => 1,
);

=head2 allow_multiple_configurations

Type: Bool

Defines whether or not this module can be configured more than once. For instance,
a module named Digid SAML can only be used once.

=cut

has 'allow_multiple_configurations' => (
    is          => 'ro',
    lazy        => 1,
    required    => 1,
    isa         => 'Bool',
    default     => 0,
);

=head2 module_type [optional]

Type: ArrayRef

There are different types of modules, which can be used for automatic module
resolving. For instance: module_type(['auth']), defines this is an authentication
module.

This way, our frontend knows which authentication types we support, for example,
by authenticating to zaaksysteem.nl

=cut

has 'module_type'   => (
    is          => 'ro',
    lazy        => 1,
    required    => 0,
    isa         => 'ArrayRef',
    default     => sub { return []; }
);

=head2 is_multiple

Type: Boolean

Defines whether this interface receives bulk transactions, or single
transactions

=cut

has 'is_multiple'  => (
    is          => 'ro',
    required    => 1,
    isa         => 'Bool',
);


=head2 is_manual

Type: Boolean

Defines whether this interface is a manual interface, this could be an upload
interface.

When manual is set, we do not supply active or test infromation.

=cut

has 'is_manual'  => (
    is          => 'ro',
    default     => '0',
    isa         => 'Bool',
);

=head2 manual_type

Type: String (one of 'file',)

When interface is a manual interface, defines the way of inserting information

=cut

has 'manual_type'  => (
    is          => 'ro',
    default     => sub { ['file'] },
    isa         => 'Maybe[ArrayRef]',
);

=head2 retry_on_error

Type: BOOL
Default: 0

Retries a transaction on error, when transaction failed.

=cut

has 'retry_on_error'    => (
    is          => 'ro',
    default     => 0,
);

=head2 max_retries

Type: Integer

Maximum number of retries, before giving up trying to connect to the other side

=cut

has 'max_retries' => (
    is          => 'ro',
    isa         => 'Num',
    lazy        => 1,
    required    => 1,
    default     => 10,
);

=head2 retry_interval

Type: Array

Array of retry intervals in seconds between request. Example:

 [
    300,
    300,
    900,
    3600,
    7200,
    84600
 ]

This example will retry after 300 seconds the first and second time, than after
900 seconds, 3600 seconds, 7200 seconds, and finally after one day. If
C<max_retries> is higher than the amount of retry intervals, it will use the
last use retry interval. In the case of this example, it will try for another
4 days before giving up.

Default:

        300,        # 5 minutes
        300,
        1800,       # 30 minutes
        14400,      # 4 hours
        14400,
        14400,
        86400,      # 1 day
        86400,
        604800,     # 1 week

Which totals to approx. 2 weeks and 2.5 days (when using 10 retries): 

=cut

has 'retry_interval' => (
    is          => 'ro',
    isa         => 'ArrayRef',
    lazy        => 1,
    required    => 1,
    default     => sub { return [
            300,        # 5 minutes
            300,
            1800,       # 30 minutes
            14400,      # 4 hours
            14400,
            14400,
            86400,      # 1 day
            86400,
            604800,     # 1 week
        ];
    }
);

=head2 has_attributes

Type: Boolean

Defines whether this interface has attributes which need to be mapped. This
can be a predefined set of attributes, or could be a dynamic set of attributes
within a given object.

=cut

has 'attribute_type'  => (
    is          => 'ro',
    default     => 'predefined',   # OR freeform
    isa         => 'Str',
);

=head2 has_attributes

Type: Boolean

Defines whether this interface has attributes which need to be mapped. This
can be a predefined set of attributes, or could be a dynamic set of attributes
within a given object.

=cut

has 'has_attributes'  => (
    is          => 'ro',
    default     => '0',
    isa         => 'Bool',
);

=head2 attribute_list

Type: HashRef

An attribute list defines a possibility to match interface attributes with
zaaksysteem.nl attributes. There are a couple of ways to do this, which are
explained below.

Every mode has a few generic options

=head3 Options

=over 4

=item optional

Defines whether the given attribute is optional. Some interface attributes are
not necessary for proper function of the interface, other are.

All attributes are default required, unless the C<optional> attribute is
defined.

=back

=head3 Attribute Modes

=over 4

=item Match against casetype

    [
        {
            external_name   => 'melding_number'
            from_casetype   => 1,
            optional        => 1,
        },
        {
            external_name   => 'melding_categorie',
            from_casetype   => 1,
        }
    ]
 
In this mode, you match a given attribute against an attribute in the given
case_type.

=item Match against an object attribute

 ### Freeform, with given attribute list
 [
    {
        external_name   => 'bsn-number',
        freeform        => 1,
        object          => 'natuurlijk_persoon' ### Spotenlighter object
    },
    {
        external_name   => 'kvk-nummber',
        freeform        => 1,
        object          => 'bedrijf'            ### Spotenlighter object
    },
 ]

This is especially useful when using the future object management module. By
defining an object, an administrator can use the spotenlighter to find the
belonging attribute

=item Predefined list

 ### Solid list
 [
    {
        external_name   => 'bsn-number',
        internal_name   => 'burgerservicenummer',
        internal_object => 'natuurlijk_persoon'
    },
    {
        external_name   => 'a-nummer',
        internal_name   => 'a_nummer',
        internal_object => 'natuurlijk_persoon'
    },
    {
        external_name   => 'geboortePlaats',
        internal_name   => 'geboorteplaats',
        internal_object => 'natuurlijk_persoon',
        optional        => 1
    },
    {
        external_name   => 'bsn-number',
        internal_name   => 'burgerservicenummer',
        internal_object => 'natuurlijk_persoon'
    },
 ]

In this case, all attributes are solid. You can think of an import module for
organizational contacts. This is a predefined object in zaaksysteem, and there
is not much room left for customization.

We still show this list for more information, and the possibility to turn off
optional fields.

=back

=cut

has 'attribute_list'  => (
    is          => 'ro',
    isa         => 'ArrayRef',
);

=head2 interface_config

Type: ArrayRef of L<Zaaksysteem::ZAPI::Form::Field>

Interface configuration. An object information about the required fields
for this interface. See L<Zaaksysteem::ZAPI::Form::Field> for more details.

=cut

has 'interface_config'  => (
    is          => 'ro',
    required    => 1,
    isa         => 'ArrayRef'
);

=head2 is_casetype_interface

Type: Boolean

Marks this interface as an interface related to a single zaaktype. This makes
the zaaktype configurable in the interface configuration.

=cut

has 'is_casetype_interface'  => (
    is          => 'ro',
    required    => 1,
    isa         => 'Bool',
);

=head2 parser_options

Type: HashRef

Options for the specific parser, a way to define the seperator for the CSV
parser etc.

=cut

has 'parser_options'  => (
    is          => 'ro',
    required    => 0,
    isa         => 'HashRef',
);

=head2 trigger_definition

Type: HashRef

Hashref containing available methods on this module, which can be triggered.

=cut

has 'trigger_definition'  => (
    is          => 'ro',
    required    => 0,
    isa         => 'HashRef',
    lazy        => 1,
    default     => sub { return {}; }
);

=head2 case_hooks

Type: ArrayRef

=cut

has 'case_hooks'            => (
    'is'        => 'rw',
    'isa'       => 'Maybe[ArrayRef]',
    'default'   => sub { return []; },
    'lazy'      => 1,
);

=head2 test_interface

Type: Bool

=cut

has 'test_interface'            => (
    'is'        => 'rw',
    'isa'       => 'Maybe[Bool]',
);

=head2 test_definition

Type: HashRef

=cut

has 'test_definition'            => (
    'is'        => 'rw',
    'isa'       => 'Maybe[HashRef]',
    'default'   => sub { return []; },
    'lazy'      => 1,
);

has 'additional_description'    => (
    'is'        => 'rw',
);

=head1 METHODS

=head2 get_attribute_mapping([ $interface_row ])

 my $mapping = $self->get_attribute_mapping;

=cut

sub get_attribute_mapping {
    my $self        = shift;
    my $interface   = shift;
    my $rv          = {
        attributes          => [],
    };
    my ($attributes);


    if ($interface && exists($interface->get_interface_config->{attribute_mapping})) {
        $attributes     = $interface->get_interface_config->{attribute_mapping};
    }

    $rv->{attributes}               = (
        $self->_prepare_attribute_list($attributes, $interface)
    );

    return $rv;
}

sub _prepare_attribute_list {
    my $self                = shift;
    my $interface_config    = shift;
    my $interface           = shift;

    unless ($interface_config) {
        $interface_config   = dclone($self->attribute_list);
    }

    my $attribute_list      = $self->attribute_list;

    ### Merge missed
    my @new_config          = ();

    my @missed_attributes;
    {
        my %configured_attributes   = map {
            $_->{external_name} => 1
        } @{ $interface_config };

        @missed_attributes          = grep {
            !$configured_attributes{ $_->{external_name} }
        } @{ $attribute_list };
    }


    for my $attribute (@{ $interface_config }, @missed_attributes) {
        my ($module_attr)           = grep {
            $_->{external_name} eq $attribute->{external_name}
        } @{ $attribute_list };

        ### First, drop, when type is magic_string or defined, when it
        ### is not found as $module_attr
        push(@new_config, $attribute) if $module_attr;

        ### Predefined
        if ($module_attr->{attribute_type} eq 'magic_string') {
            unless ($interface->case_type_id) {
                throw(
                    'sysin/modules/attributes/no_case_type_id',
                    'No casetype id setup, cannot generate attributes',
                    []
                );
            }
            
            $attribute->{case_type_id}  = (
                $interface->case_type_id->id || undef
            );

            $attribute->{include_system} = 1 if $self->direction eq 'outgoing';
        } 

        $attribute->{optional}      = 0 unless $module_attr->{optional};

        ### Append missed keys:
        for my $key (
            grep { !exists($attribute->{ $_ }) }
            keys %{ $module_attr }
        ) {
            $attribute->{ $key } = $module_attr->{ $key };
        }


        ### Defined? Do not change anything
        if ($module_attr->{attribute_type} eq 'defined') {
            $attribute->{$_}        = $module_attr->{$_} for keys %{
                $module_attr
            };

            next;
        }

        ### Modifyable (checked, internal_name, etc)
        $attribute->{checked}       = (
            $attribute->{optional} ? 0 : 1
        );

        # XXX TODO throw if one of the required attributes does not exist
    }

    return \@new_config;
}

Params::Profile->register_profile(
    method  => 'set_attribute_mapping',
    profile => {
        required => [qw/
            attributes
        /],
    }
);

sub set_attribute_mapping {
    my $self        = shift;
    my $interface   = shift;
    my $params      = shift;

    my $dv          = assert_profile($params)->valid;

    my $interface_config                    = $interface->get_interface_config;

    $interface_config->{attribute_mapping}  = 
        $self->_prepare_attribute_list($params->{'attributes'}, $interface);

    $interface->update_interface_config($interface_config);

    return $self->get_attribute_mapping($interface->discard_changes());
}

=head2 generate_interface_form

 my $json = $m->generate_interface_form;

Generates a HashRef describing the form

=cut

sub generate_interface_form {
    my $self    = shift;
    my $entry   = shift;

    my $f       = Zaaksysteem::ZAPI::Form->new(
        name        => $self->name,
        options     => {
            "autosave" => 1
        }
    );

    #$self->_generate_interface_form_manual($f);

    if (scalar @{ $self->interface_config }) {

        push(
            @{ $f->fieldsets },
            Zaaksysteem::ZAPI::Form::FieldSet->new(
                name        => 'fieldset-config',
                title       => 'Configuratie',
                description => 'Hieronder vult u de specifieke'
                    . ' configuratieparameters in met betrekking tot deze koppeling.' 
            )
        );

        my $index = ( scalar(@{ $f->fieldsets }) - 1);

        push(
            @{ $f->fieldsets->[$index]->fields },
            @{ $self->interface_config },
        );
    }

    $self->_generate_interface_form_casetype($f);
    $self->_generate_interface_form_attribute_mapping($f);
    $self->_generate_interface_form_general($f);
    $self->_generate_interface_form_extra($f);

    return $self->_load_values_into_form_object(
        $f,
        $entry
    );
}

=head2 $module->process_trigger(\%OPTIONS)

Return value: $ZAPI_READABLE_RESPONSE

    $module->process_trigger(
        {
            interface   => $interface_obj,
            action      => 'get_naw_csv',
            params      => {
                opt1        => 'val1',
                opt2        => 'val2'
            }
        }
    )

B<Options>

=over 4

=item interface

isa: Zaaksysteem::Backend::Sysin::Interface::Component

Interface object L<Zaaksysteem::Backend::Sysin::Interface::Component>, from
where this module has been triggered

=item action

isa: String

The action to trigger for this interface

=item params

isa: HashRef

A hashref containing parameters for this action

=back

=cut

Params::Profile->register_profile(
    method  => 'process_trigger',
    profile => {
        missing_optional_valid  => 1,
        required => [qw/
            interface
            action
        /],
        optional => [qw/
            params
        /],
    }
);

sub process_trigger {
    my $self        = shift;
    my $options     = assert_profile(
        {
            %{ $_[0] },
            schema  => $_[0]->{interface}->result_source->schema
        }
    )->valid;


    my $action  = $options->{action};

    ### Just call this in void contect to see if it throws an error
    my $def     = $self->get_trigger_definition($action);


    my $method  = ($def->{method} || $action);

    throw(
        'sysin/modules/process_trigger/not_found',
        'Definition is set, but no method found by name: ' . $action
    ) unless $self->can($method);

    return $self->$method($options->{params}, $options->{interface});
}

=head2 $module->get_trigger_definition($STRING_TRIGGER)

Return value: $HASHREF_DEFINITION

    $RETURN = {
        update  => 1,
    };

=cut

sub get_trigger_definition {
    my $self        = shift;
    my $action      = shift;

    unless (exists($self->trigger_definition->{ $action })) {
        throw(
            'sysin/modules/get_trigger_definition/not_found',
            'No trigger found for action: ' . $action
        );
    }

    return $self->trigger_definition->{ $action };
}

=head2 $module->run_test($interface, TEST_ID)

Return value: $TRUE_OR_EXCEPTION

Returns a true value on success, an exception on error.

B<Options>

=over 4

=item $interface

L<Zaaksysteem::Backend::Sysin::Interface::Component> row pointing to the interface
this module belongs to

=item TEST_ID

The id of the test in C<test_definition>

=back

=cut


sub run_test {
    my $self                        = shift;
    my ($interface, $test_id)       = @_;

    my ($test)                      = grep(
        { $_->{id} eq $test_id }
        @{ $self->test_definition->{tests} }
    );

    throw(
        'sysin/modules/run_test/id_not_found',
        'Cannot find test belonging to given id, id correct?'
    ) unless $test;

    my $method                      = $self->can($test->{method});

    throw(
        'sysin/modules/run_test/method_not_found',
        'Cannot find test method belonging to given id, module correct?'
    ) unless $method;

    return $method->($self, $interface, $test_id);
}


=head2 $module->process

Return value: $ROW_TRANSACTION

    ### Process by given filestore[uuid]
    my $self        = $interface;

    my $transaction = $module->process(
        {
            input_filestore_uuid    => 'c13685f0-e969-11e2-91e2-0800200c9a66',
            interface               => $self,
        }
    );

    ### Process by given input_string
    my $transaction = $interface->process(
        {
            input_string            => '<xml><name1>Wim</name1><address>Street 33</address>'
            interface               => $self,
        }
    );

Interprets the given input data, and dispatches this to the appropriate module which
is configured for this interface.

It returns a L<Zaaksysteem::Backend::Sysin::Transaction::Component> row, which contains
information about the result and the feedback for your interface.

This function receives a C<HashRef> with options:

=over 4

=item input_filestore_uuid [optional]

ISA: String UUID


=item input_string [optional]

ISA: String

=item label [optional]

ISA: String

Optional descriptive label for this transaction

=item direct [optional]

ISA: Bool
Default: false

Defines whether the resulting transaction get processed immediatly, or will be
queued for delivery.

=back

=cut


Params::Profile->register_profile(
    method  => 'process',
    profile => {
        missing_optional_valid  => 1,
        required => [qw/
            interface
        /],
        optional => [qw/
            label
            external_transaction_id

            processor_params
            direct
            schedule
            direction
        /],
        require_some    => {
            'filestore_or_string'   => [
                1,
                'input_data',
                'input_filestore_uuid',
            ],
        },
        constraint_methods => {
            direct              => qr/^\d?$/,
            input_filestore_uuid => sub {
                my $dfv     = shift;
                my $val     = shift;

                my $schema  = $dfv->get_input_data->{'schema'};

                return 1 if $schema
                    ->resultset('Filestore')
                    ->search(
                        {
                            uuid    => $val,
                        }
                    )
                    ->count;

                return;
            }
        },
        defaults    => {
            direct                  => 1,
            external_transaction_id => sub {
                my $dfv     = shift;

                my $interface = $dfv->get_input_data->{'interface'};

                return $interface->module . '-' . DateTime->now()->iso8601;
            }
        },
    }
);

has 'process_stash' => (
    is      => 'rw',
    isa     => 'HashRef',
    lazy    => 1,
    default => sub { return {}; }
);

sub process {
    my $self        = shift;
    my $options     = assert_profile(
        {
            %{ $_[0] },
            schema  => $_[0]->{interface}->result_source->schema
        }
    )->valid;

    my $interface   = $options->{interface};

    throw(
        'sysin/modules/process/inactive_module',
        'Module: ' . $interface->module . ' is inactive. Cannot process'
    ) unless $interface->active;

    ### Input file or input data?
    if ($options->{input_filestore_uuid}) {
        my $filestore   = $interface
                        ->result_source
                        ->schema
                        ->resultset('Filestore')
                        ->search(
                            {
                                uuid    => $options->{input_filestore_uuid},
                            }
                        )->first;

        throw(
            'sysin/modules/invalid_filestore_entry',
            'Invalid input_filestore_uuid'
        ) unless $filestore;

        $options->{input_file} = $filestore;
    }

    ### Create an empty transaction, for feedback.
    my $transaction = $self->_process_create_transaction($options);

    ### Validate input format, module needs to implement an AROUND modifier
    ### to throw an error in case of invalid data structure
    unless ($self->{processor_sub}) {
        $self->_process_validate_input($transaction);
    }

    ### Need to run this transaction directly, or is it pending?
    if (!defined($options->{direct}) || $options->{direct}) {
        $self->process_transaction($transaction);
    } else {
        $self->_set_retry_delay(
            $transaction,
            ($options->{schedule} || DateTime->now())
        );
    }

    return $transaction;
}


sub _set_retry_delay {
    my $self            = shift;
    my $transaction     = shift;
    my $given_time        = shift;

    ### Do not retry on a succesfull transaction
    return if $transaction->processed && !$transaction->error_count;

    ### Module is configured for retry on error
    return unless $self->retry_on_error;

    ### Fatal errors will not be retried
    return if $transaction->error_fatal;

    ### Set retry date
    my $new_time        = DateTime->now();
    my $retry_count;
    if ($transaction->automated_retry_count) {
        $retry_count    = $transaction->automated_retry_count;
    } else {
        $retry_count    = 0;
    }

    if ($given_time) {
        $transaction->date_next_retry($given_time);
    } else {
        my $next_interval   = $self->retry_interval->[$retry_count++];

        if ($next_interval) {
            $new_time->add(
                seconds => $next_interval
            );

            $transaction->date_next_retry($new_time);
            $transaction->automated_retry_count($retry_count);
        }
    }

    $transaction->update;
    return $transaction->date_next_retry;
}

sub process_transaction {
    my $self            = shift;
    my $transaction     = shift;

    ### Clear process stash
    $self->process_stash({});

    ### Disable next_retry for this moment
    $transaction->date_next_retry(undef);
    $transaction->update;

    ### Start transaction
    eval {
        $transaction->result_source->schema->txn_do(sub {
            $self->_process_input($transaction);
        });
    };

    if ($@) {
        ### XXX TODO: An error on transaction level
        ### $transaction->error_message(
        ###    'Could not process entire interface->process request: ' . $@
        ### );
        die('Error received: ' . $@);
    }

    ### Clear process stash
    $self->process_stash({});
 
    ### Mark the time on finish
    $transaction->date_last_retry(DateTime->now());

    ### Mark transaction as processed
    $transaction->processed(1);

    ### On error
    $self->_set_retry_delay($transaction);

    $transaction->update;

    return $transaction;

}

sub _process_input {
    my $self            = shift;
    my $transaction     = shift;

    $self->process_stash(
        {
            transaction => $transaction,
        }
    );

    my ($error, $success) = (0, 0);
    while (my ($row, $input) = $self->_process_get_next_row()) {
        $self->process_stash->{row} = {};

        my $record  = $transaction
                    ->transaction_records
                    ->transaction_record_create(
            {
                transaction_id  => $transaction->id,
                input           => $input,
                output          => 'bla',
            }
        );

        ### Normal processor, or custom processor
        my $processor;
        if ($self->can('_process_selector')) {
            $processor          = $self->_process_selector($row, $record);
        } else {
            $processor          = $self->can('_process_row');
        }

        my $output;
        eval {
            $transaction->result_source->schema->txn_do(sub {
                $output = $processor->($self, $record, $row);
            });
        };

        if ($@) {
            $record->is_error(1);

            if (
                UNIVERSAL::isa($@, 'Zaaksysteem::Exception::Base')
            ) {
                if (UNIVERSAL::isa($@->object, 'HASH') && $@->object->{transaction_output}) {
                    $record->output($@->object->{transaction_output});
                } else {
                    $record->output('Error: ' . $@->type . ': ' . $@->message);
                }

                if (UNIVERSAL::isa($@->object, 'HASH') && $@->object->{fatal}) {
                    $transaction->error_fatal(1);
                }
            } else {
                $record->output('Error: ' . $@);
            }

            $error++;
        } else {
            $record->output($output) unless $record->output;

            eval {
                $self->_process_mutations($record);
            };

            if ($@) {
                warn('Mutation register failed: ' . $@);
            }

            $success++;
        }

        $record->date_executed(DateTime->now());
        $record->update;
    }

    $transaction->error_count($error);
    $transaction->success_count($success);

    $transaction->update;

    #die('Errors occured') if $error;

    return 1;
}

sub _process_mutations {
    my $self        = shift;
    my $record      = shift;

    return unless (
        exists($self->process_stash->{row}) &&
        exists($self->process_stash->{row}->{mutations}) &&
        $self->process_stash->{row}->{mutations} &&
        UNIVERSAL::isa($self->process_stash->{row}->{mutations}, 'ARRAY')
    );

    for my $mutation (
        @{ $self->process_stash->{row}->{mutations} }
    ) {
        $mutation->register($record);
    }
}

sub _process_validate_input {
    my $self        = shift;
}

sub _process_create_transaction {
    my $self        = shift;
    my $options     = shift;
    my $interface   = $options->{interface};

    my $create_params = {
        processor_params    => $options->{processor_params}
    };

    if ($options->{input_file}) {
        ### Filestore entry
        $create_params->{input_file} = $options->{input_file}->id;
    } else {
        $create_params->{input_data} = $options->{input_data};
    }

    $create_params->{direction}                 = $options->{direction} || $self->direction;

    $create_params->{external_transaction_id}   = $options->{external_transaction_id};

    my $transaction = $interface
                    ->transactions
                    ->transaction_create(
                        {
                            %{ $create_params },
                            interface_id => $interface->id,
                        }
                    );

    return $transaction;
}

=head1 CLASS METHODS

=head2 list_of_modules

Return value: @list_of_module_objects

 my @list = Zaaksysteem::Backend::Sysin::Modules->list_of_modules;

 print $list[0]->name;
 print $list[0]->max_retries;

Returns a list of all module objects currently available in this release of
zaaksysteem.nl

=cut


sub list_of_modules {
    my $self    = shift;

    my @modules;
    for my $module (@{ MODULELIST() }) {
        if ($module =~ /^\+/) {
            $module =~ s/^\+//;
            $module = __PACKAGE__ . '::' . $module;
        }

        eval 'use ' . $module;
        
        push(@modules, $module->new());
    }

    return @modules;
}

=head2 list_of_available_modules

Return value: @list_of_available_modules

 my @list = Zaaksysteem::Backend::Sysin::Modules->list_of_available_modules(
    $schema
 )

 print $list[0]->name;
 print $list[0]->max_retries;

Returns a list of all available module objects to be configured. Some modules
may only be configured once, like a DigiD module. This call will only return
the modules not already used in the system. See
L<Zaaksysteem::Backend::Sysin::Modules#list_of_available_modules> for more
details

=cut

sub list_of_available_modules {
    my $self                = shift;
    my $schema              = shift;
    my (@available_modules, @used_modules);

    my @modules             = $self->list_of_modules;
    my $interfaces          = $schema
                            ->resultset('Interface')
                            ->search_active();

    while (my $interface = $interfaces->next) {
        push(@used_modules, $interface->module)
    }

    for my $module (@modules) {
        next if (
            !$module->allow_multiple_configurations &&
            grep { $module->name eq $_ } @used_modules
        );

        push(@available_modules, $module);
    }

    return @available_modules;
}

=head2 list_modules_by_module_type

=cut

sub list_modules_by_module_type {
    my $self                = shift;
    my $type                = shift;

    my @modules;
    for my $module ($self->list_of_modules) {
        next unless grep { $_ eq $type } @{ $module->module_type };

        push(@modules, $module);
    }

    return @modules;
}



=head2 find_module_by_id

Return value: $module

 my $module = Zaaksysteem::Backend::Sysin::Modules->find_module_by_id('csv');

 $module->name;
 $module->max_retries;
 $module->is_numeric;
 [...]

Returns the module object by giving the internal name of the module. See
C<list_of_modules> for a list of modules.

=cut
 
sub find_module_by_id {
    my $self        = shift;
    my $id          = shift;

    my @modules     = $self->list_of_modules;

    my ($module)    = grep { $_->name eq $id } @modules;
    
    return $module;
}

sub TO_JSON {
    my $self    = shift;

    return {
        map { $_ =>  $self->$_ } @{ MODULE_ATTRS() }
    };
}


=head1 INTERNAL METHODS

=head2 _load_values_into_form_object

Loads values from a hash into a form object

=cut

sub _load_values_into_form_object {
    my $self            = shift;
    my ($f, $entry)     = @_;

    return $f unless $entry;

    my $params              = { $entry->get_columns };

    ### Load case_type_id
    if ($params->{case_type_id}) {
        $params->{case_type_id} = $entry->result_source->schema->resultset('Zaaktype')->find(
            $params->{case_type_id}
        );
    }

    my $interface_config    = JSON::decode_json($params->{interface_config});

    for my $key (keys %{ $interface_config }) {
        my $interface_key = 'interface_' . $key;

        $params->{$interface_key} = $interface_config->{$key};
    }

    ### Delete interface_config from value listing for form
    delete($params->{interface_config});

    $f->load_values($params);

    return $f;
}

# =head2 _generate_interface_form_manual

# Generates a file upload dialog

# =cut

# sub _generate_interface_form_manual {
#     my $self    = shift;
#     my $f       = shift;

#     return;

#     return unless $self->is_manual;

#     push(
#         @{ $f->fieldsets },
#         Zaaksysteem::ZAPI::Form::FieldSet->new(
#             name        => 'fieldset-upload',
#             title       => 'Koppeling bijwerken',
#         )
#     );
#     my $index = (scalar(@{ $f->fieldsets }) - 1);

#     if ($self->manual_type eq 'file') {
#         push(
#             @{ $f->fieldsets->[ $index ]->fields },
#             Zaaksysteem::ZAPI::Form::Field->new(
#                 name    => 'manual_upload',
#                 label   => 'Invoerbestand',
#                 type    => 'file',
#                 data    => {
#                     'multi'         => 0,
#                     'resolve'       => 'uuid',
#                     'allowed'       => undef,
#                 },
#             ),
#         )
#     }
# }

=head2 _generate_interface_form_casetype

Generates the form fields when interface module is a zaaktype interface

=cut

sub _generate_interface_form_casetype {
    my $self    = shift;
    my $f       = shift;

    return unless $self->is_casetype_interface;

    push(
        @{ $f->fieldsets },
        Zaaksysteem::ZAPI::Form::FieldSet->new(
            name        => 'fieldset-mapping',
            title       => 'Zaaktype interface',
            description => 'U bewerkt een koppeling welke betrekking heeft op een'
                . ' zaaktype. Configureer hieronder uw zaaktype.'
        )
    );
    my $index = (scalar(@{ $f->fieldsets }) - 1);

    push(
        @{ $f->fieldsets->[ $index ]->fields },
        Zaaksysteem::ZAPI::Form::Field->new(
            name    => 'case_type_id',
            label   => 'Zaaktype',
            type    => 'spot-enlighter',
            data    => {
                'restrict'      => 'casetype',
                'placeholder'   => 'Type uw zoekterm',
                'label'         => 'zaaktype_node_id.titel',
                'resolve'       => 'id'
            },
            description => 'Zoek het zaaktype op behorende bij deze koppeling',
        ),
    )
}

sub _generate_interface_form_extra {
    my $self    = shift;
    my $f       = shift;

    return unless $self->additional_description;

    push(
        @{ $f->fieldsets },
        Zaaksysteem::ZAPI::Form::FieldSet->new(
            name        => 'fieldset-mapping',
            title       => 'Extra informatie',
            description => $self->additional_description
        )
    );
    my $index = (scalar(@{ $f->fieldsets }) - 1);

    # push(
    #     @{ $f->fieldsets->[ $index ]->fields },
    #     Zaaksysteem::ZAPI::Form::Field->new(
    #         name    => 'case_type_id',
    #         label   => 'Zaaktype',
    #         type    => 'spot-enlighter',
    #         data    => {
    #             'restrict'      => 'casetype',
    #             'placeholder'   => 'Type uw zoekterm',
    #             'label'         => 'zaaktype_node_id.titel',
    #             'resolve'       => 'id'
    #         },
    #         description => 'Zoek het zaaktype op behorende bij deze koppeling',
    #     ),
    # )
}

=head2 _generate_interface_form_attribute_mapping

Generates the form fields when interface contains attribute mappings.

=cut

sub _generate_interface_form_attribute_mapping {
    my $self    = shift;
    my $f       = shift;

    return unless $self->has_attributes;

    push(
        @{ $f->fieldsets },
        Zaaksysteem::ZAPI::Form::FieldSet->new(
            name        => 'fieldset-mapping',
            title       => 'Kenmerken koppelen',
            description => 'Uw koppeling maakt gebruik van attributen, welke weer'
                . ' gekoppeld kunnen worden aan velden binnen het zaaksysteem.'
                . ' Druk op de knop "Gegevensmapping" om deze velden aan elkaar te koppelen'
        )
    );

    my $index = (scalar(@{ $f->fieldsets }) - 1);

    push(
        @{ $f->fieldsets->[ $index ]->actions },
        {
            "name"          => "attribute_mapping",
            "label"         => "Kenmerkmapping",
            "type"          => "popup",
            "importance"    => "secondary",
            "disabled"      => "!(isFormValid()&&!(getModuleByName(activeLink.module).is_casetype_interface&&!case_type_id))",
            "data"          => {
                "template_url"  => "/html/sysin/links/mapping.html",
                "title"         => "Kenmerkmapping"
            }
        }   
    )
}

=head2 _generate_interface_form_general

Generates form fields for general attributes, such as active/inactive

=cut

sub _generate_interface_form_general {
    my $self    = shift;
    my $f       = shift;

    push(
        @{ $f->fieldsets },
        Zaaksysteem::ZAPI::Form::FieldSet->new(
            name        => 'fieldset-general',
            title       => 'Algemeen',
            description => 'Wanneer u tevreden bent over uw configuratie, '
                . ' controleer uw configuratie door op de Test knop te drukken.'
                . ' Wanneer alles naar wens is, kunt u de koppeling actief zetten en opslaan.'
        )
    );
    my $index = ( scalar(@{ $f->fieldsets }) - 1);

    push(
        @{ $f->fieldsets->[$index]->fields },
        Zaaksysteem::ZAPI::Form::Field->new(
            name    => 'active',
            label   => 'Actief',
            type    => 'checkbox',
            options => [
                {
                    value   => '1',
                    label   => 'Actief',
                }
            ],
            description => 'Zet de koppeling actief',
        ),
    );
}

=head2 _get_config_fields

Return value: @list_of_config_fields

=cut

sub _get_config_fields {
    my $self    = shift;

    my @fields;
    for my $field (@{ $self->interface_config }) {
        push(@fields, $field->name);
    }

    return @fields;
}


=head1 EXAMPLES

See L<SYNOPSIS>

=head1 SEE ALSO

L<Zaaksysteem::ZAPI::Form> L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::Template>

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Rudolf Leermakers

Marco Baan

Dario Gieselaar

Nick Diel

Laura van der Kaaij

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut

1;
