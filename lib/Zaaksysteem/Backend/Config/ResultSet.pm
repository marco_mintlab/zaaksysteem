package Zaaksysteem::Backend::Config::ResultSet;

use Moose;

BEGIN { extends 'DBIx::Class::ResultSet'; }

sub get_value {
    my $self = shift;
    my $param = shift;

    unless($param) {
        die 'get_value requires a parameter to be passed';
    }

    my $row = $self->search({ parameter => $param })->first;

    unless($row) {
        die sprintf 'Could not find parameter "%s" in database', $param;
    }

    return $row->value;
}

sub get {
    my $self = shift;
    my $retval = undef;

    eval {
        $retval = $self->get_value(@_);
    };

    return $retval;
}

1;
