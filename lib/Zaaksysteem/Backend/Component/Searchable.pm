package Zaaksysteem::Backend::Component::Searchable;

use Moose::Role;

=head1 NAME

Zaaksysteem::Backend::Component::Searchable - Role providing searchable methods to a Component

=head1 SYNOPSIS


=head1 DESCRIPTION

These Component Role provides extends TO_JSON to provide searchable data when it
exists

=head1 ATTRIBUTES

=head2 _searchable_object_id [required]

ISA: Integer

Defines the id which identifies this search result, when giving it to
C<< $rs->find >>


Define this in your component class when you would like to use searchable

    has '_searchable_object_id' => (
        is      => 'ro',
    );

=head2 _searchable_object_label [required]

ISA: String

 print $self->_searchable_object_label;

 # "Testzaaktype"

Defines the human readable label for this search, for displaying in search result

Define this in your component class when you would like to use searchable

    has '_searchable_object_label' => (
        is      => 'ro',
    );

=head2 _searchable_object_description [required]

ISA: String

 print $self->_searchable_object_description;

 # "Testzaaktype voor gebruik binnen unit tests"

Defines the human readable description for this search, for displaying in search result

Define this in your component class when you would like to use searchable

    has '_searchable_object_description' => (
        is      => 'ro',
    );

=head1 EXTENDS

=head2 TO_JSON

 {
    searchable_object_id            => 4232,
    searchable_object_type          => 'zaaktype',
    searchable_object_label         => 'Testzaaktype',
    searchable_object_description   => 'Dit zaaktype wordt gebruikt voor bla',
 }

=cut 

before 'TO_JSON'    => sub {
    my $self    = shift;

    return unless (
        $self->can('_searchable_object_id') &&
        $self->can('_searchable_object_label') &&
        $self->can('object_type')
    );

    ### Required parameters
    $self->_json_data->{searchable_object_id}       = $self->_searchable_object_id;
    $self->_json_data->{searchable_object_label}    = $self->_searchable_object_label;
    $self->_json_data->{searchable_object_type}     = $self->object_type;

    ### Optional description
    $self->_json_data->{searchable_object_description}  = $self->_searchable_object_description
        if $self->can('_searchable_object_description');
};


=head1 EXAMPLES

See L<SYNOPSIS>

=head1 SEE ALSO

L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::API>

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Rudolf Leermakers

Marco Baan

Dario Gieselaar

Nick Diel

Laura van der Kaaij

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut

1;