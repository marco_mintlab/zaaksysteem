package Zaaksysteem::Backend::File::Scheduler;

use Moose::Role;

around 'update_properties' => sub {
    my $orig    = shift;
    my $self    = shift;
    my ($opts)  = @_;
    $opts       ||= {};

    my $result  = $self->$orig(@_);

    ### Not a scheduler file?
    return $result unless $self->scheduled_jobs_id;

    my $task    = $self->scheduled_jobs_id;

    ### Run the job
    if (exists $opts->{accepted} && $opts->{accepted}) {
        if ($task->run()) {
            ### Prevent the infinite loop. Remove accepted...
            my $newopts     = { %$opts };
            delete($newopts->{accepted});

            ### Delete the file
            $result->update_properties(
                {
                    %{ $newopts },
                    deleted     => 1,
                    destroyed   => 1
                }
            );
        }
    }

    ### Reject the job
    if (exists $opts->{accepted} && !$opts->{accepted}) {
        if ($task->reject()) {
            ### Prevent the infinite loop. Remove accepted...
            my $newopts     = { %$opts };
            delete($newopts->{accepted});

            $result->update_properties(
                {
                    %{ $newopts },
                    deleted     => 1,
                    destroyed   => 1
                }
            );
        }
    }

    return $result;
};

### Disable triggers
around 'trigger' => sub {
    my $orig    = shift;
    my $self    = shift;

    return if $self->scheduled_jobs_id;

    return $self->$orig(@_);
};

1;