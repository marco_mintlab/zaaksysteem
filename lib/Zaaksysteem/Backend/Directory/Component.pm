package Zaaksysteem::Backend::Directory::Component;

use strict;
use warnings;

use Moose;
use Params::Profile;
use Zaaksysteem::Constants;

extends 'DBIx::Class';

use Exception::Class (
    'Zaaksysteem::Backend::Directory::Component::Exception' => {fields => 'code'},
    'Zaaksysteem::Backend::Directory::Component::Exception::General' => {
        isa         => 'Zaaksysteem::Backend::Directory::Component::Exception',
        description => 'General exception',
        alias       => 'throw_general_exception',
    },
    'Zaaksysteem::Backend::Directory::Component::Exception::Parameter' => {
        isa         => 'Zaaksysteem::Backend::Directory::Component::Exception',
        description => 'Parameter exception',
        alias       => 'throw_parameter_exception',
    },
    'Zaaksysteem::Backend::Directory::Component::Exception::Logic' => {
        isa         => 'Zaaksysteem::Backend::Directory::Component::Exception',
        description => 'Logic error',
        alias       => 'throw_logic_exception',
    },
);

=head2 update_properties

Updates the properties of a Directory entry.

=head3 Arguments

=over

=item name [optional]

The new name of the directory.

=item case_id [optional]

The case this directory belongs to.

=back

=head3 Returns

The updated Directory object.

=cut

Params::Profile->register_profile(
    method  => 'update_properties',
    profile => {
        required => [qw/
        /],
        optional => [qw/
            name
            case_id
        /],
        constraint_methods => {
            name => qr/^(\w|\s|!|@|#|-|:|\.)+$/,
            case_id => qr/^\d+$/,
        },
    }
);

sub update_properties {
	my $self = shift;
    my $opts = $_[0];

    my $dv = Params::Profile->check(
        params  => $opts,
    );

    my $valid = $dv->valid;
    if ($dv->has_invalid) {
        my @invalid = join ',',$dv->invalid;
        throw_parameter_exception(
            code  => '/directory/update_properties/invalid_parameters',
            error => "Invalid options given: @invalid",
        );
    }

    # Check if name + case are already taken
    if ($valid->{name} or $valid->{case_id}) {
        my ($result) = $self->result_source->schema->resultset('Directory')->search({
            name    => $valid->{name} || $self->name,
            case_id => $valid->{case_id} || $self->case->id,
        });

        if ($result) {
            throw_logic_exception(
                code  => '/directory/update_properties/directory_exists',
                error => sprintf("Directory with name %s and case %d already exists",
                    (
                        $valid->{name} || $self->name,
                        $valid->{case_id} || $self->case->id,
                    )
                ),
            );
        }
    }
	return $self->update($valid);
}

=head2 delete

Extends the default delete method for Directory to check if there are still 
files using this directory. Without this an ugly DBIx::Class exception would get
thrown.

=cut

sub delete {
    my $self = shift;

    my @files = $self->files;
    if (@files) {
        throw_general_exception(
            code  => '/directory/delete/not_empty',
            error => 'There are still files referencing this directory',
        );
    }

    return $self->next::method(@_);
}



