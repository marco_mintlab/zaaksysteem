package Zaaksysteem::ZAPI::Response::Row;

use Moose::Role;

sub from_row {
    my $self        = shift;
    my $row         = shift;

    die('Not a valid Row: ' . ref($row))
        unless (
            UNIVERSAL::isa($row, 'DBIx::Class') &&
            !UNIVERSAL::isa($row, 'DBIx::Class::ResultSet')
        );


    $self->_input_type('row');

    my $pager       = Data::Page->new();

    $pager->total_entries   (1);
    $pager->entries_per_page(1);
    $pager->current_page    (1);

    $self->_generate_paging_attributes(
        $self->pager($pager),
    );

    $self->result([ $row ]);

    $self->_validate_response;

    return $self->response;
}

around from_unknown => sub {
    my $orig        = shift;
    my $self        = shift;
    my ($data)      = @_;

    if (
        UNIVERSAL::isa($data, 'DBIx::Class') &&
        !UNIVERSAL::isa($data, 'DBIx::Class::ResultSet')
    ) {
        $self->from_row(@_);
    }

    $self->$orig( @_ );
};

1;
