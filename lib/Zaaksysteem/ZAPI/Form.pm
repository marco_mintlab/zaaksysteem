package Zaaksysteem::ZAPI::Form;

use Moose;

use constant FORM_PROFILE          => {
    required            => [qw/
        name        
        actions
    /],
    optional            => [qw/
        fieldsets
        options
    /],
    # constraint_methods  => {
    #     fieldsets       => sub {
    #         my ($dfv)   = @_;

    #         my $val     = $dfv->get_filtered_data->{
    #             $dfv->get_current_constraint_field
    #         };

    #         return unless UNIVERSAL::isa($val, 'ARRAY');

    #         return unless scalar @{ $val };


    #         for my $option (@{ $val }) {
    #             return unless UNIVERSAL::isa(
    #                 $option,
    #                 'Zaaksysteem::ZAPI::Form::Field'
    #             );

    #             return unless $option->validate;
    #         }

    #         return 1;
    #     },
    # },
};

=head1 NAME

Zaaksysteem::ZAPI::Form - Construct a form object, suitable for angular forms.

=head1 SYNOPSIS

    my $form    = Zaaksysteem::ZAPI::Form->new(
        name        => 'hello-world',
        fieldsets   => [
            Zaaksysteem::ZAPI::Form::FieldSet->new(
                name        => 'fieldset-gegevens',
                title       => 'Benodigde gegevens',
                description => 'Lorem Ipsum Larieda',
                fields      => []
                    Zaaksysteem::ZAPI::Form::Field->new(
                        name        => 'subject_type',
                        label       => 'Subject Type',
                        type        => 'radio',
                        required    => 1,
                        options     => [
                            {
                                value   => 'extern',
                                label   => 'Extern',
                            },
                            {
                                value   => 'intern',
                                label   => 'Intern'
                            }

                        ]
                        description => 'Select a subject type'
                    ),
                    Zaaksysteem::ZAPI::Form::Field->new(
                        name        => 'subject',
                        label       => 'Subject',
                        type        => 'text',
                        description => 'Select a subject'
                    ),
                ]
            )
        ]
    );

    $form->TO_JSON;

=head1 DESCRIPTION

This generates a readable form for the Angular Forms goodness.

=head1 ATTRIBUTES

=head2 name

The identifier name of this form

=cut

has 'name'  => (
    'is'        => 'rw',
    'isa'       => 'Str',
    'required'  => 1,
);

=head2 fieldsets

The fieldsets for this form. For more info, see
L<Zaaksysteem::ZAPI::Form::FieldSet>

=cut

has 'fieldsets'    => (
    'is'        => 'rw',
    'isa'       => 'ArrayRef',
    'lazy'      => 1,
    'default'   => sub { return []; }
);

=head2 options

The options for this form. 

=cut

has 'options'    => (
    'is'        => 'rw',
    'isa'       => 'HashRef',
    'lazy'      => 1,
    'default'   => sub { return {}; }
);

=head2 actions

The fieldsets for this form. For more info, see
L<Zaaksysteem::ZAPI::Form::Actions>

=cut

has 'actions'    => (
    'is'        => 'rw',
    'isa'       => 'ArrayRef',
    'lazy'      => 1,
    'default'   => sub {

        ### QUICK DARIO HACK
        return [
            {
                "name"          => "test",
                "label"         => "Test",
                "type"          => "popup",
                "importance"    => "secondary",
                "when"          => "getModuleByName(activeLink.module).test_interface",
                "data"          => {
                    "template_url"   => "/html/sysin/links/test.html",
                    "title" => "Test koppeling"
                }
            },
            {
                "name" => "submit",
                "label" => "Submit",
                "type" => "submit",
                "importance" => "primary",
                "data" => {
                    "url" => ""
                }
            }
        ];
    }
);


=head1 METHODS

=head2 validate

Validates this object params

=cut

sub validate {
    my $self            = shift;

    my $dv              = Data::FormValidator->check(
        $self->_object_params,
        FORM_PROFILE
    );

    die(
        'Cannot validate form, invalid or missing: '
        . join(',', $dv->invalid, $dv->missing)
    ) unless $dv->success;

    return 1;
}

=head2 load_values

Loads values from a hash into this form object

=cut

sub load_values {
    my $self            = shift;
    my $values          = shift;

    for (my $i = 0; $i < scalar(@{ $self->fieldsets }); $i++) {
        for (my $fi = 0; $fi < scalar(@{ $self->fieldsets->[$i]->fields }); $fi++) {
            next unless exists($values->{ $self->fieldsets->[$i]->fields->[$fi]->name });
            $self->fieldsets->[$i]->fields->[$fi]->value($values->{ $self->fieldsets->[$i]->fields->[$fi]->name });
        }
    }
}

=head2 TO_JSON

Creates a Angular readable JSON hash containing form information

=cut

sub TO_JSON {
    my $self        = shift;

    $self->validate;

    my $rv          = $self->_object_params;

    return $rv;
}



=head1 INTERNAL METHODS

=head2 _object_params

Returns a HASHRef containing this object parameters according to this object
profile

=cut

sub _object_params {
    my $self        = shift;

    return { map {
        $_ => $self->$_
    } (
        @{ FORM_PROFILE->{required} },
        @{ FORM_PROFILE->{optional} }
    ) };
}

=head1 EXAMPLES

See L<SYNOPSIS>

=head1 SEE ALSO

L<Zaaksysteem::ZAPI::Form::FieldSet> L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::Template>

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Rudolf Leermakers

Marco Baan

Dario Gieselaar

Nick Diel

Laura van der Kaaij

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut 

1;