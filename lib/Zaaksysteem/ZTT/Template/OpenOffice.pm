package Zaaksysteem::ZTT::Template::OpenOffice;

use Moose;

use Zaaksysteem::ZTT::Tag;
use Zaaksysteem::ZTT::Selection::OpenOffice;

use URI;

BEGIN { extends 'Zaaksysteem::ZTT::Template' };

has document => ( is => 'ro', isa => 'OpenOffice::OODoc::Document', required => 1 );

=head2 tag_selections

Returns a list of L<Zaaksysteem::ZTT::Selection> objects that reference
tags defined in the template.

=cut

sub tag_selections {
    my $self = shift;

    my @retval;

    for my $element ($self->document->selectElementsByContent('\[\[\s*[\w\.]+\s*\]\]')) {
        my ($selection, $tagname) = $element->text =~ /(\[\[\s*([\w\.]+)\s*\]\])/;

        push @retval, Zaaksysteem::ZTT::Selection::OpenOffice->new(
            element => $element,
            selection => $selection,

            tag => Zaaksysteem::ZTT::Tag->new(
                name => $tagname
            )
        );
    }

    return @retval;
}

sub sections {
    my $self = shift;

    return ($self->plain_sections, $self->table_sections);
}

sub table_sections {
    return map { Zaaksysteem::ZTT::Selection::OpenOffice->new(
        element => $_,
        decode_section_name($_->getAttribute('table:name'))
    ) } shift->subtemplate_table_sections;
}

sub subtemplate_table_sections {
    my $self = shift;

    return grep
        { $_->getAttribute('table:name') =~ m[^itereer:\w+] }
        $self->document->getTableList;
}

=head2 plain_sections

Returns a list of L<Zaaksysteem::ZTT::Selection> objects that reference each
section defined in the template. Sections are iterable sub-templates that get
processed in a different magic-string context than the case itself. They can be
used to inject subtemplates for each case relation, for example.

=cut

sub plain_sections {
    # Create a new Selection object for each subtemplate section
    # Add decoded attribute name for each constructor
    # Excuse the obtuse syntax formatting
    return map { Zaaksysteem::ZTT::Selection::OpenOffice->new(
        element => $_,
        decode_section_name($_->getAttribute('text:name'))
    ) } shift->subtemplate_sections;
}

# Decode a text:name element from the document.
#
# Example: itereer:zaak_relaties # loop1
#       => (iterate => 'zaak_relaties', name => 'loop1')
#
# Example 2: itereer:zaak_relaties
#         => (iterate => 'zaak_relaties')
sub decode_section_name {
    shift =~ m[^itereer:(?<iterate>[\w\.]+)(?:\s*\#\s*(?<name>[\w\.]+))?];

    return (
        iterate => $+{ iterate },
        name => $+{ name }
    );
}

# Get all document sections, find those that start with the magic name 'itereer:'
sub subtemplate_sections {
    my $self = shift;

    return grep
        { $_->getAttribute('text:name') =~ m[^itereer:\w+] }
        $self->document->getSections;
}

=head2 replace

Use a L<Zaaksysteem::ZTT::Selection> to replace a tag in the template with the
actual value. Second argument is expected to be a L<Zaaksysteem::ZTT::Element>,
or at least something that implements the sanitize method and has a getter at
for the value of the replacement.

=cut

sub replace {
    my $self = shift;

    my $selection = shift;
    my $replacement = shift;

    return unless $replacement;

    $replacement->sanitize;

    my $value = $replacement->value;

    if($replacement->type eq 'richtext') {
        my $p_to_replace = up($selection->element, sub { shift->isParagraph });

        for my $html (@{ $value }) {
            my $paragraph = $self->document->insertParagraph($p_to_replace,
                text => $self->document->outputTextConversion(element2paragraph($html)),
                position => 'before'
            );

            for my $a ($html->find('a')) {
                my $uri = URI->new($a->attr('href'));

                unless ($uri->scheme) {
                    $uri->scheme('http');
                }

                $self->document->setHyperlink($paragraph, $a->as_trimmed_text, $uri->canonical);
            }

            # Hackaround for inserted paragraphs, they don't seem to have a 'paragraph' newline after them,
            # so we insert an empty line and fix it visually that way.
            $self->document->insertParagraph($p_to_replace,
                text => $self->document->outputTextConversion(""),
                position => 'before'
            );

            # Set the textstyle to the original style of the selected paragraph.
            $self->document->textStyle($paragraph, $self->document->textStyle($p_to_replace));
        }

        $p_to_replace->delete;

        return;
    }

    # Convert newlines to something we can capture after replacing the text.
    $value =~ s/(, )?\n/[[BR]]/g;

    my $replace_selection = $selection->selection;

    $replace_selection =~ s/([\[\]])/\\$1/g;

    $self->document->substituteText(
        $selection->element,
        $replace_selection,
        $self->document->outputTextConversion($value)
    );

    # Reify newlines by injecting line-breaks
    $self->document->setChildElements($selection->element, 'text:line-break',
        replace => '\[\[BR\]\]'
    );
}

sub iterate {
    my $self = shift;

    my $contexts = shift;
    my $selection = shift;

    if ($selection->element->isTable) {
        return $self->iterate_table($contexts, $selection);
    }

    if ($selection->element->isSection) {
        return $self->iterate_section($contexts, $selection);
    }

    throw('ztt/iterator/unknown_type', 'Selection based on an element type I can\'t handle (yet..)');
}

sub iterate_table {
    my $self = shift;

    my $contexts = shift;
    my $selection = shift;

    # Assume the first row in the table is our template.
    my $tpl = $selection->element->child(0, 'table:table-row');

    for my $context (@{ $contexts }) {
        my $ztt = Zaaksysteem::ZTT->new_from_case($context->case);

        # Copy the template row
        my $row = $self->document->insertTableRow($tpl);

        # Evaluate each cell as flattened (plaintext) template
        for my $cell ($self->document->getRowCells($row)) {
            my $value = $ztt->process_template(flatten_section($cell))->string;

            # Hard-set the value of the cell with the processed template
            $self->document->updateCell($cell, $value);
        }
    }

    # Delete the template row
    $tpl->delete;

    # Rename the table so the next loop doesn't re-eval this one.
    $selection->element->setAttribute('table:name', 'iterdone');
}

sub iterate_section {
    my $self = shift;

    my $contexts = shift;
    my $selection = shift;

    my $element_to_replace = up($selection->element, sub { shift->isParagraph });

    for my $context (@{ $contexts }) {
        my $ztt = Zaaksysteem::ZTT->new_from_case($context->case);

        # Process a COPY of the found element, processing is a non-idempotent operation, don't want
        # this loop to break for all but the first context.
        my $value = $ztt->process_template(flatten_section($selection->element))->string;

        for my $ptext (split m[\n\s*\n], $value) {
            my $paragraph = $self->document->insertParagraph($selection->element,
                text => $self->document->outputTextConversion($ptext),
                position => 'before'
            );

            $self->document->textStyle($paragraph, $self->document->textStyle($element_to_replace));
        }
    }

    $selection->element->delete;
}

=head1 Helper functions

These functions are not class methods, they are just generic helpers 
for easy traversal of the oodoc or for converting L<Zaaksysteem::ZTT::Element>s

=cut

=head2 up

This function returns the first element to return true for the test condition
supplied as the second argument by walking up the document tree. If no such
node can be found returns undef.

=cut

sub up {
    my $element = shift;
    my $code = shift;

    while($element && !$code->($element)) {
        $element = $element->getParentNode;
    }

    return $element;
}

=head2 element2paragraph

This function returns a textual representation of the HTML::Element supplied
in the context of oodocs.

=cut

sub element2paragraph {
    my $element = shift;

    my %dispatch = (
        p => sub { split(m[\n], shift->as_trimmed_text) },

        ul => sub {
            my $element = shift;
        
            join("\n", map
                { sprintf('%s %s', chr(0x2022), $_->as_trimmed_text) }
                $element->find('li')
            )
        },

        ol => sub {
            my $element = shift;
            my $iter = 1;

            return join("\n", map
                { sprintf('%d. %s', $iter++, $_->as_trimmed_text) }
                $element->find('li')
            );
        }
    );

    return unless exists $dispatch{ $element->tag };

    return $dispatch{ $element->tag }->($element);
}

sub flatten_section {
    my $element = shift;

    my $name = $element->getName;

    return "\t" if $name =~ m[^text:tab(|-stop)$];
    return "\n" if $name eq 'text-linebreak';
    return ' ' x ($element->att('text:c') || 1) if $name eq 'text:s';
    return sprintf("%s\n", $element->text) if $name eq 'text:p';
    return $element->text if $name eq 'text:span';
    return join '', map
        { $_->isElementNode ? flatten_section($_) : $_->text }
        $element->getChildNodes;
}

1;
