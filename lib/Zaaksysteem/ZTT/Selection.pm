package Zaaksysteem::ZTT::Selection;

use Moose;

has tag => ( is => 'ro', isa => 'Zaaksysteem::ZTT::Tag' );
has selection => ( is => 'ro', isa => 'Str' );
has iterate => ( is => 'ro', isa => 'Str' );
has subtemplate => ( is => 'ro', isa => 'Str' );
has name => ( is => 'ro', isa => 'Maybe[Str]' );

1;
