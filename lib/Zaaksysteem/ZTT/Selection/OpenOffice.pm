package Zaaksysteem::ZTT::Selection::OpenOffice;

use Moose;

BEGIN { extends 'Zaaksysteem::ZTT::Selection' };

has element => ( is => 'ro', isa => 'OpenOffice::OODoc::Element', required => 1 );

1;
