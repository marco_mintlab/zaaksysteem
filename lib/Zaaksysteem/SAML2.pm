package Zaaksysteem::SAML2;

use Moose;

use Zaaksysteem::Profile;
use Zaaksysteem::Exception;

use Zaaksysteem::SAML2::SP;
use Zaaksysteem::SAML2::IdP;
use Zaaksysteem::SAML2::Binding::SOAP;
use Zaaksysteem::SAML2::Protocol::ArtifactResolve;
use Zaaksysteem::SAML2::Protocol::AuthnRequest;

use Net::SAML2::Protocol::Assertion;
use Net::SAML2::Binding::Redirect;

use Try::Tiny;

use URI;
use URI::QueryParam;
use JSON;
use MIME::Base64;

has sp    => ( is => 'ro', isa => __PACKAGE__ . '::SP' );
has idp   => ( is => 'ro', isa => __PACKAGE__ . '::IdP' );
has state => ( is => 'ro', isa => 'HashRef' );
has uri   => ( is => 'ro');


=head2 my $saml = Zaaksysteem::SAML2->new_from_interfaces(\%OPTIONS)

    my $saml    = Zaaksysteem::SAML2->new_from_interfaces(
        {
            idp         => $c->model('DB::Interface')->find_active({id => $interface_id})
            uri         => $c->uri_for('/'),
        }
    );

Start a SAML object from a given idp. It will find the local SP by searching
in the interface table when option C<sp> is not given.

B<Options>

=over 4

=item idp [required]

isa: L<Zaaksysteem::Backend::Sysin::Interface::Component>

=item sp [optional]

isa: L<Zaaksysteem::Backend::Sysin::Interface::Component>

=back

=cut


define_profile new_from_interfaces => (
    required    => [qw[idp uri]],
    optional    => [qw[state]],
    # typed       => {
    #     sp          => 'Zaaksysteem::Model::DB::Interface',
    #     idp         => 'Zaaksysteem::Model::DB::Interface'
    # }
);

sub new_from_interfaces {
    my $class = shift;
    my $opts  = assert_profile(shift)->valid;

    return $class->new(
        uri => $opts->{uri},
        sp  => $class->_build_sp ($opts->{uri}, $opts->{ sp }, $opts->{ idp }),
        idp => $class->_build_idp($opts->{uri}, $opts->{ idp }),
        state => $opts->{ state } || {}
    );
}

define_profile new_from_relaystate => (
    required    => [qw[RelayState uri]],
);

sub new_from_relaystate {
    my $class           = shift;
    my $opts            = assert_profile(shift)->valid;
    my $schema          = shift;

    my $state           = decode_json(decode_base64($opts->{ RelayState }));

    unless (exists $state->{ idp }) {
        throw('saml2/relay/idp_not_configured', "Received RelayState, but it did not contain an IdP id. Can't continue.");
    }

    unless (exists $state->{ redirect }) {
        throw('saml2/relay/no_redirect', "Received RelayState, but it did not contain a redirect. Can't continue.");
    }

    my $interface       = $schema->resultset('Interface')->find($state->{ idp });

    return $class->new_from_interfaces({
        idp     => $interface,
        uri     => $opts->{ uri },
        state   => $state
    });
}


=head2 handle_response($artifact_resolved_xml)

Return value: L<Net::SAML2::Protocol::Assertion> or error on failure

=cut

sub handle_response {
    my $self                = shift;
    my $responsexml         = shift;

    my $response            = Zaaksysteem::SAML2::Binding::SOAP->new(
        url         => $self->idp->art_url('urn:oasis:names:tc:SAML:2.0:bindings:SOAP'),
        key         => $self->sp->cert,
        cert        => $self->sp->cert,
        idp_cert    => $self->idp->cert('signing'),
        cacert      => $self->idp->cacert
    );

    my $xmlp                = XML::XPath->new( xml => $responsexml );

    $xmlp->set_namespace('samlp', 'urn:oasis:names:tc:SAML:2.0:protocol');

    my $nodeset             = $xmlp->find('//samlp:Response/samlp:Status/samlp:StatusCode');

    throw(
        'saml2/handle_response/no_status',
        'No Status found in SOAP Response' 
    ) unless ($nodeset);

    my ($status)            = $nodeset->get_nodelist;

    throw(
        'saml2/handle_response/no_status',
        'No Status found in SOAP Response' 
    ) unless ($status);

    unless($status->getAttribute('Value') eq 'urn:oasis:names:tc:SAML:2.0:status:Success') {
        my ($substatus) = grep { $_->isa('XML::XPath::Node::Element'); } $status->getChildNodes;

        throw(
            'saml2/handle_response/invalid_status',
            'Invalid status received from SOAP Response: ' . $status->getAttribute('Value'),
            { status => $substatus->getAttribute('Value') }
        );
    }

    ### Validate XML response
    #my ($subject, $saml)    = $response->handle_response($saml);
    
    my $assertion = Net::SAML2::Protocol::Assertion->new_from_xml(
        xml => $responsexml,
    );

    throw(
        'saml2/handle_response/invalid_assertion',
        'Invalid assertion'
    ) unless $assertion;

    ### Retrieve subject from correct saml module
    return $assertion;
}

=head2 authentication_redirect(\%OPTIONS)

    my $redirect_url    = $saml->authenticate({
        uri => $c->req->params->{ relay_to }
    });

Return value: $AUTHENTICATION_REDIRECT_URL

Generates a authnrequest and returns the redirect url for use

=cut

define_profile authentication_redirect => (
    required    => [],
    optional    => [qw[state]],
);

sub authentication_redirect {
    my $self            = shift;
    my $opts            = assert_profile(shift || {})->valid;

    my $auth_request;

    # Wrap potential die() with a proper exception so we can stacktrace this
    try {
        $auth_request    = Zaaksysteem::SAML2::Protocol::AuthnRequest->new(
            issuer              => $self->sp->id,
            base_url            => $self->sp->url . '/saml',
            nameid_format       => $self->idp->format('entity'),
            auth_protocol       => $self->idp->interface->get_interface_config->{authentication_level},
        );
    } catch {
        throw('saml2/authn_request', 'Unable to construct AuthnRequest, intercepted error: ' . $_);
    };

    my $redirect        = Net::SAML2::Binding::Redirect->new(
        key                 => $self->sp->cert,
        cert                => $self->sp->cacert,
        param               => 'SAMLRequest',
        url                 => $self->idp->sso_url('urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect')
    );

    return $redirect->sign($auth_request->as_xml, $opts->{ state });
}


=head2 resolve_artifact($SamlART_identifier)

Return value: $XML_RESPONSE_IDP

Contacts the IDP using the given SamlART_identifier

=cut

sub resolve_artifact {
    my $self            = shift;

    my $resolve_request = Zaaksysteem::SAML2::Protocol::ArtifactResolve->new(
        artifact            => shift,
        issuer              => $self->sp->id,
        destination         => $self->idp->art_url('urn:oasis:names:tc:SAML:2.0:bindings:SOAP')
    );

    my $redirect        = Zaaksysteem::SAML2::Binding::SOAP->new(
        url                 => $self->idp->art_url('urn:oasis:names:tc:SAML:2.0:bindings:SOAP'),
        key                 => $self->sp->cert,
        cert                => $self->sp->cert,
        idp_cert            => $self->idp->cert('signing'),
        cacert              => $self->idp->cacert
    );

    return $redirect->request($resolve_request->as_xml);
}


=head1 INTERNAL METHODS

=head2 _build_sp($uri, $interface_sp, $interface_idp)

Builds the attribute C<< $saml->sp >> from interface_sp. When C< $interface_sp > is
not given, it will try to find the sp through the idp

=cut

sub _build_sp {
    my $class           = shift;
    my $uri             = shift;
    my $interface       = shift;
    my $idp_interface   = shift;

    unless ($interface) {
        $interface      = $idp_interface
                        ->result_source
                        ->schema
                        ->resultset('Interface')
                        ->search({ module => 'samlsp' })
                        ->first;
    }

    throw(
        'saml2/build_sp/no_sp_interface',
        'No SP interface given and cannot find it in interface config' 
    ) unless ($interface);

    my $interface_config = $interface->get_interface_config;

    my %files;
    for my $opt (qw/sp_cert sp_key/) {
        throw(
            'saml2/build_sp/invalid_cert_config',
            'Certificate config not correctly configed'
        ) unless (
            exists($interface_config->{$opt}) &&
            UNIVERSAL::isa($interface_config->{$opt}, 'ARRAY') &&
            $interface_config->{$opt}->[0] &&
            $interface_config->{$opt}->[0]->{id}
        );

        $files{$opt}    = $interface
                        ->result_source
                        ->schema
                        ->resultset('Filestore')
                        ->find($interface_config->{$opt}->[0]->{id});
    }

    my $ca_file_object = $interface->result_source->schema->resultset('Filestore')->find($idp_interface->get_interface_config->{ idp_ca }->[0]->{ id });

    try {
        return Zaaksysteem::SAML2::SP->new(
            id                  => ($interface->get_interface_config->{'sp_webservice'} || $uri . 'auth/saml'),
            url                 => $uri . 'auth',
            cert                => $files{'sp_cert'}->get_path,
            cacert              => $ca_file_object->get_path,
            org_contact         => $interface->get_interface_config->{'sp_contact_email'},
            org_display_name    => $interface->get_interface_config->{'sp_contact_name'},
            org_name            => $interface->get_interface_config->{'sp_application_name'},
        );
    } catch {
        throw('saml/sp', sprintf('Unable to construct SP object, intercepted error: "%s"', $_), {
            interface_config => $interface->get_interface_config,
            uri => $uri,
            x509_paths => \%files
        });
    };
}


=head2 _build_idp($uri, $interface_idp)

Builds the attribute C<< $saml->idp >> from interface_idp.

=cut

sub _build_idp {
    my $class           = shift;
    my $uri             = shift;
    my $interface       = shift;

    my $interface_config = $interface->get_interface_config;

    my $opt             = 'idp_ca';

    throw(
        'saml2/build_sp/invalid_cert_config',
        'Certificate config not correctly configed'
    ) unless (
        exists($interface_config->{$opt}) &&
        UNIVERSAL::isa($interface_config->{$opt}, 'ARRAY') &&
        $interface_config->{$opt}->[0] &&
        $interface_config->{$opt}->[0]->{id}
    );

    my $idp_ca  = $interface
                ->result_source
                ->schema
                ->resultset('Filestore')
                ->find($interface_config->{$opt}->[0]->{id})->get_path;

    return Zaaksysteem::SAML2::IdP->new_from_url(
        url         => $interface->get_interface_config->{idp_metadata},
        cacert      => $idp_ca,
        interface   => $interface,
    );
}


