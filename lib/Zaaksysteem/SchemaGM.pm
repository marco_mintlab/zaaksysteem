package Zaaksysteem::SchemaGM;

use strict;
use warnings;

use base 'DBIx::Class::Schema';

__PACKAGE__->load_classes;


# Created by DBIx::Class::Schema::Loader v0.04006 @ 2012-04-10 16:35:12
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:dYSMf9/kNQGMmbOa8dkP8g


# You can replace this text with custom content, and it will be preserved on regeneration
1;
