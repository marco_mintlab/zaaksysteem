package Zaaksysteem::ChartGenerator::Roles::Average_handling_time_per_month;

use Moose::Role;
use Data::Dumper;

=head2 search_average_handling_time_per_month

Returns a list with the average handling time per month within the
given resultset set. Run a grouping query.
 
=cut
sub search_average_handling_time_per_month {
    my ($self, $rs) = @_;

    my $resultset = $rs->search({
        afhandeldatum => {
            '!=' => undef
        }
    }, {
        select => [
            {
                date_part => "'month', registratiedatum",
                -as => 'month'
            },
            {
                date_part => "'year', registratiedatum",
                -as => 'year',
            },
            {
                avg => {
                    date_part => "'days', afhandeldatum - registratiedatum"
                },
                -as => 'average_handling_time'
            }
        ],
        group_by => [ 'year', 'month'],
        order_by => [ 'year', 'month']
    });

    $resultset->result_class('DBIx::Class::ResultClass::HashRefInflator');
    my @results = $resultset->all();
    return @results;
}


=head2 average_handling_time_per_month

Returns a chart profile
 
=cut
sub average_handling_time_per_month {
    my ($self) = @_;

    my @results = $self->search_average_handling_time_per_month($self->resultset);

    my @month_names = qw/januari februari maart april mei juni juli augustus september oktober november december/;

    my @months = map { 
        $month_names[$_->{month}-1] . ' '. $_->{year} 
    } @results;

    my @average_handling_times = map { int $_->{average_handling_time} } @results;
    
    my $profile = {
        chart => {
            type => 'column'
        },
        title => {
            text => 'Gemiddelde behandeltijd per maand',
        },
        xAxis => {
            categories => \@months
        },
        yAxis => {
            min => '0',
            title => {
                text => 'Aantal dagen'
            }
        },
        tooltip => {
            headerFormat => '<div style="font-size:10px">{point.key}</div><table border="1">',
            pointFormat => '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' .
                '<td style="padding:0"><b>{point.y:.f} dagen</b></td></tr>',
            footerFormat => '</table>',
            shared => 1,
            useHTML => 1
        },
        plotOptions => {
            column => {
                pointPadding => 0.2,
                borderWidth => '0'
            }
        },
        series => [{
            name => 'Gem. behandeltijd (dagen)',
            data => \@average_handling_times
        }]
    };

    return $profile;
}


1;
