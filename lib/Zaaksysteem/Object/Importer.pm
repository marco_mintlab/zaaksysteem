package Zaaksysteem::Object::Importer;

use Moose;
use Moose::Util qw[apply_all_roles];

use Data::Dumper;

has 'state' => ( is => 'rw', isa => 'Zaaksysteem::Object::ImportState' );
has 'schema' => ( is => 'rw' );
has 'object_type' => ( is => 'rw' );
has 'format' => ( is => 'rw' );

sub BUILD {
    my $self = shift;
    my $args = shift;

    unless(exists $args->{ format }) {
        die('Require import format');
    }

    apply_all_roles($self, sprintf(
        'Zaaksysteem::Object::Importer::%s',
        ucfirst($args->{ format })
    ));

    unless($self->handles($args->{ object_type })) {
        die($args->{ format } . ' importer cannot handle ' . $args->{ object_type });
    }
}

# Stub handles, implicitly return false
# for all, abstract instance don't do shit.
sub handles { }
sub redirect { } 

sub hydrate_import_state {
    die('Abstract importer object cannot hydrate the import state.');
}

sub execute_import_state {
    die('Abstract importer object cannot import anything.');
}

sub execute {
    my $self = shift;
    my $schema = shift;

    $self->execute_import_state($schema);
}

sub hydrate_from_files {
    my ($self, @files) = @_;

    my %results = map { $_->uuid => $self->hydrate_import_state($_) } @files;

    return \%results;
}

1;
