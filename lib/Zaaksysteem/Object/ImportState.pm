package Zaaksysteem::Object::ImportState;

use Moose;

use Zaaksysteem::Object::ImportStateItem;

use Data::Dumper;

has 'object_type' => ( is => 'rw' );
has 'import_fileref' => ( is => 'rw' );
has 'format' => ( is => 'rw' );
has 'library_id' => ( is => 'rw' );

has 'items' => (
    is => 'rw',
    isa => 'ArrayRef[Zaaksysteem::Object::ImportStateItem]',
    default => sub { [] }
);

around BUILDARGS => sub {
    my $orig = shift;
    my $class = shift;

    my %args = @_;

    if(exists $args{ items } && ref $args{ items } eq 'ARRAY') {
        $args{ items } = [
            map { Zaaksysteem::Object::ImportStateItem->new($_) } @{ $args{ items } }
        ];
    }

    return $class->$orig(\%args);
};

sub new_item {
    my $self = shift;

    my $item = Zaaksysteem::Object::ImportStateItem->new(shift);

    push(@{ $self->items }, $item);

    return $item;
}

sub get_state {
    my $self = shift;

    return {
        object_type => $self->object_type,
        import_fileref => $self->import_fileref,
        format => $self->format,
        library_id => $self->library_id,
        items => [ map { $_->get_state } @{ $self->items } ]
    };
}

sub get_by {
    my $self = shift;

    my ($item) = $self->find_by(@_);

    return $item;
}

sub find_by {
    my $self = shift;
    my $attribute = shift;
    my $value = shift;

    return grep { $_->$attribute eq $value } @{ $self->items };
}

1;
