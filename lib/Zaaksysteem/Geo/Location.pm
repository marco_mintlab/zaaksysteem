package Zaaksysteem::Geo::Location;

use Moose;

use constant LOCATION_ATTRIBUTES => [qw/
    address
    street
    province
    city
    country
    coordinates
    zipcode
    identification
/];

my $attrs = LOCATION_ATTRIBUTES;
has [@{ $attrs }] => (
    'is'    => 'ro'
);

sub TO_JSON {
    my $self    = shift;

    my $attrs   = LOCATION_ATTRIBUTES;

    my %result;
    $result{ $_ } = $self->$_ for @{ $attrs };

    return \%result;
}



1;

