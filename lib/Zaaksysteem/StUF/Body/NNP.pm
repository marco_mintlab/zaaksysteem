package Zaaksysteem::StUF::Body::NNP;

use Moose;

extends 'Zaaksysteem::StUF::Body';

use constant    OBJECT_NAME     => 'NNP';

use constant    OBJECT_VALUES   => [
    {
        name        => 'sofi-nummer',
        isa         => 'Maybe[Zaaksysteem::StUF::Body::Field]',
    },
    {
        name        => 'aanvullingSoFi-nummer',
        isa         => 'Maybe[Zaaksysteem::StUF::Body::Field]',
    },
    {
        name        => 'handelsnaam',
        isa         => 'Maybe[Zaaksysteem::StUF::Body::Field]',
    },
    {
        name        => 'statutaireNaamVennootschapsnaam',
        isa         => 'Maybe[Zaaksysteem::StUF::Body::Field]',
    },
    {
        name        => 'zaaknaam',
        isa         => 'Maybe[Zaaksysteem::StUF::Body::Field]',
    },
    {
        name        => 'datumOprichtingNietNatuurlijkPersoon',
        isa         => 'Maybe[Zaaksysteem::StUF::Body::Field]',
    },
    {
        name        => 'handelsRegisternummer',
        isa         => 'Maybe[Zaaksysteem::StUF::Body::Field]',
    },
    {
        name        => 'rechtsvorm',
        isa         => 'Maybe[Zaaksysteem::StUF::Body::Field]',
    },
    ### Relationships
    {
        name        => 'NNPADRVBL',
        isa         => 'Maybe[Zaaksysteem::StUF::Body::Relationship]',
        related     => 'ADR',
    },
    {
        name        => 'NNPADRCOR',
        isa         => 'Maybe[Zaaksysteem::StUF::Body::Relationship]',
        related     => 'ADR',
    },
];

use constant    EXTRA_VALUES   => [
    {
        name        => 'handelsRegisterVolgnummer',
        isa         => 'Maybe[Zaaksysteem::StUF::Body::Field]',
    },
];

### Generate Moose Attributes
for my $value (@{ OBJECT_VALUES() }) {
    my %attr_opts = map {
        $_      => $value->{$_}
    } grep ({ exists($value->{$_}) } qw/is isa default lazy/);
    $attr_opts{is}          = 'rw' unless $attr_opts{is};
    $attr_opts{predicate}   = 'has_' . $value->{name};
    $attr_opts{clearer}     = 'clear_' . $value->{name};

    has $value->{name} => %attr_opts;
}

has '_object_params'  => (
    is          => 'rw',
    lazy        => 1,
    default     => sub {
        return OBJECT_VALUES;
    }
);

has '+_object_extra_params' => (
    default     => sub {
        return EXTRA_VALUES;
    }
);

has '_object_name'  => (
    is          => 'rw',
    lazy        => 1,
    default     => sub {
        return OBJECT_NAME;
    }
);

__PACKAGE__->meta->make_immutable;