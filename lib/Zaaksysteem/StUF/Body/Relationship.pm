package Zaaksysteem::StUF::Body::Relationship;

use Moose;
use Zaaksysteem::Exception;

use Zaaksysteem::StUF::Body::PRS;
use Zaaksysteem::StUF::Body::ADR;

has 'name'              => (
    is      => 'rw',
);

has '_value'            => (
    is      => 'rw',
);

has 'value'             => (
    is      => 'rw',
);

has 'definition'        => (
    is      => 'rw',
);

has 'related'           => (
    is      => 'rw',
);

has 'is_active'         => (
    is      => 'rw',
    lazy    => 1,
    default => 0,
);

sub BUILD {
    my $self        = shift;

    my $values      = $self->_value;
    my $definition  = $self->definition;

    my $related_class = 'Zaaksysteem::StUF::Body::' . $definition->{related};

    my @relations;
    if (UNIVERSAL::isa($values, 'HASH')) {
        push(@relations, $values);
    } elsif (UNIVERSAL::isa($values, 'ARRAY')) {
        push(@relations, @{ $values });
    }

    $self->value([]);
    for my $relation (@relations) {
        my $value       = $relation->{ $definition->{related} };
        next unless $value;

        my $object      = $related_class->new(
            _body_params    => $value,
        );

        $object->_body_params($value);
        $object->_load_body_params;
        $object->is_active($self->_is_active_relationship($relation));

        $object->datumSluiting(
            Zaaksysteem::StUF::Body::Field->new(
                    _value  => $relation->{'datumSluiting'}
                )->value
        ) if $relation->{'datumSluiting'};

        push(
            @{ $self->value },
            $object
        )
    }
  
    $self->related($definition->{related});
}

sub _is_active_relationship {
    my $self            = shift;
    my $related         = shift;

    ### Check huwelijk
    if (
        exists ($related->{datumSluiting})
    ) {
        return $self->_is_active(
            $related->{datumSluiting},
            $related->{datumOntbinding}
        );
    }

    if (
        exists ($related->{tijdvakRelatie})
    ) {
        return $self->_is_active(
            $related    ->{tijdvakRelatie}
                        ->{begindatumRelatie},
            $related    ->{tijdvakRelatie}
                        ->{einddatumRelatie}
        );
    }
    
    return 1;
}

sub _is_active {
    my $self                = shift;
    my ($newdate, $olddate) = @_;


    my $startdate   = Zaaksysteem::StUF::Body::Field->new(
        _value  => $newdate
    )->value;
    my $enddate     = Zaaksysteem::StUF::Body::Field->new(
        _value  => $olddate
    )->value;

    ### Unknown entries are active.
    if (!$startdate && !$enddate) {
        return 1;
    }

    my $currentdate = DateTime->now->strftime('%Y%m%d');

    if ($startdate && $startdate =~ /^\d+$/ && $startdate < $currentdate) {
        if (!$enddate || $enddate !~ /^\d+$/ || $enddate > $currentdate) {
            return 1;
        }
    }

    return 0;
}

# For XML::Compile
sub TO_STUF {
    my $self            = shift;

    return {
        $self->definition->{related} => $self->value->TO_STUF
    };
}

sub as_params {
    my $self            = shift;
    my $rv              = [];
    for my $object (@{ $self->value }) {
        push(
            @{ $rv },
            $object->as_params
        );
    }

    return $rv;
    # return {
    #     is_active       => $self->is_active,
    #     %{ $self->value->as_params }
    # }
}

1;