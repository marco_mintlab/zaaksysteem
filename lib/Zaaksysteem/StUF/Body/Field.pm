package Zaaksysteem::StUF::Body::Field;

use Moose;

has 'name'              => (
    is      => 'rw',
);

has '_value'            => (
    is      => 'rw',
);

has 'value'             => (
    is      => 'rw',
);

has 'is_kerngegeven'    => (
    is      => 'rw',
);

has 'exact'             => (
    is      => 'rw',
);

has 'exists'            => (
    is      => 'rw',
    lazy    => 1,
    default => sub {
        return 1;
    }
);

sub BUILD {
    my $self            = shift;

    $self->_parse_value();
}

sub _parse_value {
    my $self            = shift;

    return unless $self->_value;

    return unless (
        UNIVERSAL::isa($self->_value, 'HASH')
    );

    $self->exact( $self->_value->{exact} );

    if (exists($self->_value->{noValue})) {
        if ($self->_value->{noValue} eq 'geenWaarde') {
            #warn('Setting: ' . $self->name . ' to undef');
            $self->value(undef);
            return;
        } else {
            $self->exists(0);
            return;
        }
    }

    return unless exists($self->_value->{_});

    $self->value( "" . $self->_value->{_} . "");
    #$self->kerngegeven( $self->_value->{kerngegeven} );

}

# For XML::Compile
sub TO_STUF {
    my $self            = shift;
    
    my $rv = {
        '_'     => $self->value,
    };

    $rv->{exact}        = $self->exact if $self->exact;

    return $rv;
}

1;