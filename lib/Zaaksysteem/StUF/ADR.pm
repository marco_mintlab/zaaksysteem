package Zaaksysteem::StUF::ADR;

use Moose::Role;

use constant ADDRESS_MAP            => {
    'extra.authentiekeIdentificatieWoonplaats'      => 'woonplaats_identificatie',
    'extra.authentiekeWoonplaatsnaam'               => 'woonplaats_naam',

    'extra.authentiekeIdentificatieOpenbareRuimte'  => 'openbareruimte_identificatie',
    'extra.officieleStraatnaam'                     => 'openbareruimte_naam',

    'extra.identificatieNummerAanduiding'   => 'nummeraanduiding_identificatie',
    'extra.ingangsdatum'                    => 'nummeraanduiding_begindatum',
    'huisnummer'                            => 'nummeraanduiding_huisnummer',
    'huisletter'                            => 'nummeraanduiding_huisletter',
    'huisnummertoevoeging'                  => 'nummeraanduiding_huisnummertoevoeging',
    'postcode'                              => 'nummeraanduiding_postcode',
    'extra.aanduidingGegevensInOnderzoek'   => 'nummeraanduiding_inonderzoek',
    'extra.status'                          => 'nummeraanduiding_status',

    'extra.identificatiecodeVerblijfplaats' => 'verblijfsobject_identificatie'
};

=head2 METHODS

=head2 get_params_for_natuurlijk_persoon

Gets a set of params for manipulating natuurlijk_persoon

=cut

sub get_params_for_adr {
    my $self            = shift;

    my $params          = {};
    my $object_params   = $self->as_params->{ADR};

    for my $key (keys %{ ADDRESS_MAP() }) {
        my $object_key  = $key;
        my $object_value;

        if ($object_key =~ /^extra\./) {
            $object_key =~ s/^extra\.//;

            next unless exists($object_params->{ 'extraElementen' }-> { $object_key });
            $object_value = $object_params->{ 'extraElementen' }-> { $object_key };
        } else {
            next unless exists($object_params->{ $object_key });

            $object_value = $object_params->{ $object_key };
        }

        $params->{ADDRESS_MAP->{$key}} = $object_value;
    }

    return $params;
}


1;