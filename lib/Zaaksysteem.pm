package Zaaksysteem;

use Moose;

use Data::Dumper;
use Sys::Hostname;
use IO::Socket;

use Zaaksysteem::Log::CallingLogger;
use Zaaksysteem::Request;
use Zaaksysteem::Cache;

use Catalyst qw/
    ConfigLoader
    Static::Simple

    Authentication
    Authorization::Roles

    Session
    Session::Store::File
    Session::State::Cookie

    Params::Profile


    Unicode::Encoding
    I18N
    ClamAV
/;

use CatalystX::RoleApplicator;

extends qw/Catalyst Zaaksysteem::General/;

our $VERSION = '3.15.2';

#extends 'Catalyst';
#with 'CatalystX::LeakChecker';
#with 'CatalystX::LeakChecker';

__PACKAGE__->request_class('Zaaksysteem::Request');
__PACKAGE__->apply_request_class_roles(qw/
    Zaaksysteem::TraitFor::Request::Params
/);

# Configure the application.
#
# Note that settings in zaaksysteem.conf (or other external
# configuration file that you set up manually) take precedence
# over this when using ConfigLoader. Thus configuration
# details given here can function as a default configuration,
# with an external configuration file acting as an override for
# local deployment.

my $configfile = $ENV{ZAAKSYSTEEM_CONF} || '/etc/zaaksysteem/zaaksysteem.conf';

$configfile  = (-f $configfile
    ? $configfile
    : 'etc/zaaksysteem.conf'
);

__PACKAGE__->config(
    'Plugin::ConfigLoader' => {
        file => $configfile
    },
    'name'              => 'Zaaksysteem',
    'View::TT'          => {
        tpl       => 'zaak_v1',
        locale    => 'nl_NL',
    },
    'View::JSON' => {
        allow_callback  => 0,
        expose_stash    => 'json',
    },
    'View::JSONlegacy' => {
        allow_callback  => 0,    # defaults to 0
        expose_stash    => [ qw(json) ],
    },
    'View::Email'          => {
        stash_key => 'email',
        template_prefix => 'tpl/zaak_v1/nl_NL',
        default => {
            content_type => 'text/plain',
            charset => 'utf-8',
            from    => 'info@zaaksysteem.nl',
            view        => 'TT',
        },
        sender => {
            'mailer'    => 'Sendmail'
        }
    },
    'default_view'      => 'TT',
    'static'            => {
        ignore_extensions => [ qw/tmpl tt tt2/ ],
    },

    recaptcha => {
        # Keys for https://www.google.com/recaptcha/admin/site?sideid=316632375
        #pub_key => '6Lc3bd8SAAAAAMbd21cpDAXUZfdT3m0Q_FAocTdZ',
        #priv_key => '6LcTR98SAAAAAL-5OlXaxoac-OUjvS7M6IeRSD-1',
        
        # Keys for https://www.google.com/recaptcha/admin/site?siteid=316622611
        #pub_key => '6LcTR98SAAAAAHveMZhxipvBZKo9OwA_WMqWX9EX',
        #priv_key => '6LcTR98SAAAAAL-5OlXaxoac-OUjvS7M6IeRSD-1',

        # Keys for # https://www.google.com/recaptcha/admin/site?siteid=316632983
        pub_key => '6LeXb98SAAAAAL1tl8vNTFRhaOpWJtzUEn0FmGUb',
        priv_key => '6LeXb98SAAAAADFQHXt7ZvSR3Rm5IT4aOlVx9Lba',
        options => { theme => 'clean', lang => 'nl' }
    }
);

__PACKAGE__->mk_classdata($_) for qw/
    _additional_static
/;

__PACKAGE__->log(Zaaksysteem::Log::CallingLogger->new());


### Define state of machine, dev?
{

    if (hostname() =~ /^app/) {
        __PACKAGE__->config(
            'otap'  => 'prod'
        );
    } elsif (hostname() =~ /^demo/) {
        __PACKAGE__->config(
            'otap'  => 'demo'
        );
    } elsif (hostname() =~ /^accept/) {
        __PACKAGE__->config(
            'otap'  => 'accept'
        );
    } elsif (hostname() =~ /^test/) {
        __PACKAGE__->config(
            'otap'  => 'test'
        );
    } else {
        __PACKAGE__->config(
            'otap'  => 'dev'
        );
    }

    if (__PACKAGE__->config->{otap} ne 'dev') {
        __PACKAGE__->config(
            'Plugin::Session' => {
                cookie_secure => 2,
                storage => '/tmp/ezra_'
                    . __PACKAGE__->config->{otap}
            }
        );
    } else {
        __PACKAGE__->config(
            'Plugin::Session' => {
                cookie_secure => 2,
                storage => '/tmp/ezra_dev_' . $ENV{USER}
            }
        );

    }

}

### Define config location
{
    use File::Basename;

    my $working_directory = dirname($configfile);
    if (-d $working_directory) {
        __PACKAGE__->config->{config_directory} = $working_directory;
    } else {
        die(
            'Config file zaaksysteem.conf not found: '
            . 'make sure you have created /etc/zaaksysteem/zaaksysteem.conf'
        );
    }
}

# Start the application
__PACKAGE__->setup();

# Configure the customers
{
    __PACKAGE__->mk_classdata('customer');

    ### Load customer configuration from customer.d
    if (-d __PACKAGE__->config->{config_directory} . '/customer.d') {
        my $cfgs        =  Config::Any->load_files(
            {
                files => [
                    glob(
                        __PACKAGE__->config->{config_directory}
                        . '/customer.d/*.conf'
                    )
                ],
                use_ext => 1,
                flatten_to_hash => 1,
            }
        );

        for my $configfile (values %{ $cfgs }) {
            for my $customer (keys %{ $configfile }) {
                __PACKAGE__->config->{customers} = {} unless UNIVERSAL::isa(
                    __PACKAGE__->config->{customers}, 'HASH'
                );

                __PACKAGE__->config->{customers}->{$customer} = $configfile->{ $customer };
            }

        }
    }

    if (
        __PACKAGE__->config->{customers} &&
        UNIVERSAL::isa(__PACKAGE__->config->{customers}, 'HASH')
    ) {
        __PACKAGE__->customer({});
        for my $host (keys %{ __PACKAGE__->config->{customers} }) {
            __PACKAGE__->customer->{$host} = {
                'dbh'           => undef,
                'dbgh'          => undef,
                'ldaph'         => undef,
                'start_config'  => __PACKAGE__->config
                    ->{customers}
                    ->{$host},
                'run_config'    => undef,
            };
        }
    } else {
        die('Error: no customers defined in zaaksysteem.conf');
    }

    sub customer_instance {
        my $c           = shift;

        my $hostname;

        if (!ref($c) && $ENV{'ZAAKSYSTEEM_CURRENT_CUSTOMER'}) {
            $hostname = $ENV{'ZAAKSYSTEEM_CURRENT_CUSTOMER'}
        } else {
            $hostname    = $c->req->uri->host;
        }

        unless (__PACKAGE__->customer->{ $hostname }) {
            ### Second try (subdomains etc)
            for my $host (keys %{ __PACKAGE__->config->{customers} }) {
                ### XXX TODO
            }

            die(
                'Could not find configuration for hostname: '
                . $hostname
            );
        }

        my $customerdata    = __PACKAGE__->customer->{ $hostname };

        if ($customerdata->{start_config}->{dropdir}) {
            __PACKAGE__->config->{dropdir} = $customerdata
                ->{start_config}
                ->{dropdir};
        }

        my $tt_template = 'zaak_v1';
        if ($customerdata->{start_config}->{template}) {
            $tt_template = $customerdata->{start_config}->{template};
        }
        if ($customerdata->{start_config}->{customer_id}) {
            __PACKAGE__->config->{gemeente_id} = $customerdata->{start_config}->{customer_id}
        }

        __PACKAGE__->_additional_static([
            __PACKAGE__->config->{root},
            __PACKAGE__->config->{root} . '/tpl/'
                . $tt_template
                . '/' . __PACKAGE__->config->{'View::TT'}->{locale}
        ]);

        __PACKAGE__->config->{static}->{include_path} = __PACKAGE__->_additional_static;

        if ($customerdata->{start_config}->{files}) {
            __PACKAGE__->config->{files}  = $customerdata->{start_config}->{files};
        } else {
            __PACKAGE__->config->{files}  = __PACKAGE__->config->{home}
                . '/'
                . $customerdata->{start_config}->{customer_id}
                . '/files';
        }

        if ($customerdata->{start_config}->{customer_info}) {
            __PACKAGE__->config->{gemeente} =
                $customerdata->{start_config}->{customer_info};
        }

        __PACKAGE__->config->{'SVN_VERSION'} = $VERSION;

        if ($customerdata->{start_config}->{publish}) {
            __PACKAGE__->config->{publish} =
                $customerdata->{start_config}->{publish};
        }

        return $customerdata;
    }
}


__PACKAGE__->config->{ 'Plugin::Captcha' } = {
    session_name => 'captcha_string',
    new => {
        width => 300,
        height => 100,
        scramble => 1,
        ptsize => 34,
        frame => 2,
        rndmax => 5,
        thickness => 1,
        lines => 25,
        color => '#FFCC00',
    },
    create => ['ttf', 'circle', '#3E8FA4', '#999',],
    particle => [1500],
    out => {force => 'jpeg'}
};


around 'dispatch' => sub {
    my $orig    = shift;
    my $c       = shift;

    {
        my $realm               = $c->get_auth_realm('default');

        my $customer_instance   = $c->customer_instance;

        $c->config->{customer} = $customer_instance;

        $realm->store->user_basedn($customer_instance->{start_config}->{'LDAP'}->{basedn});
        $realm->store->role_basedn($customer_instance->{start_config}->{'LDAP'}->{basedn});
    }

    return $c->$orig(@_);
};

### Basic zaak authorisation
sub _can_change_messages {
    my ($c) = @_;

    return unless $c->user_exists;
    
    my $case = $c->stash->{ zaak };
    my $case_open_url = $c->uri_for(sprintf('/zaak/%d/open', $case->id));

    if ($case->is_afgehandeld) {
        $c->push_flash_message(
            'U kunt geen wijzigingen aanbrengen aan deze zaak. Deze zaak is afgehandeld'
        );

        return 1;
    }

    if($case->status eq 'new') {
        unless($case->behandelaar) {
            $c->push_flash_message(sprintf(
                'U bent niet de behandelaar van deze zaak. Klik <a href="%s">hier</a> om in behandeling te nemen.',
                $case_open_url->as_string
            ));
        } elsif ($case->behandelaar->gegevens_magazijn_id eq $c->user->uidnumber) {
            $c->push_flash_message(sprintf(
                'U heeft deze zaak nog niet in behandeling genomen. Klik <a href="%s">hier</a> om in behandeling te nemen.',
                $case_open_url->as_string
            ));
        } else {
            $c->push_flash_message('U bent niet de behandelaar van deze zaak.');
        }
    }

    if($case->status eq 'open' || $case->status eq 'stalled') {
        unless($case->behandelaar) {
            $c->push_flash_message(sprintf(
                'U bent niet de behandelaar van deze zaak. Klik <a href="%s">hier</a> om in behandeling te nemen.',
                $case_open_url->as_string
            ));
        } elsif ($c->user_exists && $case->behandelaar->gegevens_magazijn_id ne $c->user->uidnumber) {
            $c->push_flash_message('U bent niet de behandelaar van deze zaak.');
        }
    }

    if ($case->status eq 'stalled') {
        $c->push_flash_message(sprintf(
            '<span class="flash-urgent">Zaak opgeschort: %s</span>',
            $case->reden_opschorten || ''
        ));
    }
}

sub can_change {
    my ($c, $opt) = @_;

    return unless $c->stash->{zaak};

    unless($c->req->param('_can_change_messages')) {
        $c->req->param('_can_change_messages', 1);
#        $c->log->debug("calling can change messages");
        $c->_can_change_messages;
    }

    if (
        $c->stash->{zaak}->is_afgehandeld &&
        !$opt->{ignore_afgehandeld}
    ) {
#        $c->log->debug('can_change false: zaak afgehandeld');
        return;
    }

    ### Zaak beheerders mogen wijzigingen aanbrengen ondanks dat ze geen
    ### behandelaar.
    return 1 if (
        $c->check_any_zaak_permission('zaak_beheer')
    );

    ### Override when we have the correct permissions, and we have a
    ### coordinator and behandelaar
    return 1 if (
        $c->stash->{zaak}->behandelaar &&
        $c->stash->{zaak}->coordinator &&
        $c->check_any_zaak_permission('zaak_beheer','zaak_edit')
    );

    if ($c->stash->{zaak}->status eq 'new') {
        if (
            (
                !$c->stash->{zaak}->behandelaar &&
                !$c->stash->{zaak}->coordinator
            )
        ) {
            return;
        }

        ### Zaak is new, but when we are coordinator, we still can make
        ### changes
        if (
            $c->user_exists &&
            $c->stash->{zaak}->coordinator &&
            $c->stash->{zaak}->coordinator->gegevens_magazijn_id
                eq $c->user->uidnumber
        ) {
            return 1;
        }
    }

    if (
        !$c->stash->{zaak}->behandelaar ||
        !$c->user_exists ||
        !$c->user->uidnumber ||
        (
            $c->stash->{zaak}->behandelaar &&
            $c->stash->{zaak}->behandelaar->gegevens_magazijn_id ne
            $c->user->uidnumber &&
            (
                !$c->stash->{zaak}->coordinator ||
                $c->stash->{zaak}->coordinator->gegevens_magazijn_id ne
                $c->user->uidnumber
            )
        )
    ){
        $c->push_flash_message(
            'U kunt geen wijzigingen aanbrengen aan deze zaak, u bent geen behandelaar / coordinator.'
        );

        return;
    }

    return 1;
}

sub return_undef { return }


# See:
# http://search.cpan.org/~bobtfish/Catalyst-Plugin-Session-0.35/lib/Catalyst/Plugin/Session.pm
#
sub push_flash_message {
    my $c       = shift;
    my $message;

    if (UNIVERSAL::isa($_[0], 'HASH')) {
        $message    = shift;
    } else {
        $message    = sprintf shift, @_;
    }

    my $result;

    $result = $c->flash->{result} || [];

    # As long as flash->{result} is used without this sub, we need to accomodate
    unless(ref $result && ref $result eq 'ARRAY') {
        $result = [$result];
    } elsif (!ref($result)) {
        $result = {
            message         => $result,
            type            => 'info'
        };
    }

    if (!UNIVERSAL::isa($message, 'HASH')) {
        $message    = {
            message     => $message,
            type        => 'info',
        };
    }
    
    # Since same code runs multiple times sometimes, check for unicity
    my $lookup = { map { $_->{message} =>  1} grep { UNIVERSAL::isa($_, 'HASH') } @$result };
    unless($lookup->{$message->{message}}) {
        push @$result, $message;
    }

    
    $c->flash->{result} = $result;    
    
    return $result;
}

sub push_message {
    my $c           = shift;
    my $message;

    if (UNIVERSAL::isa($_[0], 'HASH')) {
        $message    = shift;
    } else {
        $message    = sprintf shift, @_;
    }

    my $result;

    $result = $c->stash->{flash} || [];

    # As long as flash->{result} is used without this sub, we need to accomodate
    unless(ref $result && ref $result eq 'ARRAY') {
        $result = [$result];
    } elsif (!ref($result)) {
        $result = {
            message         => $result,
            type            => 'info'
        };
    }

    if (!UNIVERSAL::isa($message, 'HASH')) {
        $message    = {
            message     => $message,
            type        => 'info',
        };
    }

    # Since same code runs multiple times sometimes, check for unicity
    my $lookup = { map { $_->{message} =>  1} grep { UNIVERSAL::isa($_, 'HASH') } @$result };
    unless($lookup->{$message->{message}}) {
        push @$result, $message;
    }

    $c->stash->{flash}  = $result;
    
    return $result;
}


sub uri_format_ampersands {
    my ($c, $uri) = @_;
    
    $uri =~ s|&(?!amp;)|&amp;|gis;
    
    return $uri;
}

sub format_error {
    my ($c, $error) = @_;
    my $error_ref   = ref $error;

    $c->res->status(500);

    local $Data::Dumper::Terse = 1;

    # Pretty Exception::Class errors
    if (UNIVERSAL::isa($error, 'Exception::Class::Base')) {
        my @trace_args;

        # Stracktraces can be -huge-, only add them in developer mode.
        if ($c->debug) {
            @trace_args = map {
                {
                    package    => $_->package,
                    line       => $_->line,
                    filename   => $_->filename,
                    subroutine => $_->subroutine,
                    args_passed_to_sub => [$_->args],
                }
            } $error->trace->frames;
        }

        $c->log->error('Global exception caught', Dumper $error->as_string);

        return {
            stacktrace      => \@trace_args,
            debug           => $c->debug,
            body_parameters => $c->req->body_parameters,
            error_code      => $error->code || 'unhandled',
            category        => $error_ref,
            messages        => [$error->as_string],
        };
    } elsif (UNIVERSAL::isa($error, 'Zaaksysteem::Exception::Base')) {
        my $formatter = sub {
            my $frame = shift;

            return {
                package => $frame->package,
                filename => $frame->filename,
                line => $frame->line,
                subroutine => $frame->subroutine,
                args_passwd_to_sub => []
            };
        };

        $c->log->error('Global exception caught', $error->TO_STRING);

        my @trace_args;
        
        if($c->debug) {
            @trace_args = map { $formatter->($_) } $error->stack_trace->frames;
            $c->log->debug('Stacktrace dump', $error->stack_trace->as_string);
        }

        return {
            stacktrace      => \@trace_args,
            debug           => $c->debug,
            body_parameters => $c->req->body_parameters,
            error_code      => 500,
            category        => ref $error,
            messages        => [ $error->message ]
        };
    }
    # Non-Exception::Class errors - i.e. regular die's
    else {
        $c->log->error('Global exception caught', Dumper $error);

        return {
            debug           => $c->debug,
            body_parameters => [$c->req->body_parameters],
            error_code      => 'unhandled',
            category        => sprintf("%s", $c->action),
            messages        => Dumper $error,
            stacktrace      => 'Not (yet) available for non-exception errors',
        };
    }
}


# Eh... what's this doing here?
# TODO evaluate removal
sub register_it {
    my ($it, $callback)  = @_;

    open(my $file, '>>', '/tmp/register');
    print $file $it . ' : ' . join(' : ', @{ [caller($callback || 1)] }[1,3,2]) . "\n";
    close($file);
}

sub cache {
    my $c   = shift;

    return $c->stash->{__zs_cache} if (
        exists($c->stash->{__zs_cache}) &&
        UNIVERSAL::isa($c->stash->{__zs_cache}, 'Zaaksysteem::Cache')
    );

    $c->stash->{__zs_cache_store} = {};

    return Zaaksysteem::Cache->new(storage => $c->stash->{__zs_cache_store});
}


1;

=head1 NAME

Zaaksysteem - Case management system for improving business processes

=head1 SYNOPSIS

  # On a Ubuntu system, zaaksysteem build from package:

  bash# start zaaksysteem

=head1 DESCRIPTION

Zaaksysteem is a case management solution for businesses. Althoug we primarily
focues on local government projects. The program is widely used in other SaaS
deployments

=head1 DOCUMENTATION

User documentation can be found on our wiki page:
L<http://wiki.zaaksysteem.nl/>

This document gives insight in our API documentation, for connecting Frontend
code to our Backend.

=head1 WHERE TO GO NEXT

Please look at L<Zaaksysteem::Manual> for an index of all our components.

=head1 EXAMPLES

See L<SYNOPSIS>

=head1 SEE ALSO

L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::Install>

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut

