-- Column: id

-- ALTER TABLE config DROP COLUMN id;

ALTER TABLE config ADD COLUMN id SERIAL;
ALTER TABLE config ALTER COLUMN id SET NOT NULL;

-- Constraint: config_pkey

-- ALTER TABLE config DROP CONSTRAINT config_pkey;

ALTER TABLE config
  ADD CONSTRAINT config_pkey PRIMARY KEY(id );

-- Column: advanced

-- ALTER TABLE config DROP COLUMN advanced;

ALTER TABLE config ADD COLUMN advanced boolean NOT NULL DEFAULT true;

