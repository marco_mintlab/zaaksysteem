-- Table: event

-- DROP TABLE event;

CREATE TABLE event
(
  id serial NOT NULL,
  type character(16) NOT NULL,
  created_on timestamp without time zone NOT NULL DEFAULT now(),
  data text,
  created_by character(128),
  CONSTRAINT event_pkey PRIMARY KEY (id )
)
WITH (
  OIDS=FALSE
);

