BEGIN;

CREATE TABLE email_file (
    id SERIAL PRIMARY KEY,
    file_id INTEGER NOT NULL REFERENCES file(id),
    bibliotheek_notificaties_id INTEGER NOT NULL REFERENCES bibliotheek_notificaties(id)
);

COMMIT;
