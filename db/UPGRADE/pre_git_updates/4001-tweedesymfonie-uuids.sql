alter table documents add uuid uuid;
alter table zaak_betrokkenen add uuid uuid;
alter table zaak add uuid uuid;

-- eerst tabel leeggooien..
alter table woz_objects add object_id varchar(32) not null;

alter table zaak_subcase add can_advance_parent boolean not null default false;
alter table zaaktype_relatie add can_advance_parent boolean not null default false;

--alter table zaaktype_relatie drop can_advance_parent;
--alter table zaak_subcase drop can_advance_parent;

alter table zaaktype_relatie add parent_advance_results text;
alter table zaak_subcase add parent_advance_results text;
