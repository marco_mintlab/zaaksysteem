BEGIN;

-- Get rid of the ENUMs, they're a bother to maintain and not all that well supported in DBIx::Class
ALTER TABLE file_metadata DROP COLUMN trust_level;
ALTER TABLE file_metadata DROP COLUMN origin;

DROP TYPE trust_level;
DROP TYPE origin;

ALTER TABLE file_metadata ADD COLUMN trust_level VARCHAR(100) CHECK (
    trust_level ~ '(Openbaar|Beperkt openbaar|Intern|Zaakvertrouwelijk|Vertrouwelijk|onfidentieel|Geheim|Zeer geheim)'
) DEFAULT 'Zaakvertrouwelijk' NOT NULL;

ALTER TABLE file_metadata ADD COLUMN origin VARCHAR(100) CHECK (
    origin ~ '(Inkomend|Uitgaand|Intern)'
);

COMMIT;