
ALTER TABLE checklist_antwoord
	DROP CONSTRAINT zaak_id_fkey;

ALTER TABLE documents
	DROP CONSTRAINT zaak_id_fkey;

ALTER TABLE logging
	DROP CONSTRAINT zaak_id_fkey;

ALTER TABLE zaak
	DROP CONSTRAINT zaak_pid_fkey;

ALTER TABLE zaak
	DROP CONSTRAINT zaak_relates_to_fkey;

ALTER TABLE zaak
	DROP CONSTRAINT zaak_vervolg_van_fkey;

CREATE SEQUENCE searchable_searchable_id_seq
	START WITH 1
	INCREMENT BY 1
	NO MAXVALUE
	NO MINVALUE
	CACHE 1;

CREATE SEQUENCE documenten_id_seq
	START WITH 1
	INCREMENT BY 1
	NO MAXVALUE
	NO MINVALUE
	CACHE 1;

CREATE SEQUENCE jobs_id_seq
	START WITH 1
	INCREMENT BY 1
	NO MAXVALUE
	NO MINVALUE
	CACHE 1;

CREATE SEQUENCE kennisbank_producten_id_seq
	START WITH 1
	INCREMENT BY 1
	NO MAXVALUE
	NO MINVALUE
	CACHE 1;

CREATE SEQUENCE kennisbank_relaties_id_seq
	START WITH 1
	INCREMENT BY 1
	NO MAXVALUE
	NO MINVALUE
	CACHE 1;

CREATE SEQUENCE kennisbank_vragen_id_seq
	START WITH 1
	INCREMENT BY 1
	NO MAXVALUE
	NO MINVALUE
	CACHE 1;

CREATE SEQUENCE notitie_id_seq
	START WITH 1
	INCREMENT BY 1
	NO MAXVALUE
	NO MINVALUE
	CACHE 1;

CREATE SEQUENCE seen_id_seq
	START WITH 1
	INCREMENT BY 1
	NO MAXVALUE
	NO MINVALUE
	CACHE 1;

CREATE SEQUENCE zaak_kenmerk_id_seq
	START WITH 1
	INCREMENT BY 1
	NO MAXVALUE
	NO MINVALUE
	CACHE 1;

CREATE TABLE searchable (
	search_index tsvector,
	search_term text,
	object_type character varying(100),
	searchable_id integer DEFAULT nextval('searchable_searchable_id_seq'::regclass) NOT NULL
);

CREATE TABLE documenten (
	id integer DEFAULT nextval('documenten_id_seq'::regclass) NOT NULL,
	folder boolean,
	folder_id integer,
	zaak_id integer,
	betrokkene_dsn text,
	milestone integer,
	naam text,
	zaaktype_kenmerken_id integer,
	document_type text,
	verplicht boolean,
	pip boolean,
	versie integer,
	store_type text,
	filestore_id integer,
	notitie_id integer,
	queue boolean,
	ontvangstdatum timestamp without time zone,
	created timestamp without time zone,
	last_modified timestamp without time zone,
	deleted timestamp without time zone,
	job_id integer
);

CREATE TABLE jobs (
	id integer DEFAULT nextval('jobs_id_seq'::regclass) NOT NULL,
	bericht text,
	task_context text,
	task text,
	raw_input text,
	raw_result text,
	created timestamp without time zone,
	last_modified timestamp without time zone,
	deleted timestamp without time zone
);

CREATE TABLE kennisbank_producten (
	search_index tsvector,
	search_term text,
	object_type character varying(100) DEFAULT 'kennisbank_producten'::character varying,
	id integer DEFAULT nextval('kennisbank_producten_id_seq'::regclass) NOT NULL,
	pid integer,
	bibliotheek_categorie_id integer,
	naam text,
	omschrijving text,
	voorwaarden text,
	aanpak text,
	kosten text,
	versie integer,
	author text,
	commit_message text,
	active boolean,
	created timestamp without time zone,
	last_modified timestamp without time zone,
	deleted timestamp without time zone
)
INHERITS (searchable);

CREATE TABLE kennisbank_relaties (
	id integer DEFAULT nextval('kennisbank_relaties_id_seq'::regclass) NOT NULL,
	kennisbank_producten_id integer,
	kennisbank_vragen_id integer,
	zaaktype_id integer
);

CREATE TABLE kennisbank_vragen (
	search_index tsvector,
	search_term text,
	object_type character varying(100) DEFAULT 'kennisbank_vragen'::character varying,
	id integer DEFAULT nextval('kennisbank_vragen_id_seq'::regclass) NOT NULL,
	pid integer,
	bibliotheek_categorie_id integer,
	naam text,
	vraag text,
	antwoord text,
	versie integer,
	author text,
	commit_message text,
	active boolean,
	last_viewed timestamp without time zone,
	created timestamp without time zone,
	last_modified timestamp without time zone,
	deleted timestamp without time zone
)
INHERITS (searchable);

CREATE TABLE notitie (
	id integer DEFAULT nextval('notitie_id_seq'::regclass) NOT NULL,
	onderwerp text,
	bericht text,
	contactkanaal text,
	md5 text,
	created timestamp without time zone,
	last_modified timestamp without time zone,
	deleted timestamp without time zone
);

CREATE TABLE seen (
	id integer DEFAULT nextval('seen_id_seq'::regclass) NOT NULL,
	component text,
	kennisbank_producten_id integer,
	kennisbank_vragen_id integer,
	betrokkene_dsn text,
	created timestamp without time zone
);

CREATE TABLE zaak_kenmerk (
	zaak_id integer NOT NULL,
	bibliotheek_kenmerken_id integer NOT NULL,
	"value" text NOT NULL,
	id integer DEFAULT nextval('zaak_kenmerk_id_seq'::regclass) NOT NULL
);

ALTER TABLE bedrijf
	ADD COLUMN search_index tsvector,
	ADD COLUMN search_term text,
	ADD COLUMN object_type character varying(100) DEFAULT 'bedrijf'::character varying;

ALTER TABLE bedrijf
	INHERIT searchable;

ALTER TABLE bibliotheek_categorie
	ADD COLUMN search_index tsvector,
	ADD COLUMN search_term text,
	ADD COLUMN object_type character varying(100) DEFAULT 'bibliotheek_categorie'::character varying;

ALTER TABLE bibliotheek_categorie
	INHERIT searchable;

ALTER TABLE bibliotheek_kenmerken
	ADD COLUMN search_index tsvector,
	ADD COLUMN search_term text,
	ADD COLUMN object_type character varying(100) DEFAULT 'bibliotheek_kenmerken'::character varying;

ALTER TABLE bibliotheek_kenmerken
	INHERIT searchable;

ALTER TABLE bibliotheek_sjablonen
	ADD COLUMN search_index tsvector,
	ADD COLUMN search_term text,
	ADD COLUMN object_type character varying(100) DEFAULT 'bibliotheek_sjablonen'::character varying;

ALTER TABLE bibliotheek_sjablonen
	INHERIT searchable;

ALTER TABLE documents
	ADD COLUMN search_index tsvector,
	ADD COLUMN search_term text,
	ADD COLUMN object_type character varying(100) DEFAULT 'documents'::character varying;

ALTER TABLE documents
	INHERIT searchable;

ALTER TABLE natuurlijk_persoon
	ADD COLUMN search_index tsvector,
	ADD COLUMN search_term text,
	ADD COLUMN object_type character varying(100) DEFAULT 'natuurlijk_persoon'::character varying;

ALTER TABLE natuurlijk_persoon
	INHERIT searchable;

ALTER TABLE zaak
	ADD COLUMN search_index tsvector,
	ADD COLUMN search_term text,
	ADD COLUMN object_type character varying(100) DEFAULT 'zaak'::character varying;

ALTER TABLE zaak
	INHERIT searchable;

ALTER TABLE zaaktype
	ADD COLUMN search_index tsvector,
	ADD COLUMN search_term text,
	ADD COLUMN object_type character varying(100) DEFAULT 'zaaktype'::character varying,
	ALTER COLUMN active SET DEFAULT 1;

ALTER TABLE zaaktype
	INHERIT searchable;

ALTER TABLE zaaktype_kenmerken
	ADD COLUMN pip_can_change boolean;

ALTER TABLE documenten
	ADD CONSTRAINT documenten_pkey PRIMARY KEY (id);

ALTER TABLE jobs
	ADD CONSTRAINT jobs_pkey PRIMARY KEY (id);

ALTER TABLE kennisbank_producten
	ADD CONSTRAINT kennisbank_producten_pkey PRIMARY KEY (id);

ALTER TABLE kennisbank_relaties
	ADD CONSTRAINT kennisbank_relaties_pkey PRIMARY KEY (id);

ALTER TABLE kennisbank_vragen
	ADD CONSTRAINT kennisbank_vragen_pkey PRIMARY KEY (id);

ALTER TABLE notitie
	ADD CONSTRAINT notitie_pkey PRIMARY KEY (id);

ALTER TABLE seen
	ADD CONSTRAINT seen_pkey PRIMARY KEY (id);

ALTER TABLE zaak_kenmerk
	ADD CONSTRAINT zaak_kenmerk_pkey PRIMARY KEY (id);

ALTER TABLE documenten
	ADD CONSTRAINT documenten_filestore_id_fkey FOREIGN KEY (filestore_id) REFERENCES filestore(id);

ALTER TABLE documenten
	ADD CONSTRAINT documenten_folder_id_fkey FOREIGN KEY (folder_id) REFERENCES documenten(id);

ALTER TABLE documenten
	ADD CONSTRAINT documenten_job_id_fkey FOREIGN KEY (job_id) REFERENCES jobs(id);

ALTER TABLE documenten
	ADD CONSTRAINT documenten_notitie_id_fkey FOREIGN KEY (notitie_id) REFERENCES notitie(id);

ALTER TABLE documenten
	ADD CONSTRAINT documenten_zaak_id_fkey FOREIGN KEY (zaak_id) REFERENCES zaak(id);

ALTER TABLE documenten
	ADD CONSTRAINT documenten_zaaktype_kenmerken_id_fkey FOREIGN KEY (zaaktype_kenmerken_id) REFERENCES zaaktype_kenmerken(id);

ALTER TABLE kennisbank_producten
	ADD CONSTRAINT kennisbank_producten_bibliotheek_categorie_id_fkey FOREIGN KEY (bibliotheek_categorie_id) REFERENCES bibliotheek_categorie(id);

ALTER TABLE kennisbank_producten
	ADD CONSTRAINT kennisbank_producten_pid_fkey FOREIGN KEY (pid) REFERENCES kennisbank_producten(id);

ALTER TABLE kennisbank_relaties
	ADD CONSTRAINT kennisbank_relaties_kennisbank_producten_id_fkey FOREIGN KEY (kennisbank_producten_id) REFERENCES kennisbank_producten(id);

ALTER TABLE kennisbank_relaties
	ADD CONSTRAINT kennisbank_relaties_kennisbank_vragen_id_fkey FOREIGN KEY (kennisbank_vragen_id) REFERENCES kennisbank_vragen(id);

ALTER TABLE kennisbank_relaties
	ADD CONSTRAINT kennisbank_relaties_zaaktype_id_fkey FOREIGN KEY (zaaktype_id) REFERENCES zaaktype(id);

ALTER TABLE kennisbank_vragen
	ADD CONSTRAINT kennisbank_vragen_bibliotheek_categorie_id_fkey FOREIGN KEY (bibliotheek_categorie_id) REFERENCES bibliotheek_categorie(id);

ALTER TABLE kennisbank_vragen
	ADD CONSTRAINT kennisbank_vragen_pid_fkey FOREIGN KEY (pid) REFERENCES kennisbank_vragen(id);

ALTER TABLE seen
	ADD CONSTRAINT seen_kennisbank_producten_id_fkey FOREIGN KEY (kennisbank_producten_id) REFERENCES kennisbank_producten(id);

ALTER TABLE seen
	ADD CONSTRAINT seen_kennisbank_vragen_id_fkey FOREIGN KEY (kennisbank_vragen_id) REFERENCES kennisbank_vragen(id);

ALTER TABLE zaak_kenmerk
	ADD CONSTRAINT zaak_kenmerk_bibliotheek_kenmerken_id_fkey FOREIGN KEY (bibliotheek_kenmerken_id) REFERENCES bibliotheek_kenmerken(id);

ALTER TABLE zaak_kenmerk
	ADD CONSTRAINT zaak_kenmerk_zaak_id_fkey FOREIGN KEY (zaak_id) REFERENCES zaak(id);
