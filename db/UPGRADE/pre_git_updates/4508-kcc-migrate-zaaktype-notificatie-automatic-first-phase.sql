-- update all zaaktype_notificatie records for the first phase to make the checkbox 'Versturen bij faseovergang' enabled.

select id, automatic from zaaktype_notificatie where zaak_status_id in (select id from zaaktype_status where status = 1);
begin; update zaaktype_notificatie set automatic = 1 where zaak_status_id in (select id from zaaktype_status where status = 1);
commit;
