ALTER TABLE logging ADD COLUMN created_by character varying(64);
ALTER TABLE logging ADD COLUMN modified_by character varying(64);
ALTER TABLE logging ADD COLUMN deleted_by character varying(64);
ALTER TABLE logging ADD COLUMN created_for character varying(64);
