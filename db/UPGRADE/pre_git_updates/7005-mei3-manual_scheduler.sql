BEGIN;

ALTER TABLE scheduled_jobs ADD COLUMN schedule_type VARCHAR(16) CHECK (
    schedule_type ~ '(manual|time)'
);

ALTER TABLE scheduled_jobs ALTER COLUMN scheduled_for DROP NOT NULL;

COMMIT;
