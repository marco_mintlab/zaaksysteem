BEGIN;

DROP TABLE email_file;
DROP TABLE zaaktype_notificatie_kenmerk;

CREATE TABLE bibliotheek_notificatie_kenmerk (
    id SERIAL PRIMARY KEY,
    bibliotheek_notificatie_id INTEGER NOT NULL REFERENCES bibliotheek_notificaties(id),
    bibliotheek_kenmerken_id INTEGER NOT NULL REFERENCES bibliotheek_kenmerken(id)
);

COMMIT;

