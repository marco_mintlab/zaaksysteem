BEGIN;

ALTER TABLE file ADD COLUMN search_index tsvector;
ALTER TABLE file ADD COLUMN search_term text;
ALTER TABLE file ADD COLUMN object_type character varying(100) DEFAULT 'file'::character varying;
ALTER TABLE file ADD COLUMN searchable_id integer;
-- These will get regenerated after the INHERIT succeeds
UPDATE file SET searchable_id = (SELECT id FROM searchable LIMIT 1);
ALTER TABLE file ALTER COLUMN searchable_id SET NOT NULL;
ALTER TABLE file INHERIT searchable;

ALTER TABLE ONLY file ALTER COLUMN searchable_id SET DEFAULT nextval('searchable_searchable_id_seq'::regclass);

COMMIT;
