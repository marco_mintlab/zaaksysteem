#! perl

### Test header start
use warnings;
use strict;

use lib 't/inc';
use TestSetup;

my $zs = bless {schema => connect_test_db_ok()}, 'Zaaksysteem::TestUtils';
my $schema = $zs->schema;
### Test header end

use Cwd;
use File::Basename;
use Zaaksysteem::Log::CallingLogger;

BEGIN { use_ok('Zaaksysteem::SBUS'); }

my $log         = Zaaksysteem::Log::CallingLogger->new;
my $sbus        = Zaaksysteem::SBUS->new(
    config  => {
        home        => getcwd,
        default_ontvanger => "CMG",
        default_zender    => "ZSNL",
        test_transport    => 1,
    },
    log             => $log,
    schema          => $schema,
    die_on_error    => 1,
);

SKIP: {
    skip 'Broken tests', 20;

    $zs->zs_transaction_ok(sub {
        $sbus->fake_transport(sub {
            my $xml = shift;

            ### Check valid XML
            return 'TESTFAIL' unless (
                $xml =~ /stuurgegevens>/ &&
                $xml =~ /berichtsoort>Lv01/ &&
                $xml =~ /sectormodel>BG/ &&
                $xml =~ /versieStUF>0204/ &&
                $xml =~ /versieSectormodel>0204/ &&
                $xml =~ /tijdstipBericht>\d{16}</
            );

            return 'TESTOK';
        });

        throws_ok(
            sub {
                $sbus->request(
                    {
                        operation   => 'search',
                        sbus_type   => 'StUF',
                        object      => 'PRS',
                        input       => {
                            'bsn-nummer'   => 12345
                        },
                    }
                );
            },
            qr/TESTOK/,
            'Valid stuurgegevens'
        );

        $sbus->fake_transport(sub {
            my $xml = shift;

            ### Check valid XML
            return 'TESTFAIL' unless (
                $xml =~ /entiteittype>PRS/
            );

            return 'TESTOK';
        });

        throws_ok(
            sub {
                $sbus->request(
                    {
                        operation   => 'search',
                        sbus_type   => 'StUF',
                        object      => 'PRS',
                        input       => {
                            'bsn-nummer'   => 12345
                        },
                    }
                );
            },
            qr/TESTOK/,
            'Found entiteittype PRS'
        );


        $sbus->fake_transport(sub {
            my $xml = shift;

            ### Check valid XML
            return 'TESTFAIL' unless (
                $xml =~ /bsn-nummer>12345/
            );

            return 'TESTOK';
        });

        throws_ok(
            sub {
                $sbus->request(
                    {
                        operation   => 'search',
                        sbus_type   => 'StUF',
                        object      => 'PRS',
                        input       => {
                            'bsn-nummer'   => 12345
                        },
                    }
                );
            },
            qr/TESTOK/,
            'Found searchquery'
        );
    }, 'Request a StUF call');


    $zs->zs_transaction_ok(sub {
        $sbus->fake_transport(sub {
            my $xml = shift;

            ### Check valid XML
            return 'TESTFAIL' unless (
                $xml =~ /indicatorOvername>I</ &&
                $xml =~ /berichtsoort>Lk01</ &&
                $xml =~ /mutatiesoort>T</
            );

            return 'TESTOK';
        });

        throws_ok(
            sub {
                $sbus->request(
                    {
                        operation   => 'kennisgeving',
                        sbus_type   => 'StUF',
                        object      => 'PRS',
                        input       => {
                            'afnemerIndicatie' => 'I',
                        },
                    }
                );
            },
            qr/only possible on sleutelGegevensbeheer/,
            'No afnemerIndicatie when no sleutelGegevensbeheer is given'
        );

        throws_ok(
            sub {
                $sbus->request(
                    {
                        operation   => 'kennisgeving',
                        sbus_type   => 'StUF',
                        object      => 'PRS',
                        input       => {
                            'sleutelGegevensbeheer'     => 12342829323,
                            'afnemerIndicatie'          => 'I',
                        },
                    }
                );
            },
            qr/TESTOK/,
            'Found indicatorAfnemerIndicatie'
        );
    }, 'Request afnemerIndicatie');


    $zs->zs_transaction_ok(sub {
        $sbus->fake_transport(sub {
            my $xml = shift;

            ### Check valid XML
            return 'TESTFAIL' unless (
                $xml =~ /indicatorOvername>I</ &&
                $xml =~ /berichtsoort>Lk01</ &&
                $xml =~ /mutatiesoort>V</
            );

            return 'TESTOK';
        });

        throws_ok(
            sub {
                $sbus->request(
                    {
                        operation   => 'kennisgeving',
                        sbus_type   => 'StUF',
                        object      => 'PRS',
                        input       => {
                            'sleutelGegevensbeheer'     => 12342829323,
                            'afnemerIndicatie'  => 0,
                        },
                    }
                );
            },
            qr/TESTOK/,
            'Removed indicatorAfnemerIndicatie'
        );
    }, 'Request, then remove afnemerIndicatie');
}

zs_done_testing();
