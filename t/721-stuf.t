#! perl

### Test header start
use warnings;
use strict;

use lib 't/inc';
use JSON;

use TestSetup;
use Test::Deep;

my $zs              = bless {schema => connect_test_db_ok()}, 'Zaaksysteem::TestUtils';
my $schema          = $zs->schema;

BEGIN { use_ok('Zaaksysteem::StUF') };
BEGIN { use_ok('Zaaksysteem::StUF::Stuurgegevens') };

$zs->zs_transaction_ok(sub {
    my $stuf    = Zaaksysteem::StUF->from_file(
        'share/stuf/prs/101-prs-create-tinus.xml',
    );

    ok($stuf->stuurgegevens, 'Stuurgegevens set');
    ok($stuf->body, 'Body set');
    is($stuf->stuurgegevens->entiteittype, 'PRS', 'Found correct entiteittype');
    is($stuf->stuurgegevens->berichtsoort, 'Lk01', 'Found correct berichtsoort');
    is($stuf->stuurgegevens->sectormodel, 'BG', 'Found correct sectormodel');
    is($stuf->stuurgegevens->versieStUF, '0204', 'Found correct StUF versie');
    is($stuf->stuurgegevens->versieSectormodel, '0204', 'Found correct sectormodel versie');

    ok($stuf->body->[0]->is_active, 'active entry');

    #note(explain($stuf->parser->data));
    #note(explain($stuf->TO_STUF));
    #note(explain($stuf->to_xml));
    #note(explain($stuf->as_params));
    
}, 'Parsed StUF Message');


$zs->zs_transaction_ok(sub {
    my $stuf    = Zaaksysteem::StUF->from_file(
        'share/stuf/prs/101-prs-create-tinus.xml',
    );

    ok($stuf->stuurgegevens, 'Stuurgegevens set');
    my $ack = $stuf->acknowledge(
        {
            date            => DateTime->now(),
            reference_id    => '987654321',
        }
    );
    ok($ack);
    note(explain($ack->to_xml));


    #note(explain($stuf->parser->data));
    #note(explain($stuf->TO_STUF));
    #note(explain($stuf->to_xml));
    #note(explain($stuf->as_params));
    
}, 'Generated acknowledgement (Bevestiging)');

# $zs->zs_transaction_ok(sub {
#     my $stuf    = Zaaksysteem::StUF->new(
#         entiteittype        => 'FOUT'
#     );

#     $stuf->stuurgegevens(
#         Zaaksysteem::StUF::Stuurgegevens->new(
#             referentienummer    => '82983297729',
#             tijdstipBericht     => DateTime->now()->strftime('%Y%m%d%H%M%S00'),
#             berichtsoort        => 'Fo01',
#             zender              => {
#                 applicatie          => 'ZSNL',
#             },
#             ontvanger           => {
#                 applicatie          => 'CGM',
#             },
#             entiteittype        => 'PRS',
#             kennisgeving        => {
#                 mutatiesoort        => 'T',
#             }
#         )
#     );

#     $stuf->body({
#         code            => 'StUF011',
#         plek            => 'server',
#         omschrijving    => 'Test error'
#     });

#     $stuf->element('foutBericht');
#     $stuf->namespace('http://www.egem.nl/StUF/StUF0204');

#     #like($stuf->to_xml, qr/xml version/, 'Created XML message');
#     #note(explain($stuf->TO_STUF));
# }, 'Tested foutbericht');

zs_done_testing;