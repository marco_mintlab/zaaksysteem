#! perl

### Test header start
use warnings;
use strict;

use lib 't/inc';
use TestSetup;

my $zs = bless {schema => connect_test_db_ok()}, 'Zaaksysteem::TestUtils';
my $schema = $zs->schema;
### Test header end

$zs->zs_transaction_ok(sub {
	note 'Example';
}, 'Test description');

zs_done_testing();
