#! perl

### Test header start
use warnings;
use strict;

use lib 't/inc';
use TestSetup;

my $zs = bless {schema => connect_test_db_ok()}, 'Zaaksysteem::TestUtils';
my $schema = $zs->schema;
### Test header end

use Cwd;
use File::Basename;
use Zaaksysteem::Log::CallingLogger;

BEGIN { use_ok('Zaaksysteem::SBUS'); }
my $log         = Zaaksysteem::Log::CallingLogger->new;
my $currentdir  = getcwd();
my $sbus        = Zaaksysteem::SBUS->new(
    config  => {
        home                => getcwd,
        gemeentecode        => 363, # Amsterdam
        default_ontvanger   => "CMG",
        default_zender      => "ZSNL",
        test_transport      => 1,
    },
    log             => $log,
    schema          => $schema,
    die_on_error    => 1,
);

SKIP: {
    skip 'SKIP testing', 58 if (
        defined($ENV{TEST_SBUS_ONLY}) &&
        $ENV{TEST_SBUS_ONLY} &&
        $ENV{TEST_SBUS_ONLY} ne 'PRS'
    );

    $zs->zs_transaction_ok(sub {
        my $current_num_person  = $schema
                                ->resultset('NatuurlijkPersoon')
                                ->search()
                                ->count;

        my $checks  = {
            NatuurlijkPersoon   => {
                burgerservicenummer => 987654321,
                a_nummer            => 1234567890,
                voornamen           => 'Tinues',
                voorletters         => 'V',
                geslachtsnaam       => 'Testpersoon',
                geboortedatum       => '1962-05-29',
                geslachtsaanduiding => 'M',
                system_of_record_id => '1243796',
            },
            'Adres'             => {
                postcode            => '1015JL',
                woonplaats          => 'Amsterdam',
                straatnaam          => 'Donker Curtiusstraat',
                huisnummer          => '7',
                functie_adres       => 'W',
                gemeente_code       => 363
            }
        };

        my $xml = get_xml_from_file(
            $currentdir .
            '/t/inc/API/StUF/in-prs-start.xml'
        );

        $sbus->response(
            {
                operation   => 'kennisgeving',
                sbus_type   => 'StUF',
                input_raw   => $xml
            }
        );

        # Get last added person.
        ok (
            (
                $schema->resultset('NatuurlijkPersoon')->search()->count >
                $current_num_person
            ), 'Added citizen'
        );

        check_tables_ok($schema, $checks);

    
    }, 'PRS: Add citizen');

    $zs->zs_transaction_ok(sub {
        ### Add citizen to work with
        $sbus->response(
            {
                operation   => 'kennisgeving',
                sbus_type   => 'StUF',
                object      => 'PRS',
                input_raw   =>  get_xml_from_file(
                    $currentdir .
                    '/t/inc/API/StUF/in-prs-start.xml'
                )
            }
        );
        my $checks  = {
            NatuurlijkPersoon   => {
                burgerservicenummer => 987654321,
                a_nummer            => 1234567890,
                voornamen           => 'Tinus',
                voorletters         => 'M',
                geslachtsnaam       => 'Testmeisje',
                geboortedatum       => '1962-05-30',
                geslachtsaanduiding => 'V',
            },
            'Adres'             => {
                postcode            => '1015JL',
                woonplaats          => 'Amsterdam',
                straatnaam          => 'Donker Curtiusstraat',
                huisnummer          => '7',
                functie_adres       => 'W',
            }
        };

        my $xml = get_xml_from_file(
            $currentdir .
            '/t/inc/API/StUF/in-prs-mutate-prs.xml'
        );

        $sbus->response(
            {
                operation   => 'kennisgeving',
                sbus_type   => 'StUF',
                object      => 'PRS',
                input_raw   => $xml
            }
        );


        check_tables_ok($schema, $checks);
    
    }, 'PRS: Mutate citizen');


    $zs->zs_transaction_ok(sub {
        ### Add citizen to work with
        $sbus->response(
            {
                operation   => 'kennisgeving',
                sbus_type   => 'StUF',
                object      => 'PRS',
                input_raw   =>  get_xml_from_file(
                    $currentdir .
                    '/t/inc/API/StUF/in-prs-start.xml'
                )
            }
        );

        my $checks  = {
            NatuurlijkPersoon   => {
                burgerservicenummer => 987654321,
                geslachtsnaam       => 'Testpersoon',
                geboortedatum       => '1962-05-29',
                datum_overlijden    => '2012-04-01',
            },
            'Adres'             => {
                postcode            => '1015JL',
                woonplaats          => 'Amsterdam',
                straatnaam          => 'Donker Curtiusstraat',
                huisnummer          => '7',
                functie_adres       => 'W',
            }
        };

        $sbus->response(
            {
                operation   => 'kennisgeving',
                sbus_type   => 'StUF',
                object      => 'PRS',
                input_raw   =>  get_xml_from_file(
                    $currentdir .
                    '/t/inc/API/StUF/in-prs-mutate-deceased.xml'
                )
            }
        );

        check_tables_ok($schema, $checks);

        my $row = $schema->resultset('NatuurlijkPersoon')->search(
            {},
            {
                order_by => { '-desc' => 'id' }
            }
        )->first;

        ok($row->datum_overlijden, 'Found deceased date');
        ok($row->deleted_on, 'Citizen has been removed');

    
    }, 'PRS: Decease existing citizen');

    
    $zs->zs_transaction_ok(sub {
        ### Add citizen to work with
        $sbus->response(
            {
                operation   => 'kennisgeving',
                sbus_type   => 'StUF',
                object      => 'PRS',
                input_raw   =>  get_xml_from_file(
                    $currentdir .
                    '/t/inc/API/StUF/in-prs-start.xml'
                )
            }
        );

        my $checks  = {
            NatuurlijkPersoon   => {
                burgerservicenummer => 987654321,
                geslachtsnaam       => 'Testpersoon',
                geboortedatum       => '1962-05-29',
            },
            'Adres'             => {
                postcode            => '1051JL',
                woonplaats          => 'Amsterdam',
                straatnaam          => 'Donker Curtiusstraat',
                huisnummer          => '6',
                functie_adres       => 'W',
            }
        };

        ### Move this citizen
        $sbus->response(
            {
                operation   => 'kennisgeving',
                sbus_type   => 'StUF',
                object      => 'PRS',
                input_raw   =>  get_xml_from_file(
                    $currentdir .
                    '/t/inc/API/StUF/in-prs-mutate-move.xml'
                )
            }
        );

        check_tables_ok($schema, $checks);

        my $row = $schema->resultset('NatuurlijkPersoon')->search(
            {},
            {
                order_by => { '-desc' => 'id' }
            }
        )->first;

        ok(!$row->deleted_on, 'Citizen still in municipality');

    
    }, 'PRS: Move existing citizen');

    
    $zs->zs_transaction_ok(sub {
        ### Add citizen to work with
        $sbus->response(
            {
                operation   => 'kennisgeving',
                sbus_type   => 'StUF',
                object      => 'PRS',
                input_raw   =>  get_xml_from_file(
                    $currentdir .
                    '/t/inc/API/StUF/in-prs-start.xml'
                )
            }
        );

        my $checks  = {
            NatuurlijkPersoon   => {
                burgerservicenummer => 987654321,
                geslachtsnaam       => 'Testpersoon',
                geboortedatum       => '1962-05-29',
            },
            'Adres'             => {
                postcode            => '3437AR',
                huisnummer          => '47',
            }
        };

        ### Move this citizen
        $sbus->response(
            {
                operation   => 'kennisgeving',
                sbus_type   => 'StUF',
                object      => 'PRS',
                input_raw   =>  get_xml_from_file(
                    $currentdir .
                    '/t/inc/API/StUF/in-prs-mutate-move_other_municipality.xml'
                )
            }
        );

        check_tables_ok($schema, $checks);

        my $row = $schema->resultset('NatuurlijkPersoon')->search(
            {},
            {
                order_by => { '-desc' => 'id' }
            }
        )->first;

        ok($row->deleted_on, 'Citizen moved to other municipality');

    
    }, 'PRS: Move existing citizen to other municipality');

    
    $zs->zs_transaction_ok(sub {
        ### Add citizen to work with
        $sbus->response(
            {
                operation   => 'kennisgeving',
                sbus_type   => 'StUF',
                object      => 'PRS',
                input_raw   =>  get_xml_from_file(
                    $currentdir .
                    '/t/inc/API/StUF/in-prs-start.xml'
                )
            }
        );

        my $checks  = {
            NatuurlijkPersoon   => {
                burgerservicenummer => 987654321,
                geslachtsnaam       => 'Testpersoon',
                geboortedatum       => '1962-05-29',
            },
        };

        ### Move this citizen
        $sbus->response(
            {
                operation   => 'kennisgeving',
                sbus_type   => 'StUF',
                object      => 'PRS',
                input_raw   =>  get_xml_from_file(
                    $currentdir .
                    '/t/inc/API/StUF/in-prs-mutate-emigrate.xml'
                )
            }
        );

        check_tables_ok($schema, $checks);

        my $row = $schema->resultset('NatuurlijkPersoon')->search(
            {},
            {
                order_by => { '-desc' => 'id' }
            }
        )->first;

        ok($row->deleted_on, 'Citizen emigrated');    
    }, 'PRS: Emigrate existing citizen');
}

###
### NNP COMMUNICATION
###
SKIP: {
    skip 'SKIP testing', 26 if (
        defined($ENV{TEST_SBUS_ONLY}) &&
        $ENV{TEST_SBUS_ONLY} &&
        $ENV{TEST_SBUS_ONLY} ne 'NNP'
    );

    $zs->zs_transaction_ok(sub {
        my $current_num_bedrijf  = $schema
                                ->resultset('Bedrijf')
                                ->search()
                                ->count;

        my $checks  = {
            Bedrijf   => {
                dossiernummer               => 51902672,
                subdossiernummer            => '0000',
                handelsnaam                 => 'Mintlab B.V.',
                vestiging_straatnaam        => 'Donker Curtiusstraat',
                vestiging_huisnummer        => '7',
                vestiging_huisnummertoevoeging => '522',
                vestiging_postcode          => '1051JL',
                vestiging_woonplaats        => 'Amsterdam',
                system_of_record_id         => '1126832',
            },
        };

        $sbus->response(
            {
                operation   => 'kennisgeving',
                sbus_type   => 'StUF',
                object      => 'NNP',
                input_raw   => get_xml_from_file(
                    $currentdir .
                    '/t/inc/API/StUF/in-nnp-start.xml'
                )
            }
        );

        # Get last added bedrijf
        ok (
            (
                $schema->resultset('Bedrijf')->search()->count >
                $current_num_bedrijf
            ), 'Added company'
        );

        check_tables_ok($schema, $checks);

    
    }, 'PRS: Add NNP');


    $zs->zs_transaction_ok(sub {
        ### Add previous NNP
        $sbus->response(
            {
                operation   => 'kennisgeving',
                sbus_type   => 'StUF',
                object      => 'NNP',
                input_raw   => get_xml_from_file(
                    $currentdir .
                    '/t/inc/API/StUF/in-nnp-start.xml'
                )
            }
        );

        my $checks  = {
            Bedrijf   => {
                dossiernummer               => 51902672,
                subdossiernummer            => '0002',
                handelsnaam                 => 'Freshlab B.V. zaak',
                vestiging_straatnaam        => 'Donker Curtiusstraatt',
                vestiging_huisnummer        => '6',
                vestiging_huisnummertoevoeging => '521',
                vestiging_postcode          => '1015XK',
                vestiging_woonplaats        => 'Amsterdam',
            },
        };

        $sbus->response(
            {
                operation   => 'kennisgeving',
                sbus_type   => 'StUF',
                object      => 'NNP',
                input_raw   => get_xml_from_file(
                    $currentdir .
                    '/t/inc/API/StUF/in-nnp-mutate-nnp.xml'
                )
            }
        );

        check_tables_ok($schema, $checks);    
    }, 'PRS: Mutate NNP');


    $zs->zs_transaction_ok(sub {
        ### Add previous NNP
        $sbus->response(
            {
                operation   => 'kennisgeving',
                sbus_type   => 'StUF',
                object      => 'NNP',
                input_raw   => get_xml_from_file(
                    $currentdir .
                    '/t/inc/API/StUF/in-nnp-start.xml'
                )
            }
        );

        my $checks  = {
            Bedrijf   => {
                dossiernummer               => 51902672,
                subdossiernummer            => '0000',
                handelsnaam                 => 'Mintlab B.V.',
                vestiging_straatnaam        => 'Donker Curtiusstraatt',
                vestiging_huisnummer        => '6',
                vestiging_huisnummertoevoeging => '521',
                vestiging_postcode          => '1015XK',
                vestiging_woonplaats        => 'Amsterdam',
            },
        };

        $sbus->response(
            {
                operation   => 'kennisgeving',
                sbus_type   => 'StUF',
                object      => 'NNP',
                input_raw   => get_xml_from_file(
                    $currentdir .
                    '/t/inc/API/StUF/in-nnp-mutate-move.xml'
                )
            }
        );

        check_tables_ok($schema, $checks);
    }, 'PRS: Move NNP internal');
};

###
### ADR COMMUNICATION
###
SKIP: {
    skip 'SKIP testing', 26 if (
        defined($ENV{TEST_SBUS_ONLY}) &&
        $ENV{TEST_SBUS_ONLY} &&
        $ENV{TEST_SBUS_ONLY} ne 'BAG'
    );

    $zs->zs_transaction_ok(sub {
        my $current_num_bag     = $schema
                                ->resultset('BagNummeraanduiding')
                                ->search()
                                ->count;

        my $checks  = {
            BagWoonplaats       => {
                naam            => 'Amsterdam',
                identificatie   => '1234',
            },
            BagOpenbareruimte   => {
                identificatie   => '0620300000000001',
                naam            => 'Donker Curtiusstraat',
            },
            BagNummeraanduiding => {
                identificatie   => '0620200000000001',
                huisnummer      => '7',
                postcode        => '1051JL',
                huisnummertoevoeging => '414',
                status          => 'Naamgeving uitgegeven',
            }
        };

        $sbus->response(
            {
                operation   => 'kennisgeving',
                sbus_type   => 'StUF',
                object      => 'ADR',
                input_raw   => get_xml_from_file(
                    $currentdir .
                    '/t/inc/API/StUF/in-adr-start.xml'
                )
            }
        );

        # Get last added adr
        ok (
            (
                $schema ->resultset('BagNummeraanduiding')
                        ->search()
                        ->count > $current_num_bag
            ), 'Added adr'
        );

        check_tables_ok($schema, $checks, { primary => 'identificatie' });

    
    }, 'BAG: Add BAG_NUMMERAANDUIDING');

    
    $zs->zs_transaction_ok(sub {
        $sbus->response(
            {
                operation   => 'kennisgeving',
                sbus_type   => 'StUF',
                object      => 'ADR',
                input_raw   => get_xml_from_file(
                    $currentdir .
                    '/t/inc/API/StUF/in-adr-start.xml'
                )
            }
        );

        my $current_num_bag     = $schema
                                ->resultset('BagNummeraanduiding')
                                ->search()
                                ->count;

        my $checks  = {
            BagNummeraanduiding => {
                identificatie   => '0620200000000001',
                status          => 'Naamgeving opgelost',
            }
        };

        $sbus->response(
            {
                operation   => 'kennisgeving',
                sbus_type   => 'StUF',
                object      => 'ADR',
                input_raw   => get_xml_from_file(
                    $currentdir .
                    '/t/inc/API/StUF/in-adr-mutate-adr.xml'
                )
            }
        );

        check_tables_ok($schema, $checks, { primary => 'identificatie' });

    
    }, 'BAG: Mutate BAG_NUMMERAANDUIDING');

    
    $zs->zs_transaction_ok(sub {
        my $current_num_bag     = $schema
                                ->resultset('BagVerblijfsobject')
                                ->search()
                                ->count;

        my $checks  = {
            BagVerblijfsobject       => {
                status          => 'Verblijfsobject in gebruik',
            },
            BagNummeraanduiding => {
                identificatie   => '0620200000000001',
                huisnummer      => '7',
                postcode        => '1051JL',
                huisnummertoevoeging => '414',
                status          => 'Naamgeving uitgegeven',
            },
            BagVerblijfsobjectGebruiksdoel => {
                gebruiksdoel    => 'woonfunctie',
            },
        };

        $sbus->response(
            {
                operation   => 'kennisgeving',
                sbus_type   => 'StUF',
                object      => 'VBO',
                input_raw   => get_xml_from_file(
                    $currentdir .
                    '/t/inc/API/StUF/in-vbo-start.xml'
                )
            }
        );

        # Get last added adr
        ok (
            (
                $schema ->resultset('BagVerblijfsobject')
                        ->search()
                        ->count > $current_num_bag
            ), 'Added vbo'
        );

        check_tables_ok($schema, $checks, { primary => 'identificatie' });

    
    }, 'BAG: Add VBO');

    
    $zs->zs_transaction_ok(sub {
        $sbus->response(
            {
                operation   => 'kennisgeving',
                sbus_type   => 'StUF',
                object      => 'VBO',
                input_raw   => get_xml_from_file(
                    $currentdir .
                    '/t/inc/API/StUF/in-vbo-start.xml'
                )
            }
        );

        is(
            $schema->resultset('BagVerblijfsobject')->count,
            1,
            'Got a single verblijfsobject'
        );
        is(
            $schema->resultset('BagVerblijfsobjectGebruiksdoel')->count,
            1,
            'Got a single verblijfsobject_gebruiksdoel'
        );

        my $checks  = {
            BagVerblijfsobject       => {
                status          => 'Verblijfsobject ingetrokken',
            },
            BagNummeraanduiding => {
                identificatie   => '0620200000000001',
                huisnummer      => '7',
                postcode        => '1051JL',
                huisnummertoevoeging => '414',
                status          => 'Naamgeving uitgegeven',
            },
            BagVerblijfsobjectGebruiksdoel => {
                gebruiksdoel    => 'bijeenkomstfunctie',
            },
        };

        $sbus->response(
            {
                operation   => 'kennisgeving',
                sbus_type   => 'StUF',
                object      => 'VBO',
                input_raw   => get_xml_from_file(
                    $currentdir .
                    '/t/inc/API/StUF/in-vbo-mutate-vbo.xml'
                )
            }
        );

        is(
            $schema->resultset('BagVerblijfsobject')->count,
            1,
            'Got still a single verblijfsobject'
        );

        is(
            $schema->resultset('BagVerblijfsobjectGebruiksdoel')->count,
            1,
            'Got still a single verblijfsobject_gebruiksdoel'
        );

        check_tables_ok($schema, $checks, { primary => 'identificatie' });

    
    }, 'BAG: Mutate VBO');
}

zs_done_testing();

sub check_tables_ok {
    my ($schema, $checks, $options) = @_;
    $options ||= {};

    for my $check (keys %{ $checks }) {
        my $attrs   = $checks->{ $check };

        ### Get last row
        my $row = $schema->resultset($check)->search(
            {},
            {
                order_by => { '-desc' => ($options->{primary} || 'id') }
            }
        )->first;

        ok($row, 'Found matching row to: ' . $check);
        next unless $row;

        for my $attr (keys %{ $attrs }) {
            my $dbval = $row->$attr;

            if (UNIVERSAL::isa($dbval, 'DateTime')) {
                $dbval = $dbval->ymd
            }

            is(
                $dbval,
                $attrs->{$attr},
                'Correct col: ' . $check . '::' . $attr
            );
        }
    }
}

sub get_xml_from_file {
    my $filename    = shift;
    my $rv          = '';

    open(my $fh, "<:encoding(UTF-8)", $filename) or die('Cannot open file');

    while (<$fh>) {
        $rv .= $_;
    }

    close($fh);

    return $rv;
}
