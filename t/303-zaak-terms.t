#! perl

### Test header start
use warnings;
use strict;

use lib 't/inc';
use TestSetup;

my $zs = bless {schema => connect_test_db_ok()}, 'Zaaksysteem::TestUtils';
my $schema = $zs->schema;
### Test header end


$zs->zs_transaction_ok(sub {
    ### Prepare case by setting a new date
    ###
    my $case    = $zs->get_case_ok;

    my $def     = $case->zaaktype_node_id->zaaktype_definitie_id;
    $def->servicenorm(5);
    $def->servicenorm_type('kalenderdagen');
    $def->afhandeltermijn(10);
    $def->afhandeltermijn_type('kalenderdagen');
    $def->update;

    ### Bootstrap the dates again for this case
    $case->_bootstrap_datums(
        {
            registratiedatum => DateTime->now()->subtract(days => 1)
        }
    );
    $case->update;

    ### Re-read case with search_extended to get days_left and days_perc
    ###
    my $cases   = $schema->resultset('Zaak')->search({'me.id' => $case->id});
    $case       = $cases->first;

    is($case->days_left, 3, 'Got correct days left');
    is($case->days_perc, 20, 'Got correct days percentage');
}, 'Case: correct time on bootstrap. Set on 5 days.');


zs_done_testing();
