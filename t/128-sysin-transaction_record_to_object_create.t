#! perl

### Test header start
use warnings;
use strict;

use lib 't/inc';

use TestSetup;

my $zs = bless {schema => connect_test_db_ok}, 'Zaaksysteem::TestUtils';
my $schema = $zs->schema;
### Test header end

$zs->zs_transaction_ok(sub {
    ok $schema->resultset('TransactionRecordToObject')->transaction_record_to_object_create({
        transaction_record_id => $zs->create_transaction_record_ok->id,
        local_table   => 'Zaak',
        local_id      => $zs->get_case_ok->id,
    }), 'Created TransactionRecordToObject';

}, 'transaction_record_to_object_create');


$zs->zs_transaction_ok(sub {
    throws_ok sub {
        $schema->resultset('TransactionRecordToObject')->transaction_record_to_object_create({
            transaction_record_id => $zs->create_transaction_record_ok->id,
            local_table => 'Zaak',
            local_id    => 1234567,
        });
    }, qr/Record with ID 1234567 in class 'Zaak' not found/, 'Create failed';

}, 'transaction_record_to_object_create fails with non-existing entry');

$zs->zs_transaction_ok(sub {
    my @mutation_types = ('create', 'update', 'delete');

    for my $mt (@mutation_types) {
        ok $schema->resultset('TransactionRecordToObject')->transaction_record_to_object_create({
                transaction_record_id => $zs->create_transaction_record_ok->id,
                local_table => 'Zaak',
                local_id    => $zs->get_case_ok->id,
                mutation_type => 'create',
        }), "$mt succeeds";
    }

}, 'transaction_record_to_object_create mutation types');

$zs->zs_transaction_ok(sub {
    throws_ok sub {
        $schema->resultset('TransactionRecordToObject')->transaction_record_to_object_create({
            transaction_record_id => $zs->create_transaction_record_ok->id,
            local_table => 'Zaak',
            local_id    => $zs->get_case_ok->id,
            mutation_type => 'wharharblblrlhbl',
        });
    }, qr/Invalid: mutation_type/, 'Invalid mutation_type failed';
}, 'transaction_record_to_object_create fails with invalid mutation_type');

zs_done_testing;