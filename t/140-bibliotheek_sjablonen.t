#! perl

### Test header start
use warnings;
use strict;

use lib 't/inc';
use TestSetup;

my $zs = bless {schema => connect_test_db_ok()}, 'Zaaksysteem::TestUtils';
my $schema = $zs->schema;
### Test header end

TODO: {
    local $TODO = 'Currently there is no sjabloon static data in the test suite.';
    note 'Need sjabloon tests';
}

zs_done_testing();