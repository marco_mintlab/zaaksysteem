/*global module*/
module.exports = function ( grunt ) {
	
	grunt.initConfig({
		hub: {
			dev: {
				src: [ '*/Gruntfile.js' ]
			},
			build: {
				src: [ '*/Gruntfile.js' ],
				tasks: [ 'build' ]
			}
		}
	});
	
	grunt.loadNpmTasks('grunt-hub');
	
	grunt.registerTask('default', [ 'hub:dev' ]);
	grunt.registerTask('build', [ 'hub:build' ]);
	
};