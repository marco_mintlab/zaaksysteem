<form data-ng-controller="nl.mintlab.core.form.FormController">
	
	<fieldset class="form-fieldset form-fieldset-name-<[fieldset.name]>" data-ng-repeat="fieldset in zsForm.fieldsets">
		<div class="form-fieldset-meta" data-ng-show="fieldset.title||fieldset.description">
			<div class="form-fieldset-header" data-ng-show="fieldset.title">
				<[fieldset.title]>
			</div>
			<div class="form-fieldset-desc" data-ng-show="fieldset.description" data-zs-template="<[fieldset.description]>">
			</div>		
		</div>
		<div class="form-fieldset-children">
			<div class="form-field form-field-name-<[field.name]>" data-ng-repeat="field in fieldset.fields | filter:showField:field" data-ng-include="'/html/form/form-field.html'" data-ng-class="{'form-field-required': getRequired(field), 'form-field-valid': isValid(field), 'form-field-invalid': !isValid(field) }">
			</div>
		</div>
		<div class="form-fieldset-actions" data-ng-show="hasVisibleActions(fieldset.actions)">
			<button class="form-action form-action-<[action.name]>" data-ng-repeat="action in fieldset.actions" data-ng-click="action.type=='popup'&&openPopup()||handleActionClick(action, $event)" data-ng-disabled="isActionDisabled(action)" type="<[action.type=='submit'&&'submit'||'button']>" data-zs-popup="action.type=='popup'&&'/html/form/form-modal.html'||''" data-ng-class="{ 'form-button-primary': action.importance != 'secondary', 'form-button-secondary': action.importance == 'secondary' }" data-ng-show="isVisible(action)">
				<[action.label]>
			</button>
		</div>
	</fieldset>
	
	<div class="form-field-button-list" data-ng-show="hasVisibleActions(zsForm.actions)">
		<button class="form-action form-action-<[action.name]>" data-ng-repeat="action in zsForm.actions" data-ng-click="action.type=='popup'&&openPopup()||handleActionClick(action, $event)" data-ng-disabled="isActionDisabled(action)" type="<[action.type=='submit'&&'submit'||'button']>" data-zs-popup="action.type=='popup'&&'/html/form/form-modal.html'||''" data-ng-class="{ 'form-button-primary': action.importance != 'secondary', 'form-button-secondary': action.importance == 'secondary' }" data-ng-show="isVisible(action)">
			<[action.label]>
		</button>
	</div>
	
	<div class="form-status" data-ng-class="{ 'form-status-submitting': submitting }">
		<div class="form-status-indicator"></div>
		<div class="form-status-last-saved" data-ng-show="lastSaved">Opgeslagen om <[lastSaved|date:'HH:mm']></div>
	</div>
	
</form>

<script type="text/ng-template" id="/html/form/form-field.html">
	<div class="form-field-label">
		<label for="<[getFieldId(field)]>">
			<[field.label]>
			<span class="form-field-required-indicator" data-ng-show="getRequired(field)"> (*)</span>
		</label>
	</div>
	
	<div class="form-field-input" data-ng-include="'/html/form/form-field-type-' + field.type + '.html'">
	
	</div>
	
	<div class="form-field-revert" data-ng-show="field.default!=undefined&&field.default!=null">
		<button type="button" class="form-field-revert-button" data-ng-click="revertField(field)">
			%%Herstel%%
		</button>
	</div>
	
	<div class="form-field-description" data-ng-show="field.description" tabindex="0">
		<div class="form-field-description-icon icon-font-awesome icon-question-sign"></div>
		<div class="form-field-description-message"  data-zs-template="<[field.description]>"></div>
	</div>
	
	<div class="form-field-status" data-ng-class="{'form-field-status-valid': isValid(field), 'form-field-status-invalid': !isValid(field) }">
		<div class="form-field-status-indicator"></div>
		<div class="form-field-status-message"><[field.statusMessage]></div>
	</div>
</script>

<script type="text/ng-template" id="/html/form/form-field-type-text.html">
	<input type="text" name="<[field.name]>" data-zs-form-field data-ng-model="scope[field.name]" data-ng-required="getRequired(field)" data-ng-pattern="<[field.data.pattern]>" id="<[getFieldId(field)]>" data-zs-placeholder="field.data.placeholder||''"/>
</script>

<script type="text/ng-template" id="/html/form/form-field-type-url.html">
<input type="url" name="<[field.name]>" data-zs-form-field data-ng-model="scope[field.name]" data-ng-required="getRequired(field)" id="<[getFieldId(field)]>" data-zs-placeholder="field.data.placeholder||''"/>
</script>

<script type="text/ng-template" id="/html/form/form-field-type-radio.html">
	<label data-ng-repeat="option in getOptions(field)">
		<input type="radio" name="<[field.name]>" data-zs-form-field data-ng-model="scope[field.name]" value="<[option.value]>"/>
		<[option.label]>
	</label>
</script>

<script type="text/ng-template" id="/html/form/form-field-type-checkbox.html">
	<label data-zs-form-field-checkbox data-ng-model="scope[field.name]" name="<[field.name]>" data-zs-true-value="getTrueValue(field)" data-zs-false-value="getFalseValue(field)">
		<input type="checkbox" name="val" data-zs-form-field data-ng-model="val" />
		<span class="checkbox-label" data-ng-show="field.data.checkboxlabel">
			<[field.data.checkboxlabel]>
		</span>
	</label>
</script>

<script type="text/ng-template" id="/html/form/form-field-type-select.html">
	<select data-ng-model="scope[field.name]" name="<[field.name]>" data-zs-form-field data-ng-options="option.value as option.label for option in getOptions(field)" data-ng-required="getRequired(field)">
		<option data-ng-show="field._empty" value=""><[field._empty.label]></option>
	</select>
</script>

<script type="text/ng-template" id="/html/form/form-field-type-spot-enlighter.html">
	<input type="text" name="<[field.name]>" data-zs-form-field data-ng-model="scope[field.name]" data-zs-object-required="getRequired(field)" data-zs-spot-enlighter data-zs-spot-enlighter-restrict="field.data.restrict||''" data-zs-spot-enlighter-label="<[field.data.label]>" data-zs-placeholder="field.data.placeholder" />
		
</script>

<script type="text/ng-template" id="/html/form/form-field-type-textarea.html">
	<textarea data-ng-model="scope[field.name]" data-ng-required="getRequired(field)" data-zs-form-field data-ng-pattern="<[field.data.pattern]>" id="getFieldId(field)"></textarea>
</script>

<script type="text/ng-template" id="/html/form/form-field-type-file.html">
	<div data-zs-form-field data-zs-file-form-field data-ng-model="scope[field.name]" data-zs-array-required="getRequired(field)" data-zs-file-form-field-multiple="field.data.multi">
		
		<ul class="form-file-list" data-ng-repeat="file in scope[field.name]">
			<li class="form-file-list-item">
				<span class="form-file-list-item-name"><[file.original_name]></span>
				<button type="button"class="form-file-list-item-remove icon-font-awesome icon-remove" data-ng-click="removeFile(file)"></button>
			</li>
		</ul>
		
		<div class="form-file-upload-button" data-zs-upload data-zs-upload-multiple="<[field.data.multi]>">
			<button type="button">
				%%Upload%%
			</button>
		</div>
	</div>
</script>

<script type="text/ng-template" id="/html/form/form-modal.html">
	<div data-zs-modal data-ng-init="title=action.data.title">
		<div data-ng-include="action.data.template_url">
		</div>
	</div>
</script>
