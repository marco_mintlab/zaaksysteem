/*global angular,_*/
(function ( ) {
	
	angular.module('Zaaksysteem')
		.controller('nl.mintlab.core.crud.CrudTableController', [ '$scope', '$parse', function ( $scope, $parse ) {
			
			$scope.sortedOn = null;
			$scope.reversed = false;
			
			$scope.sortOn = function ( column ) {
				if($scope.isSortedOn(column)) {
					$scope.reverse();
				}
				$scope.sortedOn = column;
				$scope.sort(column.resolve || column.id, $scope.reversed);
			};
			
			$scope.isSortedOn = function ( column ) {
				return $scope.sortedOn === column;
			};
			
			$scope.reverse = function ( ) {
				$scope.reversed = !$scope.reversed;
			};
			
			$scope.getColumnValue = function ( column, item ) {
				var resolve = column.resolve + (column.filter ? '|' + column.filter : ''),
					getter = $parse(resolve),
					val = getter(item);
				
				if(val === undefined || val === null) {
					val = '';
				}
				return val;
			};
			
			$scope.getSortValue = function ( item ) {
				var col = $scope.sortedOn,
					getter;
					
				if(!col) {
					return _.indexOf($scope.items, item);
				}
				
				if(!col.sort) {
					return $scope.getColumnValue(col, item);
				}
				
				if(typeof col.sort === 'string') {
					getter = $parse(col.sort);
				} else {
					getter = $parse(col.sort.resolve);
				}
				return getter(item);
			};
			
			$scope.getColumnTemplate = function ( column, item ) {
				var tpl = column.template ? column.template : $scope.getColumnValue(column, item);
				return tpl;
			};
			
		}]);
	
})();