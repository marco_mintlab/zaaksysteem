/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem.form')
		.directive('zsFormFieldCheckbox', [ function ( ) {
			
			return {
				require: 'ngModel',
				scope: true,
				link: function ( scope, element, attrs, ngModel ) {
					
					function setViewValue ( ) {
						var val = scope.val,
							trueValue = attrs.zsTrueValue === undefined ? true : scope.$eval(attrs.zsTrueValue),
							falseValue = attrs.zsFalseValue === undefined ? false : scope.$eval(attrs.zsFalseValue);
							
						ngModel.$setViewValue(val ? trueValue : falseValue);
					}
					
					function setChildValue ( ) {
						var viewValue = ngModel.$viewValue,
							trueValue = attrs.zsTrueValue === undefined ? true : scope.$eval(attrs.zsTrueValue);
							
						scope.val = viewValue === trueValue;
					}
					
					scope.$on('form.change.committed', function ( /*event, field*/ ) {
						setViewValue();
					});
					
					ngModel.$render = function ( ) {
						setChildValue();
					};
				}
			};
			
		}]);
	
})();