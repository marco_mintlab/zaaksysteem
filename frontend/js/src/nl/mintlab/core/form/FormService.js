/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem.form')
		.service('formService', [ function ( ) {
			
			var formsByName = {};
			
			return {
				register: function ( form ) {
					formsByName[form.getName()] = form;
				},
				unregister: function ( form ) {
					delete formsByName[form.getName()];	
				},
				get: function ( name ) {
					return formsByName[name];
				}
			};
			
		}]);

})();