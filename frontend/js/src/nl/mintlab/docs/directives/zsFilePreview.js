/*global angular,fetch*/
(function ( ) {
	
	var EMPTY_GIF = 'data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==';
	
	angular.module('Zaaksysteem.docs')
		.directive('zsFilePreview', [ '$document', '$window', 'templateCompiler', '$timeout', function ( $document, $window, templateCompiler, $timeout ) {
			
			var fromLocalToGlobal = fetch('nl.mintlab.utils.dom.fromLocalToGlobal'),
				fromGlobalToLocal = fetch('nl.mintlab.utils.dom.fromGlobalToLocal'),
				getViewportSize = fetch('nl.mintlab.utils.dom.getViewportSize'),
				setMouseEnabled = fetch('nl.mintlab.utils.dom.setMouseEnabled'),
				getComputedStyle = $window.getComputedStyle,
				body = $document.find('body'),
				popup,
				img,
				arrow,
				center;
			
			function createPopup ( element ) {
				popup = angular.element(element);
				
				setMouseEnabled(popup[0]);
				
				body.append(popup);
				img = angular.element(popup[0].querySelector('img.file-preview-image'));
				img.bind('load', onImageLoad);
				arrow = angular.element(popup[0].querySelector('.file-preview-arrow'));
				popup.bind('webkitTransitionEnd transitionend', onTransitionEnd);
			}
			
			function showPopup ( cnt ) {
				center = cnt;
				popup.addClass('file-preview-visible');
				positionPopup();
			}
			
			function hidePopup ( ) {
				popup.removeClass('file-preview-visible');
			}
			
			function onTransitionEnd ( ) {
				if(getComputedStyle(popup[0]).opacity === '0') {
					img.attr('src', EMPTY_GIF);
				}
			}
			
			function setImage ( url ) {
				img.attr('src', url);
			}
			
			function onImageLoad ( ) {
				$timeout(function ( ) {
					positionPopup();
				});
			}
			
			function positionPopup ( ) {
				var target = fromGlobalToLocal(popup[0].offsetParent, center),
					clientWidth = popup[0].clientWidth,
					clientHeight = popup[0].clientHeight,
					hOrient,
					viewportSize = getViewportSize(),
					arrowY = clientHeight/2 - arrow[0].clientHeight/2;
					
				if(target.x - clientWidth > 0) {
					hOrient = 'left';
					target.x -= clientWidth;
				} else {
					hOrient = 'right';
				}
					
				target.y -= clientHeight / 2;
				
				if(target.y < 0) {
					arrowY = -target.y;
					target.y = 0;
				} else if(target.y + clientHeight > viewportSize.height) {
					arrowY += (target.y - (viewportSize.height - clientHeight));
					target.y = viewportSize.height - clientHeight;
				}
				
				popup.css('left', target.x + 'px');
				popup.css('top', target.y + 'px');
				
				popup.attr('data-zs-file-preview-horizontal-orientation', hOrient);
				
				arrow.css('top', arrowY + 'px');
				
			}
			
			templateCompiler.getElement('/partials/directives/popup/file-preview.html').then(createPopup);
			
			return {
				compile: function ( /*tElement, tAttrs, transclude*/ ) {
					
					return function link ( scope, element/*, attrs*/ ) {
						
						scope.showPreview = function ( ) {
							var center,
								origin,
								url = scope.pip ? scope.entity.pip_thumbnail_url : scope.entity.thumbnail_url;
							
							if(!url) {
								return;
							}
								
							setImage(url);
								
							center = { x: element[0].clientWidth/2, y: element[0].clientHeight / 2 };
							origin = fromLocalToGlobal(element[0], center);
							showPopup(origin);
						};
						
						scope.hidePreview = function ( ) {
							hidePopup();
						};
						
					};
				}
			};
			
		}]);
	
})();