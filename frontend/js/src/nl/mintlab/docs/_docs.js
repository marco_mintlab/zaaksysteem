/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem.docs', [ 'Zaaksysteem.events', 'Zaaksysteem.net', 'Zaaksysteem.dom' ] )
		.run([ '$document', function ( /*$document*/ ) {
			//$document.find('head').append(angular.element('<link href="/css/docs.css" rel="stylesheet" type="text/css" media="all"/>'));
		}]);
	
	
})();