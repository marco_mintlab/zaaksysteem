/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem.case')
		.controller('nl.mintlab.case.CaseActionController', [ '$scope', 'smartHttp', 'translationService', function ( $scope, smartHttp, translationService ) {
			
			$scope.updateItem = function ( item ) {
				
				smartHttp.connect({
					method: 'POST',
					url: 'zaak/' + $scope.caseId + '/action/update',
					data: {
						id: item.id,
						automatic: item.automatic
					}
				})
					.success(function ( data ) {
						
						var itemData = data.result[0];
						
						for(var key in itemData) {
							item[key] = itemData[key];
						}
						
					})
					.error(function ( /*data*/ ) {
						item.automatic = !item.automatic;
					});
					
			};
			
			$scope.handleCheckboxClick = function ( event ) {
				event.stopPropagation();
			};
			
			$scope.resetAction = function ( event, item ) {
				var wasTainted = item.tainted;
				
				item.tainted = false;
				
				smartHttp.connect({
					method: 'POST',
					url: 'zaak/' + $scope.caseId + '/action/untaint',
					data: {
						id: item.id
					}
				})
					.success(function ( data ) {
						var itemData = data.result[0];
						
						for(var key in itemData) {
							item[key] = itemData[key];
						}
						
						$scope.reloadData();
					})
					.error(function ( /*data*/  ) {
						item.tainted = wasTainted;						
						$scope.$emit('systemMessage', {
							type: 'error',
							content: translationService.get('Actie kon niet hersteld worden')
						});
					});
				
				event.stopPropagation();
			};
			
			$scope.isDisabled = function ( action ) {
				var disabled;
				disabled = $scope.closed || ($scope.lastMilestone && action.data && action.data.relatie_type === 'deelzaak');
				return disabled;
			};
			
		}]);
	
})();