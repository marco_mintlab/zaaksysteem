/*global angular,fetch,_*/
(function ( ) {
	
	angular.module('Zaaksysteem.sysin.links')
		.controller('nl.mintlab.sysin.links.LinkController', [ '$scope', 'smartHttp', 'translationService', function ( $scope, smartHttp, translationService ) {
			
			var safeApply = fetch('nl.mintlab.utils.safeApply'),
				indexOf = _.indexOf;
			
			$scope.interfaces = [];
			$scope.modules = [];
			
			$scope.allModules = [];
			
			$scope.activeLink = null;
			
			$scope.newModuleType = '';
			$scope.newLinkLabel = '';
			
			function getData ( ) {
				
				getAvailableModules();
					
				smartHttp.connect({
					method: 'GET',
					url: '/sysin/interface/modules/all',
					params: {
						zapi_no_pager: 1
					}
				})
					.success(function ( response ) {
						$scope.allModules = response.result;
					});
					
				smartHttp.connect({
					method: 'GET',
					url: '/sysin/interface',
					params: {
						zapi_no_pager: 1
					}
				})
					.success(function onSuccess ( data ) {
						$scope.interfaces = data.result;
					})
					.error(function onError ( ) {
						
					});
			}
			
			function getAvailableModules ( ) {
				smartHttp.connect({
					method: 'GET',
					url: '/sysin/interface/modules/available'
				})
					.success(function onSuccess ( data ) {
						$scope.modules = data.result;
						$scope.newModuleType = $scope.modules[0];
					})
					.error(function onError ( ) {
						
					});
			}
			
			function getModuleByName ( name ) {
				return _.filter($scope.allModules, function ( link ) {
					return link.name === name;
				})[0];
			}
			
			$scope.showLink = function ( link ) {
				$scope.activeLink = link;
			};
			
			$scope.reloadData = function ( ) {
				getData();
			};
			
			$scope.getLinkFormUrl = function ( link ) {
				var url;
				
				if(!link) {
					url = '';
				} else {
					url = '/sysin/interface/' + link.id + '/update?zapi_form=1';
				}
				
				return url;
			};
			
			$scope.removeLink = function ( link ) {
				var index = indexOf($scope.interfaces, link),
					isActiveLink = $scope.activeLink === link;
				
				$scope.interfaces.splice(index, 1);
				
				if(isActiveLink) {
					$scope.showLink(null);
				}
				
				smartHttp.connect({
					method: 'POST',
					url: '/sysin/interface/' + link.id + '/delete'
				})
					.success(function ( /*data*/ ) {
						
						getAvailableModules();
						
						$scope.$emit('systemMessage', {
							type: 'info', 
							content: translationService.get('Koppeling "%s" is verwijderd', link.name)
						});
					})
					.error(function ( /*data*/ ) {
						$scope.interfaces.splice(index, 0, link);
						$scope.$emit('systemMessage', {
							type: 'error',
							content: translationService.get('Koppeling "%s" kon niet worden verwijderd', link.name)
						});
						if(isActiveLink) {
							$scope.showLink(link);
						}
					});
			};
			
			$scope.getModuleOptions = function ( ) {
				var module,
					modules = $scope.modules || [],
					options = [],
					i,
					l;
					
				for(i = 0, l = modules.length; i < l; ++i) {
					module = modules[i];
					options.push({
						name: module.name,
						value: module.name,
						label: module.label
					});
				}
				
				return options;
			};
			
			$scope.getModuleByName = function ( id ) {
				return getModuleByName(id);
			};
			
			$scope.$on('form.submit.success', function ( event, name, data) {
				
				safeApply($scope, function ( ) {
					
					var formName = event.targetScope.getName(),
						linkData,
						link;
					
					switch(formName) {
						case 'renamelink':
						linkData = data.result[0];
						link = event.targetScope.link;
							
						for(var key in linkData) {
							link[key] = linkData[key];
						}
						break;
						
						case 'addlink':
						
						getAvailableModules();
						
						link = data.result[0];
						
						$scope.interfaces.push(link);
						$scope.showLink(link);
						break;
					}
					
				});
			});
			
			getData();
			
		}]);
})();