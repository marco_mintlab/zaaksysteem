/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem.sysin.links')
		.controller('nl.mintlab.sysin.links.InterfaceTestController', [ '$scope', 'translationService', 'smartHttp', function ( $scope, translationService, smartHttp ) {
			
			$scope.description = '';
			$scope.tests = [];
			
			$scope.runTest = function ( test ) {
				test.status = 'running';
				test.error_message = null;
				smartHttp.connect({
					method: 'POST',
					url: '/sysin/interface/' + $scope.activeLink.id + '/test/' + test.id + '/run'
				})
					.success(function ( ) {
						test.status = 'ok';
					})
					.error(function ( response ) {
						var error = response && response.result ? response.result[0] : null,
							errorMessage;
						
						if(error) {
							errorMessage = error.messages[0];
						} else {
							errorMessage = translationService.get('Er heeft een onbekende fout plaatsgevonden');
						}
						
						test.error_message = errorMessage;
						
						test.status = 'failed';
					});
			};
			
			$scope.runAllTests = function ( ) {
				var i,
					l;
					
				for(i = 0, l = $scope.tests.length; i < l; ++i) {
					$scope.runTest($scope.tests[i]);
				}
				
			};
			
			smartHttp.connect({
				method: 'GET',
				url: '/sysin/interface/' + $scope.activeLink.id + '/test'
			})
				.success(function ( response ) {
					var i,
						l,
						test,
						tests = response.result[0].tests;
						
					$scope.description = response.result[0].description;
					$scope.tests = tests;
						
					for(i = 0, l = tests.length; i < l; ++i) {
						test = tests[i];
						test.status = 'init';
					}
					
					$scope.tests = tests;
					
				})
				.error(function ( /*response*/ ) {
					$scope.$emit('systemMessage', {
						type: 'error',
						content: 'Tests konden niet worden geladen'
					});
				});
			
		}]);
	
})();