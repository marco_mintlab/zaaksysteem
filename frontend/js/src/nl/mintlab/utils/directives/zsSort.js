/*global angular,fetch*/
(function ( ) {
	
	angular.module('Zaaksysteem')
		.directive('zsSort', [ '$document', '$parse', function ( $document, $parse ) {
			
			var getMousePosition = fetch('nl.mintlab.utils.dom.getMousePosition'),
				indexOf = fetch('nl.mintlab.utils.shims.indexOf'),
				arrayMove = fetch('nl.mintlab.utils.collection.arrayMove'),
				cancelEvent = fetch('nl.mintlab.utils.events.cancelEvent');
			
			return {
				compile: function ( /*tElement, tAttrs, transclude*/ ) {
					
					return function link ( scope, element, attrs ) {
						
						var list,
							trackingEl;
						
						function track ( el ) {
							
							resetList();
							
							trackingEl = el;
							trackingEl.addClass('zs-sorting');
							
							$document.bind('dragover', onDragOver);
							$document.bind('drop', onDrop);
							$document.bind('mousemove', onMouseMove);
						}
						
						function untrack ( ) {
							
							resort();
							
							trackingEl.removeClass('zs-sorting');
							trackingEl = null;
							
							$document.unbind('dragover', onDragOver);
							$document.unbind('drop', onDrop);
							$document.unbind('mousemove', onMouseMove);	
						}
						
						function resort ( ) {
							var trackingScope = trackingEl.scope(),
								data = $parse(trackingEl.attr('data-ng-drag-data'))(trackingScope),
								array = $parse(attrs.zsSort)(scope),
								index,
								to,
								list = element[0].querySelectorAll('[data-zs-sortable]');
								
							index = indexOf(array, data);
							
							if(index !== -1) {
								to = indexOf(list, trackingEl[0]);
								arrayMove(array, data, to);
								scope.$emit('sort', data, index, to);
							}
							
						}
						
						function resetList ( ) {
							list = element[0].querySelectorAll('[data-zs-sortable]');
						}
						
						function onDragOver ( event ) {
							var pos = getMousePosition(event);
							positionTrackedElement(pos);
							cancelEvent(event);
						}
						
						function onDrop ( event ) {
							return cancelEvent(event);
						}
						
						function onMouseMove ( event ) {
							var pos = getMousePosition(event);
							positionTrackedElement(pos);
						}
						
						function positionTrackedElement ( pos ) {
							var rect,
								target,
								before,
								el,
								i,
								l;
								
							// TODO(dario): horizontal sort
							
							target = 0;
							
							for(i = 0, l = list.length; i < l; ++i) {
								el = list[i];
								rect = el.getBoundingClientRect();
								if(pos.y <= rect.top + rect.height/2) {
									target = i;
									break;
								}
							}
							
							if(pos.y > rect.top + rect.height/2) {
								before = null;
							} else {
								before = list[target];
							}
							
							
							if(before !== trackingEl[0]) {
								trackingEl[0].parentNode.insertBefore(trackingEl[0], before);
								resetList();
							}
							
							
						}
						
						scope.$on('startdrag', function ( event, el/*, mimetype*/ ) {
							if(el.attr('data-zs-sortable') !== undefined) {
								track(el);
							}
						});
						
						scope.$on('stopdrag', function ( event, el/*, mimetype*/ ) {
							if(trackingEl && trackingEl[0] === el[0]) {
								untrack();
							}
						});
						
					};
				}
			};
			
		}]);
	
})();