/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem')
		.directive('ngEditable', [ '$timeout', function ( $timeout ) {
			return {
				link: function ( scope, element, attrs ) {
					
					var input = element.find('input'),
						label = element.find('span'),
						display = element.css('display');
						
					if(input.length === 0) {
						return;
					}
						
					scope.edit = function ( ) {
						input.css('display', display);
						label.css('display', 'none');
						$timeout(function ( ) {
							input[0].focus();
						});
					};
					
					scope.unedit = function ( ) {
						input.css('display', 'none');
						label.css('display', display);
					};
					
					function save ( ) {
						scope.$emit('editsave', attrs.ngEditable, input[0].value);
					}
					
					function cancel ( ) {
						scope.$emit('editcancel', attrs.ngEditable, input[0].value);
					}
					
					function setInputValue ( ) {
						var val = scope.$eval(attrs.ngModel);
						input.text(val);
					}
					
					input.bind('keyup', function ( event ) {
						if(event.keyCode === 13) {
							if(input.hasClass('ng-invalid')) {
								return;
							}
							save();
						} else if(event.keyCode === 27) {
							cancel();
						} else {
							return;
						}
						scope.unedit();
						scope.$apply();
					});
					
					input.bind('blur', function ( /*event*/ ) {
						if(scope.editMode) {
							save();
							scope.unedit();
							scope.$apply();
						}
					});
					
					attrs.$observe('ngEditableActive', function ( ) {
						var isActive = scope.$eval(attrs.ngEditableActive);
						if(isActive) {
							scope.edit();
						} else {
							scope.unedit();
						}
					});
					
					scope.$watch('ngModel', function ( ) {
						setInputValue();
					});
					
				}
			};
		}]);
})();