/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem')
		.directive('zsStyle', [ function ( ) {
			
			return {
				compile: function ( /*tElement, tAttrs, transclude*/ ) {
					
					return function link ( scope, element, attrs ) {
						
						attrs.$observe('zsStyle', function ( ) {
							setStyle(element);
						});
						
						function setStyle( ) {
							element.attr('style', attrs.zsStyle);
						}
						
						setStyle();
						
					};
				}
			};
			
		}]);
	
})();