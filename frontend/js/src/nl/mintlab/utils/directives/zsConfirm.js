/*global angular,fetch*/
(function ( ) {
	
	angular.module('Zaaksysteem')
		.directive('zsConfirm', [ '$document', '$parse', 'templateCompiler', function ( $document, $parse, templateCompiler ) {
			
			var safeApply = fetch('nl.mintlab.utils.safeApply'),
				body = $document.find('body'),
				parent = angular.element('<div class="confirm-wrapper"></div>');
				
			body.append(parent);
			
			return {
				scope: true,
				link: function ( scope, element, attrs ) {
					
					var isOpen = false,
						el,
						callback;
					
					function openDialog ( ) {
						if(isOpen) {
							return;
						}
						
						isOpen = true;
						
						templateCompiler.getCompiler('/html/directives/confirm/confirm.html').then(function ( cpl ) {
							cpl(scope, function ( clonedElement ) {
								el = clonedElement;
								parent.append(el);
							});
						});
					}
					
					function closeDialog ( ) {
						if(el) {
							el.remove();
							el = null;
						}
						isOpen = false;
					}
					
					element.bind('click', function ( ) {
						safeApply(scope, function ( ) {
							openDialog();
						});
					});
					
					scope.confirm = function ( label, verb, cb ) {
						scope.confirmLabel = label;
						scope.confirmVerb = verb;
						callback = cb;
						openDialog();
					};
					
					scope.ok = function ( $event ) {
						if(!callback) {
							$parse(attrs.zsConfirm)(scope, { '$event': $event });
						} else {
							callback($event);
						}
						closeDialog();
					};
					
					scope.cancel = function ( ) {
						closeDialog();
					};
					
					scope.closePopup = function ( ) {
						closeDialog();
					};
					
					attrs.$observe('zsConfirm', function ( ) {
						callback = null;
					});
					
					attrs.$observe('zsConfirmVerb', function ( ) {
						scope.confirmVerb = attrs.zsConfirmVerb;
					});
					
					attrs.$observe('zsConfirmLabel', function ( ) {
						scope.confirmLabel = attrs.zsConfirmLabel;
					});
					
				}
			};
			
			
		}]);
	
})();