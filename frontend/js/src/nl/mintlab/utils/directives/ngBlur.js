/*global angular,fetch*/
(function ( ) {

	angular.module('Zaaksysteem')
		.directive('ngBlur', function ( ) {
			
			var safeApply = fetch('nl.mintlab.utils.safeApply');
			
			return function ( scope, element, attrs ) {
				element.bind('blur', function ( ) {
					safeApply(scope, function ( ) {
						scope.$eval(attrs.ngBlur);
					});
				});
			};
			
		});
	
})();