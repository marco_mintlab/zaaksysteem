/*global angular,_,fetch*/
(function () {
	
	angular.module('Zaaksysteem')
		.directive('zsFileFormField', [ 'fileUploader', 'translationService', function ( fileUploader, translationService ) {
			
			var indexOf = _.indexOf,
				safeApply = fetch('nl.mintlab.utils.safeApply');
			
			return {
				scope: false,
				require: [ 'ngModel' ],
				compile: function ( ) {
					
					return function link ( scope, element, attrs, controllers ) {
						
						var ngModel = controllers[0];
						
						function getMax ( ) {
							return !!scope.$eval(attrs.zsFileFormFieldMultiple) ? Number.MAX_VALUE : 1;
						}
						
						scope.$on('upload.start', function ( ) {
							
						});
						
						scope.$on('upload.progress', function ( ) {
							
						});
						
						scope.$on('upload.end', function ( ) {
							
						});
						
						scope.$on('upload.complete', function ( event, upload ) {
							safeApply(scope, function ( ) {
								
								var result = upload.getData().result,
									max = getMax(),
									fileList = ngModel.$viewValue ? ngModel.$viewValue.concat() : [],
									i,
									l,
									file;
									
								for(i = 0, l = result.length; i < l; ++i) {
									file = result[i];
									while(fileList.length >= max) {
										fileList.shift();
									}
									fileList.push(file);
								}
								
								ngModel.$setViewValue(fileList);
							});
						});
						
						scope.$on('upload.error', function (  ) {
							scope.$emit('systemMessage', {
								type: 'error',
								content: translationService.get('Er ging iets fout bij het uploaden van het bestand')
							});
						});
						
						scope.removeFile = function ( file ) {
							var files = (ngModel.$viewValue || []).concat(),
								index = indexOf(files, file);
								
							if(index !== -1) {
								files.splice(index, 1);
								ngModel.$setViewValue(files);
							}
						};
						
					};
				}
			};
			
		}]);
	
})();