/*global define*/
(function ( ) {
	
	define('nl.mintlab.utils.dom.getViewportPosition', function ( ) {
		
		return function ( element ) {
			var rect;
			if(!element) {
				return { x: 0, y: 0 };
			}
			rect = element.getBoundingClientRect();
			return { x: rect.left, y: rect.top };
		};
	});
	
})();