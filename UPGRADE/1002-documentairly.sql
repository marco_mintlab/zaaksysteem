BEGIN;
--- NEW TABLES/DATA

CREATE TABLE config (
  parameter VARCHAR(100) NOT NULL,
  value     VARCHAR(250) NOT NULL
);

-- Files
CREATE TYPE trust_level AS ENUM ('Openbaar', 'Beperkt openbaar', 'Intern', 'Zaakvertrouwelijk', 'Vertrouwelijk', 'Confidentieel', 'Geheim', 'Zeer geheim');
CREATE TYPE origin AS ENUM ('Inkomend', 'Uitgaand', 'Intern');

CREATE TABLE file_metadata (
  temp_documents_id integer,
  id SERIAL PRIMARY KEY,
  description TEXT,
  document_category TEXT,
  trust_level trust_level,
  origin origin
);

CREATE TABLE directory (
  id SERIAL PRIMARY KEY,
  name VARCHAR(40),
  case_id INTEGER REFERENCES zaak(id) NOT NULL,
  immutable BOOLEAN NOT NULL DEFAULT 'f'
);


-- FIX MIGRATION LATER
ALTER TABLE filestore RENAME TO filestore_old;
ALTER SEQUENCE filestore_id_seq RENAME TO filestore_old_id_seq;
ALTER INDEX filestore_pkey RENAME TO filestore_old_pkey;

CREATE TABLE filestore (
  temp_documents_id integer,
  id SERIAL PRIMARY KEY,
  uuid uuid NOT NULL,
  thumbnail_uuid uuid,
  original_name VARCHAR(150) NOT NULL,
  size INTEGER NOT NULL,
  mimetype VARCHAR(160) NOT NULL,
  md5 VARCHAR(100) NOT NULL,
  date_created TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL
);

CREATE TABLE file (
  temp_documents_id integer,
  -- disabled for now, fix later
  --   search_index TSVECTOR,
  --   search_term TEXT,
  --   object_type CHARACTER VARYING(100) DEFAULT 'file'::character VARYING,
  id SERIAL PRIMARY KEY,
  filestore_id INTEGER REFERENCES filestore(id),
  name VARCHAR(150) NOT NULL,
  extension VARCHAR(10) NOT NULL,
  root_file_id INTEGER REFERENCES file(id),
  version INTEGER DEFAULT 1,
  case_id INTEGER REFERENCES zaak(id),
  metadata_id INTEGER REFERENCES file_metadata(id),
  subject_id VARCHAR(100),
  directory_id INTEGER REFERENCES directory(id),
  case_type_document_id INTEGER REFERENCES zaaktype_kenmerken(id),
  creation_reason VARCHAR(150) NOT NULL,
  accepted_temp INTEGER,
  accepted BOOLEAN NOT NULL DEFAULT 'f',
  rejection_reason TEXT,
  is_duplicate_name BOOLEAN NOT NULL DEFAULT 'f',
  publish_pip BOOLEAN NOT NULL DEFAULT 'f',
  publish_website BOOLEAN NOT NULL DEFAULT 'f',
  date_created TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL,
  created_by VARCHAR(100) NOT NULL,
  date_modified TIMESTAMP WITHOUT TIME ZONE,
  modified_by VARCHAR(100),
  date_deleted TIMESTAMP WITHOUT TIME ZONE,
  deleted_by VARCHAR(100),
  publish_type_temp TEXT
);

-- Contactmomenten
CREATE TYPE contactmoment_type AS ENUM ('email', 'note');
CREATE TYPE contactmoment_medium AS ENUM ('behandelaar', 'balie', 'telefoon', 'post', 'email', 'webformulier');

CREATE TABLE contactmoment (
  id SERIAL PRIMARY KEY,
  subject_id VARCHAR(100),
  case_id INTEGER REFERENCES zaak(id),
  type contactmoment_type NOT NULL,
  medium contactmoment_medium NOT NULL,
  date_created TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL,
  created_by VARCHAR(100) NOT NULL
);

CREATE TABLE contactmoment_note (
  id SERIAL PRIMARY KEY,
  message VARCHAR(200),
  contactmoment_id INTEGER REFERENCES contactmoment(id) NOT NULL
);

CREATE TABLE contactmoment_email (
  id SERIAL PRIMARY KEY,
  body_preview TEXT NOT NULL,
  filestore_id INTEGER REFERENCES filestore(id) NOT NULL,
  contactmoment_id INTEGER REFERENCES contactmoment(id) NOT NULL
);

ALTER TABLE file ALTER COLUMN filestore_id SET NOT NULL;

-- Set foreign key from bibliotheek_sjablonen to filestore
-- (This should already have been there, just fixing)
ALTER TABLE bibliotheek_sjablonen ADD CONSTRAINT bibliotheek_sjablonen_filestore_id FOREIGN KEY (filestore_id) REFERENCES filestore (id);

-- Fix created_by?

-- Fix modified_by?

-- Fix deleted_by? -- no-history user oid? static in de db

-- Fix directory_id?

-- Fix root_file_id (voorheen parent_id, dus die moet later aangepast 
-- worden naar de root file.
    
-- Delete temp columns 
ALTER TABLE file DROP COLUMN accepted_temp ;
ALTER TABLE file DROP COLUMN publish_type_temp;
ALTER TABLE file DROP COLUMN temp_documents_id;
ALTER TABLE file_metadata DROP COLUMN temp_documents_id;
ALTER TABLE filestore DROP COLUMN temp_documents_id;


-- Update foreign key to filestore from bibliotheek_sjablonen
ALTER TABLE bibliotheek_sjablonen DROP CONSTRAINT "bibliotheek_sjablonen_filestore_id_fkey";


-- Up the file ID sequence (so migrations don't break)
SELECT setval('file_id_seq', (SELECT MAX(id) FROM file));

-- Same for file_metadata
SELECT setval('file_metadata_id_seq', (SELECT MAX(id) FROM file_metadata));
-- And filestore
SELECT setval('filestore_id_seq', (SELECT MAX(id) FROM filestore));

-- Cleanup of old tables can be done at a later time. Not recommended to do it right away, too useful for possible patching/fixes or even a rollback.

-- Table: checklist

-- DROP TABLE checklist;

CREATE TABLE checklist
(
  id serial NOT NULL,
  case_id integer NOT NULL,
  case_milestone integer,
  CONSTRAINT checklist_pkey PRIMARY KEY (id ),
  CONSTRAINT checklist_case_id_fkey FOREIGN KEY (case_id)
      REFERENCES zaak (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

-- Table: checklist_item

-- DROP TABLE checklist_item;

CREATE TABLE checklist_item
(
  id serial NOT NULL,
  checklist_id integer NOT NULL,
  label text,
  state boolean NOT NULL DEFAULT false,
  sequence integer,
  user_defined boolean NOT NULL DEFAULT true,
  deprecated_answer character varying(8),
  CONSTRAINT checklist_item_pkey PRIMARY KEY (id ),
  CONSTRAINT checklist_item_checklist_id_fkey FOREIGN KEY (checklist_id)
      REFERENCES checklist (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);


-- Table: zaaktype_status_checklist_item

-- DROP TABLE zaaktype_status_checklist_item;

CREATE TABLE zaaktype_status_checklist_item
(
  id serial NOT NULL,
  casetype_status_id integer NOT NULL,
  label text,
  sequence integer,
  CONSTRAINT zaaktype_checklist_item_pkey PRIMARY KEY (id ),
  CONSTRAINT zaaktype_status_checklist_item_zaaktype_status_id_fkey FOREIGN KEY (casetype_status_id)
      REFERENCES zaaktype_status (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE zaaktype_status_checklist_item DROP COLUMN sequence;

ALTER TABLE contactmoment_email ADD COLUMN body TEXT NOT NULL;
ALTER TABLE contactmoment_email ADD COLUMN subject VARCHAR(150) NOT NULL;
ALTER TABLE contactmoment_email ADD COLUMN recipient VARCHAR(150) NOT NULL;
ALTER TABLE contactmoment_email DROP COLUMN body_preview;

CREATE INDEX ON logging USING btree (component) WHERE component IS NOT NULL;
CREATE INDEX ON logging USING btree (component_id) WHERE component_id IS NOT NULL;

DROP TABLE documenten, documents_mail, documents, dropped_documents, jobs;

ALTER TABLE file ADD COLUMN search_index tsvector;
ALTER TABLE file ADD COLUMN search_term text;
ALTER TABLE file ADD COLUMN object_type character varying(100) DEFAULT 'file'::character varying;
ALTER TABLE file ADD COLUMN searchable_id integer;
-- These will get regenerated after the INHERIT succeeds
UPDATE file SET searchable_id = (SELECT id FROM searchable LIMIT 1);
ALTER TABLE file ALTER COLUMN searchable_id SET NOT NULL;
ALTER TABLE file INHERIT searchable;

ALTER TABLE ONLY file ALTER COLUMN searchable_id SET DEFAULT nextval('searchable_searchable_id_seq'::regclass);

alter table parkeergebied add woonplaats varchar(255);

alter table parkeergebied alter column huisletter type varchar(8);

ALTER TABLE zaaktype_resultaten ADD label text;
ALTER TABLE zaaktype_resultaten ADD selectielijst text;
ALTER TABLE zaaktype_resultaten ADD archiefnominatie text;
ALTER TABLE zaaktype_resultaten ADD comments text;

-- config

INSERT INTO config (parameter, value) VALUES ('jodconverter_url', 'http://localhost:8080/converter/service');

-- zaaktype_kenmerken default view
update zaaktype_kenmerken set zaakinformatie_view = 1;

COMMIT;


-- SPECIAL: REMOVE zaak_kenmerken AND zaak_kenmerken_values

BEGIN;

DROP TABLE zaak_kenmerken, zaak_kenmerken_values;

DROP SEQUENCE zaak_kenmerken_id_seq;

DROP SEQUENCE zaak_kenmerken_values_id_seq;

COMMIT;

-- SPECIAL: MAKE sure that default 1 is set on zaaktype_notificaties:automatic

BEGIN;

alter table zaaktype_notificatie alter automatic set default 1;

COMMIT;


-- SPECIAL: Make sure that very old update scripts are ran

BEGIN;

ALTER TABLE checklist_antwoord
        DROP CONSTRAINT zaak_id_fkey;

ALTER TABLE logging
        DROP CONSTRAINT zaak_id_fkey;

ALTER TABLE zaak
        DROP CONSTRAINT zaak_pid_fkey;

ALTER TABLE zaak
        DROP CONSTRAINT zaak_relates_to_fkey;

ALTER TABLE zaak
        DROP CONSTRAINT zaak_vervolg_van_fkey;

ALTER TABLE zaak_kenmerk
        DROP CONSTRAINT zaak_kenmerk_zaak_id_key;

COMMIT;
