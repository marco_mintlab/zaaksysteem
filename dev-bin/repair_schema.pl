#!/usr/bin/perl -w

use strict;

# repair schema bullshit
# take custom content, save as .custom
# regen schema
# reinsert


my ($filename, $reinsert) = @ARGV;


my $begin = '# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:[^\n]+';
my $end = '# You can replace this text with custom content, and it will be preserved on regeneration';

die "need filename" unless($filename);

my $custom_filename = $filename . ".custom";

print "custom_filename: " . $custom_filename . "\n";

open FILE, $filename or die "couldnt open $filename for reading" . $!;
my $content = join "", <FILE>;
close FILE;

unless($reinsert) {

    
    my ($custom_content) = $content =~ m|$begin(.*)$end|s;
    $custom_content ||= '';
 
    open OUTPUT_FILE, ">$custom_filename" or die $!;
    print OUTPUT_FILE $custom_content;
    close OUTPUT_FILE;

} else {

    open CUSTOM_FILE, $custom_filename  or die "couldnt open $custom_filename for reading" . $!;
    my $custom_content = join "", <CUSTOM_FILE>;
    close CUSTOM_FILE;

    $content =~ s|($begin)|$1$custom_content|is;

    open OUTPUT_FILE, ">$filename" or die $!;
    print OUTPUT_FILE $content;
    close OUTPUT_FILE;

}
