#!/bin/bash

if [ $# -lt "1" ]; then
    echo "USAGE: $0 [LIST OF SQL-UPDATE FILES]"
    echo
    echo "Loads update scripts on test template, and dumps is back"
    exit;
fi

if [ ! -d db ] || [ ! -d lib ]; then
    echo "Please run this script from your source tree root"
    exit;
fi

if [ -z ${IS_DEV} ]; then
    echo "This script can only run on a development box. Set IS_DEV=1 if you are."
    exit;
fi

TEMPLATE="db/test-template.sql";
EMPTYTEMPLATE="db/template.sql";

PROCID=$$;
DATABASE="zs_test_${USER}_${PROCID}";
DATABASEFILE="/tmp/${DATABASE}.sql";

dbexists=`psql -l |grep $DATABASE`;
if [ "$dbexists" != "" ]; then
    echo "DB $DATABASE EXISTS, STOPPING. PLEASE TRY AGAIN";
    exit;
fi

echo "BEGIN;" > $DATABASEFILE;
for arg in $*; do
    cat $arg | sed 's/BEGIN;//i' | sed 's/COMMIT;//i' >> $DATABASEFILE;
done;
echo "COMMIT;" >> $DATABASEFILE;

echo "CREATING TEST DATABASE $DATABASE";

echo "CREATE DATABASE $DATABASE ENCODING 'UTF-8';"| psql template1 && \
    psql $DATABASE < $TEMPLATE &&
    psql $DATABASE < $DATABASEFILE;

IS_DEV=1 ZS_DB_NAME=$DATABASE dev-bin/db_redeploy.sh

echo "DUMPING DATABASE $DATABASE BACK TO $TEMPLATE"

pg_dump -O $DATABASE > $TEMPLATE

echo "DUMPING DATABASE $DATABASE BACK TO $EMPTYTEMPLATE"
pg_dump -O --schema-only $DATABASE > $EMPTYTEMPLATE

echo "DROP DATABASE $DATABASE" | psql template1
